import React,{Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import { createAppContainer } from 'react-navigation';
import {Image, View, NativeModules, Platform} from 'react-native';
import { createRootNavigator, createAlertRootNavigator } from './modules/setup/routes';
import EnquiryAlert from './modules/screens/EnquiryAlert/EnquiryAlertScreen'
import styles from './modules/screens/Splash/SplashStyle'

if(Platform.OS === 'android') {
  var NativeCall = NativeModules.NativeCall;
}

export default class App extends Component{

  constructor(props){
    super(props);
    this.state = {
      notification : [],
      userInfo: {},
      signedIn: false,
      checkedSignIn: false,
      newMessage: false
    }
    this.createNotificationListeners = this.createNotificationListeners.bind(this)
    this.navigate = this.navigate.bind(this)
    this.stopAlert = this.stopAlert.bind(this)
  }

  _showSplash = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    )
  }

  async stopAlert() {
    if(Platform.OS === 'android') {
      console.log('stop call');
      NativeCall.stopAlertRing( (err) => {console.log(err)}, (msg) => {console.log(msg)} );
    }
  }

  _navigate = async () => {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid &&  authItems.uid !== '') {
        this.setState({ userInfo: authItems, signedIn: true, checkedSignIn: true })
      }
    }else{
      this.setState({ checkedSignIn: true })
    }
    this.checkPermission();
    this.createNotificationListeners();
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    const lastUpdated = await AsyncStorage.getItem("lastUpdated");
    this.setState({ lastUpdated })
    if(this.props && this.props.messageId != null) {
      if(auth == null && Platform.OS === 'android') {
        console.log('auth tru', auth);
        this.stopAlert()
      }
      let lastMessage = await AsyncStorage.getItem("lastMessage");
      if(lastMessage && lastMessage != '') {
        if(parseInt(this.props.messageId) > parseInt(lastMessage)) {
          await AsyncStorage.setItem("lastMessage", this.props.messageId);
          this.setState({ newMessage: true })
        }
      }else{
        await AsyncStorage.setItem("lastMessage", this.props.messageId);
        this.setState({ newMessage: true })
      }
    }

    const data = await this._showSplash();
    if (data !== null) {
      this._navigate();
    }
  }

   // 1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }

    //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    //console.log("Token: ",fcmToken);
    if (!fcmToken) {
      try{
        fcmToken = await firebase.messaging().getToken();
         console.log("Token firebase: ",fcmToken);
        if (fcmToken) {
            // user has a device token
            await AsyncStorage.setItem('fcmToken', fcmToken);
        } else {
          this.checkPermission();
        }
      } catch (error) {
        console.log('error', error)
      }
        
    }

  }

    //2
  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
        console.log('permission rejected');
    }
  }

  componentWillUnmount() {
    this.createNotificationListeners();
    /*this.notificationListener();
    this.notificationOpenedListener();*/
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      console.log('Triggered when a particular notification has been received in foreground')
      if(notification.data) {
        this.navigate(notification.data);
      }
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      console.log('If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:')
      if(notificationOpen.notification.data) {
        this.navigate(notificationOpen.notification.data);
      }
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */

    const notificationOpen = await firebase.notifications()
    .getInitialNotification()
    .then(async (notificationOpen) => {
      // App was opened by a notification
      console.log('If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:')
      if (notificationOpen) {
        // Get information about the notification that was opened
        const notification = notificationOpen.notification;
        const lastNotification = await AsyncStorage.getItem(
          'lastNotification'
        );
        if (lastNotification !== notification.notificationId) {
          await AsyncStorage.setItem(
            'lastNotification',
            notification.notificationId
          );
          if(notificationOpen.notification.data) {
            this.navigate(notificationOpen.notification.data);
          }
        }
      }
    })

    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log('Triggered for data only payload in foreground')
      if(message.data) {
        this.navigate(message.data);
      }
    });
  }

  async navigate(notificationData) {
    const auth = await AsyncStorage.getItem("authUser");
    let authItems = {};
    let signedIn = false;

    if(notificationData.messageId != null) {
      //console.log('props', this.props);
      let lastMessage = await AsyncStorage.getItem("lastMessage");
      console.log('lastMessage index', lastMessage, notificationData.messageId);
      console.log('notificationData messageId', notificationData.messageId );
      if(lastMessage && lastMessage != '') {
        if(parseInt(notificationData.messageId) > parseInt(lastMessage)) {
          await AsyncStorage.setItem("lastMessage", notificationData.messageId);
        }
      }else{
        await AsyncStorage.setItem("lastMessage", notificationData.messageId);
      }
    }

    if(auth && auth != '') {
      let data = JSON.parse(auth);
      if(data.uid &&  data.uid !== '') {
        authItems = data;
        signedIn = true;

        this.setState({
          userInfo: authItems,
          notification : notificationData,
          newMessage: true,
          signedIn
        })

      }
    }
    
    
  }
  
    render(){
        const props = this.props;        
        const notification = Object.keys(this.state.notification).length > 0 ? this.state.notification : props
        
        const { checkedSignIn, signedIn, lastUpdated } = this.state;
        let Layout = createRootNavigator(signedIn, lastUpdated);

        if (!checkedSignIn) {
          return (
            <View style={styles.splashContianer}>
              <Image style={{width: 120, height: 120}} source={require('./assets/images/agent.png')} resizeMode="contain"/>
            </View>
          );
        }

        // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
        if(((notification.project_title &&
            notification.project_title.length &&
            notification.messageId &&
            notification.messageId.length)) && this.state.userInfo.agent_id && this.state.newMessage) {
 
          Layout = createAlertRootNavigator(signedIn, lastUpdated);

        } 

        //const Layout = createRootNavigator(signedIn, lastUpdated);
        const Navigator = createAppContainer(Layout)
        return(
            <Navigator screenProps = {notification}/>
        )
    }
}