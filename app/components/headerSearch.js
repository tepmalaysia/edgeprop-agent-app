import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';

export default class HeaderSearch extends Component{
	constructor(props){
		super(props);

	}
	render(){
		const MenuImage = ({navigation}) => {
	        if(!navigation.state.isDrawerOpen){
	            return <Image source={require('../assets/images/menu-button.png')}/>
	        }else{
	            return <Image source={require('../assets/images/left-arrow-1.png')}/>
	        }
	    }
		return (
			<View style={{height: 60, width: '100%', backgroundColor: 'blue'}}>
              <TouchableOpacity  style={{width: 50, justifyContent: 'center', alignItems: 'center', flex: 1, padding: 5}} onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())} }>
                <MenuImage style="styles.bar" navigation={this.props.navigation}/>
              </TouchableOpacity>
            </View>
		)
	}
}
