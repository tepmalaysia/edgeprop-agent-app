import React, { Component } from 'react'
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity
} from 'react-native'
import moment from 'moment';

const {width, height} = Dimensions.get('window');
export default class Past_TransactionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taggedTableHead: ['Date', 'Unit', 'Area', 'Price(PSF)'],
      unTaggedTableHead: ['Project/Township', 'Median Price Psf (RM)', 'Median Price (RM)', 'Filed Transactions'],
      widthArr: [100, 110, 80, 110]
    }

    this._formatDate = this._formatDate.bind(this);
  }


  _init() {}

  _onBack() {
    if (this.props.onBack) {
      this.props.onBack()
    }
  }

  formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }

  formatMoney(value){
    if ((typeof value !== 'undefined') && (value))
     return this.formatNumber(value);
  }
  
  _formatDate(transactionDate) {
      /*let monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      let str = transactionDate;
      let res = str.split(" ",3);
      let date = new Date(res[0]);
      let _date = date.getDate();
      let year = date.getFullYear();
      const monthName = monthNames[date.getMonth()];
      let returnDate = _date + " " + monthName + " " + year;*/
      let returnDate = moment.unix(transactionDate).format("DD-MM-YYYY");
      return returnDate;

      return returnDate;
  }
  
  render() {
    const state = this.state;
    const tableData = [];
    
     if (this.props.transactionDetail !== undefined) {
        if (this.props.transactionDetail.property !== undefined && this.props.transactionDetail.property[0]) {
        let rowCount = this.props.transactionDetail?(this.props.transactionDetail.property?this.props.transactionDetail.property.length:0):0;
        let transaction = this.props.transactionDetail?(this.props.transactionDetail.property?this.props.transactionDetail.property:''):'';
        if (transaction != '' && rowCount > 0 ) {
          for (let i = 0; i < rowCount; i += 1) {
            const rowData = [];
            //for (let j = 0; j < 2; j += 1) {
              if(this.props.tagged == true) {
                let getDate = this._formatDate(transaction[i].date);
                rowData.push(getDate);
                rowData.push('XXX, '+transaction[i].street_name);
                rowData.push(transaction[i].area_sqft+ ' sqft');
                rowData.push(this.formatMoney('RM '+transaction[i].transacted_price + '\n' + '(RM '+ transaction[i].unit_price_psf)  + ' psf)' );
              }else {
                rowData.push(transaction[i].project_name);
                rowData.push(transaction[i].psf);
                rowData.push(transaction[i].price);
                rowData.push(transaction[i].fieldtransactions);
              }
              
              //rowData.push(transaction[i].property_type);
              //rowData.push(transaction[i].unit_price_psf);
              
            //}
            tableData.push(rowData);
          }
        }
      }
    }

    let totalProjects = 0;
    let totalTransaction = 0;

    if (this.props.transactionDetail !== undefined) {
      if (this.props.transactionDetail.property !== undefined) {
        //totalProjects = this.props.transactionDetail.property.totalProj;
        //totalTransaction = this.props.transactionDetail.property.totalTras;
      }
    }

    let proName = '';
    let headerText = 'Past Transactions in '
     if (this.props.transactionDetail !== undefined) {
      if (this.props.transactionDetail.property !== undefined && this.props.transactionDetail.property !== undefined && this.props.transactionDetail.property[0]) {
        if(this.props.tagged == true) {
          headerText += this.props.transactionDetail.property[0].project_name + ', '+this.props.transactionDetail.property[0].planning_region;
        }else {
          headerText +=this.props.transactionDetail.property[0].area;
        }
        
      }
    }
    
    const element = (data, index) => (
      <TouchableOpacity onPress={() => this._alertIndex(index)}>
        
      </TouchableOpacity>
    );

    return (
      <>
        <View>
          <Text allowFontScaling={false} style={this.props.compStyles.commonLabel}>Past Transactions</Text>
        </View>
        <ScrollView horizontal={true}>
          <View style={styles.tableContainer}>
        <Table borderStyle={{borderColor: 'transparent'}}>
          <Row data={(this.props.tagged) ? state.taggedTableHead : state.unTaggedTableHead} style={styles.header}  widthArr={state.widthArr} textStyle={this.props.compStyles.headtext} />
          {
            tableData.map((rowData, index) => (
              <TableWrapper key={index} style={styles.row}>
                {
                  rowData.map((cellData, cellIndex) => (
                    <Cell  
                      allowFontScaling={false} 
                      key={cellIndex} 
                      data={cellData}
                      style={[ (cellIndex == 0 )? styles.widerCell : ((cellIndex == 1 )? styles.widerCell2 : ((cellIndex == 2 )? styles.widerCell3 : styles.widthrow)), index%2 && {backgroundColor: '#F2F4F8', borderColor: 'transparent'}]}
                      textStyle={this.props.compStyles.rowText}/>
                  ))
                }
              </TableWrapper>
            ))
          }
        </Table>
      </View>
        </ScrollView>
      </>

    )
  }
} 

 
const styles = StyleSheet.create({
widthrow: { width: 110, height: 70 },
widerCell: { width: 100, height: 70 },
widerCell2: { width: 110, height: 70 },
widerCell3: { width: 80, height: 70 },
row: { 
  flexDirection: 'row', 
  backgroundColor: '#FFFFFF', 
  borderWidth: 0.5, 
  borderColor: '#e6ecf2' 
}
});