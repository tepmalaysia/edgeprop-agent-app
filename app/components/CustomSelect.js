import React, { Component } from 'react';
import { StyleSheet, View, FlatList, TouchableOpacity, Image, Animated, Easing, Dimensions, Platform, Text } from 'react-native';
import { Colors, Images, Fonts, ApplicationStyles } from '../assets/theme';
import Modal from "react-native-modal";
import { CheckBox, Icon, Input } from 'react-native-elements';

const isIOS = Platform.OS === 'ios';

const { width, height } = Dimensions.get('window')

const AnimatedFlatlist = Animated.createAnimatedComponent(FlatList);

const ColorPreview = ({ color }) => (
  color ?
    <View style={[styles.colorPreview, { backgroundColor: color }]} />
    : null
)

const Field = ({ disabled, label = '', showValue = '', expandRotate, onPress, small = false, fieldWrapStyle, isColor = false, selectedColor = '', isOpen = false }) => {
  return (
    <TouchableOpacity
      style={[styles.expandBtn, fieldWrapStyle]}
      onPress={onPress}
    >
      {!isOpen && <Text numberOfLines={1} ellipsizeMode="tail" style={[styles.labelStyle, styles.labelTextStyle]}>{label || ''}</Text>}
      <View style={[styles.expandItem]}>
        <View style={styles.labelWrap}>
          {!!isColor && !selectedColor && (
            <Image style={styles.colorPreview} resizeMode="contain" source={Images.chooseColorIcon} />
          )}

          {!!isColor && !!selectedColor && (
            <ColorPreview color={selectedColor} />
          )}

          {!showValue ? (
            <Text numberOfLines={1} ellipsizeMode="tail" style={[styles.labelStyle, styles.labelTextStyle]}>{label || ''}</Text>
          ) : (
              <Text numberOfLines={1} ellipsizeMode="tail" style={styles.labelStyle}>{showValue}</Text>
            )}
        </View>
        {!disabled && (
          <Animated.View style={{ marginLeft: 10, transform: [{ rotateX: expandRotate }] }}>
            <Icon
              name='chevron-down'
              size={15}
              type='feather'
              color='#10182d'
            />
          </Animated.View>
        )}
      </View>
    </TouchableOpacity>
  )
}

class CustomSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maxPositionHeight: height - 100,
      maxHeight: 200,
      listHeight: null,
      isOpen: false,
      dropdownPosition: {
        width: 0,
        left: 0,
        top: 0
      }
    };
    this.animatedValue = new Animated.Value(0)
  }
  animate = () => {
    const { isOpen } = this.state;

    this.rootContainer.measureInWindow(async (x, y, w, h) => {
      const bottomOffset = isIOS ? 50 : 80;
      const maxPositionHeight = (height - y - bottomOffset);

      await this.setState({ dropdownPosition: { left: x, top: y, width: w }, maxPositionHeight, isOpen: !isOpen });
      this.animatedValue.setValue(!isOpen ? 1 : 0);

      Animated.timing(
        this.animatedValue,
        {
          toValue: isOpen ? 0 : 1,
          duration: 200,
          easing: Easing.linear
        }
      ).start()
    })

  }
  setListHeight = (contentWidth, contentHeight) => {
    this.setState({
      listHeight: contentHeight
    });
  }

  getShowedValue = () => {

    let showValue = '';

    const { multiple } = this.props;
    const data = this.props.data ? [].concat(this.props.data) : [];

    if (multiple) {
      let value = this.props.value || [];
      let index = 0;
      data.forEach((item, i) => {
        if (value.indexOf(this.valueExtractor(item)) !== -1) {
          showValue += (index !== 0 ? ', ' : '') + this.labelExtractor(item);
          index++;
        }
      });
    } else {
      const { value } = this.props;
      let selectedItem = data.find(item => {
        return this.valueExtractor(item) === value
      });

      if (selectedItem) {
        showValue = this.labelExtractor(selectedItem);
      }
    }

    return showValue;
  }

  _keyExtractor = (item, index) => String(index);

  valueExtractor = (item) => {
    return this.props.valueExtractor ? this.props.valueExtractor({ ...item }) : item.value;
  }

  labelExtractor = (item) => {
    return this.props.labelExtractor ? this.props.labelExtractor({ ...item }) : item.label;
  }

  onMultipleSelect = (item) => {
    const { onChangeText } = this.props;
    let value = this.props.value ? [].concat(this.props.value) : [];

    const indexInValue = value.indexOf(this.valueExtractor(item));
    const isChecked = indexInValue !== -1;

    if (isChecked) {
      value.splice(indexInValue, 1);
    } else {
      value = [].concat(value, this.valueExtractor(item));
    }

    if (onChangeText) {
      onChangeText(value);
    }
  }

  onSingleSelect = (item) => {
    const { onChangeText } = this.props;
    const newValue = this.valueExtractor(item);

    if (onChangeText) {
      onChangeText(newValue);
    }

    if (this.state.isOpen) {
      this.animate();
    }
  }

  renderOption = ({ item }) => {
    const { multiple, small, color } = this.props;
    let isChecked = false;

    if (multiple) {
      const value = this.props.value ? [].concat(this.props.value) : [];
      const indexInValue = value.indexOf(this.valueExtractor(item));
      isChecked = indexInValue !== -1;
    } else {
      const value = this.props.value;
      isChecked = this.valueExtractor(item) === value;
    }

    const itemDisabled = !!item && !!item.disabled;
    let updateFn;

    if (!itemDisabled) {
      updateFn = multiple ? () => this.onMultipleSelect(item) : () => this.onSingleSelect(item);
    }

    const withColor = !!item && !!item.color;

    let displayColor;

    if (!!color || !!withColor) {
      displayColor = !!color ? item.value : item.color;
    }

    return (
      <TouchableOpacity
        style={[styles.itemWrap, small ? styles.itemWrapSmall : null, itemDisabled ? { opacity: 0.6 } : null]}
        activeOpacity={0.6}
        onPress={updateFn}
      >
        <ColorPreview color={displayColor} />
        <Text style={[styles.textSm, { color: isChecked ? '#3670e5' : '#10182d', }]}>{this.labelExtractor(item) || ''}</Text>
      </TouchableOpacity>
    )
  }


  render() {
    const { isOpen, maxHeight, maxPositionHeight, listHeight, dropdownPosition } = this.state;
    const props = this.props;
    const err = props.error;
    const animatedHeight = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, Math.min(listHeight, maxPositionHeight, maxHeight)]
    });

    const expandRotate = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg']
    });

    const dropdownOpacity = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    });

    const disabled = Boolean(props.disabled);
    const isColor = !!props.color;

    const dropdownWrapStyle = { position: 'absolute', width: "100%", opacity: dropdownOpacity, zIndex: 100, ...dropdownPosition };
    let showValue = this.getShowedValue();

    let selectedColor = '';

    if (!props.multiple && isColor) {
      selectedColor = props.value || '';
    }

    return (
      <View
        style={[styles.container, props.style, ]}
        ref={ref => this.rootContainer = ref}
        onLayout={this.handleDropdownPosition}
      >
        <View style={[styles.fieldWrap, props.baseStyle]}>
          <Field
            small={!!props.small}
            isColor={isColor}
            selectedColor={selectedColor}
            label={props.label}
            onPress={!disabled ? this.animate : undefined}
            showValue={showValue || this.props.placeholder}
            expandRotate={expandRotate}
            disabled={disabled}
            fieldWrapStyle={props.fieldWrapStyle}
          />
        </View>


        {Boolean(err) && <View style={styles.errorRow}>
          <Text style={styles.errorText}>{err}</Text>
        </View>}
        {!!isOpen && (
          <Modal
            isVisible={isOpen}
            style={{ margin: 0 }}
            backdropColor="rgba(255, 255, 255, 0.5)"
            backdropOpacity={1}
            onBackdropPress={!!isOpen ? this.animate : undefined}
            animationIn={{ from: { opacity: 1 }, to: { opacity: 1 } }}
            animationOut={{ from: { opacity: 0 }, to: { opacity: 0 } }}
            animationInTiming={0}
            animationOutTiming={0}
          >
            <Animated.View
              style={dropdownWrapStyle}
            >
              <View style={[styles.fieldWrap, props.baseStyle, { zIndex: 101 }]}>
                <Field
                  small={!!props.small}
                  isColor={isColor}
                  label={props.label}
                  onPress={this.animate}
                  expandRotate={expandRotate}
                  disabled={disabled}
                  isOpen={this.state.isOpen}
                />
                <AnimatedFlatlist
                  style={{ height: animatedHeight, flex: 1, opacity: isOpen ? 1 : 0, width: '100%', paddingBottom: 10}}
                  onContentSizeChange={this.setListHeight}
                  contentContainerStyle={{ flexGrow: 1, paddingVertical: 15, backgroundColor: '#f5f5f5' }}
                  data={props.data}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderOption}
                  extraData={this.props.value}
                  keyboardShouldPersistTaps='handled'
                />
                {(!props.data || props.data.length === 0) && <View style={styles.noDataWrapper}><Text style={[styles.noData, { opacity: isOpen ? 1 : 0 }]}>No Options</Text></View>}
              </View>
            </Animated.View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
    alignItems: 'center',
    zIndex: 100,
    overflow: 'visible',
    
  },
  fieldWrap: {
    width: '100%',
    minHeight: 53,
    alignItems: 'stretch',
    borderRadius: 8,
    backgroundColor: '#FFFFFF',
  },
  textSm: {
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#10182d',
    lineHeight: width * 0.04,
    fontWeight: 'normal'
  },
  labelTextStyle: {
    color: '#9E9E9E',
  },
  expandBtn: {
    width: '100%',
    position: 'relative',
    top: 2,
    justifyContent: 'flex-end',
    height: 53,
    paddingVertical: 7.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)',
    borderBottomWidth: 1,
  },
  expandItem: {
    width: '100%',
    maxWidth: '100%',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingTop: 2.5
  },
  labelWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  textField: {
    width: '100%',
    flexShrink: 0,
    marginTop: -30,
  },
  labelStyle: {
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#10182d',
    lineHeight: width * 0.04,
    fontWeight: 'normal',
  },
  expandIcon: {
    width: 8,
    height: 5,
    resizeMode: 'contain',
  },
  itemWrap: {
    paddingVertical: 7.5,
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: '#f5f5f5'
  },
  itemWrapSmall: {
    paddingVertical: 5
  },
  errorRow: {
    paddingTop: 5,
    minHeight: 5,
    marginRight: 'auto',
    width: '100%'
  },
  errorText: {
    fontSize: 12,
    color: Colors.errorD,
  },
  colorPreview: {
    width: 14,
    height: 14,
    marginRight: 10,
    borderRadius: 7
  },
  noData: {
    fontSize: 12,
    color: '#9E9E9E',
    paddingBottom: 10,
    textAlign: 'center'
  },
  noDataWrapper: {
    backgroundColor: '#f5f5f5',
    paddingBottom: 20
  }
});

export default CustomSelect;