import React, { PureComponent } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ImageBackground,
  StyleSheet, 
  Dimensions,
  Linking
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
class EnquiryListItem extends PureComponent {

	
	  render() {
      
	  	let viewData = this.props.item? this.props.item : {}

	  	return(
        <View style={{paddingHorizontal: 15, marginVertical: 5}}>
                    <TouchableOpacity onPress={() => this.props.listingClick(viewData.mid)} style={[styles.listInnerItem, viewData.unRead? styles.listInnerItemActive : '']} key={this.props.index}>
                      <View style={styles.itemContainer}>
                        <View style={styles.leftText}>
                          <Text allowFontScaling={false} style={[styles.textMd]} numberOfLines = {1}>{viewData.title? viewData.title : ''}</Text>
                          <View style={[styles.cardLocation]}>
                            <Icon
                              name='ios-pin'
                              size={15}
                              type='ionicon'
                              color='#96d9c2'
                            />
                            <Text allowFontScaling={false} style={styles.cardContentLocation}>{viewData.location? viewData.location : ''}</Text>
                          </View>
                          <Text allowFontScaling={false} style={[styles.textSm,styles.textSm]} numberOfLines = {1}>{viewData.lid? viewData.lid : ''}</Text>
                        </View>
                        <View style={[styles.rightText, styles.rightCount]}>
                          <Text allowFontScaling={false} style={[styles.countText, viewData.unRead? styles.textActive : '']}>{viewData.count? viewData.count : 0}</Text>
                          <Text allowFontScaling={false} style={[styles.itemDate, viewData.unRead? styles.textActive : '']}>Enquiries</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
          
        );
	  }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    navIconRight: {
        paddingLeft: 20,
        minHeight: 24
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 35
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },  
    profileWrapper: {
        flex: 1,
        width: '100%',
        padding: 15
    },
    profileStat: {
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 6,
        shadowOffset:{width: 2,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        marginBottom: 10,
    },
    profileTop: {
        padding: 15,
        borderBottomColor: '#c5c5c5',
        borderBottomWidth: 0.5
    },
    profileBottom: {
        padding: 15,
        paddingHorizontal: 30
    },
    profilePrimary: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    avatar: {
        borderRadius: 100,
        overflow: 'hidden'
    },
    profilePrimaryInfo: {
        flex: 1,
        paddingLeft: 15,
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    hTwo: {
        fontSize: width * 0.045,
        fontFamily: 'OpenSans-Bold',
        color: '#10182d',
        lineHeight: width * 0.045,
        marginVertical: 5
    },
    listWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    listImgWrapper: {
        width: 17
    },
    listItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15
    },
    profileSub: {
        width: '100%',
        flexDirection: 'row',
        padding: 15
    },
    statItem: {
        width: '50%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    statRight: {
        paddingLeft: 10
    },
    statHead: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Bold',
        color: '#3670e5'
    },
    statSub: {
        width: '100%',
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-SemiBold',
        color: '#10182d'
    },
    premiumPlan: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        color: '#ffb400'
    },
    expirePlan: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#a1adca',
        lineHeight: width * 0.04,
    },
    listInnerItem: {
        backgroundColor: '#fff',
        borderRadius: 6,
        width: '100%',
        height: 70,
        shadowOffset:{width: 2,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        flex: 1
    },
    listInnerItemActive: {
        backgroundColor: '#f3f5f7',
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',
        width: '100%',
    },
    leftIcon:{
        marginLeft: 12,
        marginRight: 15,
    },
    listText: {
        fontSize: width * 0.032,
    },
    textSmBold: {
        fontWeight: '700',
    },
    textMd: {
        color: '#10182d',
        fontSize: width * 0.034,
        fontFamily: 'OpenSans-Bold'
    },
    centerText: {
        paddingLeft: 5,
        paddingRight: 10,
        flex: 1,
    },
    leftText: {
        paddingLeft: 15,
        paddingRight: 15,
        flex: 1,
    },
    rightText: {
        width: '18%',
        height: '100%',
        paddingVertical: 10,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    rightCount: {
        justifyContent: 'center',
        backgroundColor: '#e6ecf2'
    },
    itemDate: {
        textAlign: 'center',
        width: '100%',
        fontSize: width * 0.020,
        color: '#10182d',
    },
    countText: {
        textAlign: 'center',
        width: '100%',
        fontSize: width * 0.045,
        color: '#10182d',
    },
    textActive: {
        color: '#3670e5',
    },
    title: {
        fontSize: width * 0.042,
    },
    listContainer: {
        paddingHorizontal: 15,
        paddingVertical: 15,
    },
    enquiryListWrapper: {
        width: '100%'
    },
    listInner: {
        borderTopWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.2)',
    },
    noBorder: {
        borderTopWidth: 0,
    },
    commonLabel: {
        color: '#8991a4',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
        paddingBottom: 10
    },
    LeftMenuIcon: {
        marginLeft: -10,
    },
    cardContentLocation: {
        flex: 1,
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold',
        color: '#149d78',
        paddingLeft: 5
    },
    cardLocation: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'nowrap',
        paddingTop: 2
    },
    enquiryDividerHead: {
        marginBottom: 15
    },
    innerRightText: {
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    listAction:{
        justifyContent: 'center',
        backgroundColor: '#ECEDF1',
        width: '90%',
        borderRadius: 30,
        padding: 1
    },
    textAccepted: {
        color: 'green',
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    textRejected: {
        color: '#F06862',
        fontSize: width * 0.022,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 10,
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .35)',
    },
    overlayInner: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        flex: 1,
        lineHeight: width * 0.035,
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    },
    textMdNoRecord: {
        fontSize: width * 0.034,
        fontFamily: 'OpenSans-SemiBold',
        color: '#6d7e8e',
        textAlign: 'center',
        width: '100%',
        marginTop: 20,
        lineHeight: width * 0.045,
        fontWeight: 'normal'
      },
    noDataContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default EnquiryListItem