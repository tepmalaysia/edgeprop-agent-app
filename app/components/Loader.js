import React, { Component } from 'react'
import {View, ActivityIndicator, Dimensions} from 'react-native'
const {width, height} = Dimensions.get('window');

export default class Loader extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
          <View style={{
            position: 'absolute',
            top:0,
            bottom:0,
            left:0,
            right:0,
            opacity: 1,
            width: width,
            height: height,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: "rgba(255,255,255,0.75)"
          }}>
            <ActivityIndicator
              animating size='large'
              style={{
                height: 80
              }}
              color={'#3670e5'}
            />
          </View>
        )
    }
}