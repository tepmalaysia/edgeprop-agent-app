import React, { PureComponent } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ImageBackground,
  StyleSheet, 
  Dimensions,
  Linking
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
class PropertyItem extends PureComponent {

	 _formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    _formatMoney(val) {
        return val ? 'RM ' + this._formatNumber(val) : '-'
    }
	
	  render() {
	  	  let item = this.props.item? this.props.item : {}
	  	  let price = item.price ? item.price : '0';
	      let image = image = item.cover_image ? item.cover_image : item.images[0] ? item.images[0] : 'https://list.edgeprop.my/static/img/no-image.png';
	      let is_public_photo = image.search("public://");
	      if(is_public_photo == 0){
	        image = 'https://list.edgeprop.my/static/img/no-image.png';
	      }
	      let impression = item.impression ? item.impression : 0;
	      let whatsapp = item.whatsapp_count ? item.whatsapp_count : 0;
	      let showNumber = item.view_contact ? item.view_contact : 0;
	      let mid = item.mid ? 'LIDM' + item.mid : 'LIDN' + item.nid;
	      let idWithType = item.mid ? 'm' + item.mid : 'n' + item.nid;
	      let url = item.url ? item.url : '';
	          url = "https://www.edgeprop.my/listing/"+url;
	      let isFeatured = item.isFeatured? item.isFeatured : false

	  	return(
            <View style={{paddingHorizontal: 15, marginVertical: 5}}>
                <TouchableOpacity style={this.props.checkSelected(item.lid)? [styles.cardItem, styles.cardSelected] : styles.cardItem} onPress={() =>this.props.listingClick(item.lid, item.listing_type, idWithType)} onLongPress={() =>this.props.onLongPress(item.lid, item.listing_type, idWithType)}>
                  {this.props.checkSelected(item.lid) && <Image
                    style={styles.selectedIcon}
                    source={require('../assets/images/selected.png')}
                  />}
                  <View style={styles.cardImgWrapper}>
                    {/*  Card Image */}
                    <ImageBackground source={{uri: image}} style={styles.cardImage}>
                      <LinearGradient colors={['rgba(0,0,0,0.5)', 'transparent', 'rgba(0,0,0,0.8)']} style={styles.cardOverlay}>
                        <Text allowFontScaling={false} style={styles.cardOverlayText}>{mid}</Text>
                        <View style={styles.cardOverlayInner}>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{impression}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../assets/images/steps.png')}
                            />
                          </View>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{showNumber}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../assets/images/view.png')}
                            />
                          </View>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{whatsapp}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../assets/images/whatsapp-sm.png')}
                            />
                          </View>
                        </View>
                      </LinearGradient>
                    </ImageBackground>
                  </View>
                  {/*  Card Content */}
                  <View style={styles.cardContentWrapper}>
                    <View style={styles.cardContentTop}>
                      <Text allowFontScaling={false} style={styles.cardTitle} ellipsizeMode='tail' numberOfLines={1}>{item.title}</Text>
                      {(item.district != '' || item.state !='') &&
                      <View style={styles.cardLocation}>
                        <Icon
                          name='ios-pin'
                          size={15}
                          type='ionicon'
                          color='#96d9c2'
                        />
                        <Text allowFontScaling={false} style={styles.cardContentLocation} ellipsizeMode='tail' numberOfLines={1}>{item.district ? item.district+',' : ''} {item.state ? item.state : ''}</Text>
                      </View>
                      }

                      <Text allowFontScaling={false} style={styles.cardPrice}>{this._formatMoney(price)}</Text>
                      <View style={styles.cardLocation}>
                        <Icon
                          name='clock'
                          size={10}
                          type='feather'
                          color='#646975'
                        />
                      <Text allowFontScaling={false} style={styles.cardUpdateDate}>{moment.unix(item.changed).fromNow()}</Text>
                    </View>
                    </View>
                    {this.props.type=='active' && <View style={styles.cardContentBottom}>
                      <TouchableOpacity onPress={() => (!this.props.enableLongPress) ? this.props.onOpenUrl(url) : ''} style={styles.cardActionItem}>
                        <Icon
                          name='external-link'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>View</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.cardActionItem} onPress={() => (!this.props.enableLongPress) ? this.props.onRepost(item.listing_type,mid) : ''}>
                        <Icon
                          name='corner-left-up'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Repost</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => (!this.props.enableLongPress) ? this.props.onFeatured(mid, item) : ''} style={styles.cardActionItem}>
                        <Icon
                          name={isFeatured? 'star' : 'star-outlined'}
                          size={24}
                          type='entypo'
                          color={isFeatured? '#ffb400' : '#10182d'}
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Featured</Text>
                      </TouchableOpacity>
                    </View>}

                    {this.props.type=='expired' && <View style={styles.cardContentBottom}>
                      <TouchableOpacity style={styles.cardActionItem} onPress={() => (!this.props.enableLongPress) ? this.props.onBulkUpdate(item.listing_type, item.lid, idWithType, 'active') : ''} >
                        <Icon
                          name='power'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Activate</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => (!this.props.enableLongPress) ? this.props.onBulkUpdate(item.listing_type, item.lid, idWithType, 'deleted') : ''} style={styles.cardActionItem}>
                        <Icon
                          name='trash-2'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Delete</Text>
                      </TouchableOpacity>
                    </View>}

                    {this.props.type=='saved' && <View style={styles.cardContentBottom}>
                      <TouchableOpacity onPress={() => (!this.props.enableLongPress) ? this.props.onOpenUrl(url) : ''} style={styles.cardActionItem}>
                        <Icon
                          name='external-link'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>View</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.cardActionItem} onPress={() => (!this.props.enableLongPress) ? this.props.onBulkUpdate(item.listing_type, item.lid, idWithType, 'active') : ''} >
                        <Icon
                          name='power'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Activate</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => (!this.props.enableLongPress) ? this.props.onBulkUpdate(item.listing_type, item.lid, idWithType, 'deleted') : ''} style={styles.cardActionItem}>
                        <Icon
                          name='trash-2'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Delete</Text>
                      </TouchableOpacity>
                    </View>}
                  </View>
                </TouchableOpacity>
                </View>
        );
	  }
}
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
        flex: 1
    },
    headerContainer: {
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 10,
        backgroundColor: '#3670e5',
        borderBottomWidth: 0,
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: -12
    },
    navIconRight: {
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginRight: -12
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    }, 
    cardWrapper: {
        flex: 1,
        width: '100%',
    },
    cardItem: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 6,
        shadowOffset:{width: 2,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        minHeight: 100
    },
    cardImgWrapper: {
        width: '40%',
        overflow: 'hidden',
        borderTopLeftRadius: 6,
        borderBottomLeftRadius: 6,
        position: 'relative'
    },
    cardImage: {
        flex: 1,
        resizeMode: 'cover'
    },
    cardOverlay: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        zIndex: 1,
        paddingHorizontal: 10,
        paddingBottom: 10,
        paddingTop: 5,
        backgroundColor: 'transparent',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    cardOverlayText: {
        color: '#fff',
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold'
    },
    cardOverlayInner: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    cardOverlayItem: {
        flex: 1,
        maxHeight: 30,
        textAlign: 'center',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardContentWrapper: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        flex: 1
    },
    cardContentTop: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderBottomColor: '#c5c5c5',
        borderBottomWidth: 0.5,
        flex: 1
    },
    cardTitle: {
        fontSize: width * 0.036,
        fontFamily: 'OpenSans-Bold',
        color: '#10182d'
    },
    cardLocation: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'nowrap',
        paddingTop: 2
    },
    cardContentLocation: {
        flex: 1,
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold',
        color: '#149d78',
        paddingLeft: 5
    },
    cardPrice: {
        fontSize: width * 0.036,
        fontFamily: 'OpenSans-Bold',
        color: '#3670e5',
        paddingTop: 10
    },
    cardUpdateDate: {
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold',
        color: '#646975',
        paddingLeft: 5
    },
    cardContentBottom: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-around'
    },
    cardActionItem: {
        padding: 10,
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    cardActionText: {
        paddingTop: 7,
        fontSize: width * 0.02,
        fontFamily: 'OpenSans-SemiBold',
        color: '#10182d',
        textAlign: 'center',
        textTransform: 'uppercase'
    },
    cardSelected: {
        backgroundColor: '#e9eef8',
        position: 'relative'
    },
    selectedIcon: {
        width: 11,
        height: 11,
        position: 'absolute',
        right: 5,
        top: 5,
        zIndex: 2
    },
    textMdNoRecord: {
        fontSize: width * 0.034,
        fontFamily: 'OpenSans-SemiBold',
        color: '#6d7e8e',
        textAlign: 'center',
        width: '100%',
        marginTop: 20,
        lineHeight: width * 0.045,
        fontWeight: 'normal'
      },
    noDataContainer: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardActionItemSelected: {
        backgroundColor: '#e9eef8'
    },
    cardActionTextSelected: {
        color: '#3670e5'
    },
    bottomActions: {
        width: width,
        height: 50,
        padding: 5,
        backgroundColor: '#FFF',
        shadowOffset:{width: 2,  height: -5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    bottomActionItem: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    showFilter: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#3670e5',
        paddingHorizontal: 15,
        paddingBottom: 10
    },
    filterBadge: {
        flex: 0,
        width: 'auto',
        borderRadius: 20,
        paddingLeft: 10,
        paddingRight: 15,
        paddingTop: 2,
        paddingBottom: 2,
        backgroundColor: '#4b87ff',
        shadowOffset:{width: 2,  height: 2,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.4,
        elevation: 5,
        marginRight: 10
    },
    filterText: {
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-Regular',
        color: '#fff',
        fontWeight: 'normal',
    },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        padding: 0,
        backgroundColor: 'transparent',
        shadowOffset:{width: 0,  height: 0,},
        shadowColor: 'rgba(0, 0, 0, 0)',
        shadowOpacity: 0,
        elevation: 0,
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .15)',
    },
    overlayInner: {
        width: '100%',
        height: 'auto',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        lineHeight: width * 0.035,
        flexWrap: 'wrap',
        flex: 1
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    },
    LeftMenuIcon: {
        marginLeft: -10,
    },
    overlayPopContent: {
        padding: 15,
    },
    overlayPopText: {
        color: '#10182d',
        fontSize: width * 0.036,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.05,
    },
    overlayPopButtons: {
        borderTopWidth: 1,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row'
    },
    OverlayPopBtn: {
        padding: 18,
        flex: 1
    },
    OverlayPopBtnText: {
        color: '#149d78',
        textTransform: 'uppercase',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    fieldWrapTwoWay: {
        width: '50%',
        paddingHorizontal: 5,
    },
    fieldWrapThreeWay: {
        width: '33.333%',
        paddingHorizontal: 5,
    },
    fieldWrap: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        flexWrap: 'wrap'
    },
    errorMsg: {
        color: 'rgb(213, 0, 0)',
        fontSize: 12,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'left'
    },
    overlayFeaturedStyle: {
        backgroundColor: '#fff',
        padding: 0,
        shadowOffset:{width: 5,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0)',
        shadowOpacity: 0
    },
    errorParent: {
        width: '100%',
        height: 'auto',
        flexDirection: 'column',
        alignItems: 'center',
    },
    listViewDeleted: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 35,
        width: '100%'
    },
    listViewStyle:{
        paddingLeft: 0,
    },
    listShape: {
        color: '#fff',
        marginLeft: 5,
    },
    overlayInnerError : {
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    errorTextList: {
        marginTop: 5,
    },
    overlayTextError: {
        fontSize: width * 0.035,
        color: '#fff',
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        flex: 1,
        lineHeight: width * 0.035,
    },
    bgDangerInner: {
        flexDirection: 'row',
        width: '100%',
        marginBottom: 5
    },
    alertWrapper: {
        flex: 1,
    }

});

export default PropertyItem