import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
//import LoginScreen from './modules/screens/Login_Landing/LoginLanding';
import EnquiryScreen from './modules/screens/EnquiryList/EnquiryListScreen';
import EnquiryAlertScreen from './modules/screens/EnquiryAlert/EnquiryAlertScreen';
import EnquiryDetailsScreen from './modules/screens/EnquiryDetails/EnquiryDetailsScreen';

const SwitchNavigator = createSwitchNavigator(
  {
    //Splash : {screen: SplashScreen},
    //Login : {screen: LoginScreen},
    Enquiry : {screen: EnquiryScreen},
    EnquiryAlert : {screen: EnquiryAlertScreen},
    EnquiryDetails : {screen: EnquiryDetailsScreen},
  },
  {
    initialRouteName: "Login"
  }
);

export default SwitchNavigator;