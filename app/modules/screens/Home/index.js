/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Animated, 
  Easing,
  ImageBackground,
  Dimensions
} from 'react-native';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { DrawerActions } from 'react-navigation-drawer';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase'
import moment from 'moment';
import styles from './homeStyles';
import OverlayStyle from '../../styles/overlayStyle';
import HeaderSearch from '../../../components/headerSearch';

import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import ActiveListing from '../ListingTabs/ActiveListing';
import SavedListing from '../ListingTabs/SavedListing';
import ExpiredListing from '../ListingTabs/ExpiredListing';
import SalesData from '../../../assets/json/sale.json'
import { ListStatus } from '../../services/api'
import ListingActions from '../../../../realm/actions/ListingActions'

const FETCHONE = 'https://prolex.edgeprop.my/api/v1/fetchOne';
const PROP_STATUS = 'https://prolex.edgeprop.my/api/v1/get-property-stats';
const { width, height } = Dimensions.get('window');

/*const Tabs = createMaterialTopTabNavigator(
  tabScreens,
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    initialRouteName: 'Active', // set inital route
    lazy: true,
    tabBarOptions: {
      style: {
        backgroundColor: '#e6ecf2',
        shadowOpacity: 0,
        padding: 0,
        height: 45,
        marginTop: -5
      },
      labelStyle: {
        fontSize: width*0.03,
        fontFamily: 'OpenSans-Bold',
      },
      inactiveTintColor: '#75829a',
      activeTintColor: '#346dfe',
      indicatorStyle: {
        backgroundColor: '#346dfe',
      }
    }
  }
)*/

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/app/get-list-status';
class Home extends Component{
  showTab = true;
  intervalID = 0;
  constructor(props){
    super(props);
    this.ListingActions = new ListingActions();
    this.state = {
      enableLongPress : false,
      selected: [],
      fetchData: [],
      userInfo: {},
      sync: false,
      update: false,
      showTab: true,
      trigger: 0,
      isVisible: false,
      responseInfo: '',
      force: false,
      showAdd: true,
      showAddActive: true,
      showAddExpire: true,
      showAddSave: true,
      animatedValue: new Animated.Value(1),
      index: 1,
      routes: [
        { key: 'Saved', title: 'Saved' },
        { key: 'Active', title: 'Active' },
        { key: 'Expired', title: 'Expired' },
      ]
    }
    this.showTab = true;
    this.count = 0;
    this.filter ={
      listing_type: 'sale',
      category:'', 
    }
    
    this._actionItems = this._actionItems.bind(this);
    this._syncAction = this._syncAction.bind(this);
    this._updateAction = this._updateAction.bind(this);
    this._onSearch = this._onSearch.bind(this);
    this._getList = this._getList.bind(this);
    this.listStatusSuccess = this.listStatusSuccess.bind(this);
    this.listStatusFail = this.listStatusFail.bind(this);
    this._onCreate = this._onCreate.bind(this)
    this._toggleAdd = this._toggleAdd.bind(this)
    this._onRefetch = this._onRefetch.bind(this)
    this._updateList = this._updateList.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.fetchOneSuccess = this.fetchOneSuccess.bind(this)
    this.fetchOneFail = this.fetchOneFail.bind(this)
    this.renderScene = this.renderScene.bind(this)
    props.navigation.setParams({
      showTab: true
    });
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems })
        setTimeout(() => {
          this._getList()
          this.timer = setInterval(()=> this._getList(), 300000)
        }, 500);
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }
  

  shouldComponentUpdate(nextProps, nextState) {
    if(JSON.stringify(nextProps.navigation.state.params) != JSON.stringify(this.props.navigation.state.params)){
      if(nextProps.navigation.state.params.data != undefined){
        this.filter = nextProps.navigation.state.params.data;
        if(nextProps.navigation.state.params.message){
          this.setState({
            isVisible: true,
            force: !this.state.force,
            index: nextProps.navigation.state.params.tabIndex,
            responseInfo: nextProps.navigation.state.params.message? nextProps.navigation.state.params.message : 'Action done successfully.'
          })
        }
      }
    }
   return true;
  }

  async _getList(){
    this._updateAction(true);
    const lastUpdated = await AsyncStorage.getItem("lastUpdated");
    let body = "uid=" + this.state.userInfo.uid;
    if(lastUpdated){
      body = body+ "&changed=" + lastUpdated;
    }
    ListStatus(HOSTNAME, body)
    .then(data => this.listStatusSuccess(data))
    .catch(error => this.listStatusFail(error));
  }

  async listStatusSuccess(response){
    if(response.status == 1 && response.data){
      /*this.setState({fetchData : response.data, trigger: moment().unix()})*/
      this._updateList(response.data)
      let currentTimestamp = moment().unix();//300000
      const lastUpdated = await AsyncStorage.getItem("lastUpdated");
      let difference = currentTimestamp-parseInt(lastUpdated)
      if(difference > 300){
        try {
          await AsyncStorage.setItem("lastUpdated", moment().unix().toString())
            .then( ()=>{})
            .catch( ()=>{})
        } catch (error) {}
      }
    }else{
      this._updateAction(false);
    }
  }

  listStatusFail(error){
    
  }

  _onSearch() {
    this.props.navigation.navigate('SearchOption', {
      data: {
        options: this.filter
      }
    });
  }
  

  _onCreate(){
    this.props.navigation.navigate('ListingCreate');
  }

  _start = () => {
    Animated.timing(this.state.animatedValue, {
      toValue: 0,
      duration: 250
    }).start();
  };
  _end = () => {
    Animated.timing(this.state.animatedValue, {
      toValue: 1,
      duration: 250
    }).start();
  };

  _actionItems(status){
    this.props.navigation.setParams({
      showTab: status,
      message: null
    });
    this.setState({
      showTab: status
    })
    this.showTab = status;
  }

  _syncAction(flag){
    this.setState({ sync : flag })
  }

  _updateAction(flag){
    this.setState({ update : flag })
  }

  _toggleAdd(direction, type){
    if(direction == 'down'){
      this._start();
    }else if(direction == 'up'){
      this._end();
    }
  }

  _onRefetch(){
    this._getList();
  }

  _updateList(items) {
    if(Array.isArray(items)){
      this.setState({ fetchData: items }, () => {
        for(var i = 0; i < items.length; i++){
          let item = items[i];
          let listing_type = item.listing_type == 'rent'? 'rental' : item.listing_type;
          const data = "uid=" + this.state.userInfo.uid +
                "&token=" + this.state.userInfo.token +
                "&field_prop_listing_type=" + listing_type +
                "&property_id=" + item.lid +
                "&cache=0" +
                "&source=edit"+
                "&type=" + item.id_type;
          let dataStatus = ""; 
          let mid = item.mid? item.mid : 0;
          let nid = item.nid? item.nid : 0;
          dataStatus = "mid=" +mid +
                "&nid=" +nid ; 
          
          
          if(item.status == 'fetch'){
            this.fetchData(data, dataStatus).then(arrayOfResponses => {
            this.fetchOneSuccess(arrayOfResponses[0], arrayOfResponses[1])
          }).catch(error => this.fetchOneFail(error));
          }else if(item.status == 'delete'){
            this.count++;
            let listItem = this.ListingActions.GetListing(item.lid);
            if(listItem && listItem.length >0){
              this.ListingActions.ClearListing(item.lid);
            }
            if(this.count == this.state.fetchData.length){
              this.count = 0;
              this._updateAction(false);
              this.setState({trigger: moment().unix()})
            }
          }
        }
      })
      
    }
  }

  fetchData = (body1, body2) => {
    const urls = [
      { url : FETCHONE, body: body1},
      { url : PROP_STATUS, body: body2}
    ];
    
    const allRequests = urls.map(item => 
      fetch(item.url, {
        method: 'POST',
        headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: item.body
      }).then(response => response.json())
    );

    return Promise.all(allRequests);
  };

  fetchOneSuccess(response, statusResponse){
    this.count++;
    if(response.result){
      const listingData = this.formatListing(response.result, statusResponse);
      let result = {...listingData, 'token': this.state.userInfo.token, 'uid': parseInt(this.state.userInfo.uid)};
      let listItem = this.ListingActions.GetListing(result.lid);
      if(listItem && listItem.length >0){
        this.ListingActions.ClearListing(result.lid);
      }
      this.ListingActions.CreateListing(result);
    }
    if(this.count == this.state.fetchData.length){
      this.count = 0;
      this._updateAction(false);
      this.setState({trigger: moment().unix()})
    }
  }

  fetchOneFail(error){
    this.count++;
    if(this.count == this.state.fetchData.length){
      this.count = 0;
      this._updateAction(false);
      this.setState({trigger: moment().unix()})
    }
  }

  formatListing = (data, response) => {
    let asset = data.asset;
    let listing = {};

    listing.lid = data.mid ? data.mid : data.nid;

    listing.nid = data.nid ? data.nid : null;

    listing.mid = data.mid ? data.mid : null;

    listing.title = data.title ? data.title : '';

    listing.status = data.status ? parseInt(data.status) : 0;
  
    listing.created = data.created ? data.created : '';

    listing.changed = data.changed ? data.changed : '';

    listing.district = data.district ? data.district.toString() : '';

    listing.state = data.state ? data.state.toString() : '';

    listing.project_name = asset.project_name ? asset.project_name : '';

    listing.project_id = asset.id ? asset.id : '';

    listing.prop_status = (data && data.field_prop_status && data.field_prop_status.und && Array.isArray(data.field_prop_status.und)) ? (data.field_prop_status.und[0]? data.field_prop_status.und[0].value : '') : '';


    listing.listing_type = (data && data.field_prop_listing_type && data.field_prop_listing_type.und && Array.isArray(data.field_prop_listing_type.und)) ? (data.field_prop_listing_type.und[0]? data.field_prop_listing_type.und[0].value : '') : '';

    listing.property_type = (data && data.field_property_type && data.field_property_type.und && Array.isArray(data.field_property_type.und)) ? (data.field_property_type.und[0]? data.field_property_type.und[0].target_id : '') : '';

    listing.asset_id = (data && data.field_prop_asset && data.field_prop_asset.und && Array.isArray(data.field_prop_asset.und)) ? (data.field_prop_asset.und[0]? data.field_prop_asset.und[0].target_id : '') : '';

    listing.price = (data && data.field_prop_asking_price && data.field_prop_asking_price.und && Array.isArray(data.field_prop_asking_price.und)) ? (data.field_prop_asking_price.und[0]? data.field_prop_asking_price.und[0].value : null) : null;
    listing.street = (data && data.field_prop_street && data.field_prop_street.und && Array.isArray(data.field_prop_street.und)) ? (data.field_prop_street.und[0]? data.field_prop_street.und[0].value : null) : null;
    let coverImages = (data && data.field_cover_image && data.field_cover_image.und && Array.isArray(data.field_cover_image.und)) ? data.field_cover_image.und : [];
    listing.cover_image = (coverImages && coverImages[0] && coverImages[0].uri)? coverImages[0].uri: '';
    
   
    let images = (data && data.field_prop_images && data.field_prop_images.und && Array.isArray(data.field_prop_images.und)) ? data.field_prop_images.und : [];
    let list_uri = [];
    list_uri = images.map((item, index) => { return item.list_uri });
    listing.images = list_uri;

    listing.data = JSON.stringify(data);
    listing.url = data.url ? data.url : '';

    if(response && (response.mid || response.nid)){
      listing.show_number = response.pageviewed;
      listing.contacted = response.contactagent;
      listing.view_contact = response.viewcontact;
      listing.whatsapp_count = response.mobWhatsapp;
      listing.impression = response.impressions;
      listing.isFeatured = response.isFeatured;
      if(response.featuredData){
        listing.featuredData = JSON.stringify(response.featuredData);
      }
    }
    return listing;
  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'Saved':
        return <SavedListing 
          screenProps = {{...this.props.screenProps,...{
            data: {
              items: this.state.fetchData,
              actionHandler: this._actionItems,
              filter: this.filter,
              syncHandler: this._syncAction,
              updateHandler: this._updateAction,
              bulkHandler : this._getList,
              trigger: this.state.trigger,
              forceLoad: this.state.force,
              toggleAdd: this._toggleAdd,
              updateList: this._updateList
            }
          }}}
          navigation = {this.props.navigation}
          />;
      case 'Active':
        return <ActiveListing
          screenProps = {{...this.props.screenProps,...{
            data: {
              items: this.state.fetchData,
              actionHandler: this._actionItems,
              filter: this.filter,
              syncHandler: this._syncAction,
              updateHandler: this._updateAction,
              bulkHandler : this._getList,
              trigger: this.state.trigger,
              forceLoad: this.state.force,
              toggleAdd: this._toggleAdd,
              updateList: this._updateList
            }
          }}}
          navigation = {this.props.navigation}
        />;
      case 'Expired':
        return <ExpiredListing
          screenProps = {{...this.props.screenProps,...{
            data: {
              items: this.state.fetchData,
              actionHandler: this._actionItems,
              filter: this.filter,
              syncHandler: this._syncAction,
              updateHandler: this._updateAction,
              bulkHandler : this._getList,
              trigger: this.state.trigger,
              forceLoad: this.state.force,
              toggleAdd: this._toggleAdd,
              updateList: this._updateList
            }
          }}}
          navigation = {this.props.navigation}
        />;
      default:
        return null;
    }
  };

  render() {
    const checkSelected= (index) => {
      return this.state.selected.indexOf(index) > -1 ? true : false;
    }

    let { animatedValue } = this.state;

    const LeftNavComponent = () => {

      return (
        <View style={styles.navLeft} >
          <TouchableOpacity onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Icon
              name='menu'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <TouchableOpacity style={styles.navCenterInner} onPress={this._onSearch}>
            <Text allowFontScaling={false} style={styles.navTitle}>Find Properties...</Text>
            <Icon
              name='search'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const RightNavComponent = () => {
      return (
        <View style={styles.navRight}>
          <TouchableOpacity style={styles.navIconRight} onPress={this._onRefetch}>
            <Icon
              name='rotate-cw'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const checkFilter = (key) => {
      return (this.filter && this.filter[key]) ? true : false;
    }
    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          {/* <HeaderSearch navigation={this.props.navigation} /> */}
          <View style={styles.headerContainer}>
            <Header
              statusBarProps={{ barStyle: 'light-content' }}
              containerStyle={styles.navContainer}
              placement="left"
              centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
              leftComponent={<LeftNavComponent />}
              centerComponent={<CenterNavComponent />}
              rightComponent={<RightNavComponent />}
            />
            <ScrollView horizontal={true}>
              <View style={styles.showFilter}>
                {checkFilter('search') && <Button
                  disabled
                  icon={{
                    name: "filter",
                    type: "font-awesome",
                    size: 11,
                    color: "#9eb9ff"
                  }}
                  title={this.filter['search']}
                  disabledTitleStyle={styles.filterText}
                  disabledStyle={styles.filterBadge}
                />}
                {checkFilter('listing_type') && <Button
                  disabled
                  icon={{
                    name: "filter",
                    type: "font-awesome",
                    size: 11,
                    color: "#9eb9ff"
                  }}
                  title={this.filter['listing_type'] == 'sale' ? 'For Sale' : this.filter['listing_type'] == 'rental' ? 'For Rent' : this.filter['listing_type'] == 'auction' ? 'For Auction' : this.filter['listing_type'] == 'room_rental' ? 'For Room Rental' : 'For Sale'}
                  disabledTitleStyle={styles.filterText}
                  disabledStyle={styles.filterBadge}
                />}
                {checkFilter('category') && <Button
                  disabled
                  icon={{
                    name: "filter",
                    type: "font-awesome",
                    size: 11,
                    color: "#9eb9ff"
                  }}
                  title={this.filter['category'] == 'residential'? "Residential" : "Non-Residential"}
                  disabledTitleStyle={styles.filterText}
                  disabledStyle={styles.filterBadge}
                />}
                {checkFilter('isFeatured') && <Button
                  disabled
                  icon={{
                    name: "filter",
                    type: "font-awesome",
                    size: 11,
                    color: "#9eb9ff"
                  }}
                  title="Is Featured"
                  disabledTitleStyle={styles.filterText}
                  disabledStyle={styles.filterBadge}
                />}
              </View>
            </ScrollView>
          </View>

          <View style={styles.container}>
            {(this.state.sync || this.state.update) && <View style={[styles.syncTag]}>
              <View style={[styles.syncBadge]}>
                <Image
                  style={{width: 15, height: 15, marginRight: 10,}}
                  source={require('../../../assets/images/sync.gif')}
                />
                <Text allowFontScaling={false} style={styles.syncText}>Syncing</Text>
              </View>
            </View>}
            {<Animated.View style={[styles.floatingButton, styles.flexButton, {bottom: !this.state.showTab? 65 : 15},{transform: [
              {
                translateX: animatedValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [100, 0]
                })
              }
            ]}]} ><TouchableOpacity style={styles.flexButton} onPress={this._onCreate}>
              <Icon
                name='plus'
                type='feather'
                color='#fff'
                size={24}
              />
              </TouchableOpacity>
            </Animated.View>}
            <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
              {/*<TabNav
                screenProps = {{...this.props.screenProps,...{
                  data: {
                    items: this.state.fetchData,
                    actionHandler: this._actionItems,
                    filter: this.filter,
                    syncHandler: this._syncAction,
                    updateHandler: this._updateAction,
                    bulkHandler : this._getList,
                    trigger: this.state.trigger,
                    forceLoad: this.state.force,
                    toggleAdd: this._toggleAdd,
                    updateList: this._updateList
                  }
                }}}
                navigation = {this.props.navigation}
              />*/}
              <TabView
                  navigationState={this.state}
                  renderScene = {this.renderScene}
                  onIndexChange={index => this.setState({ index })}
                  initialLayout={{ width: Dimensions.get('window').width }}
                  renderTabBar={props =>
                    <TabBar
                      {...props}
                      style = {{
                        backgroundColor: '#e6ecf2',
                        shadowOpacity: 0,
                        padding: 0,
                        height: 45,
                        marginTop: -5
                      }}
                      labelStyle = {{
                        fontSize: width*0.03,
                        fontFamily: 'OpenSans-Bold',
                      }}
                      inactiveColor = {'#75829a'}
                      activeColor = {'#346dfe'}
                      indicatorStyle = {{
                      backgroundColor: '#346dfe',
                      }}
                      />
                    }
                />
            </SafeAreaView>
          </View>
          {/* Bulk Actions */}
          {/*!this.showTab && <View style={styles.bottomActions}>
            <TouchableOpacity style={styles.bottomActionItem}>
              <Image
                style={{width: 20, height: 21}}
                source={require('../../../assets/images/repost.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomActionItem}>
              <Image
                style={{width: 28, height: 17}}
                source={require('../../../assets/images/feature.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomActionItem}>
              <Image
                style={{width: 20, height: 21}}
                source={require('../../../assets/images/trash.png')}
              />
            </TouchableOpacity>
          </View>*/}
        </SafeAreaView>

        {/* Overlay */}
        <Overlay
          isVisible={this.state.isVisible}
          overlayStyle={[OverlayStyle.overlayStyle]}
          containerStyle={OverlayStyle.overlayContainer}
          width="92%"
          height="auto"
          onBackdropPress={() => this.setState({ isVisible: false })}
        >
          <View style={[OverlayStyle.overlayInner, OverlayStyle.bgSuccess]}>
            <Icon
              name='info'
              type='material'
              color='#fff'
              size={26}
            />
            <Text allowFontScaling={false} style={OverlayStyle.overlayText}>{this.state.responseInfo}</Text>
          </View>
        </Overlay>
      </LinearGradient>
    );
  }
}

export default Home
