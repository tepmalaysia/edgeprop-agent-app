import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
  navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    navIconRight: {
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },  
    profileWrapper: {
        flex: 1,
        width: '100%',
        padding: 15
    },
    profileStat: {
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 6,
        shadowOffset:{width: 2,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        marginBottom: 10,
    },
    profileTop: {
        padding: 15,
        borderBottomColor: '#c5c5c5',
        borderBottomWidth: 0.5
    },
    profileBottom: {
        padding: 15,
        paddingHorizontal: 30
    },
    profilePrimary: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    avatar: {
        borderRadius: 100,
        overflow: 'hidden'
    },
    profilePrimaryInfo: {
        flex: 1,
        paddingLeft: 15,
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    hTwo: {
        fontSize: width * 0.045,
        fontFamily: 'OpenSans-Bold',
        color: '#10182d',
        lineHeight: width * 0.045,
        marginVertical: 5
    },
    listWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    listImgWrapper: {
        width: 17
    },
    listItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15
    },
    profileSub: {
        width: '100%',
        flexDirection: 'row',
        padding: 15
    },
    statItem: {
        width: '50%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    statRight: {
        paddingLeft: 10
    },
    statHead: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Bold',
        color: '#3670e5'
    },
    statSub: {
        width: '100%',
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-SemiBold',
        color: '#10182d'
    },
    premiumPlan: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        color: '#ffb400'
    },
    expirePlan: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#a1adca',
        lineHeight: width * 0.04,
    },
    itemAnnouncement: {
        height: 90,
        flexDirection: 'column',
        alignItems: 'center',
        
    },
    listInnerItem: {
        backgroundColor: '#fff',
        borderRadius: 6,
        width: '100%',
        height: 70,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        marginBottom: 10,
        flex: 1,
    },
    listInnerItemActive: {
        backgroundColor: '#f3f5f7',
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',
        width: '100%',
    },
    leftIcon:{
        marginLeft: 12,
        marginRight: 15,
    },
    listText: {
        fontSize: width * 0.032,
    },
    textSmBold: {
        fontWeight: '700',
    },
    centerText: {
        paddingLeft: 5,
        paddingRight: 10,
        flex: 1,

    },
    rightText: {
        width: '20%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        height: '100%'
    },
    itemDate: {
        marginTop: 10,
        marginRight: 10,
        fontSize: width * 0.020,
        color: '#b5bbc7',
    },
    itemDateActive: {
        color: '#3670e5',
    },
    title: {
        fontSize: width * 0.042,
    },
    announcementHead: {
        borderBottomColor: '#e5dbdb',
        borderBottomWidth: 1,
        paddingLeft: 10,
        paddingRight: 15,
    },
});