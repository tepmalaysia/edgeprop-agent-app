/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView
} from 'react-native';
import { Icon } from 'react-native-elements'
import styles from './AnnouncementInnerStyles';
import { Avatar } from 'react-native-elements';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Contact from '../Contact/index';
import ForgotPassword from '../Forgot_Password/ForgotPassword';
import LoginLanding from '../Login_Landing/LoginLanding'
import HeaderSearch from '../../../components/headerSearch';
import ActiveListing from '../ListingTabs/ActiveListing';
import SavedListing from '../ListingTabs/SavedListing';
import ExpiredListing from '../ListingTabs/ExpiredListing';
import { Header, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { DrawerActions } from 'react-navigation-drawer';


let tabScreens = {
    Saved: {screen: SavedListing},
    Active: {screen: ActiveListing},
    Expired: {screen: ExpiredListing},
}

const Tabs = createMaterialTopTabNavigator(
  tabScreens,
  {
    tabBarPosition: 'top',
    swipeEnabled: false,
    initialRouteName: 'Saved', // set inital route
    lazy: true,
    tabBarOptions: {
      labelStyle: {
        fontSize: 12,
      }
    }
  }
)

const StackNavigator = createStackNavigator({
    
    //important: key and screen name (i.e. DrawerNavigator) should be same while using the drawer navigator inside stack navigator.
    
    Tabs:{
        screen: Tabs
    }
},{
  initialRouteName: 'Tabs', // set inital route
}
);

const TabNav = createAppContainer(Tabs);

class AnnouncementInner extends Component{

  constructor(props) {
    super(props);
    this.state = { spinAnim: new Animated.Value(0) }
    this._handleBackPress = this._handleBackPress.bind(this);
  }
  _handleBackPress() {
    this.props.navigation.goBack();
  }

  componentDidMount(){
    Animated.loop(Animated.timing(
       this.state.spinAnim,
     {
       toValue: 1,
       duration: 3000,
       easing: Easing.linear,
       useNativeDriver: true
     }
   )).start();
    }
  
  render() {
    const spin = this.state.spinAnim.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    });
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={this._handleBackPress}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#fff'
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>Announcements</Text>
        </View>
      )
    }


    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          {/* <HeaderSearch navigation={this.props.navigation} /> */}
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
          />
          <View style={styles.innerContainer}>
            <ScrollView>
              <View style={styles.itemAnnouncement}>
                <View style={styles.announcementHead}>
                  <View style={styles.itemContainer}>
                    <View style={styles.leftIcon}>
                      <Avatar
                        size="medium"
                        rounded
                        title="G"
                        titleStyle={styles.title}
                        overlayContainerStyle={{backgroundColor: '#343a56'}}
                      />
                    </View>
                    <View style={styles.centerText}>
                      <Text allowFontScaling={false} style={[styles.textSm,styles.textSmBold]} numberOfLines = {2}>
                      Now all the properties can be sell in the new platform Now all the properties can be sell in the new platform 
                      
                      </Text>
                    </View>
                    <View style={styles.rightText}>
                    <Text allowFontScaling={false} style={[styles.itemDate]}>01 OCT 2019</Text>
                    </View>
                    </View>
                </View>
              </View>
            </ScrollView>
          </View>

        </SafeAreaView>
      </LinearGradient>
    );
  }
}

AnnouncementInner.router = TabNav.router

export default AnnouncementInner