/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Dimensions
} from 'react-native';
import { TextField, FilledTextField, OutlinedTextField } from 'react-native-material-textfield';
import { CheckBox, Icon, Input } from 'react-native-elements';
import CustomSelect from '../../../components/CustomSelect';
import styles from './AdditionalInfoStyle';
import FurnishingOptions from '../../../assets/json/FurnishingOptions.json'
import BathroomOptions from '../../../assets/json/BathroomOptions.json'
import BedroomOptions from '../../../assets/json/BedroomOptions.json'
import TenureOptions from '../../../assets/json/TenureOptions.json'

const { width, height } = Dimensions.get('window');
class AdditionalInfo extends Component{

  constructor(props) {
    super(props);
    this.data = props;
    this.params = props.navigation.state.params.data? props.navigation.state.params.data : {};
    let listingInfo = this.data.listingDetails.data? JSON.parse(this.data.listingDetails.data) : {};
    this.state = {
      subPropertyLabel: 'Sub-Property Type',
      stateLabel: 'State',
      districtLabel: 'Area / District',
      unitLabel: 'Unit',
      tenureLabel: 'Tenure',
      furnishLabel: 'Furnished',
      bedRoomLabel: 'Bedrooms',
      bathRoomLabel: 'Bathrooms',
      showCustody: false,
      viaNav: this.params.viaNav? this.params.viaNav : false,
      property_bathrooms: listingInfo.field_prop_bathrooms? (listingInfo.field_prop_bathrooms.und? (listingInfo.field_prop_bathrooms.und[0] ? (listingInfo.field_prop_bathrooms.und[0].value? listingInfo.field_prop_bathrooms.und[0].value.toString() : '') : ''): '') : '',
      property_bedrooms: listingInfo.field_prop_bedrooms? (listingInfo.field_prop_bedrooms.und? (listingInfo.field_prop_bedrooms.und[0] ? (listingInfo.field_prop_bedrooms.und[0].value == 0? '0' : listingInfo.field_prop_bedrooms.und[0].value.toString() : '') : ''): '') : '',
      property_furnished: listingInfo.field_prop_furnished? (listingInfo.field_prop_furnished.und? (listingInfo.field_prop_furnished.und[0] ? (listingInfo.field_prop_furnished.und[0].value? listingInfo.field_prop_furnished.und[0].value : '') : ''): '') : '',
      property_info : listingInfo.field_prop_info?  (listingInfo.field_prop_info.und? (listingInfo.field_prop_info.und[0] ? listingInfo.field_prop_info.und[0].value : ''): '') : '',
      tenure: listingInfo.field_prop_lease_term? (listingInfo.field_prop_lease_term.und? (listingInfo.field_prop_lease_term.und[0] ? listingInfo.field_prop_lease_term.und[0].value : ''): '') : '',
      property_aminities: listingInfo.field_prop_additional_amenities? (listingInfo.field_prop_additional_amenities.und? listingInfo.field_prop_additional_amenities.und[0].value : '') : '',
      property_coAgency: listingInfo.field_prop_coagency? (listingInfo.field_prop_coagency.und? listingInfo.field_prop_coagency.und[0].value == 'true'? true : false : false) : false
    };
    
  }
  
  render() {

    let subProperty = [{
      label: 'Mansion House',
      value: true
    }, {
      label: 'Broken Villa',
      value: false
    }];

    let data = [{
      label: 'Value 1',
      value: true
    }, {
      label: 'Value 2',
      value: false
    }];

    return (
      <View style={styles.container} >
        <View style={styles.propContentWrapper}>

          {/*<View style={[styles.contentSection, {flexDirection: 'row', marginLeft: -5, marginRight: -5}]}>
            <View style={[styles.inputContainerHalf, {flexDirection: 'row'}]}>
              <Input 
                placeholder='Land Area' 
                inputContainerStyle={[styles.inputContainerMain, {flex: 1, width: 'auto'}]} 
                containerStyle={styles.inputContainer} 
                inputStyle={styles.inputField}
              />
              <CustomSelect
                value={!!this.state.showCustody}
                label={this.state.unitLabel}
                data={data}
                onChangeText={(showCustody) => this.setState({ showCustody })}
                style={styles.viewAllWrap}
                baseStyle={styles.viewAllBase}
                fieldWrapStyle={{ width: 'auto', marginLeft: 10, maxHeight: 40 }}
              />
            </View>
            <View style={styles.inputContainerHalf}>
              <Input 
                placeholder='Land Area Price' 
                inputContainerStyle={[styles.inputContainerMain]} 
                containerStyle={styles.inputContainer} 
                inputStyle={styles.inputField}
                leftIconContainerStyle={{ marginLeft: 10, marginRight:0 }}
                leftIcon={
                  <Text style={styles.textMd}>RM</Text>
                }
              />
            </View>
          </View>*/}

          {/*<View style={[styles.contentSection, styles.contentSectionNoBorder, {flexDirection: 'row', marginLeft: -5, marginRight: -5}]}>
            <View style={[styles.inputContainerHalf, {flexDirection: 'row'}]}>
              <Input 
                placeholder='Built Up' 
                inputContainerStyle={[styles.inputContainerMain, {flex: 1, width: 'auto'}]} 
                containerStyle={styles.inputContainer} 
                inputStyle={styles.inputField}
              />
              <CustomSelect
                value={!!this.state.showCustody}
                label={this.state.unitLabel}
                data={data}
                onChangeText={(showCustody) => this.setState({ showCustody })}
                style={styles.viewAllWrap}
                baseStyle={styles.viewAllBase}
                fieldWrapStyle={{ width: 'auto', marginLeft: 10, maxHeight: 40 }}
              />
            </View>
            <View style={styles.inputContainerHalf}>
              <Input 
                placeholder='Built Up Price' 
                inputContainerStyle={[styles.inputContainerMain]} 
                containerStyle={styles.inputContainer} 
                inputStyle={styles.inputField}
                leftIconContainerStyle={{ marginLeft: 10, marginRight:0 }}
                leftIcon={
                  <Text style={styles.textMd}>RM</Text>
                }
              />
            </View>
          </View>*/}

          {/*<View style={[styles.contentSection, styles.contentSectionNoBorder]}>
            <CustomSelect
              value={!!this.state.showCustody}
              label={this.state.tenureLabel}
              data={data}
              onChangeText={(showCustody) => this.setState({ showCustody })}
              style={styles.viewAllWrap}
              baseStyle={styles.viewAllBase}
              fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 40 }}
            />
          </View>

          <View style={[styles.contentSection]}>
            <View style={styles.CheckBoxWrapper}>
              <CheckBox
                title='Co-Agency'
                iconType='material'
                checkedIcon='check-box'
                uncheckedIcon='check-box-outline-blank'
                checkedColor='#3670e5'
                checked={true}
                containerStyle={styles.checkBox}
                textStyle={styles.textSm}
              />
            </View>
          </View>*/}
          <View style={[styles.contentSection]}>
            <Text allowFontScaling={false} style={styles.editHeading}>Additional Info</Text>
            <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay}>
                <CustomSelect
                  value={this.state.tenure}
                  label={this.state.tenureLabel}
                  placeholder='Please Select'
                  data={TenureOptions}
                  onChangeText={(tenure) => this.setState({ tenure }, () => this.data.setForm({ 'tenure' : tenure }))}
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 57 }}
                />
              </View>
              <View style={styles.fieldWrapTwoWay}>
                <CustomSelect
                  value={this.state.property_furnished}
                  label={this.state.furnishLabel}
                  placeholder='Please Select'
                  data={FurnishingOptions}
                  onChangeText={(furnished) => this.setState({ property_furnished:  furnished }, () => this.data.setForm({ 'property_furnished' : furnished }))}
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                />
              </View>
            </View>
          </View>
          <View style={[styles.contentSection, styles.contentSectionNoBorder]}>
            <Text allowFontScaling={false} style={[styles.commonLabel]}>Information</Text>
            <View style={[styles.fieldWrap]}>
              <Input 
                placeholder='Property Information' 
                inputContainerStyle={[styles.inputContainerMain]} 
                containerStyle={styles.inputContainer} 
                inputStyle={[styles.inputField, styles.inputLarge, {height: 150}]}
                multiline = {true}
                numberOfLines = {10}
                value={this.state.property_info}
                onChangeText={(info) => this.setState({'property_info' : info}, () => {
                    this.data.setForm({ 'property_info' : info })
                  })
                }
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}


export default AdditionalInfo