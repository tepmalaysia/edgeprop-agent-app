/**
 * Sample React Native ApplistingFormValidations
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Dimensions
} from 'react-native';
import { CheckBox, Icon, Input } from 'react-native-elements';
import { TextField, FilledTextField, OutlinedTextField } from 'react-native-material-textfield';
import moment from 'moment';
import styles from './BasicInfoStyle';
import CustomSelect from '../../../components/CustomSelect';
import { Images } from '../../../assets/theme'
import PropertyTypeOptions from '../../../assets/json/PropertyTypeOptions.json'
import StateOptions from '../../../assets/json/StateOptions.json'
import TenureOptions from '../../../assets/json/TenureOptions.json'
import { checkError } from '../../services/formValidation'
import { aucDay,aucMonth,aucYear } from '../../services/formOptions'
import BathroomOptions from '../../../assets/json/BathroomOptions.json'
import BedroomOptions from '../../../assets/json/BedroomOptions.json'
import PriceTypeOptions from '../../../assets/json/PriceType.json'

const { width, height } = Dimensions.get('window');
const arrSubPropTypeShowLandAreaOnly = [72, 69, 67, 43];
const arrSubPropTypeShowBuildUpOnly = [37,38,39,40,41,42,44,61, 62];
const arrSubPropTypeShowBoth = [63, 64, 65, 66, 68, 71, 73];
class BasicInfo extends Component{
  fieldRefPostal = fieldRefStreet = fieldRefBuilding = fieldRefBuPu = fieldRefLaPu = fieldRefOthersAsset = React.createRef();
  constructor(props) {
    super(props);
    this.data = props;
    let listingDetails = this.data.listingDetails;
    let listingInfo = this.data.listingDetails.data? JSON.parse(this.data.listingDetails.data) : {};
    let status = this.data.status;
    this.subProperties = [];
    this.districts = [];
    this.units = [
      {label: 'sqft',value: 'sqft'},
      {label: 'sqm',value: 'sqm'},
      {label: 'acre',value: 'acre'}
    ];
    this.landAreaPerUnit = '';
    let values = {
      listing_type: listingDetails.listing_type,
      property_type: listingDetails.property_type,
      sub_property_type: listingInfo.field_property_type? (listingInfo.field_property_type.und? (listingInfo.field_property_type.und[1] ? listingInfo.field_property_type.und[1].target_id : ''): '') : '',
      property_name: listingDetails.title,
      property_state: listingInfo.field_state? (listingInfo.field_state.und? (listingInfo.field_state.und[0] ? listingInfo.field_state.und[0].target_id : ''): '') : '',
      property_district: listingInfo.field_district? (listingInfo.field_district.und? (listingInfo.field_district.und[0] ? (listingInfo.field_district.und[0].target_id? listingInfo.field_district.und[0].target_id : '') : ''): '') : '',
      property_street: listingInfo.field_prop_street? (listingInfo.field_prop_street.und? (listingInfo.field_prop_street.und[0] ? listingInfo.field_prop_street.und[0].value : ''): '') : '',
      property_unit: listingInfo.field_prop_unit? (listingInfo.field_prop_unit.und? (listingInfo.field_prop_unit.und[0] ? listingInfo.field_prop_unit.und[0].value : ''): '') : '',
      property_postcode: listingInfo.field_prop_postcode? (listingInfo.field_prop_postcode.und? (listingInfo.field_prop_postcode.und[0] ? listingInfo.field_prop_postcode.und[0].value : ''): '') : '',
      property_price_type: listingInfo.field_prop_asking_price_type? (listingInfo.field_prop_asking_price_type.und? (listingInfo.field_prop_asking_price_type.und[0] ? listingInfo.field_prop_asking_price_type.und[0].value : ''): '') : '',
      property_asking_price: listingInfo.field_prop_asking_price? (listingInfo.field_prop_asking_price.und? (listingInfo.field_prop_asking_price.und[0] ? (listingInfo.field_prop_asking_price.und[0].value? listingInfo.field_prop_asking_price.und[0].value.toString() : '') : ''): '') : '',
      property_land_area: listingInfo.field_prop_land_area? (listingInfo.field_prop_land_area.und? (listingInfo.field_prop_land_area.und[0] ? (listingInfo.field_prop_land_area.und[0].value? listingInfo.field_prop_land_area.und[0].value.toString() : '') : ''): '') : '',
      property_land_area_unit: listingInfo.field_prop_area_unit? (listingInfo.field_prop_area_unit.und? (listingInfo.field_prop_area_unit.und[0] ? listingInfo.field_prop_area_unit.und[0].value : ''): '') : '',
      property_images : listingInfo.field_prop_images? (listingInfo.field_prop_images.und? listingInfo.field_prop_images.und : [])  : [],
      landAreaPerUnit: '',
      property_built_area: listingInfo.field_prop_built_up? (listingInfo.field_prop_built_up.und? (listingInfo.field_prop_built_up.und[0] ? (listingInfo.field_prop_built_up.und[0].value? listingInfo.field_prop_built_up.und[0].value.toString() : '') : ''): '') : '',
      property_built_area_unit: listingInfo.field_prop_built_up_unit? (listingInfo.field_prop_built_up_unit.und? (listingInfo.field_prop_built_up_unit.und[0] ? listingInfo.field_prop_built_up_unit.und[0].value : ''): '') : '',
      builtAreaPerUnit: '',
      property_coAgency: listingInfo.field_prop_coagency? (listingInfo.field_prop_coagency.und? (listingInfo.field_prop_coagency.und[0] ? listingInfo.field_prop_coagency.und[0].value : false): false) : false,
      tenure: listingInfo.field_prop_lease_term? (listingInfo.field_prop_lease_term.und? (listingInfo.field_prop_lease_term.und[0] ? listingInfo.field_prop_lease_term.und[0].value : ''): '') : '',
      property_bathrooms: listingInfo.field_prop_bathrooms? (listingInfo.field_prop_bathrooms.und? (listingInfo.field_prop_bathrooms.und[0] ? (listingInfo.field_prop_bathrooms.und[0].value? listingInfo.field_prop_bathrooms.und[0].value.toString() : '') : ''): '') : '',
      property_bedrooms: listingInfo.field_prop_bedrooms? (listingInfo.field_prop_bedrooms.und? (listingInfo.field_prop_bedrooms.und[0] ? (listingInfo.field_prop_bedrooms.und[0].value == 0? '0' : listingInfo.field_prop_bedrooms.und[0].value.toString() : '') : ''): '') : '',
      property_furnished: listingInfo.field_prop_furnished? (listingInfo.field_prop_furnished.und? (listingInfo.field_prop_furnished.und[0] ? (listingInfo.field_prop_furnished.und[0].value? listingInfo.field_prop_furnished.und[0].value : '') : ''): '') : '',
      property_info : listingInfo.field_prop_info?  (listingInfo.field_prop_info.und? (listingInfo.field_prop_info.und[0] ? listingInfo.field_prop_info.und[0].value : ''): '') : '',
      buildingName : listingInfo.asset? listingInfo.asset.name? (listingInfo.asset.name : '') : '' : '',
      field_prop_asset: listingInfo.field_prop_asset? listingInfo.field_prop_asset.und? listingInfo.field_prop_asset.und[0]? listingInfo.field_prop_asset.und[0].target_id? listingInfo.field_prop_asset.und[0].target_id : '' : '' : '' : '',
      searchOthers : listingInfo.field_prop_asset ? ((listingInfo.field_prop_asset.und)? (listingInfo.field_prop_asset.und[0]? (listingInfo.field_prop_asset.und[0].target_id == '1100'? true : false) : false) : false) : false,
      othersAsset: listingInfo.field_others_asset ? ((listingInfo.field_others_asset.und)? (listingInfo.field_others_asset.und[0]? (listingInfo.field_others_asset.und[0].value? listingInfo.field_others_asset.und[0].value : '') : '') : '') : '',
      property_auction_day: moment().format('DD'),
      property_auction_month: moment().format('MM'),
      property_auction_year: parseInt(moment().format('YYYY')),
      property_auction_ref: listingInfo.field_prop_auction_ref ? ((listingInfo.field_prop_auction_ref.und)? (listingInfo.field_prop_auction_ref.und[0]? listingInfo.field_prop_auction_ref.und[0].value : '') : '') : '',
      editState: true,
      editDistrict: true,
      editPostcode: true,
      exclusive: listingInfo.field_exclusive ? ((listingInfo.field_exclusive.und)? (listingInfo.field_exclusive.und[0]? ((listingInfo.field_exclusive.und[0].value == "1")? true : false) : false) :false) : false,
    }
    if(values.field_prop_asset && !values.searchOthers){
      if(values.property_state 
        && values.property_district 
        && (values.property_type || values.sub_property_type)){
          let asset = listingInfo.asset;
          if(asset.state_id && asset.district_id){
            values.editState = false;
            values.editDistrict = false
            if(asset.postal_code){
                values.editPostcode = false;
            }
          }
      }
    }

    let auctionDate = (listingInfo.field_prop_auction_date)? ((listingInfo.field_prop_auction_date.und)? (listingInfo.field_prop_auction_date.und[0]? listingInfo.field_prop_auction_date.und[0].value : '') : '') : '';
    if(auctionDate !=null){
      let dateCol = auctionDate.split(' ');
      let dateArray = dateCol[0].split('-');
      if(dateArray.length == 3){
        values.property_auction_day = ("0" + Number(dateArray[2])).slice(-2); 
        values.property_auction_month = ("0" + Number(dateArray[1])).slice(-2); 
        values.property_auction_year = dateArray[0]? parseInt(dateArray[0]) : '';
      }
    }

    if(values.field_prop_asset == '1100'){
      values.buildingName = 'Others' 
    }

    if(values.property_asking_price){
     let askingPrice = parseInt(values.property_asking_price);
     if(values.property_land_area && values.property_land_area_unit){
        let landArea = parseInt(values.property_land_area);
        values.landAreaPerUnit = (askingPrice/landArea).toFixed(2) + ' / '+values.property_land_area_unit;
      }
      if(values.property_built_area && values.property_built_area_unit){
        let landArea = parseInt(values.property_built_area);
        values.builtAreaPerUnit = (askingPrice/landArea).toFixed(2) + ' / '+values.property_built_area_unit;
      }
    }

    this.state = {
      subPropertyLabel: 'Sub-Property Type',
      stateLabel: 'State',
      districtLabel: 'Area / District',
      unitLabel: 'Unit',
      tenureLabel: 'Tenure',
      bedRoomLabel: 'Bedrooms',
      bathRoomLabel: 'Bathrooms',
      showCustody: false,
      //property_bathrooms: listingInfo.field_prop_bathrooms? (listingInfo.field_prop_bathrooms.und? (listingInfo.field_prop_bathrooms.und[0] ? (listingInfo.field_prop_bathrooms.und[0].value? listingInfo.field_prop_bathrooms.und[0].value.toString() : '') : ''): '') : '',
      //property_bedrooms: listingInfo.field_prop_bedrooms? (listingInfo.field_prop_bedrooms.und? (listingInfo.field_prop_bedrooms.und[0] ? (listingInfo.field_prop_bedrooms.und[0].value == 0? '0' : listingInfo.field_prop_bedrooms.und[0].value.toString() : '') : ''): '') : '',
      ...values,
      validate: this.data.validate//{flag: false, form: {}}
    };
    this._onSearchTag = this._onSearchTag.bind(this)
    this._setForm = this._setForm.bind(this)
    this._changePropertyType = this._changePropertyType.bind(this)
    this._resetSubProperty = this._resetSubProperty.bind(this)
    this._changePropertyState = this._changePropertyState.bind(this)
    this._resetDistrict = this._resetDistrict.bind(this)
    this._radioButtonCheck = this._radioButtonCheck.bind(this)
    this._calcPerUnit = this._calcPerUnit.bind(this)
    this._setAsset = this._setAsset.bind(this)
    this.fieldRefExtractor = this.fieldRefExtractor.bind(this)
    this.scrollToItem = this.scrollToItem.bind(this)
  }

  componentDidMount() {
    this._changePropertyType(this.state.property_type, false);
    this._changePropertyState(this.state.property_state, false);
  }

  shouldComponentUpdate(nextProps, nextState) {
     if(JSON.stringify(nextProps.validate) != JSON.stringify(this.props.validate)){
       this.setState({validate: nextProps.validate})
       if(nextProps.validate.scrollToNode){
          let scrollToNode = this[nextProps.validate.scrollToNode];
          this.scrollToItem(scrollToNode);
       }
     }
     return true;
  }

  fieldRefExtractor = (refKey) => (ref) => {
    if (refKey) {
      this[refKey] = ref;
    }
  }

  scrollToItem = (node) => {
    const { onScrollToNode } = this.props;
    if (onScrollToNode && node) {
      onScrollToNode(node);
    }
  }

  _setAsset(data){
    
    let autoComplete = {
      asset: data.asset? data.asset: {},
      lat: data.lat? data.lat: null,
      lng: data.lng? data.lat: null,
      property_state: data.state? data.state.toString() : '',
      property_district: data.area? data.area.toString() : '',
      property_street: data.street? data.street : '',
      property_postcode: data.postal? data.postal.toString(): '',
      searchOthers: data.searchOthers? data.searchOthers : false,
      pid: data.pid? data.pid : null,
      attrCheck: data.attrCheck? data.attrCheck : false,
      buildingName: data.buildingName? data.buildingName : ''
    }
    if(data.hasOwnProperty('othersAsset')){
      autoComplete.othersAsset = data.othersAsset;
    }
    this.setState({
      ...autoComplete
    }, () => {
      let disableKeys = {
        editState: true,
        editDistrict: true,
        editPostcode: true
      }
      if(this.state.property_state 
        && this.state.property_district 
        && (this.state.property_type || this.state.sub_property_type)){
          disableKeys.editState = false;
          disableKeys.editDistrict = false
          if(this.state.property_postcode){
              disableKeys.editPostcode = false;
          }
      }
      this._changePropertyState(this.state.property_state, false)
      this.fieldRefPostal.setValue(this.state.property_postcode)
      this.fieldRefStreet.setValue(this.state.property_street)
      this.fieldRefBuilding.setValue(this.state.buildingName)
      this._setForm('buildingName', this.state.buildingName);
      if(this.state.searchOthers){
        this.fieldRefOthersAsset.setValue(this.state.othersAsset)
      }
      this.data.setForm(autoComplete);
      this.setState({
        ...disableKeys
      });
    });
    
  }

  _onSearchTag() {
    if(this.state.property_type){
      this.props.navigation.navigate('SearchTag', {
        data:{
          propertyType: this.state.property_type,
          selectHandler: this._setAsset
        }
      });
    }else{
      if(this.data.errorHandler){
        this.data.errorHandler({ error : true, message: 'Please select Property type to search!' })
      }
    }
  }

  _changePropertyType(value, flag) {
    if(this.state.property_type != value || !!!flag){
      let subTypeLabel =this._getLabel(this._prefixType(value),PropertyTypeOptions);
      this._setForm('property_type',value, this._resetSubProperty(subTypeLabel, flag))
      if(flag){
        this._setAsset({});
      }
    }
  }

  _changePropertyState(value, flag) {
    if(this.state.property_state != value || !!!flag){
      let districts =this._getLabel(value,StateOptions);
      this._setForm('property_state',value, this._resetDistrict(districts, flag))
    }
  }

  _resetSubProperty(data, flag){
    if(flag){
      this._setForm('sub_property_type','')
    }
    this.subProperties = data;
  }

  _resetDistrict(data, flag){
    if(flag){
      this._setForm('property_district','')
    }
    this.districts = data;
  }

  _prefixType(id){
      var prefix = '';
      if(id == '33'){
          prefix = 'r-';
      }else if(id == '36'){
          prefix = 'l-';
      }else if(id == '60'){
          prefix = 'c-';
      }else if(id == '70'){
          prefix = 'i-';
      }
      return prefix+id;
  }

  _getLabel(key,collection){
        let res ='';
        if(key && collection.length > 0){
            res = collection.filter(value => value.id == key)
        }
        res = res[0]? res[0].drop : '';
        return res;
    }

  _setForm(key,value, callback) {
    this.setState({
      [key] : value
    },() => {
      this.data.setForm({ [key] : value});
      if(callback){
        callback
      }
      let validate = this.state.validate;
      if(validate.form[key]){
        let parentValidate = this.props.checkForm(this.state, this.props.action);
        this.setState({validate : parentValidate});
      }
    })
  }

  _radioButtonCheck(key, value, flag) {
    let type = 'radio-button-unchecked';
    let selected = '#cccccc';
    if(this.state[key] == value){
      type = 'radio-button-checked';
      selected = '#3670e5';
    }
    return flag? type : selected;
  }
  
  _calcPerUnit(){
    let landAreaPerUnit = '';
    let builtAreaPerUnit = '';
    if(this.state.property_asking_price){
     let askingPrice = parseInt(this.state.property_asking_price);
     if(this.state.property_land_area && this.state.property_land_area_unit){
        let landArea = parseInt(this.state.property_land_area);
        landAreaPerUnit = (askingPrice/landArea).toFixed(2) + ' / '+this.state.property_land_area_unit;
      }
      if(this.state.property_built_area && this.state.property_built_area_unit){
        let landArea = parseInt(this.state.property_built_area);
        builtAreaPerUnit = (askingPrice/landArea).toFixed(2) + ' / '+this.state.property_built_area_unit;
      }
    }
    this.setState({landAreaPerUnit, builtAreaPerUnit})
    if(this.fieldRefLaPu)
      this.fieldRefLaPu.setValue(landAreaPerUnit);
    if(this.fieldRefBuPu)
      this.fieldRefBuPu.setValue(builtAreaPerUnit)
    this.data.setForm({ 'landAreaPerUnit' : landAreaPerUnit, 'builtAreaPerUnit' : builtAreaPerUnit});

  }

  dateValidate() {
    let day = this.state.property_auction_day;
    let month = this.state.property_auction_month;
    let year = this.state.property_auction_year;
   
    let yesterday = moment().add(-1, 'days').format("YYYY-MM-DD");
    if(moment(day+'/'+month+'/'+year, 'DD/MM/YYYY').isValid())
    {
      if(!moment(year+'-'+month+'-'+day).isAfter(yesterday))
      {
        //date invalid
        this.setState({
          dateInValid: true
        });
      }else{
        //date is valid
        this.setState({
          dateInValid: false
        });
      }
    }else{
      //date invalid
      this.UPDATE_LISTING_FORM({
        dateInValid: true
      });
    }
  }

  render() {

    _fetchStyle = (key,value,type,property) =>{
        let design = {
          'container': {
            'selected': [styles.propSearchItem, styles.propSearchItemSelected],
            'unselected':[styles.propSearchItem]
          },
          'text':{
            'selected': [styles.textSm, styles.propSearchItemText, {color: '#fff'}],
            'unselected':[styles.textSm, styles.propSearchItemText]
          }
        };
        
        let item;
        switch(key){
          case 'style':
              if(this.state[type] == value){
                item = design[property].selected;
              }else{
                item = design[property].unselected;
              }
            break;
          default:
            item = '';
        }
        return item;
    }

    showLanded = () => {
      if (this.state.property_type == "33") return false;
      else if (this.state.property_type == "36") return true;
      else if (this.state.property_type == "60") return true;
      else if (this.state.property_type == "70") return true;
      else {
        let v = parseInt(this.state.sub_property_type);
        if (
        arrSubPropTypeShowLandAreaOnly.find(value => value == v) ||
        arrSubPropTypeShowBoth.find(value => value == v)
        )
        return true;
      }
      return false;
    }

    showBuildUp = () => {
      if (this.state.property_type == "33") return true;
      //else if (state.propertyType == "36") return true;
      else {
        let v = parseInt(this.state.sub_property_type);
        if (
        arrSubPropTypeShowBuildUpOnly.find(value => value == v) ||
        arrSubPropTypeShowBoth.find(value => value == v)
        )
          return true;
      }
      return false;
    }
      
    return (
      <View style={styles.container}>
        <View style={styles.propContentWrapper}>
          <View style={[styles.contentSection,styles.contentSectionNoBorder,{paddingRight: 5},{paddingTop: 15}]}>
            <Text allowFontScaling={false} style={styles.editHeading}>Basic Info</Text>
            <View style={styles.CheckBoxWrapper}>
              <CheckBox
                title='Exclusive Listing (This listing is available only in EdgeProp)'
                iconType='material'
                checkedIcon='check-box'
                uncheckedIcon='check-box-outline-blank'
                checkedColor='#3670e5'
                checked={this.state.exclusive}
                containerStyle={styles.checkBox}
                textStyle={[styles.textSm, { color: '#20a8d8', fontFamily: 'OpenSans-SemiBold'}]}
                onPress={() => this._setForm('exclusive',!this.state.exclusive)}
              />
            </View>
            <Text allowFontScaling={false} style={[styles.commonLabel, {paddingTop: 15}]}>Listing Type</Text>
            <View style={styles.propSearchSection} ref={this.fieldRefExtractor('ref_listing_type')}>
              <View style={[styles.propSearchItemWrapper]}>
                <TouchableOpacity style={_fetchStyle('style', 'sale','listing_type','container')} onPress={() => this._setForm('listing_type','sale')}>
                  <Image
                    style={{width: 26, height: 26, marginBottom: 5}}
                    source={this.state.listing_type === 'sale' ? Images.saleAlt : Images.sale}
                  />
                  <Text allowFontScaling={false} style={_fetchStyle('style', 'sale','listing_type','text')}>For Sale</Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.propSearchItemWrapper]}>
                <TouchableOpacity style={_fetchStyle('style', 'rental','listing_type','container')} onPress={() => this._setForm('listing_type','rental')}>
                  <Image
                    style={{width: 26, height: 26, marginBottom: 5}}
                    source={this.state.listing_type === 'rental' ? Images.rentAlt : Images.rent}
                  />
                  <Text allowFontScaling={false} style={_fetchStyle('style', 'rental','listing_type','text')}>For Rent</Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.propSearchItemWrapper]}>
                <TouchableOpacity style={_fetchStyle('style', 'auction','listing_type','container')} onPress={() => this._setForm('listing_type','auction')}>
                  <Image
                    style={{width: 26, height: 26, marginBottom: 5}}
                    source={this.state.listing_type === 'auction' ? Images.bidAlt : Images.bid}
                  />
                  <Text allowFontScaling={false} style={_fetchStyle('style', 'auction','listing_type','text')}>For Auction</Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.propSearchItemWrapper]}>
                <TouchableOpacity style={_fetchStyle('style', 'room_rental','listing_type','container')} onPress={() => this._setForm('listing_type','room_rental')}>
                  <Image
                    style={{width: 26, height: 26, marginBottom: 5}}
                    source={this.state.listing_type === 'room_rental' ? Images.roomAlt : Images.room}
                  />
                  <Text allowFontScaling={false} style={_fetchStyle('style', 'room_rental','listing_type','text')}>For Room Rental</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={[styles.contentSection, styles.contentSectionNoBorder, {paddingRight: 5}]}>
            <Text allowFontScaling={false} style={styles.commonLabel}>Property Type</Text>
            <View style={styles.propSearchSection} ref={this.fieldRefExtractor('ref_property_type')}>
              <View style={styles.propSearchItemWrapper}>
                <TouchableOpacity style={_fetchStyle('style', '33','property_type','container')} onPress={() => this._changePropertyType('33',true)}>
                  <Text allowFontScaling={false} style={_fetchStyle('style', '33','property_type','text')}>Non-Landed</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.propSearchItemWrapper}>
                <TouchableOpacity style={_fetchStyle('style', '36','property_type','container')} onPress={() => this._changePropertyType('36',true)}>
                  <Text allowFontScaling={false} style={_fetchStyle('style', '36','property_type','text')}>Landed</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.propSearchItemWrapper}>
                <TouchableOpacity style={_fetchStyle('style', '60','property_type','container')} onPress={() => this._changePropertyType('60',true)}>
                  <Text allowFontScaling={false} style={_fetchStyle('style', '60','property_type','text')}>Commercial</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.propSearchItemWrapper}>
                <TouchableOpacity style={_fetchStyle('style', '70','property_type','container')} onPress={() => this._changePropertyType('70',true)}>
                  <Text allowFontScaling={false} style={_fetchStyle('style', '70','property_type','text')}>Industrial</Text>
                </TouchableOpacity>
              </View> 
            </View>
          </View>

          <View style={[styles.contentSection, styles.contentSectionNoBorder]}>
            <View ref={this.fieldRefExtractor('ref_sub_property_type')}>
              <CustomSelect
                value={this.state.sub_property_type}
                label={this.state.subPropertyLabel}
                placeholder={this.state.property_type? 'Please select' : 'Please select Property Type first'}
                data={this.subProperties}
                onChangeText={(sub) => this.setState({ sub_property_type :  sub }, () => this._setForm( 'sub_property_type' , sub ))}
                style={styles.viewAllWrap}
                baseStyle={styles.viewAllBase}
                fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                error={checkError('sub_property_type', this.state.validate.form)}
              />
            </View>
            <View style={styles.fieldWrap} ref={this.fieldRefExtractor('ref_property_name')}>
              <TextField
                allowFontScaling={false}
                label='Advertisement Heading / Name '
                containerStyle={{width: '100%'}}
                style={styles.inputField}
                inputContainerStyle={styles.textFieldContainer}
                labelFontSize={width * 0.028}
                fontSize={width * 0.028}
                activeLineWidth={1}
                maxLength={50}
                tintColor='#3670e5'
                value={this.state.property_name}
                onChangeText={(name) => this._setForm('property_name',name)}
                error={checkError('property_name', this.state.validate.form)}
              />
            </View>
            
          </View>

          <View style={[styles.contentSection]}>
          <Text allowFontScaling={false} style={styles.editHeading}>property details</Text>
            <TouchableOpacity style={[styles.fieldWrap, styles.fieldWrapMulti]} onPress={this._onSearchTag}>
              <View style={[styles.fieldWrapTwoWay, {width: 'auto', paddingBottom: 15}]}>
                <Icon
                  name='search'
                  size={22}
                  type='feather'
                  color='#cccccc'
                />
              </View>
              <View style={[styles.fieldWrapTwoWay, {width: 'auto', flex: 1}]} ref={this.fieldRefExtractor('ref_buildingName')}>
                <TextField
                  allowFontScaling={false}
                  label='Project Name/Address'
                  containerStyle={{width: '100%'}}
                  style={[styles.inputField, {marginTop: -3}]}
                  inputContainerStyle={[styles.textFieldContainer, {height: 'auto'}]}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  editable={false}
                  multiline={true}
                  value={this.state.buildingName}
                  ref={component => this.fieldRefBuilding = component}
                  error={checkError('autoComplete', this.state.validate.form)}
                />
              </View>
            </TouchableOpacity>
            {this.state.searchOthers && <View style={styles.fieldWrap} ref={this.fieldRefExtractor('ref_othersAsset')}>
              <TextField
                allowFontScaling={false}
                label='Other Project Name/ Address '
                containerStyle={{width: '100%'}}
                style={styles.inputField}
                inputContainerStyle={styles.textFieldContainer}
                labelFontSize={width * 0.028}
                fontSize={width * 0.028}
                activeLineWidth={1}
                tintColor='#3670e5'
                value={this.state.othersAsset}
                onChangeText={(other) => this._setForm('othersAsset',other)}
                error={checkError('othersAsset', this.state.validate.form)}
                ref={component => this.fieldRefOthersAsset = component}
              />
              <View style={{width: '100%'}}>
                <Text style={[styles.textSm, {color: '#b0bbbf'}]} allowFontScaling={false}>Only secondary listings are allowed on our platform</Text>
              </View>
            </View>}
            <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_state')}>
                <CustomSelect
                  value={this.state.property_state}
                  label={this.state.stateLabel}
                  placeholder='Please Select'
                  data={StateOptions}
                  onChangeText={(state) => this._changePropertyState(state,true)}
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  disabled={!!!this.state.editState}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_state', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_district')}>
                <CustomSelect
                  value={this.state.property_district}
                  label={this.state.districtLabel}
                  placeholder='Please Select'
                  data={this.districts}
                  onChangeText={(district) => this.setState({ property_district :  district }, () => this._setForm( 'property_district' , district ))}
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  disabled={!!!this.state.editDistrict}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_district', this.state.validate.form)}
                />
              </View>
            </View>  
            <View style={styles.fieldWrap} ref={this.fieldRefExtractor('ref_property_street')}>
              <TextField
                allowFontScaling={false}
                label='Street'
                containerStyle={{width: '100%'}}
                style={styles.inputField}
                inputContainerStyle={styles.textFieldContainer}
                labelFontSize={width * 0.028}
                fontSize={width * 0.028}
                activeLineWidth={1}
                tintColor='#3670e5'
                value={this.state.property_street}
                onChangeText={(street) => this._setForm('property_street',street)}
                error={checkError('property_street', this.state.validate.form)}
                ref={component => this.fieldRefStreet = component}
              />
            </View>
            <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_unit')}>
                <TextField
                  allowFontScaling={false}
                  label='Unit No.'
                  containerStyle={{width: '100%'}}
                  style={styles.inputField}
                  inputContainerStyle={styles.textFieldContainer}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  value={this.state.property_unit}
                  onChangeText={(unit) => this._setForm('property_unit',unit)}
                  keyboardType={'number-pad'}
                  returnKeyType={'done'}
                  error={checkError('property_unit', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_postcode')}>
                <TextField
                  allowFontScaling={false}
                  label='Postal Code'
                  containerStyle={{width: '100%'}}
                  style={styles.inputField}
                  inputContainerStyle={styles.textFieldContainer}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  value={this.state.property_postcode.toString()}
                  onChangeText={(postcode) => this._setForm('property_postcode',postcode)}
                  keyboardType={'number-pad'}
                  returnKeyType={'done'}
                  editable={this.state.editPostcode}
                  error={checkError('property_postcode', this.state.validate.form)}
                  ref={component => this.fieldRefPostal = component}
                />
              </View>
              <View style={styles.fieldWrapOneWay}>
                <Text style={[styles.textSm, {color: '#b0bbbf'}]} allowFontScaling={false}>Unit No. will not be shown to the public</Text>
              </View>
            </View>
          </View>
          <View style={[styles.contentSection, styles.contentSectionNoBorder]}>
            <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_asking_price')}>
                <TextField
                  allowFontScaling={false}
                  label={this.state.listing_type == 'auction'? 'Reserved Price' : 'Asking Price'}
                  prefix='RM'
                  containerStyle={{width: '100%'}}
                  style={styles.inputField}
                  inputContainerStyle={styles.textFieldContainer}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  keyboardType={'decimal-pad'}
                  value={this.state.property_asking_price}
                  onChangeText={(price) => this.setState({'property_asking_price' : price}, () => {
                      this._calcPerUnit()
                      this._setForm( 'property_asking_price' , price )
                    })
                  }
                  returnKeyType={'done'}
                  error={checkError('property_asking_price', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_price_type')}>
                <CustomSelect
                  value={this.state.property_price_type}
                  label={'Asking Price Type'}
                  placeholder='Please Select'
                  data={PriceTypeOptions}
                  onChangeText={(priceType) => 
                    this.setState({'property_price_type' : priceType}, () => {
                        this._setForm( 'property_price_type' , priceType )
                      })
                    }
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_price_type', this.state.validate.form)}
                />
              </View>
            </View>
            {this.state.listing_type == 'auction' && <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_auction_day')}>
                <CustomSelect
                  value={this.state.property_auction_day}
                  label={'Auction Day'}
                  placeholder='Please Select'
                  data={aucDay}
                  onChangeText={(auctionDay) => 
                    this.setState({'property_auction_day' : auctionDay}, () => {
                        this._setForm( 'property_auction_day' , auctionDay )
                      })
                    }
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_auction_day', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_auction_month')}>
                <CustomSelect
                  value={this.state.property_auction_month}
                  label={'Auction Month'}
                  placeholder='Please Select'
                  data={aucMonth}
                  onChangeText={(auctionDay) => 
                    this.setState({'property_auction_month' : auctionDay}, () => {
                        this._setForm( 'property_auction_month' , auctionDay )
                      })
                    }
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_auction_month', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_auction_year')}>
                <CustomSelect
                  value={this.state.property_auction_year}
                  label={'Auction year'}
                  placeholder='Please Select'
                  data={aucYear}
                  onChangeText={(auctionYear) => 
                    this.setState({'property_auction_year' : auctionYear}, () => {
                        this._setForm( 'property_auction_year' , auctionYear )
                      })
                    }
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_auction_year', this.state.validate.form)}
                />
              </View>

              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_auction_ref')}>
                <TextField
                  allowFontScaling={false}
                  label='Auction Ref No.'
                  containerStyle={{width: '100%'}}
                  style={styles.inputField}
                  inputContainerStyle={styles.textFieldContainer}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  onChangeText={(auctionRef) => 
                  this.setState({'property_auction_ref' : auctionRef}, () => {
                      this._setForm( 'property_auction_ref' , auctionRef )
                    })
                  }
                  value={this.state.property_auction_ref}
                  error={checkError('property_auction_ref', this.state.validate.form)}
                />
              </View>
            </View>}
            {showLanded() && <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_land_area')}>
                <TextField
                  allowFontScaling={false}
                  label='Land Area'
                  containerStyle={{width: '100%'}}
                  style={styles.inputField}
                  inputContainerStyle={styles.textFieldContainer}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  value={this.state.property_land_area}
                  onChangeText={(landArea) => this.setState({'property_land_area' : landArea}, () => {
                      this._calcPerUnit()
                      this._setForm( 'property_land_area' , landArea )
                    })
                  }
                  keyboardType={'number-pad'}
                  returnKeyType={'done'}
                  maxLength={11}
                  error={checkError('property_land_area', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_land_area_unit')}>
                <CustomSelect
                  value={this.state.property_land_area_unit}
                  label={this.state.unitLabel}
                  placeholder='Please Select'
                  data={this.units}
                  onChangeText={(landAreaUnit) => 
                    this.setState({'property_land_area_unit' : landAreaUnit}, () => {
                        this._calcPerUnit()
                        this._setForm( 'property_land_area_unit' , landAreaUnit )
                      })
                    }
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_land_area_unit', this.state.validate.form)}
                />
              </View>
            </View>}
            {showLanded() && <View style={styles.fieldWrap}>
              <TextField
                allowFontScaling={false}
                label='Land Area Price'
                prefix='RM'
                containerStyle={{width: '100%'}}
                style={styles.inputField}
                inputContainerStyle={styles.textFieldContainer}
                labelFontSize={width * 0.028}
                fontSize={width * 0.028}
                activeLineWidth={1}
                tintColor='#3670e5'
                value={this.state.landAreaPerUnit}
                editable={false}
                error={checkError('landAreaPerUnit', this.state.validate.form)}
                ref={component => this.fieldRefLaPu = component}
              />
            </View>}
            {showBuildUp() && <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_built_area')}>
                <TextField
                  allowFontScaling={false}
                  label='Built Up'
                  containerStyle={{width: '100%'}}
                  style={styles.inputField}
                  inputContainerStyle={styles.textFieldContainer}
                  labelFontSize={width * 0.028}
                  fontSize={width * 0.028}
                  activeLineWidth={1}
                  tintColor='#3670e5'
                  value={this.state.property_built_area}
                  onChangeText={(builtArea) => this.setState({'property_built_area' : builtArea}, () => {
                      this._calcPerUnit()
                      this._setForm( 'property_built_area' , builtArea )
                    })
                  }
                  keyboardType={'number-pad'}
                  returnKeyType={'done'}
                  maxLength={11}
                  error={checkError('property_built_area', this.state.validate.form)}
                />
              </View>
              <View style={styles.fieldWrapTwoWay} ref={this.fieldRefExtractor('ref_property_built_area_unit')}>
                <CustomSelect
                  value={this.state.property_built_area_unit}
                  label={this.state.unitLabel}
                  placeholder='Please Select'
                  data={this.units}
                  onChangeText={(builtAreaUnit) => 
                    this.setState({'property_built_area_unit' : builtAreaUnit}, () => {
                      this._calcPerUnit()
                      this._setForm( 'property_built_area_unit' , builtAreaUnit )
                    })
                  }
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                  error={checkError('property_built_area_unit', this.state.validate.form)}
                />
              </View>
            </View>}
            {showBuildUp() &&<View style={styles.fieldWrap}>
              <TextField
                allowFontScaling={false}
                label='Built Up Price'
                prefix='RM'
                containerStyle={{width: '100%'}}
                style={styles.inputField}
                inputContainerStyle={styles.textFieldContainer}
                labelFontSize={width * 0.028}
                fontSize={width * 0.028}
                activeLineWidth={1}
                tintColor='#3670e5'
                value={this.state.builtAreaPerUnit}
                editable={false}
                error={checkError('builtAreaPerUnit', this.state.validate.form)}
                ref={component => this.fieldRefBuPu = component}
              />
            </View>}
            <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
               <View style={styles.fieldWrapTwoWay}>
                <CustomSelect
                  value={this.state.property_bedrooms}
                  label={this.state.bedRoomLabel}
                  placeholder='Please Select'
                  data={BedroomOptions}
                  onChangeText={(bedroom) => this.setState({ property_bedrooms : bedroom }, () => this.data.setForm({ 'property_bedrooms' : bedroom }))}
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                />
              </View>
              <View style={styles.fieldWrapTwoWay}>
                <CustomSelect
                  value={this.state.property_bathrooms}
                  label={this.state.bathRoomLabel}
                  placeholder='Please Select'
                  data={BathroomOptions}
                  onChangeText={(bathroom) => this.setState({ property_bathrooms : bathroom }, () => this.data.setForm({ 'property_bathrooms' : bathroom }))}
                  style={styles.viewAllWrap}
                  baseStyle={styles.viewAllBase}
                  fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}


export default BasicInfo
