import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    navLeft: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%',
    },
    propImgWrapper: {
        width: '100%',
        height: 220,
        overflow: 'hidden',
        position: 'relative'
    },
    cardImage: {
        flex: 1,
        resizeMode: 'cover'
    },
    cardOverlay: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        zIndex: 1,
        padding: 15,
        backgroundColor: 'transparent',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    cardOverlayInnerDetail: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    cardOverlayInner: {
        flexDirection: 'row',
        flex: 1
    },
    cardOverlayInnerLeft: {
        justifyContent: 'flex-start',
    },
    cardOverlayInnerRight: {
        justifyContent: 'flex-end',
    },
    cardOverlayInnerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 32
    },
    cardOverlayEditImage: {
        width: 60,
        height: 60,
        backgroundColor: 'rgba(16,24,45,0.9)',
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    cardOverlayText: {
        color: '#fff',
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold'
    },
    cardOverlayItem: {
        maxHeight: 30,
        textAlign: 'center',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardOverlayItemLeft: {
        paddingRight: 30
    },  
    cardOverlayItemRight: {
        paddingLeft: 30
    }, 
    bottomActions: {
        width: width,
        height: 50,
        padding: 5,
        paddingHorizontal: 10,
        backgroundColor: '#FFF',
        shadowOffset:{width: 2,  height: -5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    bottomActionItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    contentSection: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
    },
    contentSectionNoBorder: {
        borderTopWidth: 0,
        paddingTop: 0
    },
    textMd: {
        color: '#10182d',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold'
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    CheckBoxWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    checkBox: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        padding: 0,
        margin: 0,
        marginLeft: -2,
        flex: 1
    },
    commonLabel: {
        color: '#8991a4',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
        paddingBottom: 10
    },
    listWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: -10
    },
    listItem: {
        width: '50%',
        maxWidth: '50%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20
    },
    listImgWrapper: {
        width: 20
    },
    propSearchSection: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        flexWrap: 'wrap'
    },
    propSearchItemWrapper: {
        maxWidth: '50%',
        width: '50%'
    },
    propSearchItem: {
        flex: 1,
        padding: 10,
        backgroundColor: '#e6ecf2',
        borderRadius: 4,
        marginRight: 15,
        marginBottom: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    propSearchItemSelected: {
        backgroundColor: '#3670e5',
    },
    inputContainerMain: {
        borderBottomColor: 'rgba(0, 0, 0, 0.2)',
        flex: 1
    },
    inputContainer: {
        borderBottomWidth: 0,
        paddingLeft: 0,
        paddingRight: 0,
        margin: 0,
        flex: 1
    },
    inputField: {
        flex: 1,
        height: 40,
        paddingHorizontal: 15,
        paddingVertical: 13,
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
    },
    heading: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    successBtn: {
        backgroundColor: '#baf9e4',
        height: '100%'
    },
    publishBtn: {
        backgroundColor: '#149d78',
        height: '100%'
    },
    buttonContainer: {
        flex: 1,
        marginHorizontal: 5,
        position: 'relative'
    },
    successBtnTitle: {
        color: '#149d78',
        textTransform: 'uppercase',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    buttonIcon: {
        position: 'absolute',
        right: 5,
        top: 'auto'
    },
    overlayStyle: {
        backgroundColor: '#fff',
        padding: 0,
        shadowOffset:{width: 5,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .5)',
    },
    overlayInner: {
        width: '100%',
        height: 'auto',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        flex: 1,
        lineHeight: width * 0.035,
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: 'red'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    },
    overlayPopContent: {
        padding: 15,
    },
    overlayPopText: {
        color: '#10182d',
        fontSize: width * 0.036,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.05,
    },
    overlayPopButtons: {
        borderTopWidth: 1,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row'
    },
    OverlayPopBtn: {
        padding: 18,
        flex: 1
    },
    OverlayPopBtnText: {
        color: '#149d78',
        textTransform: 'uppercase',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'center'
    }
});
