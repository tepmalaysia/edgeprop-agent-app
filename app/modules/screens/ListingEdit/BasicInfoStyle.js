import { StyleSheet, Dimensions } from 'react-native';
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../assets/theme';

const { width, height } = Dimensions.get('window')

export default StyleSheet.create({
  ...ApplicationStyles,
  tabs: {
    flexGrow: 1,
    //flexShrink: 1,
  },
  controlsRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: width * 0.05,
    paddingRight: width * 0.05,
    backgroundColor: 'rgba(248, 248, 248, 0.4)',
    paddingBottom: 5,
    paddingTop: 15,
    borderColor: '#EAEAEA',
    borderTopWidth: 0.5,
    height: Math.max((height * 0.04) + 20, 50)
  },
  viewAllWrapCont: {
    width: width * 0.23,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 3,
    height: height * 0.03,
    backgroundColor: Colors.snow,
    ...ApplicationStyles.defaultShadow,
    height: height * 0.04,
    borderRadius: 10,
    paddingTop: 0

  },
  viewAllWrap: {
    width: '100%',
    marginLeft: 0,
    marginTop: 0,
    borderRadius: 8,
    paddingTop: 0,
    borderBottomColor: '#FFFFFF'
  },
  viewAllBase: {
    justifyContent: 'center',
    flex: 1
  },
  viewAllText: {
    flex: 1,
    flexDirection: 'row',
    padding: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    fontSize: 11,
    color: '#38A0EB'
  },
  viewAllImage: {
    // position: 'absolute',
    // bottom: 0,
    // right: 18.5,
    // zIndex: 12,
    backgroundColor: '#FFFFFF',
    marginTop: height * 0.012,
    width: 8,
    height: 5,
    resizeMode: 'contain'
  },
  switchLabel: {
    marginBottom: 5,
    fontSize: 12,
    opacity: 0.7,
  },
  headerImg: {
    marginLeft: -(width * 0.02522),
    width: '105.3%',
    height: 'auto',
    ...ApplicationStyles.defaultShadow,
    paddingHorizontal: 10,
    zIndex: 40,
  },
  navBar: {
    width: width,
    paddingTop: height * 0.045,
    height: height * 0.1355,
    flexDirection: 'row',
    alignItems: 'center',

  },
  navBarTitleRow: {
    flexGrow: 1,
    flexShrink: 0,
    alignItems: 'center',
  },
  navBarTitleLogo: {
    width: 42,
    height: 42,
    marginTop: -15,
  },
  navBarTitleText: {
    color: Colors.snow,
    fontSize: 22,
    fontFamily: Fonts.type.semiBold
  },
  contentSection: {
    paddingTop: 15,
    paddingBottom: 10,
    paddingHorizontal: 15,
    borderTopWidth: 10,
    borderTopColor: '#f5f5f5',
  },
  contentSectionNoBorder: {
      borderTopWidth: 0,
      paddingTop: 0
  },
  textMd: {
      color: '#10182d',
      fontSize: width * 0.03,
      fontFamily: 'OpenSans-SemiBold'
  },
  textSm: {
      fontSize: width * 0.028,
      fontFamily: 'OpenSans-Regular',
      color: '#10182d',
      lineHeight: width * 0.04,
      fontWeight: 'normal'
  },
  CheckBoxWrapper: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%'
  },
  checkBox: {
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      margin: 0,
      marginLeft: -2,
      flex: 1
  },
  fieldWrap: {
    width: width,
    marginLeft: -15,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'flex-end',
    flexWrap: 'wrap'
  },
  fieldWrapMulti: {
    paddingHorizontal: 10,
  },
  commonLabel: {
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#9E9E9E',
    lineHeight: width * 0.04,
    fontWeight: 'normal',
    paddingBottom: 10
  },
  editHeading: {
    fontSize: width * 0.032,
    fontFamily: 'OpenSans-Regular',
    color: '#b0bbbf',
    lineHeight: width * 0.04,
    fontWeight: 'normal',
    paddingBottom: 10,
    textTransform: 'uppercase',
    fontWeight: '700',
  },
  listWrapper: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop: -10
  },
  listItem: {
      width: '50%',
      maxWidth: '50%',
      flexDirection: 'row',
      alignItems: 'center',
      paddingTop: 20
  },
  listImgWrapper: {
      width: 20
  },
  propSearchSection: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'stretch',
      justifyContent: 'flex-start',
      flexWrap: 'wrap',
      marginLeft: -5,
      marginRight: -5
  },
  propSearchItemWrapper: {
      maxWidth: '50%',
      width: '50%',
      marginBottom: 5,
  },
  propSearchItem: {
      flex: 1,
      padding: 10,
      backgroundColor: '#e6ecf2',
      borderRadius: 4,
      marginHorizontal: 5,
      alignItems: 'center',
      justifyContent: 'center'
  },
  propSearchItemSelected: {
      backgroundColor: '#3670e5',
  },
  inputContainerMain: {
      borderBottomColor: 'rgba(0, 0, 0, 0.2)',
      borderBottomWidth: 1,
      flex: 1
  },
  inputContainer: {
      borderBottomWidth: 0,
      paddingLeft: 0,
      paddingRight: 0,
      margin: 0,
      flex: 1
  },
  textFieldContainer: {
    height: 53, 
    paddingTop: 12, 
    paddingBottom: 10, 
    marginBottom: 0,
    backgroundColor: 'white'
  },
  fieldWrapTwoWay: {
    width: '50%',
    paddingHorizontal: 5
  },
  fieldWrapOneWay: {
    width: '100%',
    paddingHorizontal: 5
  },
  inputField: {
      flex: 1,
      paddingHorizontal: 0,
      fontSize: width * 0.028,
      fontFamily: 'OpenSans-Regular',
      color: '#10182d',
      marginBottom: 0,
      marginTop: 0
  },
  inputLarge: {
    textAlign: 'left',
    textAlignVertical: 'top',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingTop: 0,
    height: 160
  }
});
