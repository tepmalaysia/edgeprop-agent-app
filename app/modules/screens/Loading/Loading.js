import React,{Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {Image, View, Text, ActivityIndicator} from 'react-native';
import styles from '../Splash/SplashStyle'
import ListingActions from '../../../../realm/actions/ListingActions'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import { ListStatus, FetchOne, GetData } from '../../services/api'
import Loading from '../../../components/Loader';
import moment from 'moment';

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/app/get-list-status';
//const ENQUIRYLIST = 'https://prolex.edgeprop.my/api/v1/agentMessage';
const ENQUIRYLIST = "https://www.edgeprop.my/jwdalice/api/vpex/get-user-messages"
const FETCHONE = 'https://prolex.edgeprop.my/api/v1/fetchOne';
export default class App extends Component{

	constructor(props){
    super(props);
    this.ListingActions = new ListingActions();
    this.EnquiryActions = new EnquiryActions();
    this.count = 0;
    this.state = {
      isLoading: true,
      fetchedData: [],
      check: false,
    }
    this._getList = this._getList.bind(this);
    this._getEnquiry = this._getEnquiry.bind(this);
    this.listStatusSuccess = this.listStatusSuccess.bind(this);
    this.listStatusFail = this.listStatusFail.bind(this);
    this._fetchOne = this._fetchOne.bind(this)
    this._updateList = this._updateList.bind(this)
    this.fetchOneSuccess = this.fetchOneSuccess.bind(this)
    this.fetchOneFail = this.fetchOneFail.bind(this)
    this.enquiryListSuccess = this.enquiryListSuccess.bind(this)
    this.enquiryListFail = this.enquiryListFail.bind(this)
    this._formatEnquiry = this._formatEnquiry.bind(this)
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    const lastUpdated = await AsyncStorage.getItem("lastUpdated");
    if(lastUpdated){
      this.setState({check: true}, () => this.props.navigation.replace('DrawerNavigator'))
    }else{
      this.setState({check: true})
      if(auth && auth != '') {
        let authItems = JSON.parse(auth);
        if(authItems.uid != '') {
          this.setState({ userInfo: authItems })
          //await this._getEnquiry();
          setTimeout(() => {
            this._getList();
          }, 1800);
        }
      }
    }
  }

  async _getEnquiry(){
    this.setState({ isLoading: true })
    const lastUpdated = await AsyncStorage.getItem("lastUpdated");
    let URL = ENQUIRYLIST + "?agentid=" + this.state.userInfo.uid +
              "&limit=25";
    GetData(URL)
    .then(data => this.enquiryListSuccess(data))
    .catch(error => this.enquiryListFail(error));
  }

  async enquiryListSuccess(response){
    if(response.status == 1 && response.messages){
      let data = this._formatEnquiry(response.messages);
      this.EnquiryActions.CreateEnquiry(data);
    }
  }

  _formatEnquiry(items) {
    let result = [];
    items.map((item, i) => {
      let data = {};
      const status = (item.status == 0) ? 3 : item.status;
      var mDate = new Date().toLocaleString("en-US", {timeZone: "Asia/Kuala_Lumpur"})
      data = {
            lid: item.mid ? parseInt(item.mid) : 0,
            mid: item.mid ? parseInt(item.mid) : 0,
            nid: item.nid ? parseInt(item.nid) : 0,
            agent_id: parseInt(this.state.userInfo.uid),
            district: item.district ? item.district : 'xxxx',
            state: item.state ? item.state : 'xxxx',
            title: item.title ? item.title : '',
            message_id: item._id ? parseInt(item._id) : 0,
            message: item.user.message ? item.user.message : '',
            name: item.user.name ? item.user.name.trim() : '',
            phone: item.user.phone ? item.user.phone : 'xxxx',
            email: item.user.email ? item.user.email : 'xxxx',
            status: parseInt(item.status),
            read: (item.read == '1') ? true : false,
            changed: item.created ? item.created : new Date(mDate).getTime()/1000,
          }
      result.push(data);
    });
    return result;

  }

  enquiryListFail(error){
   
  }

  async _getList(){
    this.setState({ isLoading: true })
    const lastUpdated = await AsyncStorage.getItem("lastUpdated");
    let body = "uid=" + this.state.userInfo.uid;
    if(lastUpdated){
      body = body+ "&changed=" + lastUpdated;
    }
    ListStatus(HOSTNAME, body)
    .then(data => this.listStatusSuccess(data))
    .catch(error => this.listStatusFail(error));
  }

  async listStatusSuccess(response){
    if(response.status == 1 && response.data){
      this._updateList(response.data);
    }else{
      this.setState({ isLoading: false})
      try {
        await AsyncStorage.setItem("lastUpdated", moment().unix().toString())
          .then( ()=>{
            this.props.navigation.replace('DrawerNavigator');
          })
          .catch( ()=>{
            this.props.navigation.replace('DrawerNavigator');
          })
      } catch (error) {
        this.props.navigation.replace('DrawerNavigator');
      }
    }
  }

  listStatusFail(error){
   this.props.navigation.replace('DrawerNavigator')
  }

  async _updateList(items) {
    if(Array.isArray(items)){
      this.setState({ isLoading: true, fetchedData : items })
      for(var i = 0; i < items.length; i++){
        let item = items[i];
        let listing_type = item.listing_type == 'rent'? 'rental' : item.listing_type;
        const data = "uid=" + this.state.userInfo.uid +
              "&token=" + this.state.userInfo.token +
              "&field_prop_listing_type=" + listing_type +
              "&property_id=" + item.lid +
              "&cache=0" +
              "&source=edit"+
              "&type=" + item.id_type;
        if(item.status == 'fetch'){
          this._fetchOne(data);
        }else if(item.status == 'delete'){
          this.count++;
          let listItem = this.ListingActions.GetListing(item.lid);
          if(listItem && listItem.length >0){
            this.ListingActions.ClearListing(item.lid);
          }
          if(this.count == this.state.fetchedData.length){
            this.count = 0;
            this.setState({ isLoading: false })
            try {
              await AsyncStorage.setItem("lastUpdated", moment().unix().toString())
                .then( ()=>{
                  this.props.navigation.replace('DrawerNavigator');
                })
                .catch( ()=>{
                  this.props.navigation.replace('DrawerNavigator');
                })
            } catch (error) {
              this.props.navigation.replace('DrawerNavigator');
            }
          }
        }
      }
    }
  }

  _fetchOne(body) {
    FetchOne(FETCHONE, body)
    .then(data => this.fetchOneSuccess(data, body))
    .catch(error => this.fetchOneFail(error));
  }

  async fetchOneSuccess(response, body){
    this.count++;
    if(response.result){
      const listingData = this.formatListing(response.result);
      let result = {...listingData, 'token': this.state.userInfo.token, 'uid': parseInt(this.state.userInfo.uid)};
      let listItem = this.ListingActions.GetListing(result.lid);
      if(listItem && listItem.length >0){
        this.ListingActions.ClearListing(result.lid);
      }
      this.ListingActions.CreateListing(result);
    }
    
    if(this.count == this.state.fetchedData.length){
      this.count = 0;
      this.setState({ isLoading: false })
      try {
        await AsyncStorage.setItem("lastUpdated", moment().unix().toString())
          .then( ()=>{
            this.props.navigation.replace('DrawerNavigator');
          })
          .catch( ()=>{
            this.props.navigation.replace('DrawerNavigator');
          })
      } catch (error) {
        this.props.navigation.replace('DrawerNavigator');
      }
    }
  }

  async fetchOneFail(error){
    this.count++;
    if(this.count == this.state.fetchedData.length){
      this.count = 0;
      this.setState({ isLoading: false })
      try {
        await AsyncStorage.setItem("lastUpdated", moment().unix().toString())
          .then( ()=>{
            this.props.navigation.replace('DrawerNavigator');
          })
          .catch( ()=>{
            this.props.navigation.replace('DrawerNavigator');
          })
      } catch (error) {
        this.props.navigation.replace('DrawerNavigator');
      }
    }
  }


  formatListing = (data) => {
    

    let asset = data.asset;
    let listing = {};

    listing.lid = data.mid ? parseInt(data.mid) : parseInt(data.nid);

    listing.nid = data.nid ? parseInt(data.nid) : null;

    listing.mid = data.mid ? parseInt(data.mid) : null;

    listing.title = data.title ? data.title : '';

    listing.status = data.status ? parseInt(data.status) : 0;
  
    listing.created = data.created ? data.created : '';

    listing.changed = data.changed ? data.changed : '';

    listing.district = data.district ? data.district.toString() : '';

    listing.state = data.state ? data.state.toString() : '';

    listing.project_name = asset.project_name ? asset.project_name : '';

    listing.project_id = asset.id ? asset.id : '';

    listing.prop_status = (data && data.field_prop_status && data.field_prop_status.und && Array.isArray(data.field_prop_status.und)) ? (data.field_prop_status.und[0]? data.field_prop_status.und[0].value : '') : '';

    listing.listing_type = (data && data.field_prop_listing_type && data.field_prop_listing_type.und && Array.isArray(data.field_prop_listing_type.und)) ? (data.field_prop_listing_type.und[0]? data.field_prop_listing_type.und[0].value : '') : '';

    listing.property_type = (data && data.field_property_type && data.field_property_type.und && Array.isArray(data.field_property_type.und)) ? (data.field_property_type.und[0]? data.field_property_type.und[0].target_id : '') : '';

    listing.asset_id = (data && data.field_prop_asset && data.field_prop_asset.und && Array.isArray(data.field_prop_asset.und)) ? (data.field_prop_asset.und[0]? data.field_prop_asset.und[0].target_id : '') : '';

    listing.price = (data && data.field_prop_asking_price && data.field_prop_asking_price.und && Array.isArray(data.field_prop_asking_price.und)) ? (data.field_prop_asking_price.und[0]? data.field_prop_asking_price.und[0].value : null) : null;

    listing.street = (data && data.field_prop_street && data.field_prop_street.und && Array.isArray(data.field_prop_street.und)) ? (data.field_prop_street.und[0]? data.field_prop_street.und[0].value : null) : null;
    
    let coverImages = (data && data.field_cover_image && data.field_cover_image.und && Array.isArray(data.field_cover_image.und)) ? data.field_cover_image.und : [];
    listing.cover_image = (coverImages && coverImages[0] && coverImages[0].uri)? coverImages[0].uri: '';
    
   
    let images = (data && data.field_prop_images && data.field_prop_images.und && Array.isArray(data.field_prop_images.und)) ? data.field_prop_images.und : [];
    let list_uri = [];
    list_uri = images.map((item, index) => { return item.list_uri });
    listing.images = list_uri;

    listing.data = JSON.stringify(data);
    listing.url = data.url ? data.url : '';
    return listing;
  }

  render(){
    const { check } = this.state;
    if(!check){
      return null;
    }
    return (
      <View style={styles.splashContianer}>
        <Text style={styles.textMd} allowFontScaling={false}> Please wait</Text>
        <Text style={styles.textSm} allowFontScaling={false}> Your properties are getting synced</Text>
        <Image style={{width: 120, height: 120}} source={require('../../../assets/images/blue-hourglass.gif')} resizeMode="contain"/>
      </View>
    );
  }
}