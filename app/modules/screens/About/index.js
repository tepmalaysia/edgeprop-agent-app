/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  Linking
} from 'react-native';
import styles from './AboutStyles';
import { Avatar, Icon } from 'react-native-elements';
import { Header, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { DrawerActions } from 'react-navigation-drawer';
import moment from 'moment';
import { GetData } from '../../services/api'
import Loading from '../../../components/Loader';


const ANNOUNCEMENT = 'https://prolex.edgeprop.my/api/v1/app/get-announcements';
class About extends Component{

  constructor(props) {
    super(props);
    this.state = { 
      items: [],
      isLoading: true
    }
    this.getList = this.getList.bind(this)
    this.getDataSuccess = this.getDataSuccess.bind(this)
    this.getDataFail = this.getDataFail.bind(this)
    this._handleAnnoucement = this._handleAnnoucement.bind(this)
  }

  componentDidMount(){
    this.getList();
  }

  getList () {
    GetData(ANNOUNCEMENT)
          .then(data => this.getDataSuccess(data))
          .catch(error => this.getDataFail(error));
  }

  getDataSuccess(response){
    if(response && Array.isArray(response)){
      this.setState({items : response})
    }
    this.setState({isLoading : false})
  }

  getDataFail(error){
    this.setState({isLoading : false})
  }

  _handleAnnoucement(item){
    Linking.openURL(item.pdf)
  }
  
  render() {
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Icon
              name='menu'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>Announcements</Text>
        </View>
      )
    }


    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>

          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
          />
          <View style={styles.innerContainer}>
            <ScrollView>
              <View style={styles.listInner}>
                {this.state.items.length > 0 && this.state.items.map((item,i) => {
                  return(
                    <TouchableOpacity 
                      style={[styles.listInnerItem, styles.listInnerItemActive]} 
                      key={i} 
                      onPress={() => this._handleAnnoucement(item)}>
                      <View style={styles.itemContainer}>
                        <View style={[styles.leftIcon , styles.leftIconColor]}>
                          <Avatar
                            size="medium"
                            rounded
                            title={item.title? item.title[0] : ''}
                            titleStyle={styles.title}
                            overlayContainerStyle={{backgroundColor: '#343a56'}}
                          />
                        </View>
                        <View style={styles.centerText}>
                          <Text allowFontScaling={false} style={[styles.textSm,styles.textSmBold]} numberOfLines = {2}>
                          {item.title}
                          </Text>
                        </View>
                        <View style={styles.rightText}>
                          <Text allowFontScaling={false} style={[styles.itemDate,styles.itemDateActive]}>{moment.unix(item.created).format("DD MMM YYYY")}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )
                })}
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
        {
            this.state.isLoading && (
              <Loading />
            )
          }
      </LinearGradient>
    );
  }
}


export default About