import React from 'react';
import {Image, View} from 'react-native';
import styles from './SplashStyle.js'
import AsyncStorage from '@react-native-community/async-storage';

export default class SplashScreen extends React.Component {

  _showSplash = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    )
  }

  _navigate = async () => {
    this.props.navigation.navigate('Home');
  }

  async componentDidMount() {
    const data = await this._showSplash();
    if (data !== null) {
      this._navigate();
    }
  }

  render() {
    return (
      <View style={styles.splashContianer}>
        <Image style={{width: 180, height: 80}} source={require('../../../assets/images/agent.png')} resizeMode="contain"/>
      </View>
    );
  }
}