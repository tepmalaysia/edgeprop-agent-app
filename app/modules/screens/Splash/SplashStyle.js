import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  // splash screen
  splashContianer: {
    display: 'flex',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
    width: width
  },
  textSm: {
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#6d7e8e',
    lineHeight: width * 0.04,
    fontWeight: 'normal'
  },
  textMd: {
    fontSize: width * 0.04,
    fontFamily: 'OpenSans-Regular',
    color: '#6d7e8e',
    lineHeight: width * 0.045,
    fontWeight: 'normal'
  },
});
