/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Linking
} from 'react-native';
import { Avatar, Header, Overlay, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import { DrawerActions } from 'react-navigation-drawer';
import moment from "moment";
import styles from './contactStyles';
import { ScrollView } from 'react-native-gesture-handler';
import Loading from '../../../components/Loader';
import { FetchAgent, SignOut } from '../../services/api'
import StateData from '../../../assets/json/states.json'
import ProfileActions from '../../../../realm/actions/ProfileActions'
import ListingActions from '../../../../realm/actions/ListingActions'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/fetchAgent';
const POINT = "https://prolex.edgeprop.my/api/v1/getPropPoints";
const LOGOUT = 'https://prolex.edgeprop.my/api/v1/logout';
export default class Contact extends Component{
  countryCode = "60";
  constructor(props) {
    super(props);
    this.ProfileActions = new ProfileActions();
    this.ListingActions = new ListingActions();
    this.EnquiryActions = new EnquiryActions();
    let activeCount = this.ListingActions.GetCount('active');
    let profileData = this.ProfileActions.GetProfile();
    let defaultInfo = {
          profile_uri: '',
          agentName: '',
          agentContact: '',
          agencyName: '',
          agentNo: '',
          name: '',
          email: '',
          personalAddress: '',
          personalState: '',
          personalPostal: '',
          agencyInfo: '',
          agentPlan: 1,
          agentPlanExpiry: ''
        };
    this.state = {
        isLoading: false,
        userInfo: {},
        defaultImg: require('../../../assets/images/default-img.png'),
        agentInfo: profileData[0]? profileData[0] : defaultInfo,
        listingCount: 0,
        propPoints: 0,
        isVisible: false,
        apiError: '',
        activeCount,
        points: 0
    };
    this._onRefetch = this._onRefetch.bind(this)
    this._showAlert = this._showAlert.bind(this)
    this.fetchSuccess = this.fetchSuccess.bind(this)
    this.fetchFail = this.fetchFail.bind(this)
    this.logOutSuccess = this.logOutSuccess.bind(this)
    this.logOutFail = this.logOutFail.bind(this)
    this.openOption = this.openOption.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this._setAgentPlan = this._setAgentPlan.bind(this)
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems })
        this._onRefetch();
      }
    }
  }

  _onRefetch() {
    this.setState({ isLoading: true })
    let activeCount = this.ListingActions.GetCount('active');
    this.setState({activeCount})
    
    this.fetchData().then(arrayOfResponses => {
      if(arrayOfResponses[1] && arrayOfResponses[1].status == 'success'){
        let result = arrayOfResponses[1].result;
        let points = 0;
        if(result.total_point){
          points = result.total_point.points? result.total_point.points : 0;
        }
        this.setState({points}, () => {
          try {
            AsyncStorage.setItem("agentPoint", points.toString())
              .then( ()=>{})
              .catch( ()=>{})
          } catch (error) {}
        })
      }
      this.fetchSuccess(arrayOfResponses[0])
    }).catch(error => this.fetchFail(error));
  }

  fetchSuccess = (response) => {
    this.setState({ isLoading: false })
    if(response.status == 'success'){
      const profileData = this.formatProfile(response.result);
      this._setAgentPlan(profileData.agentPlan)
      let result = {...profileData, 'token': this.state.userInfo.token };
      this.ProfileActions.SetProfile(result, 'uid = '+this.state.userInfo.uid);
      let profileInfo = this.ProfileActions.GetProfile();
      this.setState({agentInfo: profileInfo[0]})
    }
  }

  fetchFail = (error) => {
    this.setState({ isLoading: false })
  }

  async _setAgentPlan(plan) {
    try {
        await AsyncStorage.setItem("agentPlan", plan.toString())
          .then( ()=>{})
          .catch( ()=>{})
      } catch (error) {}
  }

  fetchData = () => {
    const urls = [
      HOSTNAME,
      POINT
    ];
     const body = "uid=" + this.state.userInfo.uid +
              "&token=" + this.state.userInfo.token ;
    const allRequests = urls.map(url => 
      fetch(url, {
        method: 'POST',
        headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: body
      }).then(response => response.json())
    );

    return Promise.all(allRequests);
  };

  _onLogoutPress = async () => {
    this.setState({ isLoading: true })
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    const body = "uid=" + this.state.userInfo.uid +
              "&token=" + this.state.userInfo.token +
              "&agent_id=" + this.state.userInfo.agent_id +
              "&agent_app=1" +
              "&device_id=" + fcmToken;
    SignOut(LOGOUT, body)
    .then(data => this.logOutSuccess(data))
    .catch(error => this.logOutFail());
  }

  async logOutSuccess (response){
    this.setState({ isLoading: false })
    if(response.status_code == 1) {
      const keys = ['authUser', 'lastUpdated', 'propDatesale', 'propDaterental', 'propDateauction', 'propDateroom_rental', 'agentPlan', 'syncTime']
      AsyncStorage.multiRemove(keys);
      let lastMessage = await AsyncStorage.getItem("lastMessage");
      if(lastMessage && lastMessage != '') {
        AsyncStorage.setItem("lastMessage", '0');
      }
      this.ProfileActions.ClearProfile();
      this.ListingActions.ClearListings();
      this.EnquiryActions.ClearEnquiry();
      this.props.navigation.navigate('SignedOut')
    }
  }

  logOutFail = () => {
    this.setState({ isLoading: false })
    this.setState({ isVisible: true, apiError: 'Login Failed' });
  }

  _showAlert = (msg, body) => {
    Alert.alert(
      msg, body
    );
  }

  formatProfile = (data) => {
    let agentInfo = data.agent;
    let personalInfo = data.main;
    let accountInfo = data.user;
    let profile = {};

    let photo_details =(agentInfo && agentInfo.field_agent_image && agentInfo.field_agent_image.und && Array.isArray(agentInfo.field_agent_image.und))? agentInfo.field_agent_image.und[0]: {};
    let photo_data = (Object.keys(photo_details).length>0)?[{
      s3_image_details:photo_details
    }]:[];
    //profile.agentPhoto = photo_data;
    profile.profile_uri = (photo_data[0] && photo_data[0].s3_image_details && Array.isArray(photo_data))? (photo_data[0]? photo_data[0].s3_image_details.uri : '') : '';
    if(profile.profile_uri !='' && profile.profile_uri != undefined){
      let is_public_photo = profile.profile_uri.search("public://");
      if(is_public_photo == 0){
        let updated_url_photo = profile.profile_uri.replace('public://', '/');
        profile.profile_uri = data.agent_image_url+updated_url_photo;
      }
    }
    
    profile.agentName = (agentInfo && agentInfo.field_agent_bizname && agentInfo.field_agent_bizname.und && Array.isArray(agentInfo.field_agent_bizname.und))? (agentInfo.field_agent_bizname.und[0]? agentInfo.field_agent_bizname.und[0].value : '') : '';

    profile.agentContact = (personalInfo && personalInfo.field_profile_contact && personalInfo.field_profile_contact.und && Array.isArray(personalInfo.field_profile_contact.und))? (personalInfo.field_profile_contact.und[0]? personalInfo.field_profile_contact.und[0].value : '') : '';

    profile.agencyName = (agentInfo && agentInfo.field_agent_agency && agentInfo.field_agent_agency.und && Array.isArray(agentInfo.field_agent_agency.und))? (agentInfo.field_agent_agency.und[0]? agentInfo.field_agent_agency.und[0].value : '') : '';

    profile.agentNo = (agentInfo && agentInfo.field_agent_id && agentInfo.field_agent_id.und && Array.isArray(agentInfo.field_agent_id.und))? (agentInfo.field_agent_id.und[0]? agentInfo.field_agent_id.und[0].value.trim() : '') : '';
  
    profile.name = data.user.name;

    profile.email = accountInfo.mail;

    profile.personalAddress = (personalInfo && personalInfo.field_profile_address && personalInfo.field_profile_address.und && Array.isArray(personalInfo.field_profile_address.und))? (personalInfo.field_profile_address.und[0]? personalInfo.field_profile_address.und[0].value : '') : '';

    profile.personalState = (personalInfo && personalInfo.field_profile_state && personalInfo.field_profile_state.und && Array.isArray(personalInfo.field_profile_state.und))? this.getLabel(StateData,personalInfo.field_profile_state.und[0].value,true) : null;

    profile.personalPostal = (personalInfo && personalInfo.field_profile_postcode && personalInfo.field_profile_postcode.und && Array.isArray(personalInfo.field_profile_postcode.und))? (personalInfo.field_profile_postcode.und[0]? personalInfo.field_profile_postcode.und[0].value : null) : null;
    if(profile.personalPostal){
      profile.personalPostal = parseInt(profile.personalPostal)
    }
    profile.agencyInfo = (agentInfo && agentInfo.field_agent_desc && agentInfo.field_agent_desc.und && Array.isArray(agentInfo.field_agent_desc.und))? (agentInfo.field_agent_desc.und[0]? agentInfo.field_agent_desc.und[0].value : '') : '';

    profile.agentPlan = (agentInfo && agentInfo.field_agent_plan && agentInfo.field_agent_plan.und && Array.isArray(agentInfo.field_agent_plan.und)) ? (agentInfo.field_agent_plan.und[0]? agentInfo.field_agent_plan.und[0].value : 1) : 1;
    if(profile.agentPlan){
      profile.agentPlan = parseInt(profile.agentPlan)
    }
    profile.agentPlanExpiry = (agentInfo && agentInfo.field_plan_expiry && agentInfo.field_plan_expiry.und && Array.isArray(agentInfo.field_plan_expiry.und)) ? (agentInfo.field_plan_expiry.und[0]? agentInfo.field_plan_expiry.und[0].value : '') : '';

    return profile;
  }

  getLabel = (collection,check,flag) =>{
    let label = ''
     if(flag){
      var result = collection.filter(function( obj ) {
        return obj.text.toLowerCase() == check.toLowerCase();
      });
       if(result.length >0){
          label = result[0].value;
        }
     }else{
      var result = collection.filter(function( obj ) {
        return obj.value == check;
      });
       if(result.length >0){
          label = result[0].text;
        }
     }
       return label;
  }

  _handleValue(value) {
    // handle empty value
    if (value == null || value == '' || value == 'Uncompleted') {
        return ''
    }
    // handle if value is integer
    if (value === parseInt(value, 10)) return value
    // handle string, and set the capitalization
    return this._capitalizeFirstLetter(value)
  }

  _isEmptyValue(value) {
    return (value == null || value == '' || value == 'Uncompleted' || value == undefined)
  }

  _wrapAddressInfo() {
    let agentInfo = this.state.agentInfo;
    let assetInfo = ((agentInfo.personalAddress != '' && !this._isEmptyValue(agentInfo.personalAddress)) ? (this._handleValue(agentInfo.personalAddress + (' \xB7 '))) : '')
        + ((agentInfo.personalState != undefined && !this._isEmptyValue(agentInfo.personalState)) ? (this._handleValue(agentInfo.personalState + ', ')) : '')
        + ((agentInfo.personalPostal != undefined && !this._isEmptyValue(agentInfo.personalPostal)) ? (this._handleValue(agentInfo.personalPostal)) : '');
    return assetInfo
  }

  _capitalizeFirstLetter(str) {
    if ((typeof str) == 'string') {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
        })
    }
  }

  openOption(type) {
    if(type == 'phone'){
      if(this.state.agentInfo.agentContact !=='' || this.state.agentInfo.name !== ''){
        let phone = this.state.agentInfo.agentContact? this.state.agentInfo.agentContact: this.state.agentInfo.name;
        var contact = "+" + this.countryCode + phone;
        const url = 'tel:'+contact;
        Linking.canOpenURL(url)
          .then((supported) => {
            if (supported) {
              return Linking.openURL(url)
                .catch(() => null);
            }
          });
      }
    }else if(type == 'email'){
      if(this.state.agentInfo.email !== ''){
        let url = `mailto:${this.state.agentInfo.email}`;
        Linking.canOpenURL(url)
          .then((supported) => {
            if (supported) {
              return Linking.openURL(url)
                .catch(() => null);
            }
        });
      }
    }
  }

  render() {
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft} >
          <TouchableOpacity onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Icon
              name='menu'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>Profile</Text>
        </View>
      )
    }

    const RightNavComponent = () => {
      return (
        <View style={styles.navRight}>
          <TouchableOpacity style={styles.navIconRight} onPress={this._onRefetch}>
            <Icon
              name='rotate-cw'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.navIconRight} onPress={this._onLogoutPress}>
            <Icon
              name='log-out'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          {/* <HeaderSearch navigation={this.props.navigation} /> */}
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
            rightComponent={<RightNavComponent />}
          />
          <View style={styles.innerContainer}>
          <ScrollView>
            <View style={styles.profileWrapper}>
              <View style={styles.profileStat}>
                <View style={styles.profileTop}>
                  <View style={styles.profilePrimary}>
                    <Avatar width={100} height={100} containerStyle={styles.avatar} source={this.state.agentInfo.profile_uri?  {uri: this.state.agentInfo.profile_uri}: this.state.defaultImg} />
                    <View style={styles.profilePrimaryInfo}>
                      <Text allowFontScaling={false} style={styles.hTwo}>{this.state.agentInfo.agentName}</Text>
                      <Text allowFontScaling={false} style={[styles.textSm, {color: '#555b6b'}]}>{this.state.agentInfo.agencyName}</Text>
                      <Text allowFontScaling={false} style={[styles.textSm, {color: '#555b6b'}]}>{this.state.agentInfo.agentNo}</Text>
                    </View>
                  </View>
                  <View style={styles.profileSecondary}>
                    <View style={styles.listWrapper}>
                      <View style={styles.listItem}>
                        <View style={styles.listImgWrapper}>
                          <Icon
                            name='phone'
                            type='feather'
                            size={18}
                            color={'#10182d'}
                          />
                        </View>
                        <View style={{flex: 1}}>
                          <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15, fontFamily: 'OpenSans-SemiBold'}]}>{this.state.agentInfo.agentContact? this.state.agentInfo.agentContact: this.state.agentInfo.name}</Text>
                        </View>
                      </View>
                      <View style={styles.listItem}>
                        <View style={styles.listImgWrapper}>
                          <Icon
                            name='mail'
                            type='feather'
                            size={18}
                            color={'#10182d'}
                          />
                        </View>
                        <View style={{flex: 1}}>
                          <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15, fontFamily: 'OpenSans-SemiBold'}]}>{this.state.agentInfo.email}</Text>
                        </View>
                      </View>
                      <View style={[styles.listItem, {alignItems: 'flex-start'}]}>
                        <View style={styles.listImgWrapper}>
                          <Icon
                            name='map-pin'
                            type='feather'
                            size={18}
                            color={'#10182d'}
                          />
                        </View>
                        <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15, fontFamily: 'OpenSans-SemiBold'}]}>{this._wrapAddressInfo()}</Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.profileBottom}>
                  <Text  allowFontScaling={false} style={[styles.textSm, {textAlign: 'center', fontStyle: 'italic'}]}>{this.state.agentInfo.agencyInfo}</Text>
                </View>
              </View>
              {/* Second Info */}
              <View style={styles.profileStat}>
                <View style={styles.profileSub}>
                  <View style={styles.statItem}>
                    <Image
                      style={{width: 32, height: 30}}
                      source={require('../../../assets/images/listing.png')}
                    />
                    <View style={styles.statRight}>
                      <Text allowFontScaling={false} style={styles.statHead}>{this.state.activeCount}</Text>
                      <Text allowFontScaling={false} style={styles.statSub}>Active listings</Text>
                    </View>
                  </View>
                  <View style={[styles.statItem, {paddingLeft: 15}]}>
                    <Image
                      style={{width: 34, height: 30}}
                      source={require('../../../assets/images/credits.png')}
                    />
                    <View style={styles.statRight}>
                      <Text allowFontScaling={false} style={styles.statHead}>{this.state.points}</Text>
                      <Text allowFontScaling={false} style={styles.statSub}>Credits</Text>
                    </View>
                  </View>
                </View>
              </View>
              {/* Third Info */}
              {this.state.agentInfo.agentPlan == 2 && <View style={styles.profileStat}>
                <View style={styles.profileSub}>
                  <View style={styles.statItem}>
                    <Image
                      style={[styles.premiumImage, {width: 22, height: 17}]}
                      source={require('../../../assets/images/premium.png')}
                    />
                    <View style={styles.statRight}>
                      <Text allowFontScaling={false} style={styles.premiumPlan}>Premium plan</Text>
                    </View>
                  </View>
                  <View style={[styles.statItem, {paddingLeft: 15}]}>
                    <Text allowFontScaling={false} style={[styles.statSub, {textAlign: 'right'}]}>Expires in {moment(this.state.agentInfo.agentPlanExpiry, 'YYYY/MM/DD').format("DD MMM YYYY")}</Text>
                  </View>
                </View>
              </View>}
            </View>
          </ScrollView>
          </View>

          {/* Overlay */}
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle, styles.bgDanger]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible: false })}
            >
              <View style={styles.overlayInner}>
                <Icon
                  name='info'
                  type='material'
                  color='#fff'
                  size={30}
                />
                <Text allowFontScaling={false} style={styles.overlayText}>{this.state.apiError}</Text>
              </View>
            </Overlay>

        </SafeAreaView>
        {
          this.state.isLoading && (
            <Loading />
          )
        }
      </LinearGradient>
    );
  }
}