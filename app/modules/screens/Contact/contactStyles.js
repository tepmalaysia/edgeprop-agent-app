import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    navIconRight: { 
        paddingLeft: 20,
        minHeight: 24
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },  
    profileWrapper: {
        flex: 1,
        width: '100%',
        padding: 15
    },
    profileStat: {
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 6,
        shadowOffset:{width: 2,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        marginBottom: 10,
    },
    profileTop: {
        padding: 15,
        borderBottomColor: '#c5c5c5',
        borderBottomWidth: 0.5
    },
    profileBottom: {
        padding: 15,
        paddingHorizontal: 30
    },
    profilePrimary: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    avatar: {
        borderRadius: 100,
        overflow: 'hidden'
    },
    profilePrimaryInfo: {
        flex: 1,
        paddingLeft: 15,
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    hTwo: {
        fontSize: width * 0.045,
        fontFamily: 'OpenSans-Bold',
        color: '#10182d',
        lineHeight: width * 0.045,
        marginVertical: 5
    },
    listWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    listItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15
    },
    profileSub: {
        width: '100%',
        flexDirection: 'row',
        padding: 15
    },
    statItem: {
        width: '50%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    statRight: {
        paddingLeft: 10
    },
    statHead: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Bold',
        color: '#3670e5'
    },
    statSub: {
        width: '100%',
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-SemiBold',
        color: '#10182d'
    },
    premiumPlan: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        color: '#ffb400'
    },
    expirePlan: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#a1adca',
        lineHeight: width * 0.04,
    },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .35)',
    },
    overlayInner: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        paddingLeft: 10
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    },
    LeftMenuIcon: {
        marginLeft: -10,
    }
});