/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from 'react-native';
import { CheckBox, Header, Input, Button, Icon, Overlay } from 'react-native-elements';
import moment from 'moment';
import styles from './SearchOptionStyles';

class CenterNavComponent extends Component{
  constructor(props) {
    super(props);
    this.state ={
      keyword: props.value? props.value : ''
    }
    this._textChange = this._textChange.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.clear != this.props.clear){
      this.setState({keyword: ''})
    }
    return true;
  }
  _textChange = (text) => {
    this.setState({
      keyword:text
    })
    this.props.onTextChange(text)
  }

  render() { 
    return (
      <View style={styles.navCenter}>
        <Input 
          placeholder='Find Properties...' 
          inputContainerStyle={{borderBottomWidth: 0}} 
          containerStyle={styles.inputContainer} 
          inputStyle={styles.inputField}
          value={this.state.keyword}
          onChangeText={(search) => this._textChange(search)}
        />
      </View>
    )
  }  
}

class SearchOption extends Component{
  constructor(props) {
    super(props);
    let options = props.navigation.state.params.data.options? props.navigation.state.params.data.options : {}
    this.state = {
      isVisible: false,
      clear: false,
      keyword: '',
      listing_type: options.listing_type? options.listing_type : 'sale',
      category: options.category? options.category : '',
      featured: options.isFeatured? options.isFeatured : false
    };
   this.search = options.search? options.search : '';
   this._onSearchBack = this._onSearchBack.bind(this);
   this._propertySelect = this._propertySelect.bind(this);
   this._categorySelect = this._categorySelect.bind(this);
   this._goToListing = this._goToListing.bind(this);
   this._updateSearch = this._updateSearch.bind(this);
   this._clearSearch = this._clearSearch.bind(this);
  }

  _onSearchBack() {
    this.props.navigation.goBack();
  }
  _clearSearch(){
    this.search = '';
    this.setState({
      keyword: '',
      listing_type: 'sale',
      category: '',
      featured: false,
      clear: !this.state.clear
    });
  }

  _updateSearch = search => {
    this.search = search;
  };

  _propertySelect = type => {
    this.setState({
      listing_type : type
    });
  }

  _categorySelect = type => {
    this.setState({
      category : type
    });
  }

  _goToListing = () => {
    let obj = {
      listing_type: this.state.listing_type,
      category:this.state.category, 
      search: this.search
    }
    if(this.state.featured){
      obj.isFeatured = this.state.featured;
    }
    this.props.navigation.navigate('Home', {
      data: obj,
      timeStamp: moment().unix(),
      message: null
    })
  }

  render() {
    const { search } = this.state;

    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={()=> this._onSearchBack()} style={styles.navigation}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#10182d'
            />
          </TouchableOpacity>
        </View>
      )
    }

    

    const RightNavComponent = () => {
      return (
        <View style={styles.navRight}>
          <TouchableOpacity onPress={() => this._clearSearch()}>
            <Icon
              name='clear-all'
              size={26}
              type='material'
              color='#3670e5'
            />
          </TouchableOpacity>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent onTextChange={this._updateSearch} value={this.search} clear={this.state.clear}/>}
            rightComponent={<RightNavComponent />}
          />
          <View style={styles.innerContainer}>
            <View style={styles.searchMainWrapper}>
              <ScrollView>              
                <View style={styles.contentSection}>
                  <View style={styles.searchSubSection}>
                    <View style={styles.propSearchSection}>
                      <View style={[styles.propSearchItemWrapper]}>
                        <TouchableOpacity style={this.state.listing_type === 'sale' ?[styles.propSearchItem, styles.propSearchItemSelected] : [styles.propSearchItem]} onPress={() => {this._propertySelect('sale')}}>
                          <Image
                            style={{width: 26, height: 26, marginBottom: 5}}
                            source={this.state.listing_type === 'sale' ? require('../../../assets/images/sale-alt.png') : require('../../../assets/images/sale.png')}
                          />
                          <Text allowFontScaling={false} style={[styles.textSm, styles.propSearchItemText, this.state.listing_type === 'sale' ? {color: '#fff'} : {}]}>For Sale</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={[styles.propSearchItemWrapper]}>
                        <TouchableOpacity style={this.state.listing_type === 'rental' ?[styles.propSearchItem, styles.propSearchItemSelected] : [styles.propSearchItem]} onPress={() =>this._propertySelect('rental')}>
                          <Image
                            style={{width: 26, height: 26, marginBottom: 5}}
                            source={this.state.listing_type === 'rental' ? require('../../../assets/images/rent-alt.png') : require('../../../assets/images/rent.png')}
                          />
                          <Text allowFontScaling={false} style={[styles.textSm, styles.propSearchItemText, this.state.listing_type === 'rental' ? {color: '#fff'} : {}]}>For Rent</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={[styles.propSearchItemWrapper]}>
                        <TouchableOpacity style={this.state.listing_type === 'auction' ?[styles.propSearchItem, styles.propSearchItemSelected, {marginRight: 0}] : [styles.propSearchItem]} onPress={() => this._propertySelect('auction')}>
                          <Image
                            style={{width: 26, height: 26, marginBottom: 5}}
                            source={this.state.listing_type === 'auction' ? require('../../../assets/images/bid-alt.png') : require('../../../assets/images/bid.png')}
                          />
                          <Text allowFontScaling={false} style={[styles.textSm, styles.propSearchItemText, this.state.listing_type === 'auction' ? {color: '#fff'} : {}]}>For Auction</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={[styles.propSearchItemWrapper]}>
                        <TouchableOpacity style={this.state.listing_type === 'room_rental' ?[styles.propSearchItem, styles.propSearchItemSelected] : [styles.propSearchItem]} onPress={() => {this._propertySelect('room_rental')}}>
                          <Image
                            style={{width: 26, height: 26, marginBottom: 5}}
                            source={this.state.listing_type === 'room_rental' ? require('../../../assets/images/room.png') : require('../../../assets/images/room-alt.png')}
                          />
                          <Text allowFontScaling={false} style={[styles.textSm, styles.propSearchItemText, this.state.listing_type === 'room_rental' ? {color: '#fff'} : {}]}>For Room Rental</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.contentSection}>
                  <View style={styles.propSearchSection}>
                    <TouchableOpacity style={styles.CheckBoxWrapper} onPress={() => this._categorySelect('residential')}>
                      <Icon
                        name={this.state.category === 'residential'? 'radio-button-checked' : 'radio-button-unchecked'}
                        type='material'
                        color={this.state.category === 'residential'? '#3670e5' : '#cccccc'}
                      />
                      <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 10}]}>Residential</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.CheckBoxWrapper} onPress={() => this._categorySelect('non-residential')}>
                      <Icon
                        name={this.state.category === 'non-residential'? 'radio-button-checked' : 'radio-button-unchecked'}
                        type='material'
                        color={this.state.category === 'non-residential'? '#3670e5' : '#cccccc'}
                      />
                      <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 10}]}>Non-Residential</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.contentSection}>
                  <View style={styles.CheckBoxWrapper}>
                    <CheckBox
                      title='Is Featured'
                      iconType='material'
                      checkedIcon='check-box'
                      uncheckedIcon='check-box-outline-blank'
                      checkedColor='#3670e5'
                      checked={this.state.featured}
                      containerStyle={styles.checkBox}
                      textStyle={styles.textSm}
                      onPress={() => this.setState({featured: !this.state.featured})}
                    />
                  </View>
                </View>
              </ScrollView>
            </View>
            <View style={styles.bottomActions}>
              <Button
                containerStyle={styles.buttonContainer}
                buttonStyle={styles.successBtn}
                titleStyle={styles.successBtnTitle}
                title="Search"
                onPress={this._goToListing}
              />
            </View>
          </View>
          {/* Overlay */}
          <Overlay
            isVisible={this.state.isVisible}
            overlayStyle={[styles.overlayStyle]}
            containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
            width="92%"
            height="auto"
            onBackdropPress={() => this.setState({ isVisible: false })}
          >
            <View style={[styles.overlayInner, styles.bgWarning]}>
              <Icon
                name='info'
                type='material'
                color='#fff'
                size={26}
              />
              <Text allowFontScaling={false} style={styles.overlayText}>Hello from Overlay!</Text>
            </View>
          </Overlay>
        </SafeAreaView>
      </View>
    );
  }
}

export default SearchOption;