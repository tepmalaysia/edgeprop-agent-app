import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    padding: 30,
    paddingHorizontal: 60,
    margin: 0,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center', 
    alignItems: 'center',
  },
  navigation: {
    width: '100%',
    padding: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)',
    alignItems: 'flex-start'
  },
  errorMsg: {
    color: 'red', 
    marginBottom: 0
  },
  inputWrapper: {
    width: '100%',
    marginTop: 20
  },
  inputContainer: {
    borderBottomWidth: 0,
    paddingLeft: 0,
    paddingRight: 0,
    margin: 0,
  },
  inputField: {
    flex: 1,
    height: 45,
    paddingHorizontal: 20,
    paddingVertical: 13,
    backgroundColor: '#e6ecf2',
    borderRadius: 50,
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#10182d',
  },
  hTwo: {
    fontSize: width * 0.06,
    fontFamily: 'OpenSans-Bold',
    color: '#10182d',
    lineHeight: width * 0.1,
    marginBottom: 10
  },
  textSm: {
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#10182d',
    lineHeight: width * 0.04,
  },
  formWrapper: {
    paddingTop: 30
  },  
  actionButton: {
    width: '100%',
    paddingVertical: 13,
    paddingHorizontal: 20,
    height: 45,
    backgroundColor: '#FFF',
    borderRadius: 50,
    shadowOffset:{width: 2,  height: 2,},
    shadowColor: '#acb4d1',
    shadowOpacity: 0.6,
    elevation: 3,
    marginVertical: 20
  },
  actionButtonText: {
    fontSize: width * 0.032,
    fontFamily: 'OpenSans-Bold',
    textTransform: 'uppercase',
    width: '100%',
    textAlign: 'center',
    color: '#3670e5'
  },
  overlayStyle: {
    position: 'absolute',
    bottom: 15,
    left: 'auto',
    shadowOffset:{width: 0,  height: 5,},
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOpacity: 0.6,
    elevation: 5,
    },
  overlayContainer: {
    backgroundColor: 'rgba(255, 255, 255, .35)',
  },
  overlayInner: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  overlayText: {
    color: '#fff',
    fontSize: width * 0.032,
    fontFamily: 'OpenSans-SemiBold',
    paddingLeft: 10,
    flexWrap: 'wrap',
    flex: 1
  },
  bgDanger: {
    backgroundColor: '#f44336'
  },
  bgWarning: {
    backgroundColor: '#ffc107'
  },
  bgSuccess: {
    backgroundColor: '#00c853'
  },
  bgInfo: {
    backgroundColor: '#3670e5'
  }
});
