import React, { Component } from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Alert
} from 'react-native';
import { Input, Overlay, Icon } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import styles from './ForgotPasswordStyle.js';
import { ResetPassword } from '../../services/api'
import Loading from '../../../components/Loader';

const HOSTNAME = 'https://www.edgeprop.my/jwdalice/api/user/v1/pwd-forgot';
const {width, height} = Dimensions.get('window');
var welcome = require('../../../assets/images/splash.png');
var emailValidate = /^[a-zA-Z0-9.]+@[a-zA-Z0-9.]+\.[a-zA-Z]{2,4}/;

export default class ForgotPassword extends Component {
  constructor(props) {
      super(props);
      this.state = {
          timePassed: false,
          getStarted: false,
          email: '',
          toLogin: false,
          emailError: '',
          emailErrorFlag: true,
          toProfile: false,
          checking: 0,
          isLoading: false,
          isVisible: false,
          apiError: '',
          status: '',
        };
     this._onPressButton = this._onPressButton.bind(this);
     this._onResetPassword = this.__onResetPassword.bind(this);
     this._onForgetPress = this._onForgetPress.bind(this)
    }
    __onResetPassword() {
      this.props.navigation.goBack();
    }
    _onPressButton() {
      this.setState({ getStarted: true })
      /*this.refs.navigationHelper._navigate('AppNav', {
        data: ''
      })*/
      this.props.navigation.navigate('SignedIn');
    }

    _onForgetPress = () => {
      this.state.checking = 1;
      let isValid = emailValidate.test(this.state.email);
      if(this.state.email.length > 0 && isValid) { 
        this.setState({ emailError: 'Valid Email', emailErrorFlag: false })
          this._forgotPassword();
      } else {
        this.setState({ emailError: 'Please enter valid Email id', emailErrorFlag: true });
      }
    }

    _forgotPassword = () => {
      const deviceType = Platform.OS === 'ios' ? 2 : 1;
        const body = "email="+ this.state.email
                    
        this.setState({ isLoading: true })
        ResetPassword(HOSTNAME, body)
        .then(data => this.ResetPwdSuccess(data))
    }

    ResetPwdSuccess = (response) => {
    if(response.status == 0) {
        this.setState({ isLoading: false })
        this.setState({ isVisible: true, apiError: response.msg, status: response.status });
      }
    if(response.status == 1) {
      this.setState({ isLoading: false })
      this.setState({ isVisible: true, apiError: response.msg, status: response.status });
    }
  }

  _showAlert = (msg) => {
      Alert.alert(
        msg
      );
    }

    validateForgotPwd = (email) => {
      var re = /^[a-zA-Z0-9.]+@[a-zA-Z0-9.]+\.[a-zA-Z]{2,4}/;
        return re.test(email);
    };

    render() {
      /*if(this.state.getStarted) {
        return (<SignupLanding />)
      }*/
      let emailErrorDisplay = '';
      if(this.state.checking) {
        if(this.state.email == '') {
          emailErrorDisplay = 'Required Email'
        } else { 
            if(this.state.email.length > 0 && !this.validateForgotPwd(this.state.email)) {
              emailErrorDisplay = 'Please enter valid Email'
            }
        }
      }

      return (
        <View>
          <SafeAreaView style={{height: '100%'}}>
          <TouchableOpacity onPress={()=> this.__onResetPassword()} style={styles.navigation}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#10182d'
            />
          </TouchableOpacity>
          <KeyboardAwareScrollView contentContainerStyle={{flex: 1}} keyboardShouldPersistTaps='handled'>
            <View style={styles.container} >  
              <View style={{width: '100%'}}>
                <View style={styles.loginInfo}>
                  <Text allowFontScaling={false} style={[styles.hTwo, {textAlign: 'center'}]}>Forgot Password?</Text>
                  <Text allowFontScaling={false} style={[styles.textSm, {textAlign: 'center'}]}>Don’t worry, please enter your registered user details here!</Text>
                </View>
                <View style={styles.formWrapper}>
                  <View style={styles.inputWrapper}>
                    <Input 
                      placeholder='email'
                      value={this.state.email}
                      inputContainerStyle={{borderBottomWidth: 0}} 
                      containerStyle={styles.inputContainer} 
                      inputStyle={styles.inputField}
                      errorMessage={this.state.checking ? emailErrorDisplay : ''}
                      onChangeText={(email) => this.setState({email: email.trim()})}
                    />
                  </View>
                  <TouchableOpacity style={[styles.actionButton, {backgroundColor: '#3670e5'}]} onPress={()=> this._onForgetPress()}>
                    <Text allowFontScaling={false} style={[styles.actionButtonText, {color: '#FFF'}]}>Reset Password</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>         
          </KeyboardAwareScrollView>

          {/* Overlay */}
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={this.state.status == 0 ? [styles.overlayStyle, styles.bgDanger] : [styles.overlayStyle, styles.bgSuccess]}
              containerStyle={styles.overlayContainer}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible: false })}
            >
              <View style={styles.overlayInner}>
                <Icon
                  name='info'
                  type='material'
                  color='#fff'
                  size={30}
                />
                <Text allowFontScaling={false} style={styles.overlayText}>{this.state.apiError}</Text>
              </View>
            </Overlay>

          </SafeAreaView>
          {
          this.state.isLoading && (
            <Loading />
          )
        }
        </View>
        );
      }
}