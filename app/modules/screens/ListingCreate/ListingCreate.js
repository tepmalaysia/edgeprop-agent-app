/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    Switch,
    Dimensions,
    findNodeHandle
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { CheckBox, Icon, Input, Button, Overlay } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import qs from "qs";
import moment from 'moment';
import {NavigationActions, StackActions} from 'react-navigation';
import styles from './ListingCreateStyles';
import OverlayStyle from '../../styles/overlayStyle';

import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import BasicInfo from './BasicInfo';
import AdditionalInfo from './AdditionalInfo';
import ListingActions from '../../../../realm/actions/ListingActions'
import { getDataForAdd, assginData } from '../../services/formValidation'
import { PostData, FetchOne } from '../../services/api'
import Loading from '../../../components/Loader';

const { width, height } = Dimensions.get('window');
const arrSubPropTypeShowLandAreaOnly = [72, 69, 67, 43];
const arrSubPropTypeShowBuildUpOnly = [37,38,39,40,41,42,44,61, 62];
const arrSubPropTypeShowBoth = [63, 64, 65, 66, 68, 71, 73];

let tabCheck = 0;
let currentTab = 'basic';
const FETCHONE = 'https://prolex.edgeprop.my/api/v1/fetchOne';
const AUTOREPOST = 'https://prolex.edgeprop.my/api/v1/schedule/repost/add';
class ListingCreate extends Component{

  constructor(props){
    scrollViewRef = React.createRef();
    super(props);
    this.ListingActions = new ListingActions();
    //this.data = props.navigation.state.params.data? props.navigation.state.params.data : {};
    this._handleBackPress = this._handleBackPress.bind(this);
    this._onSearchTag = this._onSearchTag.bind(this);
    this._onUploadImage = this._onUploadImage.bind(this);
    this._nextTab = this._nextTab.bind(this);
    this.setBasicData = this.setBasicData.bind(this)
    this._saveProperty = this._saveProperty.bind(this)
    this._publishProperty = this._publishProperty.bind(this)
    this._setForm = this._setForm.bind(this)
    this.checkForm = this.checkForm.bind(this)
    this._updateSuccess = this._updateSuccess.bind(this)
    this._updateFail = this._updateFail.bind(this)
    this.clearState = this.clearState.bind(this)
    this._showError = this._showError.bind(this)
    this.scrollToNode = this.scrollToNode.bind(this)
    this._fetchOne = this._fetchOne.bind(this)
    this.fetchOneSuccess = this.fetchOneSuccess.bind(this)
    this.fetchOneFail = this.fetchOneFail.bind(this)
    this.addAutoRepost = this.addAutoRepost.bind(this)
    this._callFetchOneAFterAdd = this._callFetchOneAFterAdd.bind(this)
    this._updateRepostSuccess = this._updateRepostSuccess.bind(this)

    let listingDetails = [];
    let listingInfo = listingDetails.length>0? JSON.parse(listingDetails[0].data) : {};

    let parsedData = assginData(listingInfo);
    
    //parsedData.listing_type = 'sale';
    //parsedData.property_type = 33;
    
    let values = {
      ...parsedData,
      landAreaPerUnit: '',
      builtAreaPerUnit: '',
      nid: '',
      mid: '',
      created: '',
      property_auction_day: moment().format('DD'),
      property_auction_month: moment().format('MM'),
      property_auction_year: parseInt(moment().format('YYYY')),
      property_auction_ref: ''
    }
    
    
    this.state = {
      trigger: false,
      action: 'save',
      userInfo: {},
      isVisible: false,
      isError: false,
      responseInfo: '',
      isLoading: false,
      validate: {flag: false,form: {}},
      listingDetails: {},
      status : false,
      featured: false,
      property_images: [],
      isPrompt: false,
      isAutoRepost: false,
      propType: '',
      pastTransactionDetail: {},
      ...values  
    }
    this.baseState = this.state;
    currentTab = 'basic';
    tabCheck = 0;
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems })
        this.baseState.userInfo = authItems;
      }
    }
  }

  clearState(lid, action){
        
    currentTab = 'basic';
    tabCheck = 0;
    let message = action == 1? 'Your listing is published successfully.' : 'Your listing is saved successfully.';
    this.props.navigation.navigate(
      'DrawerNavigator', 
      {}, 
      NavigationActions.navigate(
        { 
          routeName: 'Home', 
          params: { 
            data:{
              listing_type: this.state.listing_type 
            },
            message,
            timeStamp: moment().unix(),
            tabIndex: action == 1 ? 1 : 0
          },
          action: {}
        })
    );
  }

  _showError(data){
    this.setState({
      isVisible: true,
      isError: data.error? true : false,
      responseInfo: data.message? data.message : 'Some error has occured.'
    })
  }
  

  _setForm(data){
    Object.keys(data).forEach(key=>{
        
        this.setState({
          [key] : data[key]
        })
    });
  }
  _handleBackPress() {
    if(this.state.isChanged){
      this.setState({
        isPrompt: true
      })
      return;
    }else{
      this.props.navigation.goBack();
    }
  }

  _onSearchTag() {
    this.props.navigation.navigate('SearchTag');
  }

  _onUploadImage() {
    this.props.navigation.navigate('UploadImage',{
      data: {
        images : this.state.property_images,
        listingDetails : this.state.listingDetails,
        setForm : this._setForm
      }
    });
  }

  _checkSize(data){
    let size = data.length? data.length : 0;
    if(typeof data == 'object'){
      size = Object.keys(data).length;
     }
    return size;
  }

  _nextTab(){
    let validate = this.checkForm(this.state, 'next');
    
    this.setState({validate})
    if(!validate.flag){
      tabCheck = validate.flag == false ? 1 : 0;
      currentTab = 'additional';
      this.props.navigation.navigate('ADDITIONAL INFO',{
        data: {
          viaNav: true,
          listingInfo: this.state.listingDetails
        }
      })
    }
  }

  _publishProperty(){
    let validate = this.checkForm(this.state, 'publish');
    
    this.setState({validate}, () => {
      if(!validate.flag){
        let postData = {
          ...getDataForAdd(this.state, 'edit'),
          status: 1,
          field_prop_status: "active",
          token: this.state.userInfo.token,
          uid: this.state.userInfo.uid
        }
        if(this.state.mid){
          postData.mid = this.state.mid
        }
        
        this.setState({isLoading: true})
        PostData('https://prolex.edgeprop.my/api/v1/add', qs.stringify(postData))
        .then(data => this._updateSuccess(data, 1))
        .catch(error =>this._updateFail(error));
      }else{
        if(currentTab == 'additional'){
          currentTab = 'basic';
          this.props.navigation.navigate('BASIC INFO')
        }
        let count = this.checkForIssues(this.state.validate.form);
        if(count == 1 && this.state.validate.form.property_images && this.state.validate.form.property_images.error){
          this.setState({
            isVisible: true,
            isError: true,
            responseInfo: 'Please upload images for Publishing'
          })
        }
      }
    })
    
  }

  checkForIssues(data){
    let count = 0;
    Object.keys(data).length > 0 && Object.entries(data).map(([key, value]) => {
      
      if(value.error){
        count++;
      }
    })
    return count;
  }

  _saveProperty(){
    let validate = this.checkForm(this.state, 'save');
    this.setState({validate}, () => {
      if(!validate.flag){
        let postData = {
          ...getDataForAdd(this.state, 'edit'),
          status: 0,
          field_prop_status: "draft",
          token: this.state.userInfo.token,
          uid: this.state.userInfo.uid
        }
        if(this.state.mid){
          postData.mid = this.state.mid
        }
        
        this.setState({isLoading: true})
        PostData('https://prolex.edgeprop.my/api/v1/add', qs.stringify(postData))
        .then(data => this._updateSuccess(data, 0))
        .catch(error =>this._updateFail(error));
      }else{
        if(currentTab == 'additional'){
          currentTab = 'basic';
          this.props.navigation.navigate('BASIC INFO')
        }
        let count = this.checkForIssues(this.state.validate.form);
        if(count == 1 && this.state.validate.form.property_images && this.state.validate.form.property_images.error){
          this.setState({
            isVisible: true,
            isError: true,
            responseInfo: 'Please upload images for Publishing'
          })
        }
      }
    })
    
  }

  addAutoRepost() {
    var currentDate = moment();
    let start = moment().format("YYYY-MM-DD");
    var end = moment(currentDate).add(1, 'M');
    var futureMonthEnd = moment(end).endOf('month');

    if(currentDate.date() != end.date() && end.isSame(futureMonthEnd.format('YYYY-MM-DD'))) {
        end = futureMonth.add(1, 'd');
    }
    end = moment(end).format("YYYY-MM-DD");
    let days = moment().format("dddd");

    let todayNow = new Date();
    let hours = todayNow.getHours();
    let minutes = todayNow.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    let roundMin = Math.round(minutes/5)*5;
    let time = hours + ':' + roundMin + ' ' + ampm;

    const postData = "uid=" + this.state.userInfo.uid +
                "&token=" + this.state.userInfo.token +
                "&lid[0][id]=" + parseInt(this.state.created_id) +
                "&lid[0][type]=" + this.state.propType +
                "&property_id=" + parseInt(this.state.created_id) +
                "&start=" +start+
                "&end=" +end+
                "&days=" +days+
                "&time=" +time+
                "&type=" +this.state.listing_type;

    this.setState({isLoading: true})
    PostData(AUTOREPOST, postData)
        .then(data => this._updateRepostSuccess(data))
        .catch(error =>this._updateFail(error));

  }

  _updateRepostSuccess(data) {
    
    if(data.status == "success" && data.status_code == 1) {
      this._callFetchOneAFterAdd();
    }else {
      let message = data.message? data.message : 'Unable to update the property.'
      this.setState({
        isVisible: true,
        isError: true,
        responseInfo: message,
        isLoading: false
      });
    }
    
  }

  _callFetchOneAFterAdd() {
    const data = "uid=" + this.state.userInfo.uid +
                "&token=" + this.state.userInfo.token +
                "&field_prop_listing_type=" + this.state.listing_type +
                "&property_id=" + parseInt(this.state.created_id) +
                "&cache=0" +
                "&source=edit"+
                "&type=m";
    this._fetchOne(data);
  }



  _updateSuccess(data, action){
    
    if(data && data.status == 'success'){
      if(action == 1) {
        this.setState({
          created_id: data.id,
          property_action: action,
          isChanged: false,
          isAutoRepost: true,
          propType: data.type
        });
      }else {
        this.setState({
          created_id: data.id,
          property_action: action,
          isChanged: false,
          propType: data.type
        }, ()=> {
          this._callFetchOneAFterAdd()
        });
      }
    }else{
      let message = data.message? data.message : 'Unable to update the property.'
      if(typeof data.duplicate_mid != "undefined" && typeof data.duplicate_nid != "undefined"){
        let listingID='';
        if(data.duplicate_nid == 0 && data.duplicate_mid != 0){
          listingID = "LIDM"+data.duplicate_mid;
        }else{
          listingID = "LIDN"+data.duplicate_nid;
        }
        if(message == 'We have found a similar listing. Please click on the Listing ID to compare:'){
          message = 'We have found a similar listing. Please find the listing id: '+listingID;
        }
        
      }
      this.setState({
        isVisible: true,
        isError: true,
        responseInfo: message,
        isLoading: false
      });
    }
  }

  _updateFail(error){
    
    this.setState({
        isVisible: true,
        isError: true,
        responseInfo: 'Update Failed',
        isLoading: false
    });
  }

  checkForm (data, action){
    this.setState({action: action})
    let response = {
      flag: false,
      scrollToNode: null,
      timeStamp: moment().unix(),
      form: {
        listing_type : { error : false },
        property_type: {  error : false },
        sub_property_type: {  error : false },
        property_name: {  error : false },
        property_state: {  error : false },
        property_district: {  error : false },
        property_asking_price: {  error : false },
        property_land_area: {  error : false },
        property_built_area: {  error : false },
        property_built_area_unit: {  error : false },
        property_other_asset: {  error : false },
        property_images : {  error : false },
        property_postcode: {  error : false },
        property_auction_day: {  error : false },
        property_auction_month: {  error : false },
        property_auction_year: {  error : false },
        property_auction_ref: {  error : false },
        othersAsset: {  error : false },
        autoComplete: { error : false }
      }
    };
    let flag = false;
    if(!data.listing_type){
      let listing_type = {error : true, label: 'Property listing type', validate : {required: true}};
      response.form.listing_type = listing_type;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_listing_type';
      }
    }
    if(!data.property_type){
      let property_type = {error : true, label: 'Property type', validate : {required: true}};
      response.form.property_type = property_type;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_listing_type';
      }
    }
    if(!data.sub_property_type && (data.property_type || action == 'publish')){
      let sub_property_type = {error : true, label: 'Property sub-type', validate : {required: true}};
      if(!data.property_type){
        sub_property_type = {error : true, label: 'Please select Property Type first!', validate : {inValid: true}};
      }
      response.form.sub_property_type = sub_property_type;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_type';
      }
    }
    if(!data.property_name){
      let property_name = {error : true, label: 'Advertisement Heading / Name', validate : {required: true}};
      response.form.property_name = property_name;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_sub_property_type';
      }
    }if(data.property_name.length > 50){
      let property_name = {error : true, label: 'Advertisement Heading / Name', validate : {maxLength: true}};
      response.form.property_name = property_name;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_sub_property_type';
      }
    }

    if((this.isEmpty(data.asset) || (data.asset &&data.asset.reset == true)) &&  action == 'publish' ){
      if(response.form.property_type.error){
        response.form.autoComplete = {error : true, label: 'Please select Property Type first!', validate : {inValid: true}};;
      }else{
        let autoComplete = {error : true, label: 'Choose an existing project by searching.', validate : {inValid: true}};
        response.form.autoComplete = autoComplete;
      }
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_name';
      }
    }
    
    if(!data.property_other_asset &&  data.property_search_others){
      let property_other_asset = {error : true, label: 'Property assets', validate : {required: true}};
      response.form.property_other_asset = property_other_asset;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_name';
      }
    }

    if(!data.othersAsset && data.searchOthers){
      let othersAsset = {error : true, label: 'Other Project Name/ Address', validate : {required: true}};
      response.form.othersAsset = othersAsset;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_buildingName';
      }
    }

    if(!data.property_state &&  action == 'publish'){
      let property_state = {error : true, label: 'Property state', validate : {required: true}};
      response.form.property_state = property_state;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_buildingName';
      }
    }
    if(!data.property_district &&  action == 'publish'){
      let property_district = {error : true, label: 'Property area/district', validate : {required: true}};
      response.form.property_district = property_district;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_buildingName';
      }
    }
    if(!data.property_asking_price &&  action == 'publish'){
      let property_asking_price = {error : true, label: 'Property asking price', validate : {required: true}};
      response.form.property_asking_price = property_asking_price;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_postcode';
      }
    }else if(data.property_asking_price){
      let decimal = false;
      let maxLength = false;
      if(!data.property_asking_price.toString().match( /^(\d+\.?\d*|\.\d+)$/ )){
        flag = decimal = true;
        if (!response.scrollToNode) {
          response.scrollToNode = 'ref_property_postcode';
        }
      }
      if(data.property_asking_price.length > 11){
        flag = maxLength = true;
        if (!response.scrollToNode) {
          response.scrollToNode = 'ref_property_asking_price';
        }
      }
      let property_asking_price = {error : flag, label: 'Property asking price', validate : {decimal: decimal, maxLength : maxLength}};
      response.form.property_asking_price = property_asking_price;
    }
    if(!data.property_auction_ref &&  (action == 'publish' && data.listing_type == 'auction')){
      let property_auction_ref = {error : true, label: 'Property auction number', validate : {required: true}};
      response.form.property_auction_ref = property_auction_ref;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_asking_price';
      }
    }

    if(data.property_postcode &&  data.property_postcode.length > 5){
      let property_postcode = {error : true, label: 'Property postcode', validate : {maxLength: true}};
      response.form.property_postcode = property_postcode;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_district';
      }
    }
    if(!data.property_auction_day && data.listing_type == 'auction' && action == 'publish'){
      let property_auction_day = {error : true, label: 'Property auction day', validate : {required: true}};
      response.form.property_auction_day = property_auction_day;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_postcode';
      }
    }
    if(!data.property_auction_month && data.listing_type == 'auction' && action == 'publish'){
      let property_auction_month = {error : true, label: 'Property auction month', validate : {required: true}};
      response.form.property_auction_month = property_auction_month;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_postcode';
      }
    }
    if(!data.property_auction_year && data.listing_type == 'auction' && action == 'publish'){
      let property_auction_year = {error : true, label: 'Property auction year', validate : {required: true}};
      response.form.property_auction_year = property_auction_year;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'ref_property_postcode';
      }
    }
    if(data.property_auction_day && data.property_auction_month && data.property_auction_year && data.listing_type == 'auction'){
      let check = this.dateValidate(data.property_auction_day,data.property_auction_month,data.property_auction_year);
      if(check){
        let property_auction_date = {error : true, label: 'Only today and future date is valid', validate : {inValid: true}};
        response.form.property_auction_day = property_auction_date;
        response.form.property_auction_month = property_auction_date;
        response.form.property_auction_year = property_auction_date;
        flag = true;
        if (!response.scrollToNode) {
          response.scrollToNode = 'ref_property_postcode';
        }
      }
    }

    if(data.property_land_area){
      let decimal = maxLength = false;
      if(!data.property_land_area.toString().match( /^(\d+\.?\d*|\.\d+)$/ )){
        flag = decimal = true;
        if (!response.scrollToNode) {
          response.scrollToNode = data.listing_type == 'auction'? 'ref_property_auction_ref' : 'ref_property_asking_price';
        }
      }
      if(data.property_land_area.length > 11){
        flag = maxLength = true;
        if (!response.scrollToNode) {
          response.scrollToNode = 'auction'? 'ref_property_auction_ref' : 'ref_property_asking_price';
        }
      }
      let property_land_area = {error : flag, label: 'Property land area', validate : {decimal: decimal, maxLength : maxLength}};
      response.form.property_land_area = property_land_area;
    }
    if(!data.property_built_area &&  (action == 'publish' && this.showBuildUp())){
      let property_built_area = {error : true, label: 'Property built area', validate : {required: true}};
      response.form.property_built_area = property_built_area;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'auction'? this.showLanded()? 'ref_property_land_area_unit' :'ref_property_auction_ref' : 'ref_property_asking_price';
      }
    }else if(data.property_built_area){
      let decimal = maxLength = false;
      if(!data.property_built_area.toString().match( /^(\d+\.?\d*|\.\d+)$/ )){
        flag = decimal = true;
        if (!response.scrollToNode) {
          response.scrollToNode = 'auction'? this.showLanded()? 'ref_property_land_area_unit' :'ref_property_auction_ref' : 'ref_property_asking_price';
        }
      }
      if(data.property_built_area.length > 11){
        flag = maxLength = true;
        if (!response.scrollToNode) {
          response.scrollToNode = 'auction'? this.showLanded()? 'ref_property_land_area_unit' :'ref_property_auction_ref' : 'ref_property_asking_price';
        }
      }
      let property_built_area = {error : flag, label: 'Property built area', validate : {decimal: decimal, maxLength : maxLength}};
      response.form.property_built_area = property_built_area;
    }
    if(!data.property_built_area_unit &&  (action == 'publish' && this.showBuildUp())){
      let property_built_area_unit = {error : true, label: 'Property built area unit', validate : {required: true}};
      response.form.property_built_area_unit = property_built_area_unit;
      flag = true;
      if (!response.scrollToNode) {
        response.scrollToNode = 'auction'? this.showLanded()? 'ref_property_land_area_unit' :'ref_property_auction_ref' : 'ref_property_asking_price';
      }
    }
    
    if((!data.property_images ||  data.property_images.length == 0) && action == 'publish'){
      let property_images = {error : true, label: 'Property image(s)', validate : {required: true}};
      response.form.property_images = property_images;
      flag = true;
    }
    
    response.flag = flag;
    return response;
  }

  showLanded = () => {
      if (this.state.property_type == "33") return false;
      else if (this.state.property_type == "36") return true;
      else if (this.state.property_type == "60") return true;
      else if (this.state.property_type == "70") return true;
      else {
        let v = parseInt(this.state.sub_property_type);
        if (
        arrSubPropTypeShowLandAreaOnly.find(value => value == v) ||
        arrSubPropTypeShowBoth.find(value => value == v)
        )
        return true;
      }
      return false;
    }

  isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }

  dateValidate(day,month,year) {
    
    let flag = false;
    let yesterday = moment().add(-1, 'days').format("YYYY-MM-DD");
    
    if(moment(day+'/'+month+'/'+year, 'DD/MM/YYYY').isValid())
    {
      if(!moment(year+'-'+month+'-'+day).isAfter(yesterday))
      {
        //date invalid
        
        flag = true;
      }else{
        //date is valid
        
        flag = false;
      }
    }else{
      //date invalid
      
      flag = true;
    }
    return flag;
  }

  showBuildUp = () => {
    
      if (this.state.property_type == "33") return true;
      //else if (state.propertyType == "36") return true;
      else {
        let v = parseInt(this.state.sub_property_type);
        if (
        arrSubPropTypeShowBuildUpOnly.find(value => value == v) ||
        arrSubPropTypeShowBoth.find(value => value == v)
        )
          return true;
      }
      return false;
  }

  setBasicData(data, state, action){
    if(action == 'next'){
      tabCheck = data.flag == false ? 1 : 0;
      if(tabCheck == 1){
        currentTab = 'additional';
        this.props.navigation.navigate('ADDITIONAL INFO',{
          data: {
            viaNav: true,
            listingInfo: state
          }
        })
      }
    }else if(action == 'save'){
      if(!data.flag){
        let postData = {
          ...getDataForAdd(state),
          status: 0,
          field_prop_status: "draft",
          token: this.state.userInfo.token,
          uid: this.state.userInfo.uid
        }
        
      }
    }
    
  }


  initProperty(){
    let cover_image = this.state.listingDetails.cover_image;
    let images      = this.state.property_images? this.state.property_images: [];
     
    let default_images = 'https://list.edgeprop.my/static/img/no-image.png';
    cover_image = cover_image? cover_image : images[0] ? images[0].gallery_uri? images[0].gallery_uri :  default_images : default_images;
    
    let is_public_photo = cover_image.search("public://");
    if(is_public_photo == 0){
      cover_image = default_images;
    }
    let listingInfo = this.state.listingDetails.data? JSON.parse(this.state.listingDetails.data) : {};

    return {
      lid: this.state.listingDetails.mid? `LIDM${this.state.listingDetails.mid} ` : `LIDN${this.state.listingDetails.nid} `,
      cover_image,
      image_count: this._checkSize(images)
    }
  }

  formatListing = (data) => {
    

    let asset = data.asset;
    let listing = {};

    listing.lid = data.mid ? parseInt(data.mid) : parseInt(data.nid);

    listing.nid = data.nid ? parseInt(data.nid) : null;

    listing.mid = data.mid ? parseInt(data.mid) : null;

    listing.title = data.title ? data.title : '';

    listing.status = data.status ? parseInt(data.status) : 0;
  
    listing.created = data.created ? data.created : '';

    listing.changed = data.changed ? data.changed : '';

    listing.district = data.district ? data.district.toString() : '';

    listing.state = data.state ? data.state.toString() : '';

    listing.project_name = asset.project_name ? asset.project_name : '';

    listing.project_id = asset.id ? asset.id : '';

    listing.prop_status = (data && data.field_prop_status && data.field_prop_status.und && Array.isArray(data.field_prop_status.und)) ? (data.field_prop_status.und[0]? data.field_prop_status.und[0].value : '') : '';

    listing.listing_type = (data && data.field_prop_listing_type && data.field_prop_listing_type.und && Array.isArray(data.field_prop_listing_type.und)) ? (data.field_prop_listing_type.und[0]? data.field_prop_listing_type.und[0].value : '') : '';

    listing.property_type = (data && data.field_property_type && data.field_property_type.und && Array.isArray(data.field_property_type.und)) ? (data.field_property_type.und[0]? data.field_property_type.und[0].target_id : '') : '';

    listing.asset_id = (data && data.field_prop_asset && data.field_prop_asset.und && Array.isArray(data.field_prop_asset.und)) ? (data.field_prop_asset.und[0]? data.field_prop_asset.und[0].target_id : '') : '';

    listing.price = (data && data.field_prop_asking_price && data.field_prop_asking_price.und && Array.isArray(data.field_prop_asking_price.und)) ? (data.field_prop_asking_price.und[0]? data.field_prop_asking_price.und[0].value : null) : null;

    listing.street = (data && data.field_prop_street && data.field_prop_street.und && Array.isArray(data.field_prop_street.und)) ? (data.field_prop_street.und[0]? data.field_prop_street.und[0].value : null) : null;
    
    let coverImages = (data && data.field_cover_image && data.field_cover_image.und && Array.isArray(data.field_cover_image.und)) ? data.field_cover_image.und : [];
    listing.cover_image = (coverImages && coverImages[0] && coverImages[0].uri)? coverImages[0].uri: '';
    
   
    let images = (data && data.field_prop_images && data.field_prop_images.und && Array.isArray(data.field_prop_images.und)) ? data.field_prop_images.und : [];
    let list_uri = [];
    list_uri = images.map((item, index) => { return item.list_uri });
    listing.images = list_uri;

    listing.data = JSON.stringify(data);
    listing.url = data.url ? data.url : '';
    return listing;
  }

  _fetchOne(body) {
    FetchOne(FETCHONE, body)
    .then(data => this.fetchOneSuccess(data))
    .catch(error => this.fetchOneFail(error));
  }

  fetchOneSuccess(response, flag, action){
    
    if(response.result){
      const listingData = this.formatListing(response.result);
      let result = {...listingData, 'token': this.state.userInfo.token, 'uid': parseInt(this.state.userInfo.uid)};
      let listItem = this.ListingActions.GetListing(result.lid);
      if(listItem && listItem.length >0){
        this.ListingActions.ClearListing(result.lid);
      }
      this.ListingActions.CreateListing(result);
    }
    this.setState({ isLoading: false })
    this.clearState(this.state.created_id, this.state.property_action);
  }

  fetchOneFail(error){
    this.setState({ isLoading: false })
    this.clearState(this.state.created_id, this.state.property_action);
  }

  scrollToNode = (node) => {
    if (node && this.scrollViewRef) {
      node.measureLayout(
        findNodeHandle(this.scrollViewRef), (x, y) => {
          this.scrollViewRef.scrollTo({ x: 0, y: y, animated: true });
        });
    }
  }

  render() {
    let viewData = this.initProperty()
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          enableResetScrollToCoords={true}
          enableOnAndroid={true}
          keyboardShouldPersistTaps={'handled'}
          ref={component => this.scrollViewRef = component}
        >
          {/*  Property Image */}
          <View style={styles.propImgWrapper}>
            <ImageBackground source={{uri: viewData.cover_image}} style={styles.cardImage}>
                <LinearGradient colors={['rgba(0,0,0,0.7)', 'transparent', 'rgba(0,0,0,1)']} style={styles.cardOverlay}>
                  <SafeAreaView>
                    <TouchableOpacity style={styles.navLeft} onPress={this._handleBackPress}>
                      <Icon
                        name='arrow-left'
                        size={26}
                        type='feather'
                        color='#fff'
                      />
                    </TouchableOpacity>
                  </SafeAreaView>
                  <View style={[styles.cardOverlayInner, styles.cardOverlayInnerCenter]}>
                    <TouchableOpacity onPress={this._onUploadImage} style={[styles.cardOverlayEditImage]}>
                      <Icon
                        name='edit-2'
                        size={24}
                        type='feather'
                        color='#fff'
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.cardOverlayInnerDetail}>
                    <View style={[styles.cardOverlayInner, styles.cardOverlayInnerRight]}>
                      {viewData.image_count >0 &&<View style={[styles.cardOverlayItem, styles.cardOverlayItemRight]}>
                        <Text allowFontScaling={false} style={styles.cardOverlayText}>1/{viewData.image_count}</Text>
                        <Image
                          style={{width: 15, height: 15}}
                          source={require('../../../assets/images/gallery.png')}
                        />
                      </View>}
                    </View>
                  </View>
                </LinearGradient>
            </ImageBackground>
          </View>
          
          
        <BasicInfo            
              listingDetails = {this.state}
              status = {this.state.status}
              setForm = {this._setForm}
              validate = {this.state.validate}
              trigger = {this.state.trigger}
              navigation = {this.props.navigation}
              errorHandler={this._showError}
              onScrollToNode={this.scrollToNode}
              checkForm ={this.checkForm}
              action={this.state.action}
          />
          <AdditionalInfo 
              listingDetails = {this.state.listingDetails}
              status = {this.state.status}
              setForm = {this._setForm}
              validate = {this.state.validate}
              trigger = {this.state.trigger}
              navigation = {this.props.navigation} 
          />
        </KeyboardAwareScrollView>
        {/* Bulk Actions */}
        <SafeAreaView >
          <View style={styles.bottomActions}>
            <Button
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.successBtn}
              titleStyle={styles.successBtnTitle}
              title="Save"
              onPress={this._saveProperty}
            />
            <Button
              containerStyle={styles.buttonContainer}
              buttonStyle={[styles.successBtn, {backgroundColor: '#149d78'}]}
              titleStyle={[styles.successBtnTitle, {color: '#fff'}]}
              title="Publish"
              onPress={this._publishProperty}
            />
          </View>

          {/* Overlay */}
          <Overlay
            isVisible={this.state.isPrompt}
            overlayStyle={[styles.overlayStyle]}
            containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
            width="75%"
            height="auto"
            onBackdropPress={() => console.log('backdrop')}
          >
            <View>
              <View style={styles.overlayPopContent}>
                <Text allowFontScaling={false} style={styles.overlayPopText}>There are images that are not yet uploaded. Are you sure that you want to continue without these images?</Text>
              </View>
              <View style={styles.overlayPopButtons}>
                <TouchableOpacity style={[styles.OverlayPopBtn, {borderRightColor: 'rgba(0,0,0,0.2)', borderRightWidth: 1}]} onPress={() => {
                  this.setState({
                    isPrompt: false
                  })
                }}>
                  <Text allowFontScaling={false} style={[styles.OverlayPopBtnText, {color: '#ff3c3c'}]}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.OverlayPopBtn} onPress={() => { 
                    this.setState({
                      isPrompt: false,
                    })
                    this.props.navigation.goBack()
                  }
                }>
                  <Text allowFontScaling={false} style={styles.OverlayPopBtnText}>OK</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Overlay>
          {/* Overlay */}
          {/* Auto Repost Overlay */}
            <Overlay
              isVisible={this.state.isAutoRepost}
              overlayStyle={[styles.overlayStyle]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="75%"
              height="auto"
              onBackdropPress={() => console.log('backdrop')}
            >
              <View>
                <View style={styles.overlayPopContent}>
                  <Text allowFontScaling={false} style={[styles.overlayPopText, {marginBottom: 10, fontFamily: 'OpenSans-SemiBold'}]}>Fresh listings get more quality leads.</Text>
                  <Text allowFontScaling={false} style={[styles.overlayPopText, {fontSize: width * 0.032}]}>Do you wish to opt for weekly auto repost ?</Text>
                  <Text allowFontScaling={false} style={[styles.overlayPopText, {fontSize: width * 0.032}]}>1 credit will be deducted (per repost per listing).</Text>
                </View>
                <View style={styles.overlayPopButtons}>
                  <TouchableOpacity style={[styles.OverlayPopBtn, {borderRightColor: 'rgba(0,0,0,0.2)', borderRightWidth: 1}]} onPress={() => {
                    this.setState({
                      isAutoRepost: false
                    }),
                    this.addAutoRepost()
                  }}>
                    <Text allowFontScaling={false} style={styles.OverlayPopBtnText}>Yes</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.OverlayPopBtn} onPress={() => { 
                      this.setState({
                        isAutoRepost: false,
                      })
                      this._callFetchOneAFterAdd()
                    }
                  }>
                    <Text allowFontScaling={false} style={[styles.OverlayPopBtnText,{color: '#ff3c3c'}]}>No</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Overlay>
          {/* Overlay */}
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[OverlayStyle.overlayStyle]}
              containerStyle={[OverlayStyle.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible: false }, () =>{
                  if(this.state.isError){
                    return;
                  }
                })
              }
            >
              <View style={[OverlayStyle.overlayInner, this.state.isError? OverlayStyle.bgDanger : OverlayStyle.bgSuccess]}>
                <Icon
                  name='info'
                  type='material'
                  color='#fff'
                  size={26}
                />
                <Text allowFontScaling={false} style={OverlayStyle.overlayText}>{this.state.responseInfo}</Text>
              </View>
            </Overlay>
        </SafeAreaView>
        {
          this.state.isLoading && (
            <Loading />
          )
        }
      </View>
    );
  }
}

export default ListingCreate;
