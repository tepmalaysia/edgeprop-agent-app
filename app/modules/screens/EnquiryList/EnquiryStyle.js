import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 20,
    marginBottom: 10,
    flex: 1
  },
  textContainer: {
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    width: '100%'
  },
  textHeaderContainer: {
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 1.22,
    elevation: 5,
    backgroundColor : '#488bf8',
  },
  headerText: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 20,
    color: '#fff'
  },
  userTitle: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 24,
    fontWeight: '500',
    marginBottom: 25,
    color: '#333333',
    textAlign: 'center',
    width: '100%'
  },
  enquiryListContainer: {
    display: 'flex',
    height: height,
    paddingTop: 80,
    paddingBottom: 20,
    backgroundColor: '#fff'
  },
  listSection: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
  },
  itemContainer: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemText: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 18,
    color: '#414141'
  },
  itemValue: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#606060'
  },
  separator: {
    marginTop: 5,
    borderBottomColor: '#ECECEC',
    borderBottomWidth: 1,
  },
  profileImg: {
    width: 100,
    height: 100
  },
  profileContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  listContainerItems: {
    maxWidth: '100%'
  },
  listItems: {
    flexShrink: 0,
    display: 'flex',
    flexDirection: 'row'
  },
  listArrow: {
    width: 16,
    height: 16
  },
  greenBox: {
    backgroundColor: '#35F22D',
    width: 20,
    height: 20,
    borderRadius: 3
  },
  redBox: {
    backgroundColor: '#DF0D0C',
    width: 20,
    height: 20,
    borderRadius: 3,
  },
  grayBox: {
    backgroundColor: '#929292',
    width: 20,
    height: 20,
    borderRadius: 3,
  },
  itemTextContent: {
    paddingHorizontal: 10
  },
  iconArrowContianer: {
    width: 10,
    marginLeft: 10
  },
  disableBox: {
    opacity: 0.35
  },
  signOutContainer: {
    position: 'absolute',
    zIndex: 9,
    left: 0,
    top: 0,
    width: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
  signOutBtn: {
    fontSize: 16,
    minWidth: 100,
    textAlign: 'right',
    fontWeight: '500',
    color: '#488BF8'
  },
  //////////////
  //Enquiry Alert
  alertContainer: {
    paddingBottom: 60,
    justifyContent: 'space-between',
    flexDirection: 'column',
    height: height,
    alignItems: 'center'
  },
  linearGradient: {
    height: height,
    display: 'flex',
    paddingLeft: 15,
    paddingRight: 15
  },
  buttonText: {
    fontSize: 18,
    // fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  alertContainerTitle: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 30,
    width: '100%',
    fontWeight: '500',
    color: '#fff',
    marginVertical: 30,
    textAlign: 'center'
  },
  imageContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  imageThumb: {
    height: 200,
    width: 200,
    borderRadius: 100,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    backgroundColor: '#000',
    shadowOpacity: 0.22,
    shadowRadius: 1.22,
    elevation: 50,
    margin: 'auto'
  },
  thumb: {
    width: '100%',
    height: '100%'
  },
  alertContainerStatus: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 24,
    fontWeight: '500',
    color: '#fff',
    marginTop: 15,
    width: '100%',
    textAlign: 'center'
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: 50,
    marginTop: 60,
    justifyContent: 'space-around'
  },
  roundButtonGreen: {
    width: 80,
    height: 80,
    borderRadius: 80,
    overflow: 'hidden',
    backgroundColor:'#4DB949',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 1.22,
    elevation: 20,
  },
  roundButtonLoad: {
    width: 80,
    height: 80,
    borderRadius: 80,
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  roundButtonRed: {
    width: 80,
    height: 80,
    borderRadius: 80,
    overflow: 'hidden',
    backgroundColor:'#E11B20',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 1.22,
    elevation: 20,
  },
  buttonRoundedContainer: {
    width: 80,
    textAlign: 'center'
  },
  textStyle: {
    // fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    color: '#fff',
    paddingTop: 5,
    fontWeight: '500'
  },
  tickImg: {
    width: 40,
    height: 40
  },
  textCounterStyle: {
    // fontFamily: 'Poppins-Medium',
    fontSize: 20,
    color: '#fff',
    fontWeight: '500',
  },
  //////////////////////
  //Enquiry Details Page
  enquiryContainer: {
    height: height,
    overflow: 'hidden'
  },
  enquiryHeader: {
    paddingHorizontal: 30,
    paddingVertical: 20,
    borderColor: '#ECECEC',
    borderBottomWidth: 1
  },
  enquiryHeadText: {
    textAlign: 'center',
    color: '#fff',
    // fontFamily: 'Poppins-Medium',
    fontSize: 20,
    fontWeight: '500'
  },
  enquiryMessageContainer: {
    padding: 20,
    marginBottom: 10,
  },
  enquiryItem: {
   marginBottom: 20,
  },
  enquiryItemText: {
    color: '#fff',    
    // fontFamily: 'Poppins-Medium',
    fontSize: 18,
    fontWeight: '500'
  },
  enquiryFooter: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 30,
    width: '100%',
    paddingHorizontal: 15,
    justifyContent: 'space-between'
  },
  btnCallFooter: {
    flex: 1,
    backgroundColor: '#ffa700',
    marginLeft: 5,
    padding: 12,
    borderRadius: 3,
    marginRight: 5
  },
  btnWhatsappFooter: {
    flex: 1,
    padding: 12,
    borderRadius: 5,
    backgroundColor: '#20B93A',
    marginLeft: 5,
    marginRight: 5
  },
  btnText: {
    textAlign: 'center',
    color: '#fff',
    // fontFamily: 'Poppins-Medium',
    fontSize: 18,
    fontWeight: '500'
  },
  backNav: {
    height: 20,
    width: 20
  },
  enquiryHeaderBack: {
    position: 'absolute',
    left: 15,
    top: 22,
    zIndex: 1,
    width: 30,
    height: 30
  },
  
});
