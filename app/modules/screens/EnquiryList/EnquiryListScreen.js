import React from 'react';
import {Image,
        View,
        TouchableOpacity,
        Text,
        ScrollView,
        FlatList,
        Button,
        Alert,
        SafeAreaView,
        BackHandler} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../Common/Loader';
import NoResult from '../Common/NoResult';
import CapitalizedText from '../Common/CapitalizedText';
import styles from '../EnquiryList/EnquiryStyle';
import { EnquirySchema } from '../../realm/Enquiry';
import AppStateChange from '../AppStateChange'

const Realm = require('realm');

const HOSTNAME = 'https://www.edgeprop.my/jwdalice/api/user/v1/logout';
export default class EnquiryList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        userInfo: {},
        itemLoading: true,
        relatedData:[]
    }
    this._formatDate = this._formatDate.bind(this)
  }
  
  _openUpDetailsScreen = (item) => {
    this.props.navigation.navigate('EnquiryDetails', {
      'property_id': item.property_id
    });
  }

  _signOut = () => {
    //AsyncStorage.clear();
    this._signOutApiCall()   
  }

  async _getToken() {
    return await AsyncStorage.getItem('fcmToken')
  }

  _signOutApiCall = async () => {
    const token = await this._getToken();
    const body = "uid=" + this.state.userInfo.uid +
              "&key=" + this.state.userInfo.accesskey +
              "&agent_id=" + this.state.userInfo.agent_id +
              "&agent_app=1" +
              "&device_id=" + token
    fetch(HOSTNAME, {
      method: 'POST',
      headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: body
    })
    .then((response) => response.json())
    .then((responseText) => {
      if(responseText.status == 1) {
        AsyncStorage.removeItem('authUser');
        this.props.navigation.navigate('Login')
      }
    })
    .catch((error) => {
      this._showAlert("Unable to handled requested operation")
    });
  }

  _fetchEnquiryList = () => {
    Realm.open({schema: [EnquirySchema], schemaVersion: 1})
    .then(realm => {
      const enquiry = realm.objects('PropertyEnquiryTabel').filtered('agent_id = ' + this.state.userInfo.agent_id).sorted('property_id', true);
      if(enquiry.length > 0) {
        this.setState({ relatedData: enquiry })
      }
      this.setState({ itemLoading: false })
    })
    .catch(error => {
      
    });
  }

  _showAlert = (msg, body) => {
    Alert.alert(
      msg, body
    );
  }

  _formatDate = (timestamp) => {
    const d = new Date(timestamp);
    const monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    const dateFormat = d.getDate()+'-'+monthName[d.getMonth()]+' '+(d.getHours() > 12 ? d.getHours() - 12 : d.getHours())+':'+(d.getMinutes()<10?'0':'') + d.getMinutes()+' '+(d.getHours() >= 12 ? "PM" : "AM");
    return dateFormat
  }

  _getItemColor = (status) => {
    switch(status) {
      case 1:
        return styles.greenBox // Accepted
      case 2:
        return styles.grayBox // Auto Rejected
      case 3:
       return styles.redBox; // Manual Rejected
      case 4:
       return styles.grayBox; // Accepted after message read by someone
    }
  }

  _getEnquiryMessageStatus = (status, message_id) => {
    switch(status) {
      case 2:
        return this._showAlert("Missed Enquiry Request", "Message ID: "+ message_id)
      case 3:
       return this._showAlert("Rejected Enquiry Request", "Message ID: "+ message_id)
      case 4:
       return this._showAlert("Message already read by agent", "Message ID: "+ message_id)
    }
  }

  handleBackButton = () => {
   //BackHandler.exitApp()
   return true
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.status == 1) {
        this.setState({ userInfo: authItems })
      }
    }
    this._fetchEnquiryList();
    const params = this.props.navigation.state.params ? this.props.navigation.state.params : '';
    if(params.rejectedByAdmin) {
      let message = params.messageId ? " Message ID: " + params.messageId : ''
      this._showAlert(params.rejectedByAdmin, message)
    }
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  render() {
    let _renderRelatedItem = (item, i) => {
      return (
        <TouchableOpacity key={i} style={styles.listContainerItems} onPress={() => item.status == 1 ? this._openUpDetailsScreen(item) : this._getEnquiryMessageStatus(item.status, item.message_id)}>
          <View style={[item.status == '1' ? styles.itemContainer : styles.disableBox, styles.itemContainer]}>
            <View style={styles.listSection}>
              <View style={this._getItemColor(item.status) }>
              </View>
              <View style={styles.itemTextContent}>
                <Text style={styles.itemText}>{item.title}</Text>
              </View>
            </View>
            <View style={styles.listItems}>
              <View><Text>{this._formatDate(item.timestamp)}</Text></View>
              <View style={styles.iconArrowContianer}>                
                {
                  item.status == 1 && (
                    <Image
                    style={styles.listArrow}
                    source={require('../../assets/images/info-arrow.png')}
                    />
                  )
                }                
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )
    }
    return (
      <SafeAreaView> 
       <AppStateChange/>
        <View style={styles.enquiryListContainer}>
          <View style={styles.signOutContainer}>
            <TouchableOpacity
              onPress = {() => this._signOut()}
            >
            <Text style={styles.signOutBtn}>Sign Out</Text>
            </TouchableOpacity>
          </View>
          
          <View style={styles.profileContainer}>
              <Image
                  style={styles.profileImg}
                  source={require('../../assets/images/user.png')}
              />
          </View>
          <View style={styles.textContainer}>
            <CapitalizedText style={styles.userTitle}>{this.state.userInfo.name ? this.state.userInfo.name : ''}</CapitalizedText>
          </View>
          <View style={styles.textHeaderContainer}>
            <Text style={styles.headerText}>Enquiry Log</Text>
          </View>
            {
              this.state.itemLoading && (
                <View style={{marginTop: 50}}><Loading/></View>
              )
            }
            {
              !this.state.itemLoading && this.state.relatedData.length == 0 && (
                <View style={{marginTop: 50}}><NoResult/></View>
              )
            }
            
            <ScrollView nestedScrollEnabled={true} style={styles.container}>
              <FlatList
                data={this.state.relatedData}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) =>
                  _renderRelatedItem(item, index)
                }
                keyExtractor={item => String(item.property_id)}
              />
            </ScrollView>

        </View>
      </SafeAreaView>
    );
  }
}