import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
  navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    navIconRight: {
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 35
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },  
    textMd: {
        color: '#10182d',
        fontSize: width * 0.034,
        fontFamily: 'OpenSans-Bold'
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    enquiryDetailWrapper: {
        width: '100%',
        paddingVertical: 15
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        flex: 1,
        paddingHorizontal: 15
    },
    leftIcon: {
        marginRight: 20
    },
    centerText: {
        paddingRight: 20,
        flex: 1,
    },
    textSmBold: {
        fontWeight: '700',
    },
    rightText: {
        width: '18%',
        height: '100%',
        paddingVertical: 5,
        justifyContent: 'center'
    },
    itemDate: {
        textAlign: 'right',
        width: '100%',
        fontSize: width * 0.020,
        color: '#10182d',
    },
    listWrapper: {
        flex: 1,
        paddingHorizontal: 15,
        marginBottom: 15
    },
    enqPropInfo: {
        paddingVertical: 15,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)'
    },
    listItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 10
    },
    messageWrapper: {
        padding: 15
    },
    cardLocation: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'nowrap',
        paddingTop: 2
    },
    cardContentLocation: {
        flex: 1,
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold',
        color: '#149d78',
        paddingLeft: 5
    },
    bottomActions: {
        width: width,
        height: 50,
        padding: 5,
        backgroundColor: '#FFF',
        shadowOffset:{width: 2,  height: -5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    bottomActionItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
});