import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  Linking,
  Alert
} from 'react-native';
import { Avatar, Icon } from 'react-native-elements';
import {Header, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import styles from './EnquiryDetailStyle';
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import Loading from '../../../components/Loader';

const HOSTNAME = 'https://www.edgeprop.my/jwdalice/api/vpex/accept-or-reject-message';

class EnquiryDetails extends Component {

  constructor(props) {
    super(props);
   
    this.navData = props.navigation.state.params ? props.navigation.state.params.data ? props.navigation.state.params.data : {} : {};
    this.EnquiryActions = new EnquiryActions();
    this.state = {
      isLoading: false,
      updated: false,
      enquiryDetails: {},
      parent: this.navData.parent ? this.navData.parent : '',
      userInfo: {}
    }
    this._handleBackPress = this._handleBackPress.bind(this);
    this.makeCall = this.makeCall.bind(this);
    this.openWhatsApp = this.openWhatsApp.bind(this);
    this.getEnquiryDetail = this.getEnquiryDetail.bind(this);
    this.openMail = this.openMail.bind(this);
    this.openProperty = this.openProperty.bind(this);
    this._acceptOrRejectRequest = this._acceptOrRejectRequest.bind(this);
  }

  _handleBackPress() {
    if(this.state.parent && this.state.parent == 'alert') {
      this.props.navigation.navigate('Enquiry');
    }else {

      if(this.navData.updateList && this.state.updated == true) {
        this.navData.updateList(false);
      }
      this.props.navigation.goBack();
    }
  }

  _acceptOrRejectRequest = async (message_id, agent_id) => {
    let status = 1
    const body = "msgid=" + message_id +
              "&agentid=" + agent_id +
              "&status=1";
    fetch(HOSTNAME, {
      method: 'POST',
      headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: body
    })
    .then((response) => response.json())
    .then((responseText) => {
      if(responseText.hasOwnProperty('data')) {
        status = (responseText.status == 0) ? 2 : (responseText.status == 4) ? 3 : responseText.status
      } else {
        status = 3;
      }

      let update = {
          read : true,
          status: status
        }
      if(status == 1) {
        this.setState({
          updated: true
        });
        update = {
          email: responseText.data.email,
          message: responseText.data.message,
          name: responseText.data.name,
          phone: responseText.data.phone,
          read : true,
          status: status
        }
      }
      this.EnquiryActions.SetEnquiry(update, 'message_id = '+message_id);
      this.getEnquiryDetail(message_id);
      this.setState({
        isLoading: false
      });
    })
    .catch((error) => {
       
    });
  }

  async getEnquiryDetail(message_id) {
    let data = this.EnquiryActions.GetEnquiryDetail(message_id);
    if(data.length > 0  && data[0].read == false) {
      this.setState({
        isLoading: true
      });
      await this._acceptOrRejectRequest(message_id, data[0].agent_id);
    }
    this.setState({
      enquiryDetails: data.length>0? data[0] : {},
    });
  }


  shouldComponentUpdate(nextProps, nextState) {
     if(nextProps.navigation.state.params.data.parent != this.props.navigation.state.params.data.parent){
        this.setState({ parent: nextProps.navigation.state.params.data.parent})
        this.getEnquiryDetail(nextProps.navigation.state.params.data.message_id);
      }
      return true;
  }

  async componentDidMount() {
    this.getEnquiryDetail(this.navData.message_id);

    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems})
      }
    }
  }

  openProperty(lid) {
    this.props.navigation.navigate('Listing', {
        data: {
          lid: lid,
          uid: this.state.userInfo.uid,
          token: this.state.userInfo.token
        }
      });
  }

  makeCall(caller) {
    Linking.openURL(`tel:${caller}`);
  }

  openMail(email) {
    if(email !== ''){
        let url = `mailto:${email}`;
        Linking.canOpenURL(url)
          .then((supported) => {
            if (supported) {
              return Linking.openURL(url)
                .catch(() => null);
            }
        });
      }
  }

  openWhatsApp(contact) {
    let message = "Reply to enquiry:";
    Linking.canOpenURL(
        `whatsapp://send?text=hello&phone=${contact}`
      ).then(supported => {
        if (!supported) {
          Alert.alert('App not installed');
        } else {
          Linking.openURL(
                  `whatsapp://send?text=${message}&phone=${contact}`
                );
        }
    });
  }
  
  render() {
    let lid = this.state.enquiryDetails.mid? `LIDM${this.state.enquiryDetails.mid}` : `LIDN${this.state.enquiryDetails.nid}`;
    let location = this.state.enquiryDetails.district? this.state.enquiryDetails.district+', ' : '';
    location += this.state.enquiryDetails.state? this.state.enquiryDetails.state : '';
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={()=> this._handleBackPress()}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#fff'
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter} />
      )
    }

    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          <Header
              statusBarProps={{ barStyle: 'light-content' }}
              containerStyle={styles.navContainer}
              placement="left"
              centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
              leftComponent={<LeftNavComponent />}
              centerComponent={<CenterNavComponent />}
            />
          <View style={styles.innerContainer}>
            <ScrollView>
              <View style={styles.enquiryDetailWrapper}>
                <View style={styles.itemContainer}>
                  <View style={styles.leftIcon}>
                    <Avatar
                      size="medium"
                      rounded
                      title={this.state.enquiryDetails.name? this.state.enquiryDetails.name[0]: ''}
                      titleStyle={styles.title}
                      overlayContainerStyle={{backgroundColor: '#343a56'}}
                    />
                  </View>
                  <View style={styles.centerText}>
                    <Text allowFontScaling={false} style={styles.textMd}>{this.state.enquiryDetails.name}</Text>
                  </View>
                  <View style={styles.rightText}>
                    <Text allowFontScaling={false} style={styles.itemDate}>{moment.unix(this.state.enquiryDetails.changed).format("DD MMM YYYY")}</Text>
                  </View>
                </View>
                <View style={styles.listWrapper}>
                  <View style={styles.listItem}>
                    <View style={styles.listImgWrapper}>
                      <Icon
                        name='phone'
                        type='feather'
                        size={18}
                        color={'#10182d'}
                      />
                    </View>
                    <TouchableOpacity onPress={() => this.makeCall(this.state.enquiryDetails.phone)} style={{flex: 1}}>
                      <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15, fontFamily: 'OpenSans-Regular', color: '#3670e5'}]}>{this.state.enquiryDetails.phone}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.listItem}>
                    <View style={styles.listImgWrapper}>
                      <Icon
                        name='mail'
                        type='feather'
                        size={18}
                        color={'#10182d'}
                      />
                    </View>
                    <TouchableOpacity onPress={() => this.openMail(this.state.enquiryDetails.email)} style={{flex: 1}}>
                      <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15, fontFamily: 'OpenSans-Regular', color: '#3670e5'}]}>{this.state.enquiryDetails.email}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  onPress={() => this.openProperty(this.state.enquiryDetails.lid)}
                  style={styles.enqPropInfo}>
                  <View style={styles.itemContainer}>
                    <View style={styles.centerText}>
                      <Text allowFontScaling={false} style={[styles.textSm,styles.textSmBold]} numberOfLines = {2}>{this.state.enquiryDetails.title}</Text>
                      <View style={styles.cardLocation}>
                        <Icon
                          name='ios-pin'
                          size={15}
                          type='ionicon'
                          color='#96d9c2'
                        />
                        <Text allowFontScaling={false} style={styles.cardContentLocation} numberOfLines = {1}>{location}</Text>
                      </View>
                    </View>
                    <View style={[styles.rightText, {justifyContent: 'flex-start'}]}>
                      <Text allowFontScaling={false} style={[styles.itemDate,styles.textActive]}>{lid}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={styles.messageWrapper}>
                  <Text allowFontScaling={false} style={styles.textSm}>
                   {this.state.enquiryDetails.message}
                  </Text>
                </View>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
        {/* Bulk Actions */}
        <SafeAreaView >
          <View style={styles.bottomActions}>
            <TouchableOpacity
              onPress={() => this.openWhatsApp(this.state.enquiryDetails.phone)}
              style={styles.bottomActionItem}
            >
              <Icon
                name='whatsapp'
                type='font-awesome'
                size={26}
                color='#10182d'
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.makeCall(this.state.enquiryDetails.phone)} 
              style={styles.bottomActionItem}
            >
              <Icon
                name='phone'
                type='feather'
                size={24}
                color='#10182d'
              />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        {
          this.state.isLoading && (
              <Loading />
            )
        }
      </LinearGradient>
    );
  }
}

export default EnquiryDetails;