import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9,
        borderBottomWidth: 0
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 35
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    navIconRight: {
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTitle: {
        flex: 1,
        color: '#fff'
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#10182d'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },
    contentSection: {
        height: 40,
        paddingHorizontal: 15,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        backgroundColor: '#fff'
    },
    editImageWrapper: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        padding: 10,
    },
    editImageItem: {
        padding: 5,
        width: '33.333%',
        maxWidth: '33.333%',
        height: width * 0.25,
        position: 'relative'
    },
    cardImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 3,
        overflow: 'hidden'
    },
    defaultImage: {
        borderWidth: 2,
        borderColor: '#fff'
    },
    cardOverlay: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardOverlayInner: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5
    },
    editImageAction: {
        width: 30,
        height: 30,
        backgroundColor: 'rgba(16, 24, 45, 0.6)',
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5   
    },
    bottomActions: {
        width: width,
        height: 50,
        padding: 5,
        backgroundColor: '#FFF',
        shadowOffset:{width: 2,  height: -5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    bottomActionItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textMd: {
        color: '#10182d',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold'
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    CheckBoxWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    checkBox: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        padding: 0,
        margin: 0,
        marginLeft: -2,
        flex: 1
    },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        padding: 0,
        backgroundColor: 'transparent',
        shadowColor: 'rgba(0, 0, 0, 0)',
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .15)',
    },
    overlayInner: {
        width: '100%',
        height: 'auto',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        flex: 1,
        lineHeight: width * 0.035,
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    },
    overlayPopContent: {
        padding: 15,
    },
    overlayPopText: {
        color: '#10182d',
        fontSize: width * 0.036,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.05,
    },
    overlayPopButtons: {
        borderTopWidth: 1,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row'
    },
    OverlayPopBtn: {
        padding: 18,
        flex: 1
    },
    OverlayPopBtnText: {
        color: '#149d78',
        textTransform: 'uppercase',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    modalNav: {
        paddingHorizontal: 15,
        height: 50,
        justifyContent: 'center'
    },
    modalContainer: {
        flex: 1, 
        backgroundColor: '#10182d'
    }
});
