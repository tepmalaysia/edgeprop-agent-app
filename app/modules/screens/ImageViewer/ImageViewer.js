/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    Switch,
    Modal,
    Dimensions
} from 'react-native';
import { CheckBox, Icon, Input, Header, Overlay } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';
import ImageViewer from 'react-native-image-zoom-viewer';
import styles from './ImageViewerStyle';

const { width, height } = Dimensions.get('window');

const images = [{
  url: 'https://dbv47yu57n5vf.cloudfront.net/s3fs-public/styles/list_view_thumb_nowatermark/public/80751_1575633362_5EdgPro5_img_0324_0.png',
  
}, {
  url: '',
  props: {
      source: require('../../../assets/images/property-bg.png')
  }
}]

export default class ImageListing extends Component{

  constructor(props){
    super(props);
    this.data = props.navigation.state.params.data? props.navigation.state.params.data : {};
    this.state = {
      isDefault: false,
      index: 0,
      modalVisible: false,
      images: {}
    }
  }

  componentDidMount() {
    let images = [];
    this.data.images.map((item, index) => {
      let image ={
        'url' : item,
        'freeHeight': true
      }
      images.push(image);
    });
    this.setState({images})
  }

  openModal(index) {
    this.setState({index, modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  _handleBackPress() {
    this.props.navigation.goBack();
  }

  render() {
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={()=> this._handleBackPress()}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#fff'
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>{this.data.count} image(s)</Text>
        </View>
      )
    }
    
    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#10182d', '#10182d']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
          />
          <View style={styles.innerContainer}>
            <ScrollView>
              <View style={styles.editImageWrapper}>
                {this.data.images.length > 0 && this.data.images.map((item, index) => {
                  return (
                    <TouchableOpacity style={[styles.editImageItem]} onPress={() => this.openModal(index)} key={index}>
                      <ImageBackground source={{uri : item}} style={[styles.cardImage, index == 0? styles.defaultImage : '']}>
                        
                      </ImageBackground>
                    </TouchableOpacity>
                    )
                })}
                <Modal
                  transparent={true}
                  visible={this.state.modalVisible}
                  animationType={'slide'}
                  onRequestClose={() => this.closeModal()}>
                  <View style={styles.modalContainer}>
                    <SafeAreaView style={styles.SafeAreaView}>
                      <View style={styles.innerContainer}>
                        <View style={styles.modalNav}>
                          <TouchableOpacity onPress={()=> this.closeModal()} style={styles.navLeft}>
                            <Icon
                              name='arrow-left'
                              size={26}
                              type='feather'
                              color='#fff'
                            />
                          </TouchableOpacity>
                        </View>
                        <ImageViewer
                          imageUrls={this.state.images}
                          index={this.state.index}
                          backgroundColor='#10182d'
                          onMove={data => console.log(data)}
                          enableSwipeDown={true}
                          onSwipeDown={() => this.closeModal()}
                        />
                      </View>
                    </SafeAreaView>
                    </View>
                </Modal>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
        {/* Bulk Actions */}
      </LinearGradient>
    );
  }
}
