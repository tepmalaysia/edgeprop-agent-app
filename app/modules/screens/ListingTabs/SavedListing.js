/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Linking,
  FlatList,
  InteractionManager
} from 'react-native';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import HeaderSearch from '../../../components/headerSearch';
import styles from './ListingTabsStyle';
import { FetchListing, FetchOne, BulkUpdate, PutData } from '../../services/api'
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../../../components/Loader';
import PropertyItem from '../../../components/PropertyItem'
import moment from 'moment';
import ListingActions from '../../../../realm/actions/ListingActions'
import realm from 'realm';
import qs from "qs";

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/search';
const FETCHONE = 'https://prolex.edgeprop.my/api/v1/fetchOne';
const BULKUPDATE = 'https://prolex.edgeprop.my/api/v1/bulkUpdate';
const PROP_STATUS = 'https://prolex.edgeprop.my/api/v1/get-property-stats';
class SavedList extends Component{
  showTab = true;
  isScrolling = false;
  _navListener = null;
  constructor(props){
    scrollOffset= 0;
    direction = '';
    super(props);
    this.ListingActions = new ListingActions();
    let listingData = [];//this.ListingActions.GetListings('draft', this.props.screenProps.data.filter);
    this.data = props.screenProps.data;
    this.state = {
      enableLongPress : false,
      selected: [],
      userInfo: {},
      isLoading: false,
      ListingData: [],
      paginationCount: 15,
      tab: 'saved',
      page: 1,
      total: 0,
      fetchData: props.screenProps.data.items? props.screenProps.data.items : [],
      allListing: listingData,
      isVisible: false,
      apiMessage: '',
      bulkValue: [],
      _listingType: '',
      valueId: '',
      alpha: [],
      updated_records: {},
      skipped_records: {},
      errorApiMessage: ''
    }
    this.count = 0;
    this.showTab = true;
    this._listingClick = this._listingClick.bind(this);
    this._actionItems = this._actionItems.bind(this);
    this._onLongPress = this._onLongPress.bind(this);
    this._onSearch = this._onSearch.bind(this);
    this._loadMore = this._loadMore.bind(this)
    this._bulkUpdate = this._bulkUpdate.bind(this)
    this._rePost = this._rePost.bind(this)
    this.expireAll = this.expireAll.bind(this)
    this._handleBackdrop = this._handleBackdrop.bind(this)
    this._cancelBulk = this._cancelBulk.bind(this)
    this._refresh = this._refresh.bind(this)
    this._featureNot = this._featureNot.bind(this)
    this._openUrl = this._openUrl.bind(this)
    this._handleUpdation = this._handleUpdation.bind(this)
    this._onScroll = this._onScroll.bind(this)
    this.checkSelected = this.checkSelected.bind(this)
    this.mount = true;
    props.navigation.setParams({
      showTab: this.showTab
    });
  }

  async componentDidMount() {
    this._navListener = this.props.navigation.addListener('willFocus', (route) => { 
      
       this._actionItems(this.showTab);
       if(this.data.toggleAdd){
          this.data.toggleAdd('', 'showAddSave');
        }
    });
    const auth = await AsyncStorage.getItem("authUser");
    InteractionManager.runAfterInteractions(() => {
      if(auth && auth != '') {
        let authItems = JSON.parse(auth);
        if(authItems.uid != '') {
          let listingData = this.ListingActions.GetListings('draft', this.props.screenProps.data.filter);
          this.setState({ userInfo: authItems, allListing: listingData })
          //this._onRefetch();
        }
      }
    });
  }

  componentWillUnmount() {
    if(this._navListener != null)
     this._navListener.remove();
  }

  _refresh(parentRefresh) {
    let listingData = this.ListingActions.GetListings('draft', this.props.screenProps.data.filter);
    this.setState({ allListing: listingData })
    if(this.data.bulkHandler && parentRefresh){
      this.data.bulkHandler()
    }
  }

  _featureNot() {
    alert('This feature is not available');
  }

  _openUrl(url){
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        
      }
    });
  }
  

  shouldComponentUpdate(nextProps, nextState) {
     if((JSON.stringify(nextProps.screenProps.data.filter) != JSON.stringify(this.props.screenProps.data.filter))
      || (JSON.stringify(nextProps.screenProps.data.forceLoad) != JSON.stringify(this.props.screenProps.data.forceLoad))){
       InteractionManager.runAfterInteractions(() => {
        let listingData = this.ListingActions.GetListings('draft', nextProps.screenProps.data.filter);
        this.setState({allListing: listingData})
       });
     }
      if(nextProps.screenProps.data.trigger != this.props.screenProps.data.trigger){
        InteractionManager.runAfterInteractions(() => {
          let listingInfo = this.ListingActions.GetListings('draft', this.props.screenProps.data.filter);
          this.setState({ allListing: listingInfo,isLoading: false })
        });
     }
     return true;
  }

  _onSearch() {
    this.props.navigation.navigate('SearchOption');
  }

  _listingClick(index, listingType, idWithType){
    var numb = idWithType.replace(/[0-9]/g, '');
    if(!this.state.enableLongPress){
      this.props.navigation.navigate('Listing', {
        data: {
          lid: index,
          uid: this.state.userInfo.uid,
          token: this.state.userInfo.token
        },
        refresh: this._refresh
      });
    }else{
      let array = this.state.selected;
      let offset = array.indexOf(index);
      let alph = this.state.alpha;
      let alphaoffset = alph.indexOf(numb);
      if( offset > -1){
        array.splice(offset, 1);
      }else{
        if(array.length == 5){
          //this.setState({ isVisible: true, apiMessage: 'Maximum allowed section is 5!' });
          alert('Please select a maximum of 5 listings');
          return;
        }
        array.push(index);
        alph.push(numb);
      }
      
      this.setState({selected: array, valueId: idWithType, _listingType: listingType, alpha: alph, refresh: !this.state.refresh})
      if(array.length == 0){
        this.setState({enableLongPress: false})
        this._actionItems(true);
      }
    }
  }

  _actionItems(status){
    this.showTab = status;
    if(this.data.actionHandler){
      this.data.actionHandler(status);
    }
  }

   _onLongPress(index, listingType, idWithType){
    var numb = idWithType.replace(/[0-9]/g, '');
    let array = this.state.selected;
    let offset = array.indexOf(index);
    let alph = this.state.alpha;
    let alphaoffset = alph.indexOf(numb);
    if( offset== -1){
      if(array.length == 5){
        //this.setState({ isVisible: true, apiMessage: 'Maximum allowed section is 5!' });
        alert('Maximum allowed section is 5!');
        return;
      }
      array.push(index);
      alph.push(numb);
    }
    
    
    this.setState({selected: array, _listingType: listingType, alpha: alph, refresh: !this.state.refresh})
    if(!this.state.enableLongPress){
      this._actionItems(false);
      this.setState({enableLongPress: true})
    }
    
  }

  _formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    _formatMoney(val) {
        return val ? 'RM ' + this._formatNumber(val) : '-'
    }

    _loadMore() {
      
    }


    _rePost(listingtype, idtype) {
      var numb = idtype.match(/\d/g);
      numb = numb.join("");
      let value = idtype.replace(/[0-9]/g, '');
      value ='LIDM' ? 'm' : 'n';
      let Idvalues = value.concat(numb)

      this.setState({ isLoading: true })
      const body = "uid=" + this.state.userInfo.uid +
            "&token=" + this.state.userInfo.token +
            "&field_prop_listing_type=" + listingtype +
            "&repost=1" +
            "&ids[0]=" + Idvalues +
            "&columns[changed]="
      BulkUpdate(BULKUPDATE, body)
      .then(data => this._bulkSuccess(data))
      .catch(error => this.fetchFail(error));
    }


    _bulkUpdate(listingtype, lid, alpha, columnStatus) {
     
      this.setState({ isLoading: true });
      const body = "uid=" + this.state.userInfo.uid +
            "&token=" + this.state.userInfo.token +
            "&field_prop_listing_type=" + listingtype +
            "&repost=0&" +
            "&ids[0]=" + alpha +
            "&columns[field_prop_status]=" + columnStatus
      BulkUpdate(BULKUPDATE, body)
      .then(data => this._bulkSuccess(data, lid, columnStatus))
      .catch(error => this.fetchFail(error));
    }

    _bulkSuccess(data, lid, columnStatus) {
      let message = '';
      let errorMessage = '';
      let skippedCount = data.result.skipped_records.count;
      let updatedCount = data.result.updated_records.count;
      if(data.message == 'Property bulk update successfully done'){
        if(columnStatus == 'deleted') {
          this.ListingActions.ClearListing(lid);
          message = 'Your listing is deleted Successfully';
          this._refresh();
        } else if(columnStatus == 'active') {
          message = updatedCount + ' Listing(s) Published';
          let updatedIds = data.result.updated_records.updated_id;
          this._handleUpdation(updatedIds, 'active');
        }
      } else {
        data.result.skipped_records.message.map((item, index) => {
          errorMessage = item;
        });
      }
      this.setState({ isLoading: false, isVisible: true, apiMessage: message, errorApiMessage: errorMessage,  skipped_records: data.result.skipped_records, updated_records: data.result.updated_records });
    }

    _handleUpdation(updatedIds, action){
      for(var i=0; i< updatedIds.length ; i++){
        let update = {
          prop_status : action
        }
        this.ListingActions.SetListing(update, 'lid = '+updatedIds[i]); 
      }
      let listingData = this.ListingActions.GetListings('draft', this.props.screenProps.data.filter);
      this.setState({ allListing: listingData })
    }

    _againFetchOne() {
      this.setState({ isVisible: false, enableLongPress: false, selected: [], refresh: !this.state.refresh })
      this.showTab = !this.showTab;
      this.setState({ isLoading : true });
      this._updateList(this.state.fetchData);
    }

    expireAll(listingtype, idtype, alpha){
      
      if(this.state.selected.length > 0){
        let ids = '' 
        for (var i = 0; i< idtype.length; i++) {
          ids+= "ids["+i+"]="+alpha[i]+idtype[i]+"&";
        }
      

      this.setState({ isLoading: true })
      
      let listing_type = this.props.screenProps.data.filter? this.props.screenProps.data.filter.listing_type? this.props.screenProps.data.filter.listing_type : 'sale' : 'sale';
        
        const body = "uid=" + this.state.userInfo.uid +
            "&token=" + this.state.userInfo.token +
            "&field_prop_listing_type=" + listing_type +
            "&repost=0&" +
            ids +
            "columns[field_prop_status]=deleted"
            
         BulkUpdate(BULKUPDATE, body)
        .then(data => this._bulkDeleteSuccess(data, idtype))
        .catch(error => console.log(error));
      }

    }

    _bulkDeleteSuccess(data, lid) {

      let skippedCount = data.result.skipped_records.count;
      let updatedCount = data.result.updated_records.count;
      let message = '';
      let errorMessage = '';

      if(updatedCount > 0) {
        message = updatedCount + ' Listing(s) Deleted';
        let updatedIds = data.result.updated_records.updated_id;
        for(var i =0; i< updatedIds.length; i++){
          this.ListingActions.ClearListing(updatedIds[i]);
        }
      }
      
      if(skippedCount > 0) {
        errorMessage = skippedCount + ' Listing(s) Failed';
      }

      this.setState({ isLoading: false, isVisible: true, apiMessage: message, errorApiMessage: errorMessage, skipped_records: data.result.skipped_records, updated_records: data.result.updated_records });
      this._refresh();
    }

    _handleBackdrop(){
      if(this.state.updated_records.count && this.state.updated_records.count> 0){
        if(this.data.bulkHandler){
          this.data.bulkHandler()
        }
      }
      this.setState({
        isVisible: false,
        selected: [],
        enableLongPress: false,
        updated_records: {},
        skipped_records: {},
        refresh: !this.state.refresh
      })
      this._actionItems(true);
    }

    _cancelBulk(){
      this.setState({
        isVisible: false,
        selected: [],
        enableLongPress: false,
        refresh: !this.state.refresh
      })
      this._actionItems(true);
    }

    _onScroll = event => {
      const currentOffset = event.nativeEvent.contentOffset.y;
      const dif = currentOffset - (this.scrollOffset || 0);
      let direction = '';
      if (Math.abs(dif) < 3) {
        
      } else if (dif < 0) {
        direction = 'up';
      } else {
        if(currentOffset > 0){
          direction = 'down';
        }
      }

      this.scrollOffset = currentOffset;
      if(direction && this.direction !=direction ){
        this.direction = direction;
        
        if(this.data.toggleAdd){
          this.data.toggleAdd(direction, 'showAddSave');
        }
      }
    }

    checkSelected= (index) => {
      return this.state.selected.indexOf(index) > -1 ? true : false;
    }


  render() {
    

    const LeftNavComponent = () => {
      return (
        <View style={styles.LeftMenuIcon} >
          <TouchableOpacity style={{padding: 15}}  onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../../assets/images/menu-button.png')}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <TouchableOpacity style={styles.navCenterInner} onPress={this._onSearch}>
            <Text allowFontScaling={false} style={styles.navTitle}>Find Properties...</Text>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../../assets/images/search.png')}
            />
          </TouchableOpacity>
        </View>
      )
    }
    
    var _renderRelatedItem = (item,i) => {          
      let price = item.price ? item.price : '0';
      let image = item.cover_image ? item.cover_image : item.images[0] ? item.images[0] : 'https://list.edgeprop.my/static/img/no-image.png';
      let is_public_photo = image.search("public://");
      if(is_public_photo == 0){
        image = 'https://list.edgeprop.my/static/img/no-image.png';
      }
      let impression = item.impression ? item.impression : 0;
      let whatsapp = item.whatsapp_count ? item.whatsapp_count : 0;
      let showNumber = item.view_contact ? item.view_contact : 0;
      let mid = item.mid ? 'LIDM' + item.mid : 'LIDN' + item.nid;
      let idWithType = item.mid ? 'm' + item.mid : 'n' + item.nid;
      let url = item.url ? item.url : '';
      url = "https://www.edgeprop.my/listing/"+url;

        return (
            <View style={{paddingHorizontal: 15, marginVertical: 5}}>
                
                <TouchableOpacity style={checkSelected(item.lid)? [styles.cardItem, styles.cardSelected] : styles.cardItem} onPress={() => this._listingClick(item.lid, item.listing_type, idWithType)} onLongPress={() =>this._onLongPress(item.lid, item.listing_type, idWithType)}>
                  {checkSelected(item.lid) && <Image
                    style={styles.selectedIcon}
                    source={require('../../../assets/images/selected.png')}
                  />}
                  <View style={styles.cardImgWrapper}>
                    {/*  Card Image */}
                    <ImageBackground source={{uri: image}} style={styles.cardImage}>
                      <LinearGradient colors={['rgba(0,0,0,0.5)', 'transparent', 'rgba(0,0,0,0.8)']} style={styles.cardOverlay}>
                        <Text allowFontScaling={false} style={styles.cardOverlayText}>{mid}</Text>
                        <View style={styles.cardOverlayInner}>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{impression}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../../../assets/images/steps.png')}
                            />
                          </View>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{showNumber}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../../../assets/images/view.png')}
                            />
                          </View>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{whatsapp}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../../../assets/images/whatsapp-sm.png')}
                            />
                          </View>
                        </View>
                      </LinearGradient>
                    </ImageBackground>
                  </View>
                  {/*  Card Content */}
                  <View style={styles.cardContentWrapper}>
                    <View style={styles.cardContentTop}>
                      <Text allowFontScaling={false} style={styles.cardTitle} ellipsizeMode='tail' numberOfLines={1}>{item.title}</Text>
                      {(item.district != '' || item.state !='') &&
                      <View style={styles.cardLocation}>
                        <Icon
                          name='ios-pin'
                          size={15}
                          type='ionicon'
                          color='#96d9c2'
                        />
                        <Text allowFontScaling={false} style={styles.cardContentLocation} ellipsizeMode='tail' numberOfLines={1}>{item.district ? item.district+',' : ''} {item.state ? item.state : ''}</Text>
                      </View>
                      }

                      <Text allowFontScaling={false} style={styles.cardPrice}>{this._formatMoney(price)}</Text>
                      <View style={styles.cardLocation}>
                        <Icon
                          name='clock'
                          size={10}
                          type='feather'
                          color='#646975'
                        />
                      <Text allowFontScaling={false} style={styles.cardUpdateDate}>{moment.unix(item.changed).fromNow()}</Text>
                    </View>
                    </View>
                    <View style={styles.cardContentBottom}>
                      <TouchableOpacity onPress={() => (!this.state.enableLongPress) ? this._openUrl(url) : ''} style={styles.cardActionItem}>
                        <Icon
                          name='external-link'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>View</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.cardActionItem} onPress={() => (!this.state.enableLongPress) ? this._bulkUpdate(item.listing_type, item.lid, idWithType, 'active') : ''} >
                        <Icon
                          name='power'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Activate</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => (!this.state.enableLongPress) ? this._bulkUpdate(item.listing_type, item.lid, idWithType, 'deleted') : ''} style={styles.cardActionItem}>
                        <Icon
                          name='trash-2'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Delete</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
                </View>
        )        
      }



    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          {/* <HeaderSearch navigation={this.props.navigation} /> */}
          
          <View style={styles.innerContainer}>

          {(this.state.allListing.length == 0 && this.state.isLoading == false) && (
            <View style={styles.noDataContainer}>
              <Image
                style={{width: 75, height: 75}}
                source={require('../../../assets/images/no-list.png')}
              />
              <Text allowFontScaling={false} style={styles.textMdNoRecord}>No Records Found ! </Text>
            </View>
          )}
          <View style={styles.cardWrapper}>
            {<FlatList
              data={this.state.allListing}
              renderItem={({ item, index }) => 
                <PropertyItem 
                  item={item} 
                  index={index} 
                  type={'saved'}
                  selected={this.state.selected}
                  listingClick={this._listingClick}
                  onLongPress={this._onLongPress}
                  enableLongPress={this.state.enableLongPress}
                  onBulkUpdate={this._bulkUpdate}
                  onOpenUrl={this._openUrl}
                  checkSelected={this.checkSelected}
                  refresh={this.state.refresh}
              />}
              keyExtractor={(item) => item.lid.toString()} 
              onMomentumScrollBegin={() => { this.isScrolling = true; }}
              onEndReached={()=>{
                if(this.isScrolling){
                  this._loadMore();
                }
              }}
              ListHeaderComponent={() => ( <View style={{paddingTop: 10}}/>)}
              ListFooterComponent={() => ( <View style={{paddingBottom: 10}}/>)}
              onEndReachedThreshold={0.5}
              onScroll={this._onScroll}
              bounces={false}
            />}
          </View>
            
          </View>
          {/* Bulk Actions */}
          {!this.showTab && <View style={styles.bottomActions}>
            {/*<TouchableOpacity style={styles.bottomActionItem} onPress={() => this._bulkUpdate(this.state._listingType, this.state.selected, this.state.alpha)}>
              <Icon
                name='corner-left-up'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomActionItem}>
              <Icon
                name='star'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>*/}
            <TouchableOpacity style={styles.bottomActionItem} onPress={() =>this.expireAll(this.state._listingType, this.state.selected, this.state.alpha, 'deleted')}>
              <Icon
                name='trash-2'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>
            <TouchableOpacity style={[styles.bottomActionItem, {justifyContent: 'center'}]} onPress={this._cancelBulk}>
              <Text>Cancel</Text>
            </TouchableOpacity>
          </View>}

          {/* Overlay */}
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={this._handleBackdrop}
            >
              <View>
                {(this.state.updated_records.count > 0 || this.state.apiMessage != '') && 
                <View style={[styles.overlayInner, styles.bgInfo]}>
                  <Icon
                    name='info'
                    type='material'
                    color='#fff'
                    size={30}
                  />
                  <Text allowFontScaling={false} style={styles.overlayText}>{this.state.apiMessage}</Text>
                </View>}

                {this.state.skipped_records.count > 0 && 
                <View style={[styles.overlayInner, styles.overlayInnerError, styles.bgDanger]}>
                  <View style={{flexDirection: 'row', width: '100%', marginBottom: 5, alignItems: 'center'}}>
                    <Icon
                      name='info'
                      type='material'
                      color='#fff'
                      size={30}
                    />
                    <Text allowFontScaling={false} style={styles.overlayText,styles.overlayTextError}>{this.state.errorApiMessage}</Text>
                  </View>
                  <View>
                    {!!this.state.skipped_records.count &&  this.state.skipped_records.count > 0 && !!this.state.skipped_records.title && this.state.skipped_records.title.map((message,i) => {
                    return (
                    <View style={styles.listViewDeleted} key={i}>
                      <Text allowFontScaling={false} style={styles.listShape}>{'\u2022'}</Text>
                      <Text allowFontScaling={false} style={styles.overlayText}>{message}</Text>
                    </View>
                    )
                   })}
                  </View>
                </View>}
              </View>
            </Overlay>


        </SafeAreaView>
        {
          this.state.isLoading && (
            <Loading />
          )
        }
      </LinearGradient>
    );
  }
}

export default SavedList
