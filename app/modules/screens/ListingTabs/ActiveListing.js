/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  FlatList,
  Linking,
  InteractionManager
} from 'react-native';
import { Header, Button, Icon, Overlay } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import qs from "qs";
import HeaderSearch from '../../../components/headerSearch';
import styles from './ListingTabsStyle';
import overlayStyle from '../../styles/overlayStyle';
import { FetchListing, FetchOne, BulkUpdate, PostData } from '../../services/api'
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../../../components/Loader';
import moment from 'moment';
import ListingActions from '../../../../realm/actions/ListingActions'
import CustomSelect from '../../../components/CustomSelect'
import PropertyItem from '../../../components/PropertyItem'
import { aucDay,aucMonth,aucYear } from '../../services/formOptions'

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/search';
const FETCHONE = 'https://prolex.edgeprop.my/api/v1/fetchOne';
const BULKUPDATE = 'https://prolex.edgeprop.my/api/v1/bulkUpdate';
const PROP_STATUS = 'https://prolex.edgeprop.my/api/v1/get-property-stats';
const FEATURED = 'https://prolex.edgeprop.my/api/v1/addFeatured';
class ActiveList extends Component{
  showTab = true;
  tabFlag = false
  isScrolling = false;
  _navListener = null;
  constructor(props){
    scrollOffset= 0;
    direction = '';
    super(props);
    this.ListingActions = new ListingActions();
    let listingData = [];//this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
    this.allListing = [];
    this.data = props.screenProps.data;
    this.state = {
      enableLongPress : false,
      selected: [],
      userInfo: {},
      isLoading: false,
      ListingData: [],
      paginationCount: 15,
      tab: 'active',
      page: 1,
      total: 0,
      fetchData: props.screenProps.data.items? props.screenProps.data.items : [],
      trigger: 0,
      isVisible: false,
      apiMessage: '',
      bulkValue: [],
      _listingType: '',
      valueId: '',
      alpha: [],
      updated_records: {},
      skipped_records: {},
      added_records: {},
      featuredShow: false,
      mid: '',
      isError: false,
      feature_day: moment().format('DD'),
      feature_month: moment().format('MM'),
      feature_year: parseInt(moment().format('YYYY')),
      featureItem: {},
    }
    this.count = 0;
    this.synCount = 0;
    this.showTab = true;
    this._listingClick = this._listingClick.bind(this);
    this._actionItems = this._actionItems.bind(this);
    this._onLongPress = this._onLongPress.bind(this);
    this._onSearch = this._onSearch.bind(this);
    this._loadMore = this._loadMore.bind(this)
    this._bulkUpdate = this._bulkUpdate.bind(this)
    this._rePost = this._rePost.bind(this)
    this._getStatus = this._getStatus.bind(this)
    this._getStats = this._getStats.bind(this)
    this.getStatsSuccess = this.getStatsSuccess.bind(this)
    this.getStatsFail = this.getStatsFail.bind(this)
    this._refresh = this._refresh.bind(this)
    this._featureNot = this._featureNot.bind(this)
    this._openUrl = this._openUrl.bind(this)
    this._cancelBulk = this._cancelBulk.bind(this)
    this.expireAll = this.expireAll.bind(this)
    this._handleBackdrop = this._handleBackdrop.bind(this)
    this._checkStatus = this._checkStatus.bind(this)
    this._makeFeatured = this._makeFeatured.bind(this)
    this.featuredSuccess = this.featuredSuccess.bind(this)
    this.featuredFail = this.featuredFail.bind(this)
    this._checkForm = this._checkForm.bind(this)
    this._bulkSuccess = this._bulkSuccess.bind(this)
    this._bulkFail = this._bulkFail.bind(this)
    this._handleUpdation = this._handleUpdation.bind(this)
    this._onScroll = this._onScroll.bind(this)
    this._featuredOption = this._featuredOption.bind(this)
    this.checkSelected = this.checkSelected.bind(this)
    props.navigation.setParams({
      showTab: this.showTab
    });
    this.units = [
      {label: 'sqft',value: 'sqft'},
      {label: 'sqm',value: 'sqm'},
      {label: 'acre',value: 'acre'}
    ];
  }

  async componentDidMount() {
    this._navListener = this.props.navigation.addListener('willFocus', (route) => { 
      
       if(this.tabFlag == true) {
          this._actionItems(this.showTab);
       }
       this.tabFlag = true;
       if(this.data.toggleAdd){
          this.data.toggleAdd('', 'showAddActive');
        }
    });
    const auth = await AsyncStorage.getItem("authUser");
    InteractionManager.runAfterInteractions(() => {
      if(auth && auth != '') {
        let authItems = JSON.parse(auth);
        if(authItems.uid != '') {
          let listingData = this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
          this.allListing = listingData;
          this.setState({ userInfo: authItems, trigger: moment().unix() })
          this._checkStatus(this.props.screenProps.data.filter,1500)
        }
      }
    });
  }

  componentWillUnmount() {
    if(this._navListener != null)
     this._navListener.remove();
  }

  async _checkStatus(filter, delay){
    let listing_type = filter? filter.listing_type? filter.listing_type : 'sale' : 'sale';
    let key = "propDate"+listing_type;
    
    const propDate = await AsyncStorage.getItem(key);
    let today = moment.unix(moment().unix()).format("MMDD");
    let lastDate = '0101';
    if(propDate){
      lastDate = moment.unix(propDate).format("MMDD");
    }
    if(today>lastDate){
      setTimeout(() => {
        this._getStatus();
      }, delay);
    }
  }

  _cancelBulk(){
      this.setState({
        isVisible: false,
        selected: [],
        enableLongPress: false,
        refresh: !this.state.refresh
      })
      this._actionItems(true);
  }

  expireAll(listingtype, idtype, alpha){
      
      if(this.state.selected.length > 0){
        let ids = '' 
        for (var i = 0; i< idtype.length; i++) {
          ids+= "ids["+i+"]="+alpha[i]+idtype[i]+"&";
        }
      

      this.setState({ isLoading: true })
      
      let listing_type = this.props.screenProps.data.filter? this.props.screenProps.data.filter.listing_type? this.props.screenProps.data.filter.listing_type : 'sale' : 'sale';
        
        const body = "uid=" + this.state.userInfo.uid +
            "&token=" + this.state.userInfo.token +
            "&field_prop_listing_type=" + listing_type +
            "&repost=0&" +
            ids +
            "columns[field_prop_status]=expired"
        
         BulkUpdate(BULKUPDATE, body)
        .then(data => this._bulkSuccess(data,'Expired'))
        .catch(error => this._bulkFail(error));
      }

  }

  _refresh(parentRefresh) {
    let listingData = this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
    this.allListing = listingData;
    this.setState({ trigger: moment().unix() })
    if(this.data.bulkHandler && parentRefresh){
      this.data.bulkHandler()
    }
  }

  _featureNot() {
    alert('This feature is not available');
  }

  _openUrl(url){
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
     if((JSON.stringify(nextProps.screenProps.data.filter) != JSON.stringify(this.props.screenProps.data.filter))
      || (JSON.stringify(nextProps.screenProps.data.forceLoad) != JSON.stringify(this.props.screenProps.data.forceLoad))){
        InteractionManager.runAfterInteractions(() => {
          this._checkStatus(nextProps.screenProps.data.filter,500);
          let listingData = this.ListingActions.GetListings('active', nextProps.screenProps.data.filter);
          this.allListing = listingData;
          this.setState({trigger: moment().unix()})
        });
     }
     if(nextProps.screenProps.data.trigger != this.props.screenProps.data.trigger){
       InteractionManager.runAfterInteractions(() => {
        let listingInfo = this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
        this.allListing = listingInfo;
        this.setState({ trigger: moment().unix(),isLoading: false })
       });
     }
     return true;
  }

  async _getStatus(){
    if(Array.isArray(this.allListing) && this.allListing.length >0){
      if(this.data.syncHandler){
          this.data.syncHandler(true);
      }
      try {
        let listing_type = this.props.screenProps.data.filter? this.props.screenProps.data.filter.listing_type? this.props.screenProps.data.filter.listing_type : 'sale' : 'sale';
        let key = "propDate"+listing_type;
        await AsyncStorage.setItem(key, moment().unix().toString())
          .then( ()=>{})
          .catch( ()=>{})
      } catch (error) {}

      for(var i = 0; i < this.allListing.length; i++){
        let property = this.allListing[i];
        let mid = property.mid? property.mid: 0;
        let nid = property.nid? property.nid: 0;
        const data = "mid=" +mid +
              "&nid=" + nid;
        
        this._getStats(data);
      }
    }
  }

  _getStats(body){
    FetchListing(PROP_STATUS, body)
    .then(data => this.getStatsSuccess(data))
    .catch(error => this.getStatsFail(error));
  }

  getStatsSuccess(response){
    this.synCount++;
    if(response && (response.mid || response.nid)){
      let update = {
        show_number : response.pageviewed,
        contacted : response.contactagent,
        view_contact : response.viewcontact,
        whatsapp_count : response.mobWhatsapp,
        impression : response.impressions,
        isFeatured : response.isFeatured 
      }
      if(response.featuredData){
        update.featuredData = JSON.stringify(response.featuredData);
      }
      let where = response.mid? "mid = '"+response.mid+"'" : "nid = '"+response.nid+"'";
      this.ListingActions.SetListing(update,where);
    }
    if(this.synCount == this.allListing.length){
      this.synCount = 0;
      if(this.data.syncHandler){
        this.data.syncHandler(false);
        let listingInfo = this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
        this.allListing = listingInfo;
        this.setState({trigger: moment().unix()})
      }
    }
  }
  
  getStatsFail(error){
    this.synCount++;
    if(this.synCount == this.state.fetchData.length){
      this.synCount = 0;
      if(this.data.syncHandler){
        this.data.syncHandler(false);
        let listingInfo = this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
        this.allListing = listingInfo;
        this.setState({trigger: moment().unix()})
      }
    }
  }

  _onSearch() {
    this.props.navigation.navigate('SearchOption');
  }

  _listingClick(index, listingType, idWithType){
    var numb = idWithType.replace(/[0-9]/g, '');
    if(!this.state.enableLongPress){
      this.props.navigation.navigate('Listing', {
        data: {
          lid: index,
          uid: this.state.userInfo.uid,
          token: this.state.userInfo.token
        },
        refresh: this._refresh
      });
    }else{
      let array = this.state.selected;
      let offset = array.indexOf(index);
      let alph = this.state.alpha;
      let alphaoffset = alph.indexOf(numb);
      if( offset > -1){
        array.splice(offset, 1);
      }else{
        if(array.length == 5){
          //this.setState({ isVisible: true, apiMessage: 'Maximum allowed section is 5!' });
          alert('Please select a maximum of 5 listings');
          return;
        }
        array.push(index);
        alph.push(numb);
      }

      
      
      this.setState({selected: array, valueId: idWithType, _listingType: listingType, alpha: alph, refresh: !this.state.refresh})
      if(array.length == 0){
        this.setState({enableLongPress: false})
        this._actionItems(true);
      }
    }
  }

  _handleBackdrop(){
      if((this.state.updated_records.count && this.state.updated_records.count> 0) 
        || (this.state.added_records.count && this.state.added_records.count> 0)){
        if(this.data.bulkHandler){
          this.data.bulkHandler()
        }
      }
      this.setState({
        isVisible: false,
        selected: [],
        isError: false,
        mid: '',
        featureItem: {},
        enableLongPress: false,
        updated_records: {},
        skipped_records: {},
        added_records: {},
        feature_day: moment().format('DD'),
        feature_month: moment().format('MM'),
        feature_year: parseInt(moment().format('YYYY')),
        featured_error: false,
        refresh: !this.state.refresh
      })
      this._actionItems(true);
    }

  _actionItems(status){
    this.showTab = status;
    if(this.data.actionHandler){
      this.data.actionHandler(status);
    }
  }

   _onLongPress(index, listingType, idWithType){
    var numb = idWithType.replace(/[0-9]/g, '');
    let array = this.state.selected;
    let offset = array.indexOf(index);
    let alph = this.state.alpha;
    let alphaoffset = alph.indexOf(numb);
    if( offset== -1){
      if(array.length == 5){
        //this.setState({ isVisible: true, apiMessage: 'Maximum allowed section is 5!' });
        alert('Maximum allowed section is 5!');
        return;
      }
      array.push(index);
      alph.push(numb);
    }
    
    
    this.setState({selected: array, _listingType: listingType, alpha: alph, refresh: !this.state.refresh})
    if(!this.state.enableLongPress){
      this._actionItems(false);
      this.setState({enableLongPress: true})
    }
    
  }

  _formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    _formatMoney(val) {
        return val ? 'RM ' + this._formatNumber(val) : '-'
    }

    _loadMore() {
      
    }


    _rePost(listingtype, idtype) {
      var numb = idtype.match(/\d/g);
      numb = numb.join("");
      let value = idtype.replace(/[0-9]/g, '');
      value ='LIDM' ? 'm' : 'n';
      let Idvalues = value.concat(numb)
      

      this.setState({ isLoading: true })
      const body = "uid=" + this.state.userInfo.uid +
            "&token=" + this.state.userInfo.token +
            "&field_prop_listing_type=" + listingtype +
            "&repost=1" +
            "&ids[0]=" + Idvalues +
            "&columns[changed]="
            
      BulkUpdate(BULKUPDATE, body)
      .then(data => this._bulkSuccess(data, "reposted"))
      .catch(error => this._bulkFail(error));
    }


    _bulkUpdate(listingtype, idtype, alpha) {
      let ids = '' 
        for (var i = 0; i< idtype.length; i++) {
          ids+= "ids["+i+"]="+alpha[i]+idtype[i]+"&";
        }
      

      this.setState({ isLoading: true })
      const body = "uid=" + this.state.userInfo.uid +
            "&token=" + this.state.userInfo.token +
            "&field_prop_listing_type=" + listingtype +
            "&repost=1&" +
            ids +
            "columns[changed]=";
      BulkUpdate(BULKUPDATE, body)
      .then(data => this._bulkSuccess(data))
      .catch(error => this._bulkFail(error));
    }

    _bulkFail(error){
      let message = 'Seems like something went wrong.';
      if(error.message){
        message = error.message;
      }
      this.setState({ isLoading: false, isVisible: true, apiMessage: message });
    }

    _handleUpdation(updatedIds, action){
      let selected = this.state.selected;
      for(var i=0; i< updatedIds.length ; i++){
        let offset =  selected.indexOf(updatedIds[i]);
        if( offset > -1){
          selected.splice(offset, 1);
          let update = {
            prop_status : action
          }
          this.ListingActions.SetListing(update, 'lid = '+updatedIds[i]);
        }
      }
      let listingData = this.ListingActions.GetListings('active', this.props.screenProps.data.filter);
      this.allListing = listingData;
      this.setState({ trigger: moment().unix(), selected: selected, refresh: !this.state.refresh })
    }

    _bulkSuccess(data, type) {
      let message = '';
      if(data.status == 'success'){
        if(type == 'Expired') {
          message = 'Property expired successfully done.'
          let updatedIds = data.result.updated_records.updated_id;
          this._handleUpdation(updatedIds, 'expired');
        }else {
          if(data.message == "Skipped properties are not eligible for this bulk action. Please check skipped messages") {
            message = data.result.skipped_records.message[0];
          }else {
            message = 'Property reposted successfully.';
          }
        }
      }
      if(message == 'You have exceeded your Repost Limit for the day.\nUpgrade to our PRO Agent subscription to enjoy unlimited reposts!'){
        this.setState({ isLoading: false, isVisible: true, apiMessage: message });
      }else{
        this.setState({ isLoading: false, isVisible: true, apiMessage: message, skipped_records: data.result.skipped_records, updated_records: data.result.updated_records });
      }
    }

    _againFetchOne() {
      this.setState({ isVisible: false, enableLongPress: false, selected: [], refresh: !this.state.refresh })
      this.showTab = !this.showTab;
      if(this.data.updateList){
        this.data.updateList(this.state.fetchData);
      }
    }

    async _featuredOption(mid, item) {
      if(item.featuredData){
        let featuredData = JSON.parse(item.featuredData);
        if(featuredData.start_date && featuredData.end_date){
          let startDate = moment.unix(featuredData.start_date).format("DD-MMM-YYYY");
          let endDate = moment.unix(featuredData.end_date).format("DD-MMM-YYYY");
          let message = 'Listing is already Featured from '+startDate+' to '+endDate;
          this.setState({ isLoading: false, isVisible: true, apiMessage: message });
          return;
        }
      }
      const plan = await AsyncStorage.getItem("agentPlan");
      if(plan == 2){
        this.setState({featuredShow : true, mid : mid, featureItem : item});
      }else{
        this.setState({ isLoading: false, isVisible: true, apiMessage: 'Upgrade to our PRO Agent subscription.' });
      }
    }
    _checkForm(){
      let check = this.dateValidate(this.state.feature_day,this.state.feature_month,this.state.feature_year);
      this.setState({ featured_error: check });
    }

    dateValidate(day,month,year) {
    
    let flag = false;
    let yesterday = moment().add(-1, 'days').format("YYYY-MM-DD");
    if(moment(day+'/'+month+'/'+year, 'DD/MM/YYYY').isValid())
    {
      if(!moment(year+'-'+month+'-'+day).isAfter(yesterday))
      {
        flag = true;
      }else{
        flag = false;
      }
    }else{
      flag = true;
    }
    return flag;
  }

    _makeFeatured(){
      if(!this.state.featured_error){
        this.setState({ isLoading: true })
        let obj = {
          uid: this.state.userInfo.uid,
          featureds:[],
          token: this.state.userInfo.token
        }
        let start_date = this.state.feature_year+'-'+this.state.feature_month+'-'+this.state.feature_day;
        obj.featureds.push({property_id : this.state.mid , start_date: start_date})      
        PostData(FEATURED, qs.stringify(obj))
        .then(data => this.featuredSuccess(data))
        .catch(error => this.featuredFail(error));
      }
      
    }

  featuredSuccess(data){
    this.setState({ isLoading: false, featuredShow : false }, () => {
      if(data.result){
        let result = data.result;
        let message = data.message; 
        let isError = false;
        let property_point_crossed = result.point_error_messages.property_point_crossed;
        if(result.added_records && result.added_records.count > 0){
          let property_id = result.added_records.property_id;
          if(property_id.length >0){
            message = property_id[0].message;
          }
        }else if(result.skipped_records && result.skipped_records.count > 0){
          let details = result.skipped_records.details;
          if(details.length >0){
            message = details[0].message;
          }
          isError = true;
        }else if(property_point_crossed && property_point_crossed.length >0){
          let cross_proints = Object.values(property_point_crossed);
          let cross_point_id = Object.keys(property_point_crossed);
          let cross_id = cross_point_id[0];
          let cross_msg = cross_proints[0];
          message = cross_id+':'+cross_msg;
          isError = true;
        }
        
        
        if(message == 'This agent is not under premium plan'){
          message = 'Upgrade to our PRO Agent subscription.'
        }
        this.setState({ isVisible: true, isError, apiMessage: message, added_records: result.added_records });
        if(!isError){
          let listing_type = this.props.screenProps.data.filter? this.props.screenProps.data.filter.listing_type? this.props.screenProps.data.filter.listing_type : 'sale' : 'sale';
          
          let id = this.state.mid;
          let key = 'm';
          let mid = this.state.featureItem.mid;
          let nid = this.state.featureItem.nid;
          if(this.state.mid.startsWith('LIDM')){
            id = id.replace('LIDM', '')
            key = 'm';
          }else if(this.state.mid.startsWith('LIDN')){
            id = id.replace('LIDN')
            key = 'n';
          }
          if(this.data.updateList){
            this.data.updateList([
              { 
                id_type: key,
                lid: id,
                listing_type: listing_type,
                status: 'fetch',
                mid: mid,
                nid: nid
              }
            ]);
          }
        }
      }else{
        this.setState({ isVisible: true, isError: true, apiMessage: 'Unable to make listing as featured!' });
      }
    })
  }

  featuredFail(error){
    this.setState({ isLoading: false, featuredShow : false })
    this.setState({ isVisible: true, isError: true, apiMessage: 'Unable to make listing as featured!' });
  }

  _onScroll = event => {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const dif = currentOffset - (this.scrollOffset || 0);
    let direction = '';
    if (Math.abs(dif) < 3) {
      
    } else if (dif < 0) {
      direction = 'up';
    } else {
      if(currentOffset > 0){
        direction = 'down';
      }
    }

    this.scrollOffset = currentOffset;
    if(direction && this.direction !=direction ){
      this.direction = direction;
      if(this.data.toggleAdd){
        this.data.toggleAdd(direction, 'showAddActive');
      }
    }
  }

  checkSelected= (index) => {
    return this.state.selected.indexOf(index) > -1 ? true : false;
  }

  render() {
    

    const LeftNavComponent = () => {
      return (
        <View style={styles.LeftMenuIcon} >
          <TouchableOpacity style={{padding: 15}}  onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../../assets/images/menu-button.png')}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <TouchableOpacity style={styles.navCenterInner} onPress={this._onSearch}>
            <Text allowFontScaling={false} style={styles.navTitle}>Find Properties...</Text>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../../assets/images/search.png')}
            />
          </TouchableOpacity>
        </View>
      )
    }

    var _renderRelatedItem = (item,i) => {  
      let price = item.price ? item.price : '0';
      let image = image = item.cover_image ? item.cover_image : item.images[0] ? item.images[0] : 'https://list.edgeprop.my/static/img/no-image.png';
      let is_public_photo = image.search("public://");
      if(is_public_photo == 0){
        image = 'https://list.edgeprop.my/static/img/no-image.png';
      }
      let impression = item.impression ? item.impression : 0;
      let whatsapp = item.whatsapp_count ? item.whatsapp_count : 0;
      let showNumber = item.view_contact ? item.view_contact : 0;
      let mid = item.mid ? 'LIDM' + item.mid : 'LIDN' + item.nid;
      let idWithType = item.mid ? 'm' + item.mid : 'n' + item.nid;
      let url = item.url ? item.url : '';
          url = "https://www.edgeprop.my/listing/"+url;
      let isFeatured = item.isFeatured? item.isFeatured : false
        return (
            <View style={{paddingHorizontal: 15, marginVertical: 5}}>
                
                <TouchableOpacity style={checkSelected(item.lid)? [styles.cardItem, styles.cardSelected] : styles.cardItem} onPress={() => this._listingClick(item.lid, item.listing_type, idWithType)} onLongPress={() =>this._onLongPress(item.lid, item.listing_type, idWithType)}>
                  {checkSelected(item.lid) && <Image
                    style={styles.selectedIcon}
                    source={require('../../../assets/images/selected.png')}
                  />}
                  <View style={styles.cardImgWrapper}>
                    {/*  Card Image */}
                    <ImageBackground source={{uri: image}} style={styles.cardImage}>
                      <LinearGradient colors={['rgba(0,0,0,0.5)', 'transparent', 'rgba(0,0,0,0.8)']} style={styles.cardOverlay}>
                        <Text allowFontScaling={false} style={styles.cardOverlayText}>{mid}</Text>
                        <View style={styles.cardOverlayInner}>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{impression}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../../../assets/images/steps.png')}
                            />
                          </View>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{showNumber}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../../../assets/images/view.png')}
                            />
                          </View>
                          <View style={styles.cardOverlayItem}>
                            <Text allowFontScaling={false} style={styles.cardOverlayText}>{whatsapp}</Text>
                            <Image
                              style={{width: 15, height: 15}}
                              source={require('../../../assets/images/whatsapp-sm.png')}
                            />
                          </View>
                        </View>
                      </LinearGradient>
                    </ImageBackground>
                  </View>
                  {/*  Card Content */}
                  <View style={styles.cardContentWrapper}>
                    <View style={styles.cardContentTop}>
                      <Text allowFontScaling={false} style={styles.cardTitle} ellipsizeMode='tail' numberOfLines={1}>{item.title}</Text>
                      {(item.district != '' || item.state !='') &&
                      <View style={styles.cardLocation}>
                        <Icon
                          name='ios-pin'
                          size={15}
                          type='ionicon'
                          color='#96d9c2'
                        />
                        <Text allowFontScaling={false} style={styles.cardContentLocation} ellipsizeMode='tail' numberOfLines={1}>{item.district ? item.district+',' : ''} {item.state ? item.state : ''}</Text>
                      </View>
                      }

                      <Text allowFontScaling={false} style={styles.cardPrice}>{this._formatMoney(price)}</Text>
                      <View style={styles.cardLocation}>
                        <Icon
                          name='clock'
                          size={10}
                          type='feather'
                          color='#646975'
                        />
                      <Text allowFontScaling={false} style={styles.cardUpdateDate}>{moment.unix(item.changed).fromNow()}</Text>
                    </View>
                    </View>
                    <View style={styles.cardContentBottom}>
                      <TouchableOpacity onPress={() => (!this.state.enableLongPress) ? this._openUrl(url) : ''} style={styles.cardActionItem}>
                        <Icon
                          name='external-link'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>View</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.cardActionItem} onPress={() => (!this.state.enableLongPress) ? this._rePost(item.listing_type,mid) : ''}>
                        <Icon
                          name='corner-left-up'
                          size={24}
                          type='feather'
                          color='#10182d'
                        />
                        <Text allowFontScaling={false} style={styles.cardActionText}>Repost</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => (!this.state.enableLongPress) ? this._featuredOption(mid, item) : ''} style={styles.cardActionItem}>
                        <Icon
                          name={isFeatured? 'star' : 'star-outlined'}
                          size={24}
                          type='entypo'
                          color={isFeatured? '#ffb400' : '#10182d'}
                        />
                        {/* <Icon
                          name='star'
                          size={28}
                          type='ionicons'
                          color='#ffb400'
                        /> */}
                        <Text allowFontScaling={false} style={styles.cardActionText}>Featured</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableOpacity>
                </View>
        )        
      }



    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          {/* <HeaderSearch navigation={this.props.navigation} /> */}
          
          <View style={styles.innerContainer}>

          {(this.allListing.length == 0 && this.state.isLoading == false) && (
            <View style={styles.noDataContainer}>
              <Image
                style={{width: 75, height: 75}}
                source={require('../../../assets/images/no-list.png')}
              />
              <Text allowFontScaling={false} style={styles.textMdNoRecord}>No Records Found ! </Text>
            </View>
          )}
          <View style={styles.cardWrapper}>
            <FlatList
              data={this.allListing}
              renderItem={({ item, index }) => 
                <PropertyItem 
                  item={item} 
                  index={index} 
                  type={'active'}
                  selected={this.state.selected}
                  listingClick={this._listingClick}
                  onLongPress={this._onLongPress}
                  enableLongPress={this.state.enableLongPress}
                  onOpenUrl={this._openUrl}
                  onRepost={this._rePost}
                  onFeatured={this._featuredOption}
                  checkSelected={this.checkSelected}
                  refresh={this.state.refresh}
              />}
              keyExtractor={(item) => item.lid.toString()} 
              onMomentumScrollBegin={() => { this.isScrolling = true; }}
              onEndReached={()=>{
                if(this.isScrolling){
                  this._loadMore();
                }
              }}
              ListHeaderComponent={() => ( <View style={{paddingTop: 10}}/>)}
              ListFooterComponent={() => ( <View style={{paddingBottom: 10}}/>)}
              onEndReachedThreshold={0.5}
              onScroll={this._onScroll}
              bounces={false}
            />
          </View>
            
          </View>
          {/* Bulk Actions */}
          {!this.showTab && <View style={styles.bottomActions}>
            <TouchableOpacity style={styles.bottomActionItem} onPress={() => this._bulkUpdate(this.state._listingType, this.state.selected, this.state.alpha)}>
              <Icon
                name='corner-left-up'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>
           {/*<TouchableOpacity style={styles.bottomActionItem}>
              <Icon
                name='star'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>*/}
            <TouchableOpacity style={styles.bottomActionItem} onPress={() =>this.expireAll(this.state._listingType, this.state.selected, this.state.alpha)}>
              <Icon
                name='archive'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomActionItem} onPress={this._cancelBulk}>
              <Text>Cancel</Text>
            </TouchableOpacity>
          </View>}

          {/* Overlay */}
           <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={this._handleBackdrop}
            >
              <View>
                <View style={[styles.overlayInner, this.state.isError? styles.bgDanger : styles.bgInfo]}>
                  <Icon
                    name='info'
                    type='material'
                    color='#fff'
                    size={30}
                  />
                  <Text allowFontScaling={false} style={styles.overlayText}>{this.state.apiMessage}</Text>
                </View>
                {/*!!this.state.updated_records.count &&  this.state.updated_records.count > 0 && !!this.state.updated_records.title && this.state.updated_records.title.map((message,i) => {
                    return (
                  <View style={[styles.overlayInner, styles.bgSuccess]} key={i}>
                    <Icon
                      name='info'
                      type='material'
                      color='#fff'
                      size={30}
                    />
                    
                    <Text allowFontScaling={false} style={styles.overlayText}>{message}</Text>
                  </View>)
                  })*/}
                {/*!!this.state.skipped_records.count &&  this.state.skipped_records.count > 0 && !!this.state.skipped_records.title && this.state.skipped_records.title.map((message,i) => {
                    return (
                  <View style={[styles.overlayInner, styles.bgDanger]} key={i}>
                    <Icon
                      name='info'
                      type='material'
                      color='#fff'
                      size={30}
                    />
                    
                    <Text allowFontScaling={false} style={styles.overlayText}>{message}</Text>
                  </View>)
                  })*/}
                  {this.state.skipped_records.count > 0 && 
                    <View style={[styles.overlayInner, styles.overlayInnerError, styles.bgDanger]}>
                      <View style={styles.bgDangerInner}>
                        <Icon
                          name='info'
                          type='material'
                          color='#fff'
                          size={30}
                        />
                        <View style={styles.alertWrapper}>
                          {!!this.state.skipped_records.count &&  this.state.skipped_records.count > 0 && !!this.state.skipped_records.title && this.state.skipped_records.title.map((message,i) => {
                          return (
                          <View style={[styles.listViewDeleted, styles.listViewStyle]} key={i}>
                            <Text allowFontScaling={false} style={styles.listShape}>{'\u2022'}</Text>
                            <Text allowFontScaling={false} style={styles.overlayText}>{message}</Text>
                          </View>
                          )
                        })}
                        </View>
                      </View>  
                    </View>}
              </View>
            </Overlay>

        {/* Overlay */}
        <Overlay
          isVisible={this.state.featuredShow}
          overlayStyle={[styles.overlayFeaturedStyle]}
          containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
          width="75%"
          height="auto"
          onBackdropPress={() => console.log('backdrop')}
        >
          <View>
            <View style={[styles.overlayPopContent, {borderBottomWidth: 1, borderBottomColor: 'rgba(0, 0, 0, 0.2)'}]}>
              <Text allowFontScaling={false} style={styles.overlayPopText}>Add to Featured</Text>
            </View>
            <View style={[styles.overlayPopContent, {paddingHorizontal: 10}]}>
              <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
                <View style={styles.fieldWrapThreeWay}>
                  <CustomSelect
                    value={this.state.feature_day}
                    label={'Day'}
                    placeholder='Please Select Day'
                    data={aucDay}
                    style={styles.viewAllWrap}
                    baseStyle={styles.viewAllBase}
                    fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                    onChangeText={(day) => this.setState({ feature_day :  day }, () => this._checkForm())}
                  />
                </View>
                <View style={styles.fieldWrapThreeWay}>
                  <CustomSelect
                    value={this.state.feature_month}
                    label={'Month'}
                    placeholder='Please Select Month'
                    data={aucMonth}
                    style={styles.viewAllWrap}
                    baseStyle={styles.viewAllBase}
                    fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                    onChangeText={(month) => this.setState({ feature_month :  month }, () => this._checkForm())}
                  />
                </View>
                <View style={styles.fieldWrapThreeWay}>
                  <CustomSelect
                    value={this.state.feature_year}
                    label={'Year'}
                    placeholder='Please Select Year'
                    data={aucYear}
                    style={styles.viewAllWrap}
                    baseStyle={styles.viewAllBase}
                    fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                    onChangeText={(year) => this.setState({ feature_year :  year }, () => this._checkForm())}
                  />
                </View>
              </View>
              {this.state.featured_error &&<View style={styles.errorContainer}>
                <Text allowFontScaling={false} style={styles.errorMsg}>Past date is not allowed</Text>
              </View>}
            </View>
            <View style={styles.overlayPopButtons} >
              <TouchableOpacity style={[styles.OverlayPopBtn, {borderRightColor: 'rgba(0,0,0,0.2)', borderRightWidth: 1}]} onPress={()=> this.setState({ featuredShow : false })} >
                <Text allowFontScaling={false} style={[styles.OverlayPopBtnText, {color: '#ff3c3c'}]}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.OverlayPopBtn} onPress={()=> this._makeFeatured()}>
                <Text allowFontScaling={false} style={styles.OverlayPopBtnText}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Overlay>

        </SafeAreaView>
        {
          this.state.isLoading && (
            <Loading />
          )
        }
      </LinearGradient>
    );
  }
}

export default ActiveList
