import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#3670e5',
        position: 'relative',
        zIndex: 9
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    navIconRight: { 
        paddingLeft: 20,
        minHeight: 24
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },
    commonLabel: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-Regular',
        color: '#9E9E9E',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        paddingBottom: 2
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
});