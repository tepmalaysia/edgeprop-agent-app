/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  Linking
} from 'react-native';
import styles from './InfoStyles';
import { Avatar, Icon, ListItem } from 'react-native-elements';
import { Header, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { DrawerActions } from 'react-navigation-drawer';
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';
import * as NetInfo from "@react-native-community/netinfo"
import { GetData } from '../../services/api'
import Loading from '../../../components/Loader';

let connectionType = {
	'wifi': 'WiFi', 
	'cellular': 'Cellular',
	'none': '',
	'unknown': '',
	'bluetooth': '',
	'ethernet': '',
	'wimax': '',
	'vpn': '',
	'other': '',
}
class Info extends Component{

  constructor(props) {
    super(props);
    this.state = { 
    	osName: '-',
    	deviceModel: '-',
      osVersion: '-',
    	deviceBuildVersion: '-',
    	deviceBuildCode: '-',
    	connectType: '-',
    }
  }

  componentDidMount() {
  	this.setState({
  		osName: DeviceInfo.getBrand(),
    	deviceModel: DeviceInfo.getModel(),
      osVersion: DeviceInfo.getSystemVersion(),
    	deviceBuildVersion: DeviceInfo.getVersion(),
    	deviceBuildCode: DeviceInfo.getBuildNumber(),
  	})
  	NetInfo.fetch().then(state => {
	  this.setState({ connectType : connectionType[state.type]})
	});
  }
  
  render() {

	let list = [
	  {
	    name: 'Device',
	    icon: 'smartphone',
	    subtitle: this.state.osName+' '+this.state.osVersion+','+this.state.deviceModel//'Android 10, OnePlus ONEPLUS A600'
	  },
	  {
	    name: 'Build Version',
	    icon: 'airplay',
	    subtitle: this.state.deviceBuildVersion+' ('+this.state.deviceBuildCode+')'//'1.8.2 (194)'
	  },
	  {
	    name: 'Internet Connection',
	    icon: 'activity',
	    subtitle: this.state.connectType//'WiFi'
	  },
	]

  if(Platform.OS === 'android') {
    list = [
      {
        name: 'Device',
        icon: 'smartphone',
        subtitle: 'Android '+this.state.osVersion +', '+ this.state.deviceModel//'Android 10, OnePlus ONEPLUS A600'
      },
      {
        name: 'Build Version',
        icon: 'airplay',
        subtitle: this.state.deviceBuildVersion+' ('+this.state.deviceBuildCode+')'//'1.8.2 (194)'
      },
      {
        name: 'Internet Connection',
        icon: 'activity',
        subtitle: this.state.connectType//'WiFi'
      },
    ]
  }


    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={() => {this.props.navigation.goBack()}}>
            <Icon
              name='arrow-left'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>About</Text>
        </View>
      )
    }


    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>

          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
          />
          <View style={styles.innerContainer}>
            <ScrollView>
              <View style={styles.aboutWrapper}>
              	{
				    list.map((l, i) => (
				      <ListItem
				        key={i}
				        title={l.name}
				        subtitle={l.subtitle}
				        titleStyle={styles.commonLabel}
				        subtitleStyle={styles.textSm}
				        leftIcon={
				        	<Icon
							  name={l.icon}
							  type='feather'
							  size={24}
							  color='#10182d'
							/>}
				        bottomDivider
				      />
				    ))
				 }
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
        {
            this.state.isLoading && (
              <Loading />
            )
          }
      </LinearGradient>
    );
  }
}


export default Info