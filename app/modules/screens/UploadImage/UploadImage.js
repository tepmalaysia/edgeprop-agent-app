/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    Switch,
    Dimensions
} from 'react-native';
import { CheckBox, Icon, Input, Header, Overlay } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';
import * as Progress from 'react-native-progress';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './UploadImageStyle';
import { futch, PostData } from '../../services/api'
import qs from "qs";

const { width, height } = Dimensions.get('window');
export default class Listing extends Component{

  constructor(props){
    super(props);
    this.data = props.navigation.state.params.data? props.navigation.state.params.data : {};
    let listingInfo = this.data.listingDetails? (this.data.listingDetails.data? JSON.parse(this.data.listingDetails.data) : {}) : {};
    
    this._handleBackPress = this._handleBackPress.bind(this);
    this._chooseImage = this._chooseImage.bind(this)
    this._deleteImage = this._deleteImage.bind(this)
    this._removeImage = this._removeImage.bind(this)
    this._removeImageLocal = this._removeImageLocal.bind(this)
    this._uploadImage = this._uploadImage.bind(this)
    this.checkImages = this.checkImages.bind(this)
    this._selectAll = this._selectAll.bind(this)
    this._selectImage = this._selectImage.bind(this)
    this._uploadAll = this._uploadAll.bind(this)
    this._deleteAll = this._deleteAll.bind(this)
    this._deleteSuccess = this._deleteSuccess.bind(this)
    this._deleteFail = this._deleteFail.bind(this)
    this.setImageAsDefault = this.setImageAsDefault.bind(this)
    this._imageLimit = this._imageLimit.bind(this)
    this.accept = [
      "image/png",
      "image/gif",
      "image/jpeg",
      "image/webp"
    ];
    this.maxSize = 10 * 1024 * 1024;
    this.minSize = 1 * 1024;
    this.minWidth = 200;
    this.minHeight = 200;
    this.apiActive = false;
    this.activeCount = 0;
    this.resolvedCount = 0;
    this.state = {
      images : this.data.images? this.data.images: {},
      chosenImages : [],
      userInfo: {},
      progress: [],
      upload: false,
      isVisible: false,
      selected: [],
      selectAll: false,
      isError: false,
      message: '',
      messages: [],
      isDefault: false,
      isWatermark: this.data.watermark? true : false
    }
    
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems })
      }
    }
  }

  _handleBackPress() {
    let filterItems = this.state.chosenImages.filter(item => item.uploaded != 2);
    if(filterItems.length > 0){
      this.setState({
        isVisible: true,
        message: 'You have unsaved images do you want to disgard them?'
      })
    }else{
      if(this.data.setForm){
        let passedData = { 'property_images': this.state.images };
        if(this.state.chosenImages.length > 0){
          passedData.isChanged = true;
        }
        this.data.setForm(passedData)
      }
      this.props.navigation.goBack();
    }
  }

  _chooseImage () {
    ImagePicker.openPicker({
      multiple: true,
      mediaType: 'photo',
      cropping: true,
      smartAlbums: ['UserLibrary']
    }).then(images => {
      if(images.length > 0){
        this.checkImages(images)
      }
    }).catch(e => {
      
    });
  }

  checkImages(images){
    let filtered = this.state.chosenImages;
    let messages = [];
    let progress = [];
    this.setState({ messages, progress })
    let filterItems = this.state.chosenImages.filter(item => item.uploaded != 2);
    let total = this.state.images.length + filterItems.length;
    let limit = false;
    images.map((image,i) => {
      let error = false;
      let message = '';
      
      if( total > 19) {
        error = true;
        limit = true;
        message = 'Maximum of 20 files are allowed to upload.'
      }
      if(this.accept.indexOf(image.mime) == -1){
        error = true;
        message = 'Only png gif jpg jpeg file types are allowed.'
      }
      if(image.size > this.maxSize){
        error = true;
        message += 'Maximum image size to upload is 10MB.'
      } 
      if(image.size < this.minSize){
        error = true;
        message += 'Image is too small to upload.'
      } 
      if(image.height < this.minHeight && image.width < this.minWidth){
        error = true;
        message +='Minimum image dimension is - Width : 200px, Height : 200px.'
      }
      if(!error){
        filtered.push({...image, uploaded: 0});
        total++;
        progress.push(0)
      }else{
        if(messages.indexOf(message) < 0){
          messages.push(message)
        }
      }
    })
    
    this.setState({
      chosenImages : filtered,
      message: limit? 'Image(s) limit exceeded!' : 'Image(s) is not valid!',
      messages,
      isVisible: messages.length > 0? true : false,
      progress
    })
  }

  _uploadAll(){
    if(this.apiActive) {
      this.setState({isVisible : true, message: 'Please wait or upload individually' });
      return;
    }
    this.state.chosenImages.map((item,i) => {
      if(item.uploaded == 0){
        this._uploadImage(item, i);
      }
    });
  }

  _uploadImage(data, index){
    this.apiActive = true;
    this.activeCount++;
    let actualIndex = index;
    this.state.chosenImages.map((item,i) => {
      if(item.path == data.path){
        actualIndex = i;
      }
    });
    let chosenImages = [...this.state.chosenImages];
    chosenImages[actualIndex].uploaded = 1;
    let stateProgress = [...this.state.progress];
    stateProgress[index] = 0;
    this.setState({upload : true, chosenImages, progress : stateProgress })
    if(Platform.OS ==='android' && data.filename == undefined) {
      data.filename = data.path.split('/').pop();
    }
    var photo = {
        uri: data.path,
        type: data.mime,
        name: data.filename,
    };

    let body = new FormData();
    body.append('file', photo);
    body.append('image_type', 1)
    body.append('uid', this.state.userInfo.uid);
    body.append('token', this.state.userInfo.token);


    futch('https://prolex.edgeprop.my/api/v1/imageUpload', {
      method: 'post',
      body: body,
      headers: {
        "Content-Type": "multipart/form-data"
      }
    }, (progressEvent) => {
      const progress = progressEvent.loaded / progressEvent.total;
      let stateProgress = [...this.state.progress];
      stateProgress[index] = progress;
      this.setState({
        progress : stateProgress
      })
      /*if(progress == 1){
        let chosenImages = [...this.state.chosenImages];
        chosenImages[index].uploaded = 2;
        this.setState({upload : false, chosenImages })
      }*/
    }).then((res) => {
      this.resolvedCount++;
      if(this.activeCount == this.resolvedCount){
        this.activeCount = this.resolvedCount = 0;
        this.apiActive = false;
      }
      if(res.readyState == 4 && res.status == 200){
        let data = res._response? JSON.parse(res._response) : {};
        if(data.s3_image_details){
          let images = this.state.images;
          images.push(data.s3_image_details[0]);
          let chosenImages = this.state.chosenImages;
          chosenImages[actualIndex].uploaded = 2;
          this.setState({
            chosenImages,
            images,
            upload : false
          })
          /*if(!this.apiActive){
            this.setState({
              isVisible: true,
              message: 'Inorder to save the images ,please go back and save.'
            })
          }*/
        }
      }else{
        let data = {};
        try{
          data = res._response? JSON.parse(res._response) : {};
        }catch(err){
          data = {};
          if(res.status == 413){
            data.message ='Image size is too large to upload';
          }
        }
        
        let messages = this.state.messages;
        let msg = data.message? data.message : 'Image upload failed';
        let message = photo.name+': '+msg;
        messages.push(message)
        let chosenImages = this.state.chosenImages;
        chosenImages[actualIndex].uploaded = 0;
        this.setState({
          chosenImages,
          upload : false,
          message: 'Image upload failed',
          messages,
          isVisible: true
        })
      }
    }, (err) => {
      let data = {};
        try{
          data = res._response? JSON.parse(res._response) : {};
        }catch(err){
          data = {};
          if(res.status == 413){
            data.message ='Image size is too large to upload';
          }
        }
      let messages = this.state.messages;
      let msg = data.message? data.message : 'Image upload failed';
      let message = photo.name+': '+msg;
      messages.push(message)
      this.setState({
        message: 'Image upload failed',
        messages,
        isVisible: true
      })
      this.resolvedCount++;
      if(this.activeCount == this.resolvedCount){
        this.activeCount = this.resolvedCount = 0;
        this.apiActive = false;
      }
      let chosenImages = this.state.chosenImages;
      chosenImages[actualIndex].uploaded = 0;
      this.setState({
        chosenImages,
        upload : false
      })
    })
  }

  _imageLimit() {
    this.setState({
      message: 'Maximum 20 files are allowed to upload.',
      isVisible:true
    })
  }
     
  _deleteAll() {
    if(this.apiActive) {
      this.setState({isVisible : true});
      return;
    }
    if(this.state.chosenImages.filter(item => item.uploaded != 2).length > 0){
      this.setState({
        isError : true, 
        isVisible: true,
        message: 'You have un-uploaded images!'
      });
      return;
    }

    this.setState({chosenImages : []})
    let keepImages = [];
    this.state.images.map((item, i) => {
      let index = this.state.selected.indexOf(item.filename);
      if(index == -1){
        keepImages.push(item);
      }
    });
    this.setState({
      images : keepImages, 
      selected: [],
      isVisible: true, 
      isError : false, 
      message: 'You need to save the property on Edit screen to reflect these changes!'
    });
    
  }

  _deleteSuccess(data){
    
  }

  _deleteFail(error){
    
  }

  _deleteImage(item, flag) {
    
    if(flag){
      let images = this.state.images;

    }else{
      let images = this.state.chosenImages;
      this._removeImageLocal(images, item)
    }
  }

  _removeImage(images, index) {

  }

  array_move(arr, old_index, new_index) {
    let updatearray = [];
    updatearray.push(arr[old_index]);
    Object.entries(arr).map(([key, value]) => {
      if(key !=old_index){
        updatearray.push(value);
      }
    });
    return updatearray; // for testing
  }

  setImageAsDefault(){
    if(this.state.selected.length == 1){
      let actualIndex = 0;
      if(this.state.isDefault){
        Object.entries(this.state.images).map(([key, value]) => {
          if(value.filename == this.state.selected[0]){
            actualIndex = key;
          }
        });
        if(actualIndex != 0){
          this.setState({ images : this.array_move(this.state.images, actualIndex, 0)})
        }
      }
    } else if(this.state.isDefault){
      this.setState({
        isVisible: true,
        isDefault: false,
        message: 'Select an image to set it as default!'
      })
    }
    
  }

  _removeImageLocal(images,data) {
    let index = -1;
    this.state.chosenImages.map((item,i) => {
      if(item.path == data.path){
        index = i;
      }
    });
    let updated = images.splice(index, 1);
    this.setState({
      chosenImages : images
    })

  }

  _selectAll(){
    if(this.apiActive) {
      this.setState({isVisible : true});
      return;
    }
    let array = this.state.selected;
    if(this.state.selectAll){
      array = [];
    }else{
      this.state.images.map((image,i) => {
        let offset = array.indexOf(image.filename);
        if( offset == -1){
          array.push(image.filename);
        }
      });
    }
    
    this.setState({selected: array, selectAll : !this.state.selectAll, isDefault: false})
  }

  _selectImage(fileName){
    if(this.apiActive) {
      this.setState({isVisible : true});
      return;
    }
    let array = this.state.selected;
    let offset = array.indexOf(fileName);
    if( offset > -1){
      array.splice(offset, 1);
    }else{
      array.push(fileName);
    }
    this.setState({selected: array, isDefault: false})
  }

  render() {
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={()=> this._handleBackPress()}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#fff'
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>Add image</Text>
        </View>
      )
    }

    const _checkSize = (data) => {
      let size = data.length? data.length : 0;
      if(typeof data == 'object'){
        size = Object.keys(data).length;
       }
      return size;
    }

    const _showProgress = (index) => {
      let flag = false;
      if(this.state.progress[index] && this.state.progress[index] != 1){
        flag = true;
      }
      return flag;
    }

    const checkSelected= (index) => {
      return this.state.selected.indexOf(index) > -1 ? true : false;
    }

    let filterItems = this.state.chosenImages.filter(item => item.uploaded != 2);
    
    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
          />
          <View style={styles.innerContainer}>
            <View style={styles.contentSection}>
              <View style={styles.CheckBoxWrapper}>
                {this.state.selected.length < 2 && <CheckBox
                  title='Set as default image'
                  iconType='material'
                  checkedIcon='check-box'
                  uncheckedIcon='check-box-outline-blank'
                  checkedColor='#3670e5'
                  checked={this.state.isDefault}
                  containerStyle={styles.checkBox}
                  textStyle={styles.textSm}
                  onPress={() => this.setState({isDefault: !this.state.isDefault}, () => this.setImageAsDefault())}
                />}
                <CheckBox
                  title='Add name in watermark'
                  iconType='material'
                  checkedIcon='check-box'
                  uncheckedIcon='check-box-outline-blank'
                  checkedColor='#3670e5'
                  checked={this.state.isWatermark}
                  containerStyle={styles.checkBox}
                  textStyle={styles.textSm}
                  onPress={() => this.setState({isWatermark: !this.state.isWatermark}, () => this.data.setForm({watermark : this.state.isWatermark}))}
                />
              </View>
            </View>
            <ScrollView>
              <View style={styles.editImageWrapper}>

                  {Object.keys(this.state.images).length > 0 && Object.entries(this.state.images).map(([key, value]) => {
                    return (
                      <TouchableOpacity style={[styles.editImageItem]} key={key} onPress={() => this._selectImage(value.filename)}>
                        <ImageBackground source={{uri : value.gallery_uri}} style={key == 0 ? [styles.cardImage, styles.defaultImage] : [styles.cardImage]}>
                          {checkSelected(value.filename) &&
                            <View style={styles.cardOverlay}>
                              <View style={styles.cardOverlayInner}>
                                <Icon
                                  name='check'
                                  size={40}
                                  type='feather'
                                  color='#fff'
                                />
                              </View>
                            </View>
                          }
                        </ImageBackground>
                      </TouchableOpacity>
                      )
                  })}

                  {filterItems.length > 0 && filterItems.map((item,i) => {
                    return (
                      <TouchableOpacity style={[styles.editImageItem]} key={i}>
                        <ImageBackground source={{uri : item.path}} style={[styles.cardImage, styles.defaultImage]}>
                          <View style={styles.cardOverlay}>
                            <View style={styles.cardOverlayInner}>
                              {item.uploaded == 0 &&<TouchableOpacity style={styles.editImageAction} onPress={() => this._uploadImage(item, i)}>
                                <Icon
                                  name='upload'
                                  size={17}
                                  type='feather'
                                  color='#fff'
                                />
                              </TouchableOpacity>}
                              {item.uploaded == 0 && <TouchableOpacity style={styles.editImageAction} onPress={() => this._deleteImage(item, false)}>
                                <Icon
                                  name='trash-2'
                                  size={17}
                                  type='feather'
                                  color='#fff'
                                />
                              </TouchableOpacity>}
                              {item.uploaded == 1 &&<Progress.Bar 
                                progress={this.state.progress[i]} 
                                width={width * 0.2} 
                                height={4}
                                color='#fff'
                                unfilledColor='rgba(16, 24, 45, 0.6)'
                              />}
                            </View>
                          </View>
                        </ImageBackground>
                      </TouchableOpacity>
                      )
                  })}
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
        {/* Bulk Actions */}
        <SafeAreaView >
          <View style={styles.bottomActions}>
            {/*Object.keys(this.state.images).length > 0 && <TouchableOpacity style={styles.bottomActionItem} onPress={this._selectAll}>
              <Icon
                name='done-all'
                type='ionicons'
                size={24}
                color={this.state.selectAll? '#3670e5' :'#10182d'}
              />
            </TouchableOpacity>*/}
            {filterItems.length > 0 && <TouchableOpacity style={styles.bottomActionItem} onPress={this._uploadAll}>
              <Icon
                name='upload'
                type='feather'
                size={24}
                color='#10182d'
              />
            </TouchableOpacity>}
            <TouchableOpacity style={styles.bottomActionItem} onPress={this._chooseImage}>
              <Icon
                name='add-circle'
                type='ionicons'
                size={24}
                color='#10182d'
              />
            </TouchableOpacity>
            {this.state.selected.length > 0 && <TouchableOpacity style={styles.bottomActionItem} onPress={this._deleteAll}>
              <Icon
                name='trash-2'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>}
          </View>
          {/* Overlay */}
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible: false, message: '', messages: [] })}
            >
              <View>
                <View style={[styles.overlayInner, this.state.isError? styles.bgDanger : styles.bgWarning]}>
                  <Icon
                    name='info'
                    type='material'
                    color='#fff'
                    size={26}
                  />
                  <Text allowFontScaling={false} style={styles.overlayText}>{this.state.message}</Text>
                </View>

                {this.state.messages.length > 0 && this.state.messages.map((message,i) => {
                    return (
                <View style={[styles.overlayInner, this.state.isError? styles.bgDanger : styles.bgWarning]} key={i}>
                  <Icon
                    name='info'
                    type='material'
                    color='#fff'
                    size={26}
                  />
                  
                  <Text allowFontScaling={false} style={styles.overlayText}>{message}</Text>
                </View>)
                })}
              </View>
            </Overlay>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
