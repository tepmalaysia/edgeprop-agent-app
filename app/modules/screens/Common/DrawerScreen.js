import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import PropTypes from 'prop-types';
import {ScrollView, Text, View, SafeAreaView, Image, TouchableOpacity, Alert, Dimensions} from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import LinearGradient from 'react-native-linear-gradient';
import { Avatar } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import DeviceInfo from 'react-native-device-info';
import Intercom from 'react-native-intercom';
import styles from '../../styles/index';
import { SignOut, FetchAgent } from '../../services/api'
import Loading from '../../../components/Loader';
import ProfileActions from '../../../../realm/actions/ProfileActions'
import ListingActions from '../../../../realm/actions/ListingActions'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import StateData from '../../../assets/json/states.json'

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/logout';
const FETCHAGENT = 'https://prolex.edgeprop.my/api/v1/fetchAgent';
const POINT = "https://prolex.edgeprop.my/api/v1/getPropPoints";

const {width, height} = Dimensions.get('window');

class DrawerScreen extends Component {
  constructor(props) {
    super(props);
    this.ProfileActions = new ProfileActions();
    this.ListingActions = new ListingActions();
    this.EnquiryActions = new EnquiryActions();
    let profileData = this.ProfileActions.GetProfile();
    let activeCount = this.ListingActions.GetCount('active');
    let defaultInfo = {
          profile_uri: '',
          agentName: '',
          agentContact: '',
          agencyName: '',
          agentNo: '',
          name: '',
          email: '',
          personalAddress: '',
          personalState: '',
          personalPostal: '',
          agencyInfo: '',
          agentPlan: 1,
          agentPlanExpiry: ''
        };
    this.state = {
        isLoading: false,
        userInfo: {},
        defaultImg: require('../../../assets/images/default-img.png'),
        agentInfo: profileData[0]? profileData[0] : defaultInfo,
        activeCount,
        points: 0
    };
    this._onLogoutPress = this._onLogoutPress.bind(this)
    this._showAlert = this._showAlert.bind(this)
    this.logOutSuccess = this.logOutSuccess.bind(this)
    this.logOutFail = this.logOutFail.bind(this)
    this._onRefetch = this._onRefetch.bind(this)
    this.fetchSuccess = this.fetchSuccess.bind(this)
    this.fetchFail = this.fetchFail.bind(this)
    this._setAgentPlan = this._setAgentPlan.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this.onChatPress = this.onChatPress.bind(this)
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems })
        this._onRefetch();
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if((nextProps.navigation.state.isDrawerOpen !== this.props.navigation.state.isDrawerOpen) && nextProps.navigation.state.isDrawerOpen){
      let activeCount = this.ListingActions.GetCount('active');
      AsyncStorage.getItem("agentPoint")
      .then((val) => { 
        this.setState({points: val ? val : 0});
      });
      this.setState({activeCount});
    }
    return true;
  }
  _onRefetch() {
    //this.setState({ isLoading: true })
    const body = "uid=" + this.state.userInfo.uid +
              "&token=" + this.state.userInfo.token  
    /*FetchAgent(FETCHAGENT, body)
    .then(data => this.fetchSuccess(data))
    .catch(error => this.fetchFail(error));*/
    this.fetchData().then(arrayOfResponses => {
      if(arrayOfResponses[1] && arrayOfResponses[1].status == 'success'){
        let result = arrayOfResponses[1].result;
        let points = 0;
        if(result.total_point){
          points = result.total_point.points? result.total_point.points : 0;
        }
        this.setState({points}, () => {
          try {
            AsyncStorage.setItem("agentPoint", points.toString())
              .then( ()=>{})
              .catch( ()=>{})
          } catch (error) {}
        })
      }
      this.fetchSuccess(arrayOfResponses[0])
    }).catch(error => this.fetchFail(error));
  }

  fetchSuccess = (response) => {
    //this.setState({ isLoading: false })
    const profileData = this.formatProfile(response.result);
    this._setAgentPlan(profileData.agentPlan)
    let result = {...profileData, 'token': this.state.userInfo.token };
    this.ProfileActions.SetProfile(result, 'uid = '+this.state.userInfo.uid);
    let profileInfo = this.ProfileActions.GetProfile();
    this.setState({agentInfo: profileInfo[0]})
  }

  fetchFail = (error) => {
    
    //this.setState({ isLoading: false })
  }

  fetchData = () => {
    const urls = [
      FETCHAGENT,
      POINT
    ];
     const body = "uid=" + this.state.userInfo.uid +
              "&token=" + this.state.userInfo.token ;
    const allRequests = urls.map(url => 
      fetch(url, {
        method: 'POST',
        headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: body
      }).then(response => response.json())
    );

    return Promise.all(allRequests);
  };

  async _setAgentPlan(plan) {
    try {
        await AsyncStorage.setItem("agentPlan", plan.toString())
          .then( ()=>{})
          .catch( ()=>{})
      } catch (error) {}
  }

  formatProfile = (data) => {
    let agentInfo = data.agent;
    let personalInfo = data.main;
    let accountInfo = data.user;
    let profile = {};

    let photo_details =(agentInfo && agentInfo.field_agent_image && agentInfo.field_agent_image.und && Array.isArray(agentInfo.field_agent_image.und))? agentInfo.field_agent_image.und[0]: {};
    let photo_data = (Object.keys(photo_details).length>0)?[{
      s3_image_details:photo_details
    }]:[];
    //profile.agentPhoto = photo_data;
    profile.profile_uri = (photo_data[0] && photo_data[0].s3_image_details && Array.isArray(photo_data))? (photo_data[0]? photo_data[0].s3_image_details.uri : '') : '';
    if(profile.profile_uri !='' && profile.profile_uri != undefined){
      let is_public_photo = profile.profile_uri.search("public://");
      if(is_public_photo == 0){
        let updated_url_photo = profile.profile_uri.replace('public://', '/');
        profile.profile_uri = data.agent_image_url+updated_url_photo;
      }
    }

    profile.agentName = (agentInfo && agentInfo.field_agent_bizname && agentInfo.field_agent_bizname.und && Array.isArray(agentInfo.field_agent_bizname.und))? (agentInfo.field_agent_bizname.und[0]? agentInfo.field_agent_bizname.und[0].value : '') : '';

    profile.agentContact = (personalInfo && personalInfo.field_profile_contact && personalInfo.field_profile_contact.und && Array.isArray(personalInfo.field_profile_contact.und))? (personalInfo.field_profile_contact.und[0]? personalInfo.field_profile_contact.und[0].value : '') : '';

    profile.agencyName = (agentInfo && agentInfo.field_agent_agency && agentInfo.field_agent_agency.und && Array.isArray(agentInfo.field_agent_agency.und))? (agentInfo.field_agent_agency.und[0]? agentInfo.field_agent_agency.und[0].value : '') : '';

    profile.agentNo = (agentInfo && agentInfo.field_agent_id && agentInfo.field_agent_id.und && Array.isArray(agentInfo.field_agent_id.und))? (agentInfo.field_agent_id.und[0]? agentInfo.field_agent_id.und[0].value.trim() : '') : '';
  
    profile.name = data.user.name;

    profile.email = accountInfo.mail;

    profile.personalAddress = (personalInfo && personalInfo.field_profile_address && personalInfo.field_profile_address.und && Array.isArray(personalInfo.field_profile_address.und))? (personalInfo.field_profile_address.und[0]? personalInfo.field_profile_address.und[0].value : '') : '';

    profile.personalState = (personalInfo && personalInfo.field_profile_state && personalInfo.field_profile_state.und && Array.isArray(personalInfo.field_profile_state.und))? this.getLabel(StateData,personalInfo.field_profile_state.und[0].value,true) : null;

    profile.personalPostal = (personalInfo && personalInfo.field_profile_postcode && personalInfo.field_profile_postcode.und && Array.isArray(personalInfo.field_profile_postcode.und))? (personalInfo.field_profile_postcode.und[0]? personalInfo.field_profile_postcode.und[0].value : null) : null;
    if(profile.personalPostal){
      profile.personalPostal = parseInt(profile.personalPostal)
    }
    profile.agencyInfo = (agentInfo && agentInfo.field_agent_desc && agentInfo.field_agent_desc.und && Array.isArray(agentInfo.field_agent_desc.und))? (agentInfo.field_agent_desc.und[0]? agentInfo.field_agent_desc.und[0].value : '') : '';

    profile.agentPlan = (agentInfo && agentInfo.field_agent_plan && agentInfo.field_agent_plan.und && Array.isArray(agentInfo.field_agent_plan.und)) ? (agentInfo.field_agent_plan.und[0]? agentInfo.field_agent_plan.und[0].value : 1) : 1;
    if(profile.agentPlan){
      profile.agentPlan = parseInt(profile.agentPlan)
    }
    profile.agentPlanExpiry = (agentInfo && agentInfo.field_plan_expiry && agentInfo.field_plan_expiry.und && Array.isArray(agentInfo.field_plan_expiry.und)) ? (agentInfo.field_plan_expiry.und[0]? agentInfo.field_plan_expiry.und[0].value : '') : '';

    return profile;
  }

  getLabel = (collection,check,flag) =>{
    let label = ''
     if(flag){
      var result = collection.filter(function( obj ) {
        return obj.text.toLowerCase() == check.toLowerCase();
      });
       if(result.length >0){
          label = result[0].value;
        }
     }else{
      var result = collection.filter(function( obj ) {
        return obj.value == check;
      });
       if(result.length >0){
          label = result[0].text;
        }
     }
       return label;
  }

  navigateToScreen = (route, data) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      params: data,
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
    if(route == 'Home'){
      setTimeout(() => {
          this.props.navigation.dispatch(DrawerActions.closeDrawer())
      }, 300);
    }
    
  }

  _onLogoutPress = async () => {
    this.setState({ isLoading: true })
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    const body = "uid=" + this.state.userInfo.uid +
              "&token=" + this.state.userInfo.token +
              "&agent_id=" + this.state.userInfo.agent_id +
              "&agent_app=1" +
              "&device_id=" + fcmToken;
    SignOut(HOSTNAME, body)
    .then(data => this.logOutSuccess(data))
    .catch(error => this.logOutFail());
  }

  async logOutSuccess (response) {
    if(response.status_code == 1) {
      const keys = ['authUser', 'lastUpdated', 'propDatesale', 'propDaterental', 'propDateauction', 'propDateroom_rental', 'agentPlan', 'syncTime']
      AsyncStorage.multiRemove(keys);
      let lastMessage = await AsyncStorage.getItem("lastMessage");
      if(lastMessage && lastMessage != '') {
        AsyncStorage.setItem("lastMessage", '0');
      }
      this.ProfileActions.ClearProfile();
      this.ListingActions.ClearListings();
      this.EnquiryActions.ClearEnquiry();
      this.setState({ isLoading: false })
      this.props.navigation.navigate('SignedOut')
    }
  }

  async onChatPress() { 
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
    setTimeout(async () => {
        Intercom.registerIdentifiedUser({ userId: this.state.userInfo.name })
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        Intercom.sendTokenToIntercom(fcmToken)
        Intercom.displayMessageComposer();
    }, 500);
  }

  logOutFail = () => {
    this.setState({ isLoading: false })
    this._showAlert("Logout Failed")
  }

  _showAlert = (msg, body) => {
    Alert.alert(
      msg, body
    );
  }

  _featureNot() {
    alert('This feature is not available');
  }

  render () {
    return (
      <LinearGradient colors={['#1f2636', '#1f2636', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.drawerWrapper}>
        <SafeAreaView forceInset={{ top: 'never' }}>
          <View style={styles.drawerContainer}>
            <View style={styles.drawerMain}>
              <Avatar rounded size="large" containerStyle={styles.avatar} source={this.state.agentInfo.profile_uri?  {uri: this.state.agentInfo.profile_uri}: this.state.defaultImg} />
              <Text allowFontScaling={false} style={styles.hTwo}>{this.state.agentInfo.agentName}</Text>
              {this.state.agentInfo.agentPlan == 2 && <View style={styles.plan}>
                <Image
                  style={[styles.premiumImage, {width: 22, height: 17}]}
                  source={require('../../../assets/images/premium.png')}
                />
                <View style={{flex: 1}}>
                  <Text allowFontScaling={false} style={styles.premiumPlan}>Premium plan</Text>
                  <Text allowFontScaling={false} style={styles.expirePlan}>Expires in {moment(this.state.agentInfo.agentPlanExpiry, 'YYYY/MM/DD').format("DD MMM YYYY")}</Text>
                </View>
              </View>}
              <View style={styles.stats}>
                <View style={styles.statItem}>
                  <Image
                    style={{width: 32, height: 30}}
                    source={require('../../../assets/images/listing.png')}
                  />
                  <View style={styles.statRight}>
                    <Text allowFontScaling={false} style={styles.statHead}>{this.state.activeCount}</Text>
                    <Text allowFontScaling={false} style={styles.statSub}>Active listings</Text>
                  </View>
                </View>
                <View style={[styles.statItem, {paddingLeft: 30}]}>
                  <Image
                    style={{width: 34.5, height: 31.5}}
                    source={require('../../../assets/images/credits.png')}
                  />
                  <View style={styles.statRight}>
                    <Text allowFontScaling={false} style={styles.statHead}>{this.state.points}</Text>
                    <Text allowFontScaling={false} style={styles.statSub}>Credits</Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.drawerSub}>
              <ScrollView>
                <View style={styles.navContainer}>
                  <View style={styles.navWrapper}>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('Home',{ data : {listing_type: 'sale'}, timeStamp: moment().unix(), message: null })}>
                      <Text allowFontScaling={false} style={styles.menuText}>For Sale</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('Home',{ data : {listing_type: 'rental'}, timeStamp: moment().unix(), message: null  })}>
                      <Text allowFontScaling={false} style={styles.menuText}>For Rent</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('Home',{data : {listing_type: 'auction'}, timeStamp: moment().unix(), message: null  })}>
                      <Text allowFontScaling={false} style={styles.menuText}>For Auction</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('Home',{data : {listing_type: 'room_rental'}, timeStamp: moment().unix(), message: null  })}>
                      <Text allowFontScaling={false} style={styles.menuText}>For Room Rental</Text>
                    </TouchableOpacity>
                  </View>
                  {/* <View style={styles.navWrapper}>
                    <Text allowFontScaling={false} style={styles.commonLabel}>Agent Tools</Text>
                    <TouchableOpacity style={styles.menuItem} onPress={this._featureNot}>
                      <Text allowFontScaling={false} style={styles.menuText}>Transactions</Text>
                    </TouchableOpacity>
                  </View> */}
                  <View style={styles.navWrapper}>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('Enquiry')}>
                      <Text allowFontScaling={false} style={styles.menuText}>Enquiries</Text>
                      <Text allowFontScaling={false} style={styles.menuLabel}></Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('About')}>
                      <Text allowFontScaling={false} style={styles.menuText}>Announcements</Text>
                      <Text allowFontScaling={false} style={styles.menuLabel}></Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menuItem} onPress={this.navigateToScreen('Info')}>
                      <Text allowFontScaling={false} style={styles.menuText}>About</Text>
                      <Text allowFontScaling={false} style={styles.menuLabel}></Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.menuItem} onPress={this.onChatPress}>
                      <Text allowFontScaling={false} style={styles.menuText}>Support Center</Text>
                      <Text allowFontScaling={false} style={styles.menuLabel}></Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ScrollView>
              <View style={styles.navWrapper}>
                <TouchableOpacity style={[styles.menuItem, {maxWidth: width*.35}]} onPress={this._onLogoutPress}>
                  <Text allowFontScaling={false} style={styles.menuText}>Logout</Text>
                </TouchableOpacity>
                <View style={{width: '100%', paddingHorizontal: 20}}>
                  <Text allowFontScaling={false} style={[styles.textSm, {color: '#8991a4'}]}>Version {DeviceInfo.getVersion()}</Text>
                </View>
              </View>
            </View>
          </View>
        </SafeAreaView>
        {
          this.state.isLoading && (
            <Loading />
          )
        }
      </LinearGradient>
    );
  }
}

DrawerScreen.propTypes = {
  navigation: PropTypes.object
};

export default DrawerScreen;
