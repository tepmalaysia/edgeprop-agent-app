/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  InteractionManager,
  FlatList
} from 'react-native';
import { Avatar, Icon } from 'react-native-elements';
import { Header, Button } from 'react-native-elements';
import styles from './EnquiryStyle';
import EnquiryListItem from '../../../components/EnquiryListItem'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'

class EnquiryByListing extends Component{

  constructor(props) {
    super(props);
    this.data = props.screenProps.data;
    this.EnquiryActions = new EnquiryActions();
    this.state = {
      items : []//enquiryGroup
    }
    this._listingClick = this._listingClick.bind(this)
    this.updateList = this.updateList.bind(this)
    this.formatListData = this.formatListData.bind(this)
  }

  updateList(){
    if(this.data.updateList) {
      this.data.updateList();
    }
  }

  componentDidMount() {
    /*   // 1: Component is mounted off-screen */
    InteractionManager.runAfterInteractions(() => {
      let items = this.data.items? this.data.items : [];
      let enquiryGroup = this.EnquiryActions.GroupByProperty(items);
      enquiryGroup = this.formatListData(enquiryGroup);
      this.setState({
        items: enquiryGroup
      })
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.screenProps.data.items != this.props.screenProps.data.items){
      InteractionManager.runAfterInteractions(() => {
        let enquiryGroup = this.EnquiryActions.GroupByProperty(nextProps.screenProps.data.items);
        enquiryGroup = this.formatListData(enquiryGroup);
        this.setState({
          items: enquiryGroup,
        });
      });
    }
    return true;
  }

  _listingClick(mid){
    if(mid != null){
      this.props.navigation.navigate('EnquiryByListingDetail', {
        data: {
          mid: mid,
          updateList: this.updateList
        }
      });
    }
  }

  formatListData(list) {
    let result = [];
    if(Object.keys(list).length > 0) {
      Object.entries(list).map(([i, data]) => {
        let item = {
          unRead: false,
          title: data[0].title? data[0].title : '',
          location: '',
          lid: '',
          mid: data[0].mid? data[0].mid : 0,
          count: data.length? data.length : 0
        };
        let unRead = false;
        data.map((prop,i) => {
          if(!prop.read){
            unRead = true;
          }
        })
        item.unRead = unRead;
        item.location = data[0].district? data[0].district+', ' : '';
        item.location += data[0].state? data[0].state : '';
        item.lid = data[0].lid == data[0].mid? 'LIDM'+data[0].lid : 'LIDN'+data[0].lid;
        result.push(item);
      })
    }
    return result;
  }
  
  render() {

    return (
      <>
      {(Object.keys(this.state.items).length == 0) && (
                  <View style={styles.noDataContainer}>
                    <Image
                      style={{width: 75, height: 75}}
                      source={require('../../../assets/images/no-list.png')}
                    />
                    <Text allowFontScaling={false} style={styles.textMdNoRecord}>No Messages Found ! </Text>
                  </View>
      )}
      {(Object.keys(this.state.items).length > 0) && (
      <View style={styles.enquiryListWrapper}>
          <View style={[styles.listInner, styles.noBorder]}>
            {Object.keys(this.state.items).length > 0 &&
                <FlatList
                  data={this.state.items}
                  showsVerticalScrollIndicator={false}
                  renderItem={({ item, index }) => 
                    <EnquiryListItem 
                      item={item} 
                      index={index} 
                      listingClick={this._listingClick}
                    />}
                  keyExtractor={item => String(item.lid)}
                  ListHeaderComponent={() => ( <View style={{paddingTop: 10}}/>)}
                  ListFooterComponent={() => ( <View style={{paddingBottom: 10}}/>)}
                />
            }
          </View>
      </View>
      )}
    </>
    );
  }
}

export default EnquiryByListing;