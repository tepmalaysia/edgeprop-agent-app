/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  Dimensions,
  InteractionManager
} from 'react-native';
import { Avatar, Icon, Overlay } from 'react-native-elements';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Contact from '../Contact/index';
import ForgotPassword from '../Forgot_Password/ForgotPassword';
import LoginLanding from '../Login_Landing/LoginLanding'
import HeaderSearch from '../../../components/headerSearch';
import { Header, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { DrawerActions } from 'react-navigation-drawer';
import EnquiryByDate from './EnquiryByDate';
import EnquiryByListing from './EnquiryByListing';
import styles from './EnquiryStyle';
import { GetData } from '../../services/api'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../../../components/Loader';
import moment from "moment";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

const ENQUIRYLIST = "https://www.edgeprop.my/jwdalice/api/vpex/get-user-messages"
const { width, height } = Dimensions.get('window')

class Enquiry extends Component{
  _navListener = null;
  constructor(props) {
    super(props);
    this.EnquiryActions = new EnquiryActions();
    let enquiryData = this.EnquiryActions.GetEnquiry();
    this.state = { 
      isLoading: false,
      enquiryList: [],//enquiryData,
      isVisible: false,
      message: '',
      userInfo: {},
      index: 0,
      routes: [
        { key: 'byListing', title: 'BY LISTING' },
        { key: 'byDate', title: 'BY DATE' },
      ]
    }
    this._showAlert = this._showAlert.bind(this);
    this._getEnquiryData = this._getEnquiryData.bind(this);
    this._fetchEnquiry = this._fetchEnquiry.bind(this);
    this.enquiryListSuccess = this.enquiryListSuccess.bind(this);
    this.enquiryListFail = this.enquiryListFail.bind(this);
    this._updateMessage = this._updateMessage.bind(this);
    this.renderScene = this.renderScene.bind(this);
  }

  _showAlert = (msg, body) => {
    this.setState({ 
        isVisible: true, 
        message: msg
      });
  }

  async _fetchEnquiry(check){

    this.setState({ isLoading: true })
    var mDate = new Date().toLocaleString("en-US", {timeZone: "Asia/Kuala_Lumpur"})
    let currentTime = (new Date(mDate).getTime()/1000).toString();
    let lastSync = await AsyncStorage.getItem("syncTime");
    let limit = '';
    if(lastSync == null) {
      check = false;
      limit = 25;
    }else {
      check = true;
    }
    await AsyncStorage.setItem("syncTime", currentTime);
    
    let URL = ENQUIRYLIST + "?agentid=" + this.state.userInfo.uid +
              "&limit="+limit;
    if(check == true) {
      URL = URL + "&sync_time="+lastSync;
    }
    await AsyncStorage.setItem("syncTime", currentTime); 
    GetData(URL)
    .then(data => this.enquiryListSuccess(data, check))
    .catch(error => this.enquiryListFail(error));
  }

  async enquiryListSuccess(response, check){
    if(response.status == 1 && response.messages){
      let data = this._formatEnquiry(response.messages);
      if(check == true) {
        data.map((item, i) => {
          let enqItem = this.EnquiryActions.GetEnquiryDetail(item.message_id);
          if(enqItem && enqItem.length >0){
            this.EnquiryActions.ClearEnquiryItem(item.message_id);
          }
          this.EnquiryActions.CreateEnquiry(item);
        });
      }else {
        this.EnquiryActions.CreateEnquiry(data);
      } 
      
    }else {
      this.setState({ isLoading: false })
    }
    this._getEnquiryData();
  }

  _formatEnquiry(items) {
    let result = [];
    items.map((item, i) => {
      let data = {};
      let status = (item.read == 0) ? 3 : (item.read_by == this.state.userInfo.uid) ? 1 : 2;    
      var mDate = new Date().toLocaleString("en-US", {timeZone: "Asia/Kuala_Lumpur"})
      data = {
            lid: item.mid ? parseInt(item.mid) : 0,
            mid: item.mid ? parseInt(item.mid) : 0,
            nid: item.nid ? parseInt(item.nid) : 0,
            agent_id: parseInt(this.state.userInfo.uid),
            district: item.district ? item.district : 'xxxx',
            state: item.state ? item.state : 'xxxx',
            title: item.title ? item.title : '',
            message_id: item._id ? parseInt(item._id) : 0,
            message: item.user.message ? item.user.message : '',
            name: item.user.name ? item.user.name.trim() : '',
            phone: item.user.phone ? item.user.phone : 'xxxx',
            email: item.user.email ? item.user.email : 'xxxx',
            status: parseInt(status),
            read: (item.read == '1') ? true : false,
            changed: item.created ? item.created : new Date(mDate).getTime()/1000,
          }
      result.push(data);
    });
    return result;
  }

  enquiryListFail(error){
   this.setState({ isLoading: false })
  }

  _getEnquiryData() {
    let enquiryData = this.EnquiryActions.GetEnquiry();
    this.setState({
      enquiryList: enquiryData.length>0? enquiryData : {},
      isLoading: false
    });
  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'byListing':
        return <EnquiryByListing 
          screenProps = {{
            data: {
              items : this.state.enquiryList,
              updateList: this._getEnquiryData
            }
          }}
          navigation = {this.props.navigation}
          />;
      case 'byDate':
        return <EnquiryByDate
          screenProps = {{
            data: {
              items : this.state.enquiryList,
              updateList: this._getEnquiryData
            }
          }}
          navigation = {this.props.navigation}
        />;
      default:
        return null;
    }
  };

  async _updateMessage() {
    var mDate = new Date().toLocaleString("en-US", {timeZone: "Asia/Kuala_Lumpur"})
    let currentTimestamp = (new Date(mDate).getTime()/1000).toString();
    let lastUpdated = await AsyncStorage.getItem("syncTime");
    if(lastUpdated == null) {
      this._fetchEnquiry(false);
    }else {
      let difference = currentTimestamp-parseInt(lastUpdated)
      if(difference > 300){
          this._fetchEnquiry(true);
      }else {
        this._getEnquiryData();
      }
    }
  }


  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems})
      }
    }

    this._navListener = this.props.navigation.addListener('willFocus', (route) => { 
      this._updateMessage();
    });

    const params = this.props.navigation.state.params ? this.props.navigation.state.params : '';
    if(params.rejectedByAdmin) {
      let message = params.messageId ? " Message ID: " + params.messageId : ''
      this._showAlert(params.rejectedByAdmin, message)
    }
    InteractionManager.runAfterInteractions(() => {
      this._updateMessage();
    });
  }

  componentWillUnmount() {
     if(this._navListener != null)
     this._navListener.remove();
  }
  
  render() {
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={() => {this.props.navigation.dispatch(DrawerActions.toggleDrawer())}}>
            <Icon
              name='menu'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter}>
          <Text allowFontScaling={false} style={styles.navTitle}>Enquiries</Text>
        </View>
      )
    }

    const RightNavComponent = () => {
      return (
        <View style={styles.navRight}>
          <TouchableOpacity style={styles.navIconRight} onPress={()=> this._fetchEnquiry(true)}>
            <Icon
              name='rotate-cw'
              type='feather'
              size={24}
              color={'#fff'}
            />
          </TouchableOpacity>
        </View>
      )
    }


    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          {/* <HeaderSearch navigation={this.props.navigation} /> */}
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent />}
            rightComponent={<RightNavComponent />}
          />
          <View style={styles.innerContainer}>
              {/*<EnquiryByDate 
                navigation={this.props.navigation} />
              <EnquiryByListing />*/}
            <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
               {/*<EnquiryTabNav
                  screenProps = {{
                    data: {
                      items : this.state.enquiryList,
                      updateList: this._getEnquiryData
                    }
                  }}
                  navigation = {this.props.navigation}
                />*/}
                <TabView
                  navigationState={this.state}
                  renderScene = {this.renderScene}
                  onIndexChange={index => this.setState({ index })}
                  initialLayout={{ width: Dimensions.get('window').width }}
                  renderTabBar={props =>
                                  <TabBar
                                    {...props}
                                    style = {{
                                      backgroundColor: '#e6ecf2',
                                      shadowOpacity: 0,
                                      padding: 0,
                                      height: 45,
                                      marginTop: -5
                                    }}
                                    labelStyle = {{
                                      fontSize: width*0.03,
                                      fontFamily: 'OpenSans-Bold',
                                    }}
                                    inactiveColor = {'#75829a'}
                                    activeColor = {'#346dfe'}
                                    indicatorStyle = {{
                                      backgroundColor: '#346dfe',
                                    }}
                                  />
                                }
                />
              </SafeAreaView>
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle, styles.bgDanger]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible : false, message: ''})}
            >
              <View style={styles.overlayInner}>
                <Icon
                  name='info'
                  type='material'
                  color='#fff'
                  size={30}
                />
                <Text allowFontScaling={false} style={styles.overlayText}>{this.state.message}</Text>
              </View>
            </Overlay>
          </View>
        </SafeAreaView>
        {
          this.state.isLoading && (
              <Loading />
            )
          }
      </LinearGradient>
    );
  }
}

export default Enquiry;
