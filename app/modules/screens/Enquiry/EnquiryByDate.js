/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  InteractionManager,
  FlatList
} from 'react-native';
import { Avatar, Icon } from 'react-native-elements';
import {Header, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import styles from './EnquiryStyle';
import EnquiryItem from '../../../components/EnquiryItem'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import moment from 'moment';

class EnquiryByDate extends Component{

  constructor(props) {
    super(props);
    this.state = {};
    this.EnquiryActions = new EnquiryActions();
    this.data = props.screenProps.data;
    this.state = {
      items : [],//items,
      thisWeek: [],
      lastWeek: []
    }
    this._onEnquiryDetail = this._onEnquiryDetail.bind(this);
    this.orderList = this.orderList.bind(this)
    this.updateList = this.updateList.bind(this)
  }

  componentDidMount() {
    /*   // 1: Component is mounted off-screen */
    InteractionManager.runAfterInteractions(() => {
      let items = this.data.items? this.data.items : [];
      if(items.length > 0) {
        this.setState({ items : items}, () => this.orderList());
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.screenProps.data.items != this.props.screenProps.data.items){
       InteractionManager.runAfterInteractions(() => {
        if(nextProps.screenProps.data.items.length > 0) {
           this.setState({
              items: nextProps.screenProps.data.items,
            }, () => this.orderList());
         }
       });
    }
    return true;
  }

  orderList(){
    var curr = new Date; // get current date
    var first = curr.getDate() - curr.getDay() + 1; // First day is the day of the month - the day of the week
    var firstday = new Date(curr.setDate(first));
        firstday = new Date(firstday.setHours(0));
        firstday = new Date(firstday.setMinutes(0));
        firstday = new Date(firstday.setSeconds(0));
    let weekStart = moment(firstday).format("X");
    let thisWeek = this.state.items.filter(item => item.changed > weekStart);
    let lastWeek = this.state.items.filter(item => item.changed <= weekStart);
    this.setState({ thisWeek, lastWeek })
  }

  updateList(flag){
    this.data.updateList();
  }

  _onEnquiryDetail(message_id) {
    this.props.navigation.push('EnquiryDetail', {
        data: {
          message_id: message_id,
          updateList: this.updateList
        }
      });
  }
  
  render() {

    return (
      <ScrollView>
      <View style={styles.enquiryListWrapper}>
        <View style={[styles.listInner, styles.noBorder]}>
          {this.state.thisWeek.length > 0 &&
            <FlatList
              data={this.state.thisWeek}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => 
                    <EnquiryItem 
                      item={item} 
                      index={index} 
                      onEnquiryDetail={this._onEnquiryDetail}
                    />}
               keyExtractor={item => String(item.message_id)}
               ListHeaderComponent={() => ( <View style={{paddingTop: 10}}/>)}
               ListFooterComponent={() => ( <View style={{paddingBottom: 10}}/>)}
            />
         }
        </View>
        <View style={styles.listInner}>
          <Text allowFontScaling={false} style={[styles.commonLabel, {paddingHorizontal: 15, paddingTop: 10}]}>Last Week</Text>
           {this.state.lastWeek.length > 0 &&
            <FlatList
              data={this.state.lastWeek}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => 
                    <EnquiryItem 
                      item={item} 
                      index={index} 
                      onEnquiryDetail={this._onEnquiryDetail}
                    />}
              keyExtractor={item => String(item.message_id)}
              ListHeaderComponent={() => ( <View style={{paddingTop: 10}}/>)}
              ListFooterComponent={() => ( <View style={{paddingBottom: 10}}/>)}
            />
          }
        </View>
      </View>
      </ScrollView>
    );
  }
}

export default EnquiryByDate;