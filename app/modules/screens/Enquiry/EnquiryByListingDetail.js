/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated, 
  Easing,
  ScrollView,
  FlatList
} from 'react-native';
import { Avatar, Icon } from 'react-native-elements';
import {Header, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import styles from './EnquiryStyle';
import EnquiryItem from '../../../components/EnquiryItem'
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import moment from 'moment';

class EnquiryByListingDetail extends Component{

  constructor(props) {
    super(props);
    this.state = {
      items: {}
    };
    this.data = props.navigation.state.params.data? props.navigation.state.params.data : {};
    this.EnquiryActions = new EnquiryActions();
    this._onEnquiryDetail = this._onEnquiryDetail.bind(this);
    this._handleBackPress = this._handleBackPress.bind(this);
    this._getEnquiryList = this._getEnquiryList.bind(this)
    
  }

  _handleBackPress() {
     this.props.navigation.goBack();
  }

  _getEnquiryList(flag) {
    let items = this.EnquiryActions.GetEnquiryListing(this.data.mid);
    this.setState({
      items: items.length>0? items : {},
    });
    if(flag == false) {
      this.data.updateList();
    }
  }

  componentDidMount() {
    this._getEnquiryList(true);
  }
  

 _onEnquiryDetail(message_id) {
    this.props.navigation.push('EnquiryDetail', {
        data: {
          message_id: message_id,
          parent: 'list',
          updateList: this._getEnquiryList
        }
      });
  }
  
  render() {
    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft}>
          <TouchableOpacity onPress={()=> this._handleBackPress()}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#fff'
            />
          </TouchableOpacity>
        </View>
      )
    }

    const CenterNavComponent = () => {
      return (
        <View style={styles.navCenter} />
      )
    }

    return (
      <LinearGradient colors={['#3670e5', '#3670e5', '#ffffff', '#ffffff']} locations={[0.25,0.5,0.75,1]} style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          <Header
              statusBarProps={{ barStyle: 'light-content' }}
              containerStyle={styles.navContainer}
              placement="left"
              centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
              leftComponent={<LeftNavComponent />}
              centerComponent={<CenterNavComponent />}
            />
          <View style={styles.innerContainer}>
              <View style={styles.enquiryListWrapper}>
                <View style={[styles.listInner, styles.noBorder]}>
                  {this.state.items.length > 0 &&
                      <FlatList
                        data={this.state.items}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }) => 
                          <EnquiryItem 
                            item={item} 
                            index={index} 
                            onEnquiryDetail={this._onEnquiryDetail}
                          />}
                        keyExtractor={item => String(item.message_id)}
                        ListHeaderComponent={() => ( <View style={{paddingTop: 10}}/>)}
                        ListFooterComponent={() => ( <View style={{paddingBottom: 10}}/>)}
                      />
                  }
                </View>
              </View>
          </View>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}

export default EnquiryByListingDetail;