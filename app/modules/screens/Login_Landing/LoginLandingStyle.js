import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    padding: 60,
    margin: 0,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center', 
    alignItems: 'center'
  },
  appIcon: {
    paddingBottom: 30,
    alignItems: 'center',
  },
  errorMsg: {
    color: 'red', 
    marginBottom: 0
  },
  inputWrapper: {
    width: '100%',
    marginTop: 20
  },
  inputContainer: {
    borderBottomWidth: 0,
    paddingLeft: 0,
    paddingRight: 0,
    margin: 0,
  },
  inputField: {
    flex: 1,
    height: 45,
    paddingHorizontal: 20,
    paddingVertical: 13,
    backgroundColor: '#e6ecf2',
    borderRadius: 50,
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#10182d',
  },
  textSm: {
    fontSize: width * 0.028,
    fontFamily: 'OpenSans-Regular',
    color: '#10182d',
    lineHeight: width * 0.04,
  },
  forgotPswd: {
    alignItems: 'flex-end',
    paddingVertical: 15
  },
  actionButton: {
    width: '100%',
    paddingVertical: 13,
    paddingHorizontal: 20,
    height: 45,
    backgroundColor: '#FFF',
    borderRadius: 50,
    shadowOffset:{width: 2,  height: 2,},
    shadowColor: '#acb4d1',
    shadowOpacity: 0.6,
    elevation: 3,
    marginVertical: 20
  },
  actionButtonText: {
    fontSize: width * 0.032,
    fontFamily: 'OpenSans-Bold',
    textTransform: 'uppercase',
    width: '100%',
    textAlign: 'center',
    color: '#3670e5'
  },
  loginSeperator: {
    position: 'relative',
    width: '100%',
    alignItems: 'center'
  },
  seperatorLine: {
    position: 'absolute',
    top: '50%',
    left: 0,
    width: '100%',
    height: 1,
    backgroundColor: '#e6e6e6',
  },
  seperatorText: {
    position: 'relative',
    zIndex: 1,
    width: 60,
    backgroundColor: '#fff',
    textAlign: 'center',
    fontSize: width * 0.034,
    fontFamily: 'OpenSans-SemiBold',
    color: '#d3d3d3',
  },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .35)',
    },
    overlayInner: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        paddingLeft: 10
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    }
});
