import React, { Component } from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Platform,
  Alert
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import AsyncStorage from '@react-native-community/async-storage';
import { Input, Overlay, Icon } from 'react-native-elements';
import styles from './LoginLandingStyle.js'
import Loading from '../../../components/Loader';
import { SignIn } from '../../services/api'
import ProfileActions from '../../../../realm/actions/ProfileActions'

const HOSTNAME = 'https://prolex.edgeprop.my/api/v1/login';
var phoneValidate = /^(?=\d{8,}$)\d{8,9}/;
export default class LoginLanding extends Component {
    constructor(props) {
      super(props);
      this.ProfileActions = new ProfileActions();
      this.state = {
          isLoading: false,
          phone:'',
          pass: '',
          hasPassError: false,
          hasPhoneError: false,
          phoneErrorMsg : '',
          passErrorMsg: '',
          clicking: false,
          isVisible: false,
          apiError: '',
      };
      this._onLoginPress = this._onLoginPress.bind(this)
      this._onForgot = this._onForgot.bind(this)
      this._setAuthAsyc = this._setAuthAsyc.bind(this)
      this.logInSuccess = this.logInSuccess.bind(this)
      this.logInFail = this.logInFail.bind(this)
      this._onPressButton = this._onPressButton.bind(this)
      this._getToken = this._getToken.bind(this)
    }
    _onLoginPress() {
      this.setState({ getStarted: true })
      this._onPressButton();
    }
    _onForgot() {
      this.props.navigation.navigate('ForgotPassword');
    }

    _onPressButton() {
      let phoneError = false;
      let passError = false;
      this.state.clicking = true;
      let isValid = true;//phoneValidate.test(this.state.phone);
        if(!isValid) {
          this.setState({ hasPhoneError: true });
        } else {
          this.setState({ hasPhoneError: phoneError });
        }

        if(this.state.pass.length >= 5) {
          this.setState({ hasPassError : passError })
        } else {
          this.setState({ hasPassError: true });
        }

        if(this.state.pass.length >= 5 && isValid) {
          this._login()
        }

    }

    async _getToken() {
      return await AsyncStorage.getItem('fcmToken')
    }

    async _login() {
      const token = await this._getToken();
      //const token = 'dXuHsVw2nok:APA91bGOk9cHs-6hgsEjmeEfSLRS5i5eEZm3kxQpHon4w_6mpH_r5PhuFrpzL5poQVknJWUv41hqLuFeT0nKnO4G1vcHgY-KxZss0_zblx6StsxBOFmBEbY4EZ8q0264_sEqvXiEEroM';
      const deviceType = Platform.OS === 'ios' ? 2 : 1;
      //if(token && token.length > 0) {
        this.setState({ isLoading: true })
        const body = "username="+ this.state.phone +
                    "&password=" + this.state.pass +
                    "&device_id=" + token +
                    "&device_type=" + deviceType
  
        SignIn(HOSTNAME, body)
        .then(data => this.logInSuccess(data))
        .catch(error => this.logInFail(error));
      /*}else {
        this.setState({ isVisible: true, apiError: 'Something went wrong, please try after sometime' });
      }*/
    }

    logInSuccess = (response) => {
      if(response.status_code == 0) {
        this.setState({ isLoading: false })
        this.setState({ hasAuthError: true });
        this.setState({ isVisible: true, apiError: response.message });
      }
      if(response.status_code == 1) {
        this.setState({ hasAuthError: false });
        this._setAuthAsyc(response.result)
      }
    }

    logInFail = (error) => {
      this.setState({ isVisible: true, apiError: 'Login Failed', isLoading: false });
    }

    _setAuthAsyc = async (response) => {
      try {
        await AsyncStorage.setItem("authUser", JSON.stringify(response))
          .then( async ()=>{
            this.ProfileActions.CreateProfile({'uid': parseInt(response.uid), 'token': response.token});
            await AsyncStorage.setItem("lastMessage", '0')
            this.setState({ isLoading: false })
            this.props.navigation.navigate('SignedIn');
          })
          .catch( ()=>{
            
          })
      } catch (error) {
        // Error saving data
        
      }
    }
    _showAlert = (msg) => {
      Alert.alert(
        msg
      );
    }

    validatePhoneNumber = (phone) => {
      var re = /^(?=\d{8,}$)\d{8,9}/;
        return re.test(phone);
    };

    render() {
      const { phone, pass, hasPhoneError, clicking } = this.state;
      let phoneErrorDisplay = ''
      let pwdErrorDisplay = ''
      if(clicking) {
        if(phone == '') {
          phoneErrorDisplay = 'Required mobile number'
        } else { 
            if(phone != '' && !this.validatePhoneNumber(this.state.phone)) {
              phoneErrorDisplay = 'Enter valid number'
            }
        }
      }
      if(clicking) {
        if(pass == '') {
          pwdErrorDisplay = 'Required password'
        } else if(pass.length < 5) {
          pwdErrorDisplay = 'Enter valid password'
        }
      }

      return (
        <View>
          <SafeAreaView style={{height: '100%'}}>
            <KeyboardAwareScrollView contentContainerStyle={{flex: 1}} keyboardShouldPersistTaps='handled'>
              <View style={styles.container} >  
                <View style={{width: '100%'}}>
                  <View style={styles.appIcon}>
                    <Image
                      style={{width: 100, height: 100}}
                      source={require('../../../assets/images/agent.png')}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <Input 
                      placeholder='Reg. Mobile Number' 
                      autoCapitalize="none"
                      errorStyle={styles.errorMsg}
                      errorMessage={clicking ? '' : ''}
                      value={this.state.phone}
                      inputContainerStyle={{borderBottomWidth: 0}} 
                      containerStyle={styles.inputContainer} 
                      inputStyle={styles.inputField}
                      onChangeText={(phone) => this.setState({phone: phone.trim()})}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <Input 
                      placeholder='Password' 
                      secureTextEntry={true}
                      inputContainerStyle={{borderBottomWidth: 0}} 
                      containerStyle={styles.inputContainer} 
                      inputStyle={styles.inputField}
                      errorStyle={styles.errorMsg}
                      errorMessage={clicking ? pwdErrorDisplay : ''}
                      onChangeText={(pass) => this.setState({pass: pass})}
                    />
                  </View>
                  <View style={{width: '100%', alignItems: 'flex-end'}}>
                    <TouchableOpacity style={styles.forgotPswd} onPress={()=> this._onForgot()}>
                      <Text allowFontScaling={false} style={[styles.textSm, {fontFamily: 'OpenSans-SemiBold', color: '#3670e5',}]}>Forgot Password?</Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={[styles.actionButton, {backgroundColor: '#3670e5'}]} onPress={()=> this._onLoginPress()}>
                    <Text allowFontScaling={false} style={[styles.actionButtonText, {color: '#FFF'}]}>Login</Text>
                  </TouchableOpacity>
                  {/* <View style={styles.loginSeperator}>
                    <Text allowFontScaling={false} style={styles.seperatorText}>OR</Text>
                    <View style={styles.seperatorLine} />
                  </View>
                  <TouchableOpacity style={styles.actionButton}>
                    <Text allowFontScaling={false} style={styles.actionButtonText}>Login with OTP</Text>
                  </TouchableOpacity> */}
                </View>
              </View>         
            </KeyboardAwareScrollView>

            {/* Overlay */}
            <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle, styles.bgDanger]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible: false })}
            >
              <View style={styles.overlayInner}>
                <Icon
                  name='info'
                  type='material'
                  color='#fff'
                  size={30}
                />
                <Text allowFontScaling={false} style={styles.overlayText}>{this.state.apiError}</Text>
              </View>
            </Overlay>

          </SafeAreaView>
          {
            this.state.isLoading && (
              <Loading />
            )
          }
        </View>
        );
      }
}
