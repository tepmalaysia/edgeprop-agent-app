import React from 'react';
import {NativeModules, View, Text, Easing, Image, Button, TouchableOpacity, ScrollView, AppState, Platform, ActivityIndicator, Vibration, BackHandler, ImageBackground} from 'react-native';
import styles from '../EnquiryList/EnquiryStyle';
import { withNavigation } from 'react-navigation';
import EnquiryActions from '../../../../realm/actions/EnquiryActions'
import AsyncStorage from '@react-native-community/async-storage';
import SplshStyles from '../Splash/SplashStyle';
import Loading from '../../../components/Loader';

if(Platform.OS === 'android') {
  var NativeCall = NativeModules.NativeCall;
}
var _isMounted = false;
var _timer = '';
const Realm = require('realm');
const HOSTNAME = 'https://www.edgeprop.my/jwdalice/api/vpex/accept-or-reject-message';
const DURATION = 10000 ;
const PATTERN = [ 1000, 2000, 3000] ;
let intervalId;
export default class EnquiryAlert extends React.Component {
  constructor(props) {
    super(props);
    _isMounted = false;
    _timer = '';
    this.state = {
      timeCounter: 45,
      disableBtn: false,
      projectTitle: this.props.screenProps.project_title,
      messageId: this.props.screenProps.messageId,
      projectImage: this.props.screenProps.image_url,
      appState: AppState.currentState,
      // loading: false
      isLoading: false

    }
    this._saveRealmData = this._saveRealmData.bind(this)
    this._acceptOrRejectRequest = this._acceptOrRejectRequest.bind(this)
    this.EnquiryActions = new EnquiryActions();
  }

  _startVibrationFunction=()=>{
    Vibration.vibrate(DURATION)
    // Android Device Will Vibrate in above pattern Infinite Time.
    // iOS Device Will Vibrate in above pattern Infinite Time.
    Vibration.vibrate(PATTERN, true)
  }

  _stopVibrationFunction=()=>{
    // Stop Vibration.
    Vibration.cancel();
  }

  _startTimer = () => {
    if(this.intervalId) {
      clearInterval(this.intervalId)  
    }

    this._isMounted = true;
    if (this._isMounted) {
      this.intervalId =  setInterval(() => {
        if(this.state.timeCounter > 0) {
          this.setState({
            timeCounter: this.state.timeCounter - 1
          })
        } else {
          this.setState({disableBtn: true})
          this._isMounted = false;
          this._acceptOrRejectRequest('2')
          clearInterval(this.intervalId)
        }
      }, 1000);
    }
  }

  async stopAlert() {
    if(Platform.OS === 'android') {
      NativeCall.stopAlertRing( (err) => {}, (msg) => {} );
    }
  }

  async startAlert() {
    if(Platform.OS === 'android') {
      NativeCall.startAlertRing( (err) => {}, (msg) => {} );
    }
  }

  _acceptOrRejectRequest = async (status) => {
    clearInterval(this.intervalId)
    if(Platform.OS === 'android') {
      this.stopAlert()
    }
    this._stopVibrationFunction()
    this.setState({disableBtn: true, isLoading: true})
    let agentId = await this._getAgentId(); 
  
    const body = "msgid=" + this.state.messageId +
              "&agentid=" + agentId +
              "&status=" + status;
    fetch(HOSTNAME, {
      method: 'POST',
      headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: body
    })
    .then((response) => response.json())
    .then((responseText) => {
      if(responseText.hasOwnProperty('data')) {
        const status = responseText.status == 0 ? 3 : responseText.status
        this._saveRealmData(responseText, status);
      } else {
        this.props.navigation.navigate('Enquiry', {
          'rejectedByAdmin': responseText.msg
        });
      }
      
    })
    .catch((error) => {
      
        this.props.navigation.navigate('Enquiry', {
          'rejectedByAdmin': 'Unable to handled requested operation'
        });
    });
  }

  _saveRealmData = async (response, status) => {
    let items = response.data
    let message = null
    let newStatus = 1;
    if(status == 4) {
      newStatus = 2;
      message = { 'rejectedByAdmin': response.msg, 'messageId': this.state.messageId }       
    }else if(status == 2) {
      newStatus = 3;
      message = { 'rejectedByAdmin': 'Enquiry automatically rejected', 'messageId': this.state.messageId }       
    }else if(status == 3) {
      newStatus = 3;
      message = { 'rejectedByAdmin': 'Enquiry manually rejected', 'messageId': this.state.messageId }       
    }
    let agentId = await this._getAgentId();
    var mDate = new Date().toLocaleString("en-US", {timeZone: "Asia/Kuala_Lumpur"})
    let data = {
            lid: items.mid,
            mid:items.mid,
            agent_id: parseInt(agentId),
            district: items.district,
            state: items.state,
            title: items.project_title ? items.project_title : 'xxxx',
            message_id: this.state.messageId ? parseInt(this.state.messageId) : 0,
            message: items.message ? items.message : 'xxxx',
            name: items.name ? items.name : 'xxxx',
            phone: items.phone ? items.phone : 'xxxx',
            email: items.email ? items.email : 'xxxx',
            status: parseInt(newStatus),
            read: (status == 1) ? true : false,
            changed: new Date(mDate).getTime()/1000
          }
    this.EnquiryActions.CreateEnquiry(data);
    this._isMounted = false;
    if(Platform.OS === 'android') {
      NativeCall.closeApp( (err) => {}, (msg) => {} );
    }
    if(status == 1) {
       this.props.navigation.replace('EnquiryDetail', {
        data: {
          message_id: this.state.messageId,
          parent: 'alert'
        }
      });
    } else {
      //this.props.navigation.replace('DrawerNavigator');
      this.props.navigation.navigate('Enquiry', message);
    } 
    
  }

  _getAgentId = async () => {
    const auth = await AsyncStorage.getItem("authUser");
    let authItems = JSON.parse(auth)
    if(authItems.uid) {
      return authItems.uid
    }
    this.props.navigation.navigate('Enquiry', {
      'rejectedByAdmin': 'No Agent Id Found'
    });
  }

  _handleAppStateChange = async (nextAppState) => {
   
    this.setState({appState: nextAppState}, () => {
      if (this.state.appState.match(/background/)) {
        console.log('App has gone to the background!');
        if(!this.state.disableBtn) {
          this.setState({disableBtn: true}, () => this._acceptOrRejectRequest('3'))
        }
      } else {
        console.log('App has come to the foreground!');
      }
    });
  };

  async componentDidMount() {
    
    let lastMessage = await AsyncStorage.getItem("lastMessage");
    if(lastMessage && lastMessage == '0' ) {
      await AsyncStorage.multiRemove(['lastMessage']);
      const lastUpdated = await AsyncStorage.getItem("lastUpdated");
      if(lastUpdated){
        this.props.navigation.replace('DrawerNavigator');
      }else{
        this.props.navigation.replace('Loading');
      }
    }else {
      this.setState({ loading: true });
      this._startTimer();
      this.startAlert();
      this._startVibrationFunction();
    }
    
    if(Platform.OS === 'ios') {
      AppState.addEventListener('change', this._handleAppStateChange);
    }
    if(Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener("hardwareBackPress", async () => {
        if(!this.state.disableBtn) {
          this.setState({disableBtn: true}, () => this._acceptOrRejectRequest('3'))
        }
        return true;
      });
    }
  }

  async componentWillUnmount() {
    clearInterval(this.intervalId)
    if(Platform.OS === 'android') {
      this.stopAlert()
      if(this.backHandler)
        this.backHandler.remove()
    }
    if(Platform.OS === 'ios') {
      AppState.removeEventListener('change', this._handleAppStateChange);
    }
    this._isMounted = false;
  }

  render() {
    const { loading } = this.state;
    if(!loading) {
       return null;
    }
    return (
      <ImageBackground source={require('../../../assets/images/bgalert.png')} style={{width: '100%', height: '100%'}}>
        <ScrollView style={styles.linearGradient} scrollEnabled={false}>
          <View style={styles.alertContainer}>
            <View style={{width: '100%', flex: 1}}>
              <View>
                <Text style={styles.alertContainerTitle}>EdgeProp.my</Text>
              </View>
              <View style={styles.imageContainer}>
                  <View style={styles.imageThumb}>                  
                  { 
                    this.state.projectImage ? (<Image
                      resizeMode='cover'
                      style={styles.thumb}
                      source={{ uri: this.state.projectImage }}
                    />) : (
                      <Image
                        resizeMode='cover'
                        style={styles.thumb}
                        source={require('../../../assets/images/agent.png')}
                      />
                    )
                  }
                  </View>
                  <View style={{width: '100%'}}>
                    <Text style={styles.alertContainerStatus}>{this.state.projectTitle}</Text>
                  </View>
                </View>
              </View>
            <View style={styles.buttonContainer}>
              {
                !this.state.isLoading && (
                  <View style={styles.buttonRoundedContainer}>
                    <TouchableOpacity
                      disabled = {this.state.disableBtn}
                      style={styles.roundButtonGreen}
                      activeOpacity = { .5 }
                      onPress = {() => this._acceptOrRejectRequest('1')}
                    >
                    <Image
                      style={styles.tickImg}
                      source={require('../../../assets/images/tick.png')}
                    />
                    </TouchableOpacity>
                    <Text style={styles.textStyle}> Accept </Text>
                  </View>
                )
              }
              <View style={[styles.buttonRoundedContainer, {flex: 1, alignItems: 'center'}]}>
                {
                   this.state.isLoading && (
                    <View style={styles.roundButtonLoad}>
                      <ActivityIndicator
                        animating size='large'
                        style={{
                          height: 80
                        }}
                        color={'#fff'}
                      />
                    </View>
                  )
                }
              </View>
              {
                !this.state.isLoading && (
                  <View style={styles.buttonRoundedContainer}>
                    <TouchableOpacity
                          disabled = {this.state.disableBtn}
                          style={styles.roundButtonRed}
                          activeOpacity = { .5 }
                          onPress = {() => this._acceptOrRejectRequest('3')}
                      >
                    <Text style={styles.textCounterStyle}> {this.state.timeCounter} </Text>
                    </TouchableOpacity>
                    <Text style={styles.textStyle}> Reject </Text>
                  </View>
                )
              }
            </View>
            {/* <View style={styles.loaderContainer}>
              <View style={styles.loaderInner}>
                <Text style={styles.textCounterStyle}>helooooo</Text>


              </View>
            </View> */}
          </View>
        </ScrollView>
    </ImageBackground>
    );
  }
}