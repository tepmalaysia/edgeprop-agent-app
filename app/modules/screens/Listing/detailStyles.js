import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    navLeft: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%',
        height: 220,
    },
    propImgWrapper: {
        width: '100%',
        height: 220,
        overflow: 'hidden',
        position: 'relative'
    },
    cardImage: {
        flex: 1,
        resizeMode: 'cover'
    },
    cardOverlay: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        zIndex: 1,
        padding: 15,
        backgroundColor: 'transparent',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    cardOverlayInnerDetail: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    cardOverlayInner: {
        flexDirection: 'row',
        flex: 1
    },
    cardOverlayInnerLeft: {
        justifyContent: 'flex-start',
    },
    cardOverlayInnerRight: {
        justifyContent: 'flex-end',
    },
    cardOverlayText: {
        color: '#fff',
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold'
    },
    cardOverlayItem: {
        maxHeight: 30,
        textAlign: 'center',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardOverlayItemLeft: {
        paddingRight: 30
    },  
    cardOverlayItemRight: {
        paddingLeft: 30
    }, 
    bottomActions: {
        width: width,
        height: 50,
        padding: 5,
        backgroundColor: '#FFF',
        shadowOffset:{width: 2,  height: -5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    bottomActionItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentSection: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(0, 0, 0, 0.2)',
    },
    propStatus: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    textMd: {
        color: '#10182d',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold'
    },
    cardTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Bold',
        color: '#10182d'
    },
    cardLocation: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'nowrap',
        paddingTop: 2
    },
    cardContentLocation: {
        flex: 1,
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-SemiBold',
        color: '#149d78',
        paddingLeft: 5
    },
    cardPrice: {
        fontSize: width * 0.042,
        fontFamily: 'OpenSans-ExtraBold',
        color: '#3670e5',
        paddingTop: 10
    },
    cardUpdateDate: {
        fontSize: width * 0.026,
        fontFamily: 'OpenSans-SemiBold',
        color: '#646975',
        paddingLeft: 5
    },
    propMajorInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    propMajorInfoLeft: {
        flex: 1,
    },
    propMajorInfoRight: {
        width: '32%',
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    CheckBoxWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    checkBox: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        padding: 0,
        margin: 0,
        marginLeft: -2,
        flex: 1
    },
    commonLabel: {
        color: '#8991a4',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
        paddingBottom: 10
    },
    textStat: {
        color: '#8991a4',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
    },
    listWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: -10
    },
    listItem: {
        width: '50%',
        maxWidth: '50%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20
    },
    listImgWrapper: {
        width: 20
    },
    header: { 
        height: 30
    },
    headtext: { 
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
        margin: 6,
        color: '#A0ACC1', 
        paddingHorizontal: 0,
        flex: 1,
        flexWrap: 'wrap' 
    },
    rowText: { 
        margin: 6,
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        flex: 1,
        flexWrap: 'wrap' 
    },
    row: { 
        flexDirection: 'row', 
        backgroundColor: '#FFFFFF', 
        borderWidth: 0.5, 
        borderColor: '#e6ecf2' 
    },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 10,
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .35)',
    },
    overlayInner: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        flex: 1,
        lineHeight: width * 0.035,
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    },
    overlayPopContent: {
        padding: 15,
    },
    overlayPopText: {
        color: '#10182d',
        fontSize: width * 0.036,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.05,
    },
    overlayPopButtons: {
        borderTopWidth: 1,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
        flexDirection: 'row'
    },
    OverlayPopBtn: {
        padding: 18,
        flex: 1
    },
    OverlayPopBtnText: {
        color: '#149d78',
        textTransform: 'uppercase',
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    fieldWrapTwoWay: {
        width: '50%',
        paddingHorizontal: 5,
    },
    fieldWrapThreeWay: {
        width: '33.333%',
        paddingHorizontal: 5,
    },
    fieldWrap: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        flexWrap: 'wrap'
    },
    errorMsg: {
        color: 'rgb(213, 0, 0)',
        fontSize: 12,
        fontFamily: 'OpenSans-Regular',
        lineHeight: width * 0.04,
        fontWeight: 'normal',
        textAlign: 'left'
    },
    overlayFeaturedStyle: {
        backgroundColor: '#fff',
        padding: 0,
        shadowOffset:{width: 5,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5
    },
    publishWrapper: {
        paddingHorizontal: 15
    },
});
