/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    Switch
} from 'react-native';
import { CheckBox, Icon, Overlay } from 'react-native-elements'
import LinearGradient from 'react-native-linear-gradient';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import moment from 'moment';
import qs from "qs";
import {NavigationActions, StackActions} from 'react-navigation';
import styles from './detailStyles';
import ListingActions from '../../../../realm/actions/ListingActions'
import TypeOptions from '../../../assets/json/TypeOptions.json'
import Features from '../../../assets/json/Features.json'
import Fittings from '../../../assets/json/Fittings.json'
import Space from '../../../assets/json/Space.json'
import PropertyTypeOptions from '../../../assets/json/PropertyTypeOptions.json'
import { Images } from '../../../assets/theme';
import { FetchOne, GetData, BulkUpdate, PostData } from '../../services/api'
import Past_TransactionDetail from '../../../components/Past_TransactionDetail'
import ShareHelper from '../../../components/ShareHandler'
import CollapsibleText from '../../../components/CollapsibleText'
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../../../components/Loader';
import MultiTapHandler from '../../services/MultiTapHandler'
import CustomSelect from '../../../components/CustomSelect';
import { aucDay,aucMonth,aucYear } from '../../services/formOptions'

const FETCHONE = 'https://prolex.edgeprop.my/api/v1/fetchOne';
const BULKUPDATE = 'https://prolex.edgeprop.my/api/v1/bulkUpdate';
const PROP_STATUS = 'https://prolex.edgeprop.my/api/v1/get-property-stats';
const API_PAST_TRANSACTION_WITH_PROJECT = "https://edgeprop.my/jwdalice/api/v1/transactions/details";
const API_PAST_TRANSACTION_WITHOUT_PROJECT = "https://edgeprop.my/jwdalice/api/v1/transactions/search";
const FEATURED = 'https://prolex.edgeprop.my/api/v1/addFeatured';
export default class Listing extends Component{

  constructor(props){
    super(props);
    this.ListingActions = new ListingActions();
    this.data = props.navigation.state.params.data? props.navigation.state.params.data : {};
    this._handleBackPress = this._handleBackPress.bind(this)
    this._onEdit = this._onEdit.bind(this)
    this._toggleSwitch = this._toggleSwitch.bind(this)
    this._updateList =  this._updateList.bind(this)
    this._fetchOne = this._fetchOne.bind(this)
    this.fetchOneSuccess = this.fetchOneSuccess.bind(this)
    this.fetchOneFail = this.fetchOneFail.bind(this)
    this.getDataSuccess = this.getDataSuccess.bind(this)
    this.getDataFail = this.getDataFail.bind(this)
    this._callPastTransactions = this._callPastTransactions.bind(this)
    this._rePost = this._rePost.bind(this)
    this._expire = this._expire.bind(this)
    this._deleteItem = this._deleteItem.bind(this)
    this._featureNot = this._featureNot.bind(this)
    this._backFromEdit = this._backFromEdit.bind(this)
    this._activate = this._activate.bind(this)
    this._navigateToListing = this._navigateToListing.bind(this)
    this.fetchData = this.fetchData.bind(this)
    this._featuredOption = this._featuredOption.bind(this)
    this._checkForm = this._checkForm.bind(this)
    this._makeFeatured = this._makeFeatured.bind(this)
    this.featuredSuccess = this.featuredSuccess.bind(this)
    this.featuredFail = this.featuredFail.bind(this)
    this._onImageViewer = this._onImageViewer.bind(this);
    let listingDetails = this.ListingActions.GetListing(this.data.lid);
    this.tagged = false;
    this.category = {
            '33' : 'RESIDENTIAL',
            '36' : 'RESIDENTIAL',
            '60 ' : 'COMMERCIAL',
            '70' : 'INDUSTRIAL'
        }
    this.state = {
      userInfo: {},
      isVisible: false,
      isLoading: false,
      apiMessage: '',
      expired: false,
      loadMore: 0,
      parentRefresh: 0,
      listingDetails: listingDetails.length>0? listingDetails[0] : {},
      status : listingDetails.length>0? listingDetails[0].status : false,
      featured: listingDetails.length>0? listingDetails[0].isFeatured : false,
      tableHead: ['Date', 'Type', 'Area', 'Price (PSF)'],
      widthArr: [100, 100, 100, 100],
      pastTransactionDetail: listingDetails.length>0? listingDetails[0].past_transactions? JSON.parse(listingDetails[0].past_transactions) : {} : {},
      added_records: {},
      featuredShow: false,
      mid: '',
      isError: false,
      feature_day: moment().format('DD'),
      feature_month: moment().format('MM'),
      feature_year: parseInt(moment().format('YYYY')),
      isPrompt: false
    }
    if(listingDetails[0])
      this._updateList(listingDetails[0], false);
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.uid != '') {
        this.setState({ userInfo: authItems })
      }
    }
  }



  _updateList(item, flag, action) {
    let type = item.lid == item.mid ? 'm' : 'n';
    const data = "uid=" + this.data.uid +
            "&token=" + this.data.token +
            "&field_prop_listing_type=" + item.listing_type +
            "&property_id=" + parseInt(item.lid) +
            "&cache=0" +
            "&source=edit"+
            "&type=" + type;
    let dataStatus = ""; 
    let mid = item.mid? item.mid : 0;
    let nid = item.nid? item.nid : 0;
          dataStatus = "mid=" +mid +
                "&nid=" +nid ; 
    this.fetchData(data, dataStatus).then(arrayOfResponses => {
      this.fetchOneSuccess(arrayOfResponses[0], flag, action, arrayOfResponses[1])
    }).catch(error => this.fetchOneFail(error));
  }

  _fetchOne(body, flag, action) {
    FetchOne(FETCHONE, body)
    .then(data => this.fetchOneSuccess(data, flag, action))
    .catch(error => this.fetchOneFail(error));
  }

  _featureNot() {
    alert('This feature is not available');
  }

  fetchOneSuccess(response, flag, action, statusResponse){
    if(response.result){
      this._callPastTransactions(response.result);
      const listingData = this.formatListing(response.result, statusResponse);
      this.ListingActions.SetListing(listingData, 'lid = '+this.data.lid);
      if(!!!flag){
        let listItem = this.ListingActions.GetListing(this.data.lid);
        if(listItem && listItem.length >0){
          this.setState({
            listingDetails: listItem.length>0? listItem[0] : {},
            status : listItem.length>0? (listItem[0].status == 1? true : false) : false,
            featured: listItem.length>0? listItem[0].isFeatured : false,
          });
        }
        this.setState({
          expired : (this.state.listingDetails.prop_status == 'expired') ? true : false
        });
      }else{
        this._navigateToListing(action)
      }
    }
    this.setState({ isLoading: false })
  }

  _onImageViewer() {
    let listingInfo = this.state.listingDetails.data? JSON.parse(this.state.listingDetails.data) : {};
    let images = (listingInfo && listingInfo.field_prop_images && listingInfo.field_prop_images.und && Array.isArray(listingInfo.field_prop_images.und)) ? listingInfo.field_prop_images.und : [];
    let gallery_uri = images.map((item, index) => { return item.gallery_uri });
    if(gallery_uri.length > 0){
      this.props.navigation.navigate('ImageViewer', {
        data: {
          images: gallery_uri,
          count: gallery_uri.length
        }
      });
    }
    
  }

  _backFromEdit(item){
    this.setState({loadMore: moment().unix(), parentRefresh : moment().unix()})
    this._updateList(item, false);
  }

  _navigateToListing(type){
    let messages= {
      '0': 'Your listing is expired successfully.',
      '2': 'Your listing is reposted successfully.',
      '4': 'Your listing is activated successfully.',
    }
    let message = messages[type]? messages[type] : 'Action done successfully.';
    this.props.navigation.navigate(
      'DrawerNavigator', 
      {}, 
      NavigationActions.navigate(
        { 
          routeName: 'Home', 
          params: { 
            data:{
              listing_type: this.state.listingDetails.listing_type
            },
            message,
            timeStamp: moment().unix(),
            tabIndex: (type == 2 || type == 4)? 1 : 2
          },
          action: {}
        })
    );
  }

  _againFetchOne(type) {
      //this.setState({ isVisible: false})
      if(type == 1){
        /*this.props.navigation.navigate('Home',{
          data: {
            listing_type: this.state.listingDetails.listing_type 
          },
          message: 'Your listing is deleted successfully.',
          timeStamp: moment().unix() 
        });*/
        let tabIndexs= {
          'draft': 0,
          'active': 1,
          'expired': 2,
        }
        let tabIndex = tabIndexs[this.state.listingDetails.prop_status]? tabIndexs[this.state.listingDetails.prop_status] : 0;
        this.props.navigation.navigate(
          'DrawerNavigator', 
          {}, 
          NavigationActions.navigate(
            { 
              routeName: 'Home', 
              params: { 
                data:{
                  listing_type: this.state.listingDetails.listing_type
                },
                message: 'Your listing is deleted successfully.',
                timeStamp: moment().unix(),
                tabIndex
              },
              action: {}
            })
        );
      }else{
        let flag = (type == 0 || type == 2 || type == 4)? true : false;
        this._updateList(this.state.listingDetails, flag, type);
      }
  }

  _expire(listingtype, idtype, check) {
    var numb = idtype.match(/\d/g);
    numb = numb.join("");
    let value = idtype.replace(/[0-9]/g, '');
    value ='LIDM' ? 'm' : 'n';
    let Idvalues = value.concat(numb)
      

    this.setState({ isLoading: true })
    const body = "uid=" + this.state.userInfo.uid +
        "&token=" + this.state.userInfo.token +
        "&field_prop_listing_type=" + listingtype +
        "&repost=0" +
        "&ids[0]=" + Idvalues +
        "&columns[field_prop_status]=expired"
            let val = check? 3 : 0;
    BulkUpdate(BULKUPDATE, body)
    .then(data => this._bulkSuccess(data, val))
    .catch(error => this.fetchFail(error));
  }

  _activate(listingtype, idtype) {
    var numb = idtype.match(/\d/g);
    numb = numb.join("");
    let value = idtype.replace(/[0-9]/g, '');
    value ='LIDM' ? 'm' : 'n';
    let Idvalues = value.concat(numb)
      

    this.setState({ isLoading: true })
    const body = "uid=" + this.state.userInfo.uid +
        "&token=" + this.state.userInfo.token +
        "&field_prop_listing_type=" + listingtype +
        "&repost=0" +
        "&ids[0]=" + Idvalues +
        "&columns[field_prop_status]=active"
            
    BulkUpdate(BULKUPDATE, body)
    .then(data => this._bulkSuccess(data, 4))
    .catch(error => this.fetchFail(error));
  }

  _deleteItem(listingtype, idtype) {
    var numb = idtype.match(/\d/g);
    numb = numb.join("");
    let value = idtype.replace(/[0-9]/g, '');
    value ='LIDM' ? 'm' : 'n';
    let Idvalues = value.concat(numb)
      

    this.setState({ isLoading: true })
    const body = "uid=" + this.state.userInfo.uid +
        "&token=" + this.state.userInfo.token +
        "&field_prop_listing_type=" + listingtype +
        "&repost=0" +
        "&ids[0]=" + Idvalues +
        "&columns[field_prop_status]=deleted"
            
    BulkUpdate(BULKUPDATE, body)
    .then(data => this._bulkSuccess(data, 1))
    .catch(error => this.fetchFail(error));
  }

  _rePost(listingtype, idtype) {
    var numb = idtype.match(/\d/g);
    numb = numb.join("");
    let value = idtype.replace(/[0-9]/g, '');
    value ='LIDM' ? 'm' : 'n';
    let Idvalues = value.concat(numb)
      

    this.setState({ isLoading: true })
    const body = "uid=" + this.state.userInfo.uid +
        "&token=" + this.state.userInfo.token +
        "&field_prop_listing_type=" + listingtype +
        "&repost=1" +
        "&ids[0]=" + Idvalues +
        "&columns[changed]="
            
    BulkUpdate(BULKUPDATE, body)
    .then(data => this._repostSuccess(data))
    .catch(error => this.fetchFail(error));
  }  

  _bulkSuccess(data, flag) {
    if(data.result && data.result.skipped_records && data.result.skipped_records.count > 0){
      let text = 'Property deletion failed. Please do after sometimes';
      if(flag == 3){
        text = 'Property expiry failed. Please do after sometimes';
      }else if(flag == 4){
        text = 'Property activation failed. Please do after sometimes';
        data.result.skipped_records.message.map((item, index) => {
          text = item;
        });
      }
      this.setState({ 
        isLoading: false, 
        isVisible: true, 
        apiMessage: text, 
        isError: true 
      });
    }else{
      let message = data.message;
      let isVisible = false; 
      let apiMessage = '';
      let isError = false;
      if(message == 'Property bulk update successfully done'){
        if(flag == 1){
          this.ListingActions.ClearListing(this.data.lid);
        }
        /*if(flag ==2){
          isVisible = true; 
          if(flag == 2){
            apiMessage = 'Your listing is reposted successfully.';
          }
        }*/
      } else {
        data.result.skipped_records.message.map((item, index) => {
          errorMessage = item;
        });
      }
      this.setState({ 
        isLoading: true, 
        isVisible: isVisible, 
        apiMessage: apiMessage, 
        isError: isError
      }, () => {
        this._againFetchOne(flag)
      });
    }
  }

  _repostSuccess(data) {

    let message = data.message;
    if(data.result && data.result.skipped_records && data.result.skipped_records.count > 0){
      if(data.message == "Skipped properties are not eligible for this bulk action. Please check skipped messages") {
        message = data.result.skipped_records.message[0];
      }
      this.setState({ 
        isLoading: false, 
        isVisible: true, 
        apiMessage: message, 
        isError: true 
      });
    }else{
      if(message == 'Property bulk update successfully done'){
        message = 'Property listing is reposted successfully.'
      }
      /*this.setState({ 
        isLoading: false, 
        isVisible: false, 
        apiMessage: message, 
        //parentRefresh : moment().unix() 
      }, () => {
        this._againFetchOne(2)
      });*/
      this._againFetchOne(2)
    }
  }

  fetchData = (body1, body2) => {
    const urls = [
      { url : FETCHONE, body: body1},
      { url : PROP_STATUS, body: body2}
    ];
    
    const allRequests = urls.map(item => 
      fetch(item.url, {
        method: 'POST',
        headers: new Headers({
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
        body: item.body
      }).then(response => response.json())
    );

    return Promise.all(allRequests);
  };

  fetchFail(data) {
    let message = 'Seems like something went wrong.';
    if(data.message){
        message = data.message;
      }
    this.setState({ isLoading: false, isVisible: true, apiMessage: message });
  }

  formatListing = (data, response) => {
    let asset = data.asset;
    let listing = {};

    //listing.lid = data.mid ? data.mid : data.nid;

    listing.nid = data.nid ? data.nid : null;

    listing.mid = data.mid ? data.mid : null;

    listing.title = data.title ? data.title : '';

    listing.status = data.status ? parseInt(data.status) : 0;
  
    listing.created = data.created ? data.created : '';

    listing.changed = data.changed ? data.changed : '';

    listing.district = data.district ? data.district.toString() : '';

    listing.state = data.state ? data.state.toString() : '';

    listing.project_name = asset.project_name ? asset.project_name : '';

    listing.project_id = asset.id ? asset.id : '';

    listing.prop_status = (data && data.field_prop_status && data.field_prop_status.und && Array.isArray(data.field_prop_status.und)) ? (data.field_prop_status.und[0]? data.field_prop_status.und[0].value : '') : '';

    listing.listing_type = (data && data.field_prop_listing_type && data.field_prop_listing_type.und && Array.isArray(data.field_prop_listing_type.und)) ? (data.field_prop_listing_type.und[0]? data.field_prop_listing_type.und[0].value : '') : '';

    listing.property_type = (data && data.field_property_type && data.field_property_type.und && Array.isArray(data.field_property_type.und)) ? (data.field_property_type.und[0]? data.field_property_type.und[0].target_id : '') : '';

    listing.asset_id = (data && data.field_prop_asset && data.field_prop_asset.und && Array.isArray(data.field_prop_asset.und)) ? (data.field_prop_asset.und[0]? data.field_prop_asset.und[0].target_id : '') : '';

    listing.price = (data && data.field_prop_asking_price && data.field_prop_asking_price.und && Array.isArray(data.field_prop_asking_price.und)) ? (data.field_prop_asking_price.und[0]? data.field_prop_asking_price.und[0].value : null) : null;

    listing.street = (data && data.field_prop_street && data.field_prop_street.und && Array.isArray(data.field_prop_street.und)) ? (data.field_prop_street.und[0]? data.field_prop_street.und[0].value : null) : null;

    let coverImages = (data && data.field_cover_image && data.field_cover_image.und && Array.isArray(data.field_cover_image.und)) ? data.field_cover_image.und : [];
    listing.cover_image = (coverImages && coverImages[0] && coverImages[0].uri)? coverImages[0].uri: '';
    
   
    let images = (data && data.field_prop_images && data.field_prop_images.und && Array.isArray(data.field_prop_images.und)) ? data.field_prop_images.und : [];
    let list_uri = [];
    list_uri = images.map((item, index) => { return item.list_uri });
    listing.images = list_uri;

    listing.data = JSON.stringify(data);
    listing.url = data.url ? data.url : '';
    if(response && (response.mid || response.nid)){
      listing.show_number = response.pageviewed;
      listing.contacted = response.contactagent;
      listing.view_contact = response.viewcontact;
      listing.whatsapp_count = response.mobWhatsapp;
      listing.impression = response.impressions;
      listing.isFeatured = response.isFeatured;
      if(response.featuredData){
        listing.featuredData = JSON.stringify(response.featuredData);
      }
    }

    return listing;
  }

  fetchOneFail(error){
    this.setState({ isLoading: false })
  }

  _handleBackPress() {
    if(this.props.navigation.state.params.refresh) {
      this.props.navigation.state.params.refresh(this.state.parentRefresh);
    }
    this.props.navigation.goBack();
  }

  _callPastTransactions(result){
      let listingType = result.field_prop_listing_type? (result.field_prop_listing_type.und? (result.field_prop_listing_type.und[0] ? result.field_prop_listing_type.und[0].value : ''): '') : '';
      if(listingType == 'sale') {
          let category = result.field_property_type? (result.field_property_type.und? result.field_property_type.und : []) : [];
          category.map((obj, i) => {
              if (obj.target_id in this.category)
                  category = this.category[obj.target_id];
          });
          if(category.length == 0) {
              category = 'RESIDENTIAL';
          }

          let state= result.state;
          let area= result.district;
          let pastApiUrl = API_PAST_TRANSACTION_WITHOUT_PROJECT;
          
          let project = result.asset? (result.asset.project_name? result.asset.project_name: '') : '';
          let pastTransactionParams = "?state="+state+"&area="+encodeURIComponent(area)+"&category="+category+"&orderby=contract_date&sortby=desc&page=1";
          if(project != '') {
              this.tagged = true;
              pastApiUrl = API_PAST_TRANSACTION_WITH_PROJECT;
              pastTransactionParams +="&project="+project;
          }
          
          GetData(pastApiUrl+pastTransactionParams)
          .then(data => this.getDataSuccess(data))
          .catch(error => this.getDataFail(error));
      }
  }

  getDataSuccess(data){
    let pastTransactions = {
      past_transactions: JSON.stringify(data)
    }
    this.ListingActions.SetListing(pastTransactions, 'lid = '+this.data.lid);
    let listItem = this.ListingActions.GetListing(this.data.lid);
    if(listItem && listItem.length >0){
      this.setState({
          pastTransactionDetail: listItem[0].past_transactions? JSON.parse(listItem[0].past_transactions) : {}
      });
    }
  }

  getDataFail(error){
    
  }

  _onEdit() {
    this.props.navigation.navigate('ListingEdit',{
      data: {
        lid: this.data.lid,
        refresh: this._backFromEdit
      }
    });
  }

  _toggleSwitch(value) {
    let lid = this.state.listingDetails.mid? `LIDM${this.state.listingDetails.mid}` : `LIDN${this.state.listingDetails.nid}`;
    if(value){
      this._activate(this.state.listingDetails.listing_type, lid);
    }else{
      this._expire(this.state.listingDetails.listing_type, lid , false);
    }
    //this.setState({status : value});
  }

  _handleCollection(key,collection) {
      let temp = []
      for (let i = 0; i < collection.length; i++) {
          temp = [...temp, ...collection[i][key].map(data => data)];
      }
      return temp
  }

  _prefixType(id){
      var prefix = '';
      if(33<id && id<36){
          prefix = 'r-';
      }else if(36<id && id<45){
          prefix = 'l-';
      }else if(60<id && id<70){
          prefix = 'c-';
      }else if(70<id && id<74){
          prefix = 'i-';
      }
      return prefix+id;
  }

  _getLabel(key,collection){
        let res ='';
        if(key && collection.length > 0){
            res = collection.filter(value => value.id == key)
        }
        res = res[0]? res[0].value : '';
        return res;
    }

  _checkSize(data){
    let size = data.length? data.length : 0;
    if(typeof data == 'object'){
      size = Object.keys(data).length;
     }
    return size;
  }

  _shareItem(){
      if(this.state.listingDetails.url != '') {
        this.refs.share._share('https://www.edgeprop.my/listing/'+this.state.listingDetails.url.replace(/^"(.*)"$/, '$1'));
      }else{
        alert('Feature not available.')
      }
    }

  async _featuredOption(mid) {
      if(this.state.listingDetails.featuredData){
        let featuredData = JSON.parse(this.state.listingDetails.featuredData);
        if(featuredData.start_date && featuredData.end_date){
          let startDate = moment.unix(featuredData.start_date).format("DD-MMM-YYYY");
          let endDate = moment.unix(featuredData.end_date).format("DD-MMM-YYYY");
          let message = 'Listing is already Featured from '+startDate+' to '+endDate;
          this.setState({ isLoading: false, isVisible: true, apiMessage: message });
          return;
        }
      }
      const plan = await AsyncStorage.getItem("agentPlan");
      if(plan == 2){
        this.setState({featuredShow : true, mid : mid});
      }else{
        this.setState({ isLoading: false, isVisible: true, isError: true, apiMessage: 'Upgrade to our PRO Agent subscription.' });
      }
    }

  _checkForm(){
      let check = this.dateValidate(this.state.feature_day,this.state.feature_month,this.state.feature_year);
      this.setState({ featured_error: check });
    }

    dateValidate(day,month,year) {
    
    let flag = false;
    let yesterday = moment().add(-1, 'days').format("YYYY-MM-DD");
    if(moment(day+'/'+month+'/'+year, 'DD/MM/YYYY').isValid())
    {
      if(!moment(year+'-'+month+'-'+day).isAfter(yesterday))
      {
        flag = true;
      }else{
        flag = false;
      }
    }else{
      flag = true;
    }
    return flag;
  }

    _makeFeatured(){
      if(!this.state.featured_error){
        this.setState({ isLoading: true })
        let obj = {
          uid: this.state.userInfo.uid,
          featureds:[],
          token: this.state.userInfo.token
        }
        let start_date = this.state.feature_year+'-'+this.state.feature_month+'-'+this.state.feature_day;
        obj.featureds.push({property_id : this.state.mid , start_date: start_date})      
        PostData(FEATURED, qs.stringify(obj))
        .then(data => this.featuredSuccess(data))
        .catch(error => this.featuredFail(error));
      }
      
    }

  featuredSuccess(data){
    this.setState({ isLoading: false, featuredShow : false }, () => {
      if(data.result){
        let result = data.result;
        let message = data.message; 
        let isError = false;
        let property_point_crossed = result.point_error_messages.property_point_crossed;
        if(result.added_records && result.added_records.count > 0){
          let property_id = result.added_records.property_id;
          if(property_id.length >0){
            message = property_id[0].message;
          }
        }else if(result.skipped_records && result.skipped_records.count > 0){
          let details = result.skipped_records.details;
          if(details.length >0){
            message = details[0].message;
          }
          isError = true;
        }else if(property_point_crossed && property_point_crossed.length >0){
          let cross_proints = Object.values(property_point_crossed);
          let cross_point_id = Object.keys(property_point_crossed);
          let cross_id = cross_point_id[0];
          let cross_msg = cross_proints[0];
          message = cross_id+':'+cross_msg;
          isError = true;
        }
        if(message == 'This agent is not under premium plan'){
          message = 'Upgrade to our PRO Agent subscription.'
        }
        if(!isError){
          this._updateList(this.state.listingDetails, false);
        }
        this.setState({ isVisible: true, isError, apiMessage: message, added_records: result.added_records });
      }else{
        this.setState({ isVisible: true, isError: true, apiMessage: 'Unable to make listing as featured!' });
      }
    })
  }

  featuredFail(error){
    this.setState({ isLoading: false, featuredShow : false })
    this.setState({ isVisible: true, isError: true, apiMessage: 'Unable to make listing as featured!' });
  }

  sqftValue(value, unit){
    let factors = {'sqft': 1,'sqm': 10.7639,'acre': 43560};
    let puValue = unit? unit.toLowerCase() : 'sqft';
    let divide = factors[puValue];
    let result =value/divide;
    if(result){
      let count = this.countDecimals(result);
      if(count > 6){
        result = result.toFixed(6)
      }else{
        result = result.toFixed(count)
      }
    }
    return result;
  }

  countDecimals(value) {
    if(Math.floor(value) === value) return 0;
    return value.toString().split(".")[1].length || 0; 
  }

  initProperty(){
    let cover_image = this.state.listingDetails.cover_image;
    let images      = this.state.listingDetails.images? this.state.listingDetails.images: [];
    let default_images = 'https://list.edgeprop.my/static/img/no-image.png';
    cover_image = cover_image? cover_image : images[0] ? images[0] : default_images;
    let is_public_photo = cover_image.search("public://");
    if(is_public_photo == 0){
      cover_image = default_images;
    }
    let listingInfo = this.state.listingDetails.data? JSON.parse(this.state.listingDetails.data) : {};

    let expiry = listingInfo.field_prop_expiry_date? listingInfo.field_prop_expiry_date.und? listingInfo.field_prop_expiry_date.und[0]? listingInfo.field_prop_expiry_date.und[0].value : 0 : 0 : 0;
    /**  ADDITIONAL INFO  **/
    let additionalInfo = [];
    let subType = listingInfo.field_property_type? (listingInfo.field_property_type.und? (listingInfo.field_property_type.und[1] ? listingInfo.field_property_type.und[1].target_id : ''): '') : '';
    let subTypeLabel =this._getLabel(this._prefixType(subType),this._handleCollection('sub',PropertyTypeOptions));
        subType = subTypeLabel? subTypeLabel: '';
        

    let listingType  = listingInfo.field_prop_listing_type? (listingInfo.field_prop_listing_type.und? (listingInfo.field_prop_listing_type.und[0] ? listingInfo.field_prop_listing_type.und[0].value : ''): '') : '';
    let listingTypeLabel  = this._getLabel(listingType,TypeOptions);
        listingType  = listingTypeLabel? listingTypeLabel : listingType;
    
    additionalInfo.push({
      'type' : subType? subType : listingType,
      'icon' : require('../../../assets/images/home.png'),
      width: 18,
      height: 18
    })

    let bedrooms = listingInfo.field_prop_bedrooms? (listingInfo.field_prop_bedrooms.und? (listingInfo.field_prop_bedrooms.und[0] ? listingInfo.field_prop_bedrooms.und[0].value : ''): '') : '';
    if(bedrooms !==''){
      let label = 'BR'
      if(bedrooms == 0){
        label = 'Studio';
      }else{
        label = bedrooms+' BR';
      }
      additionalInfo.push({
          'type' : label,
          'icon' : require('../../../assets/images/bed.png'),
          width: 18,
          height: 18
        })
    }
    let bathrooms = listingInfo.field_prop_bathrooms? (listingInfo.field_prop_bathrooms.und? (listingInfo.field_prop_bathrooms.und[0] ? listingInfo.field_prop_bathrooms.und[0].value : ''): '') : '';
    if(bathrooms){
      additionalInfo.push({
          'type' : bathrooms+' B',
          'icon' : require('../../../assets/images/bath.png'),
          width: 18,
          height: 18
        })
    }

    let buildArea = listingInfo.field_prop_built_up? (listingInfo.field_prop_built_up.und? (listingInfo.field_prop_built_up.und[0] ? listingInfo.field_prop_built_up.und[0].value : ''): '') : '';
    let buildAreaUnit = listingInfo.field_prop_built_up_unit? (listingInfo.field_prop_built_up_unit.und? (listingInfo.field_prop_built_up_unit.und[0] ? listingInfo.field_prop_built_up_unit.und[0].value? listingInfo.field_prop_built_up_unit.und[0].value.toUpperCase() : ''  : ''): '') : '';
    if(buildArea){
      additionalInfo.push({
          'type' : this._formatNumber(buildArea) + ' '+buildAreaUnit,
          'icon' : require('../../../assets/images/area.png'),
          width: 18,
          height: 18
        })
    }
    
    let pricepu = listingInfo.field_prop_built_up_price_pu? (listingInfo.field_prop_built_up_price_pu.und? (listingInfo.field_prop_built_up_price_pu.und[0] ? listingInfo.field_prop_built_up_price_pu.und[0].value : ''): '') : '';
    if(pricepu){
      let psfValue = this.sqftValue(pricepu,buildAreaUnit);
      if(pricepu){
        additionalInfo.push({
            'type' : this._formatNumber(psfValue) + ' PSF',
            'icon' : require('../../../assets/images/graph.png'),
            width: 18,
            height: 18
          })
      }
    }
    
    let landArea = listingInfo.field_prop_land_area? (listingInfo.field_prop_land_area.und? (listingInfo.field_prop_land_area.und[0] ? listingInfo.field_prop_land_area.und[0].value : ''): '') : '';
    let landAreaUnit = listingInfo.field_prop_area_unit? (listingInfo.field_prop_area_unit.und? (listingInfo.field_prop_area_unit.und[0] ? listingInfo.field_prop_area_unit.und[0].value? listingInfo.field_prop_area_unit.und[0].value.toUpperCase() : ''  : ''): '') : '';
    if(landArea){
      additionalInfo.push({
          'type' : this._formatNumber(landArea) + ' '+landAreaUnit,
          'icon' : require('../../../assets/images/shape.png'),
          width: 18,
          height: 18
        })
    }
    
    let priceLandpu = listingInfo.field_prop_price_pu? (listingInfo.field_prop_price_pu.und? (listingInfo.field_prop_price_pu.und[0] ? listingInfo.field_prop_price_pu.und[0].value : ''): '') : '';
    if(priceLandpu){
      let psfLandValue = this.sqftValue(priceLandpu,landAreaUnit);
      if(pricepu){
        additionalInfo.push({
            'type' : this._formatNumber(psfLandValue) + ' PSF',
            'icon' : require('../../../assets/images/earnings.png'),
            width: 18,
            height: 18
          })
      }
    }
    
    let features = listingInfo.field_prop_features? (listingInfo.field_prop_features.und?  listingInfo.field_prop_features.und : []) : [];
        features = features.map((obj, i) => {
            return {key:obj.value,value:this._getLabel(obj.value,Features)};
        })
    let facilities = listingInfo.field_prop_fixtures_fittings? (listingInfo.field_prop_fixtures_fittings.und? listingInfo.field_prop_fixtures_fittings.und : []) : [];
        facilities = facilities.map((obj, i) => {
            return {key:obj.value,value:this._getLabel(obj.value,Fittings)};
        })
    let inOutSpace = listingInfo.field_prop_inout_space? (listingInfo.field_prop_inout_space.und? listingInfo.field_prop_inout_space.und : []) : [];
        inOutSpace = inOutSpace.map((obj, i) => {
            return {key:obj.value,value:this._getLabel(obj.value,Space)};
        })
    let feature_facility = []
    feature_facility.push(...features, ...facilities, ...inOutSpace)
    

    return {
      lid: this.state.listingDetails.mid? `LIDM${this.state.listingDetails.mid}` : `LIDN${this.state.listingDetails.nid}`,
      cover_image,
      impression: this.state.listingDetails.impression? this.state.listingDetails.impression : 0,
      show_number: this.state.listingDetails.view_contact? this.state.listingDetails.view_contact : 0,
      contacted: this.state.listingDetails.contacted? this.state.listingDetails.contacted : 0,
      whatsapp_count: this.state.listingDetails.whatsapp_count? this.state.listingDetails.whatsapp_count : 0,
      image_count: this._checkSize(images),
      status: this.state.listingDetails.status,
      title: this.state.listingDetails.title,
      price: (listingInfo && listingInfo.field_prop_asking_price && listingInfo.field_prop_asking_price.und && Array.isArray(listingInfo.field_prop_asking_price.und)) ? (listingInfo.field_prop_asking_price.und[0]? listingInfo.field_prop_asking_price.und[0].value : 0) : 0,
      updated: this.state.listingDetails.changed,
      additionalInfo,
      feature_facility,
      created: this.state.listingDetails.created,
      expiry,
      listingDescription : listingInfo.field_prop_info?  (listingInfo.field_prop_info.und? (listingInfo.field_prop_info.und[0] ? listingInfo.field_prop_info.und[0].value : ''): '') : ''
    }
  }

  _handleValue(value) {
        // handle empty value
        if (value == null || value == '' || value == 'Uncompleted') {
            return ''
        }
        // handle if value is integer
        if (value === parseInt(value, 10)) return value
        // handle string, and set the capitalization
        return this._capitalizeFirstLetter(value)
    }

    _isEmptyValue(value) {
        return (value == null || value == '' || value == 'Uncompleted' || value == undefined)
    }

    _capitalizeFirstLetter(str) {
        if ((typeof str) == 'string') {
            return str.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
            })
        }
    }

    _formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    _formatMoney(val) {
        return val ? 'RM ' + this._formatNumber(val) : '-'
    }

  _wrapAssetInfo() {
        let listingInfo = this.state.listingDetails.data? JSON.parse(this.state.listingDetails.data) : {};
        let assetData = {
          assetStreetName: (listingInfo && listingInfo.field_prop_street && listingInfo.field_prop_street.und && Array.isArray(listingInfo.field_prop_street.und)) ? (listingInfo.field_prop_street.und[0]? listingInfo.field_prop_street.und[0].value : '') : '',
          assetDistrict: this.state.listingDetails.district,
          assetPostalCode: (listingInfo && listingInfo.field_prop_postcode && listingInfo.field_prop_postcode.und && Array.isArray(listingInfo.field_prop_postcode.und)) ? (listingInfo.field_prop_postcode.und[0]? listingInfo.field_prop_postcode.und[0].value : '') : '',
          assetState: this.state.listingDetails.state,
        }
        let assetInfo = ((assetData.assetStreetName != undefined && !this._isEmptyValue(assetData.assetStreetName)) ? (this._handleValue(assetData.assetStreetName + (' \xB7 '))) : '')
            + ((assetData.assetDistrict != undefined && !this._isEmptyValue(assetData.assetDistrict)) ? (this._handleValue(assetData.assetDistrict + ', ')) : '')
            + ((assetData.assetPostalCode != undefined && !this._isEmptyValue(assetData.assetPostalCode)) ? (this._handleValue(assetData.assetPostalCode) + ' ') : '')
            + ((assetData.assetState != undefined && !this._isEmptyValue(assetData.assetState)) ? (this._handleValue(assetData.assetState) + (' \xB7 ')) : '')
        return assetInfo
    }

  render() {
    let viewData = this.initProperty()

    const state = this.state;
    const tableData = [];
    for (let i = 0; i < 10; i += 1) {
      const rowData = [];
      for (let j = 0; j < 9; j += 1) {
        rowData.push(`${i}${j}`);
      }
      tableData.push(rowData);
    }
    return (
      <View style={styles.container}>
        <ShareHelper
          ref={"share"}
          message={"This property might interests you"}
        />
        <ScrollView>
          {/*  Property Image */}
          <View style={styles.propImgWrapper}>
            <TouchableOpacity onPress={this._onImageViewer} style={{flex:1}}>
              <ImageBackground source={{uri: viewData.cover_image}} style={styles.cardImage}>
                  <LinearGradient colors={['rgba(0,0,0,0.7)', 'transparent', 'rgba(0,0,0,1)']} style={styles.cardOverlay}>
                    <SafeAreaView>
                      <TouchableOpacity style={styles.navLeft} onPress={this._handleBackPress}>
                        <Icon
                          name='arrow-left'
                          size={26}
                          type='feather'
                          color='#fff'
                        />
                      </TouchableOpacity>
                    </SafeAreaView>
                    <View style={styles.cardOverlayInnerDetail}>
                      <View style={[styles.cardOverlayInner, styles.cardOverlayInnerLeft]}>
                        <View style={[styles.cardOverlayItem, styles.cardOverlayItemLeft]}>
                          <Text allowFontScaling={false} style={styles.cardOverlayText}>{viewData.impression}</Text>
                          <Image
                            style={{width: 15, height: 15}}
                            source={require('../../../assets/images/steps.png')}
                          />
                        </View>
                        <View style={[styles.cardOverlayItem, styles.cardOverlayItemLeft]}>
                          <Text allowFontScaling={false} style={styles.cardOverlayText}>{viewData.show_number}</Text>
                          <Image
                            style={{width: 15, height: 15}}
                            source={require('../../../assets/images/view.png')}
                          />
                        </View>
                        <View style={[styles.cardOverlayItem, styles.cardOverlayItemLeft]}>
                          <Text allowFontScaling={false} style={styles.cardOverlayText}>{viewData.whatsapp_count}</Text>
                          <Image
                            style={{width: 15, height: 15}}
                            source={require('../../../assets/images/whatsapp-sm.png')}
                          />
                        </View>
                      </View>
                      <View style={[styles.cardOverlayInner, styles.cardOverlayInnerRight]}>
                        <View style={[styles.cardOverlayItem, styles.cardOverlayItemRight]}>
                          <Text allowFontScaling={false} style={styles.cardOverlayText}>{viewData.contacted}</Text>
                          <Image
                            style={{width: 15, height: 15}}
                            source={require('../../../assets/images/comment-white.png')}
                          />
                        </View>
                        <View style={[styles.cardOverlayItem, styles.cardOverlayItemRight]}>
                          <Text allowFontScaling={false} style={styles.cardOverlayText}>{viewData.image_count}</Text>
                          <Image
                            style={{width: 15, height: 15}}
                            source={require('../../../assets/images/gallery.png')}
                          />
                        </View>
                      </View>
                    </View>
                  </LinearGradient>
              </ImageBackground>
            </TouchableOpacity>
          </View>
          <View style={styles.propContentWrapper}>
            {/* Status */}
            {<View style={[styles.contentSection]}>
              <Text allowFontScaling={false} style={styles.commonLabel}>Status</Text>
              <View style={styles.propStatus}>
                <Text allowFontScaling={false} style={styles.textSm}>Publish</Text>
                <Switch value={this.state.status} onValueChange = {this._toggleSwitch}/>
              </View>
            </View>}
            {/* Main Info */}
            <View style={[styles.contentSection, styles.propMajorInfo]}>
              <View style={styles.propMajorInfoLeft}>
                <Text allowFontScaling={false} style={styles.cardTitle}>{viewData.title}</Text>
                <View style={styles.cardLocation}>
                  <Icon
                    name='ios-pin'
                    size={15}
                    type='ionicon'
                    color='#96d9c2'
                  />
                  <Text allowFontScaling={false} style={styles.cardContentLocation}>{this._wrapAssetInfo()}</Text>
                </View>
                {viewData.price > 0 &&
                  <Text allowFontScaling={false} style={styles.cardPrice}>{this._formatMoney(viewData.price)}</Text>
                }
                {/*<View style={styles.cardLocation}>
                  <Icon
                    name='clock'
                    size={10}
                    type='feather'
                    color='#646975'
                    containerStyle={{marginTop: 2}}
                  />
                  <Text allowFontScaling={false} style={styles.cardUpdateDate}>{moment.unix(viewData.updated).fromNow()}</Text>
                </View>*/}
                <View style={{marginTop: 5}}>
                  <Text allowFontScaling={false} style={styles.textSm}>Updated: {moment.unix(viewData.updated).format("DD MMM YYYY")}</Text>
                  {viewData.expiry!=0 && <Text allowFontScaling={false} style={styles.textSm}>Expiry In: {moment(viewData.expiry).format("DD MMM YYYY")}</Text>}
                  <Text allowFontScaling={false} style={styles.textSm}>Created: {moment.unix(viewData.created).format("DD MMM YYYY")}</Text>
                </View>
              </View>
              <View style={styles.propMajorInfoRight}>
                <Text allowFontScaling={false} style={[styles.textSm, {textAlign: 'right'}]}>{viewData.lid}</Text>
              </View>
            </View>
            {/* Feature */}
            {this.state.listingDetails.prop_status == 'active' && <View style={[styles.contentSection, styles.propStatus]}>
              <View style={styles.CheckBoxWrapper}>
                <CheckBox
                  title='Is Featured'
                  iconType='material'
                  checkedIcon='check-box'
                  uncheckedIcon='check-box-outline-blank'
                  checkedColor='#3670e5'
                  checked={this.state.featured}
                  containerStyle={styles.checkBox}
                  textStyle={styles.textSm}
                  onPress={() => this._featuredOption(viewData.lid)}
                />
              </View>
              <TouchableOpacity 
                style={{paddingHorizontal: 5}}
                onPress={() => this._featuredOption(viewData.lid)}
              >
                <Icon
                  name={this.state.featured? 'star' : 'star-outlined'}
                  size={this.state.featured? 24 : 24}
                  type='entypo'
                  color={this.state.featured? '#ffb400' : '#10182d'}
                />
              </TouchableOpacity>
            </View>}
            <View style={styles.contentSection}>
              <Text allowFontScaling={false} style={styles.commonLabel}>Additional Details</Text>
              <View style={[styles.listWrapper, {paddingBottom: 5}]}>
                {viewData.additionalInfo.length > 0 && viewData.additionalInfo.map((item,i) => {
                  return (
                    <View style={styles.listItem} key={i}>
                      <View style={styles.listImgWrapper}>
                        <Image
                          style={{width: item.width? item.width : 17, height: item.height? item.height : 17}}
                          source={item.icon}
                        />
                      </View>
                      <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15 }]}>{item.type}</Text>
                    </View>
                  )
                })}
              </View>
            </View>
            {viewData.feature_facility.length > 0 && <View style={styles.contentSection}>
              <Text allowFontScaling={false} style={styles.commonLabel}>Facilities & Amenities</Text>
              <View style={[styles.listWrapper, {paddingBottom: 5}]}>
                {viewData.feature_facility.length > 0 && viewData.feature_facility.map((item,i) => {
                  return (
                    <View style={styles.listItem} key={i}>
                      <View style={styles.listImgWrapper}>
                        <Image
                          style={{width: 17, height: 17}}
                          source={Images.check}
                        />
                      </View>
                      <Text allowFontScaling={false} style={[styles.textSm, {paddingLeft: 15 }]}>{item.value}</Text>
                    </View>
                  )
                })}
              </View>
            </View>}
            {viewData.listingDescription.length > 0 &&
              <View style={styles.contentSection}>
                <Text allowFontScaling={false} style={styles.commonLabel}>Description</Text>
                <View>
                  {/*<Text allowFontScaling={false} ellipsizeMode='tail' numberOfLines={8} style={styles.textSm}>{viewData.listingDescription}</Text>*/}
                  <CollapsibleText onLayoutChange={this.props.onLayoutChange} text={viewData.listingDescription} numberOfLines={10} isDesc={true} loadMore={this.state.loadMore}/>
                  {/*<TouchableOpacity>
                    <Text allowFontScaling={false} style={[styles.textSm, {color: '#3670e5', paddingTop: 10}]}>View more</Text>
                  </TouchableOpacity>*/}
                </View>
              </View>
            }
            {/*<View style={styles.contentSection}>
              <Text allowFontScaling={false} style={styles.commonLabel}>Past Transactions</Text>
              <ScrollView horizontal={true}>
                <View>
                  <Table>
                      <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.headtext}/>
                    </Table>
                    <ScrollView style={styles.dataWrapper}>
                      <Table>
                        {
                          tableData.map((rowData, index) => (
                            <Row
                              key={index}
                              data={rowData}
                              widthArr={state.widthArr}
                              style={[styles.row, index%2 && {backgroundColor: '#e6ecf2'}]}
                              textStyle={styles.rowText}
                            />
                          ))
                        }
                      </Table>
                    </ScrollView>
                </View>
              </ScrollView>
            </View>*/}
            {this.state.pastTransactionDetail && this.state.pastTransactionDetail.property && (this.state.pastTransactionDetail.totalpages > 0) && (
              <View style={styles.contentSection}>
                <View style={{flex: 1}}>
                  <Past_TransactionDetail
                      transactionDetail = {this.state.pastTransactionDetail}
                      tagged = {this.tagged}
                      compStyles = {styles}
                  />
                </View>
              </View>
            )}
              {/* <ScrollView horizontal={true}>
                <View style={styles.tableContainer}>
                  <Table borderStyle={{borderColor: 'transparent'}}>
                    <Row data={state.tableHead} style={styles.head}  widthArr={state.widthArr} textStyle={styles.headtext}/>
                    {
                      tableData.map((rowData, index) => (
                        <TableWrapper key={index} style={styles.row}>
                          {
                            rowData.map((cellData, cellIndex) => (
                              <Cell  key={cellIndex} data={cellData}
                              style={[ styles.widthrow, index%2 && {backgroundColor: '#F2F4F8', borderColor: 'transparent'}]}
                              textStyle={styles.text}/>
                            ))
                          }
                        </TableWrapper>
                      ))
                    }
                  </Table>
                </View>
              </ScrollView> */}
          </View>
        </ScrollView>
        {/* Bulk Actions */}
        <SafeAreaView >
          <View style={styles.bottomActions}>
            <TouchableOpacity onPress={this._onEdit} style={styles.bottomActionItem}>
              <Icon
                name='edit-2'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>
            {this.state.status == true && <TouchableOpacity onPress={() => this._rePost(this.state.listingDetails.listing_type, viewData.lid)} style={styles.bottomActionItem}>
              <Icon
                name='corner-left-up'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>}
            {(this.state.listingDetails.prop_status == 'expired' || this.state.listingDetails.prop_status == 'draft') && <TouchableOpacity style={styles.bottomActionItem} onPress={() => this.setState({isPrompt : true})}>
              <Icon
                name='trash-2'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>}
            {(this.state.expired != true && this.state.listingDetails.prop_status == 'active') && <TouchableOpacity style={styles.bottomActionItem} onPress={MultiTapHandler(() => this._shareItem(), true ? 1000 : 0)}>
              <Icon
                name='share-2'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>}
            {(this.state.expired != true && this.state.listingDetails.prop_status == 'active') && <TouchableOpacity onPress={() => this._expire(this.state.listingDetails.listing_type, viewData.lid, false)} style={styles.bottomActionItem}>
              <Icon
                name='archive'
                size={24}
                type='feather'
                color='#10182d'
              />
            </TouchableOpacity>}
          </View>
          {/* Overlay */}
          <Overlay
              isVisible={this.state.isVisible}
              overlayStyle={[styles.overlayStyle, this.state.isError ? styles.bgDanger : styles.bgSuccess]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="92%"
              height="auto"
              onBackdropPress={() => this.setState({ isVisible : false, isError: false, apiMessage: ''})}
            >
              <View style={styles.overlayInner}>
                <Icon
                  name='info'
                  type='material'
                  color='#fff'
                  size={30}
                />
                <Text allowFontScaling={false} style={styles.overlayText}>{this.state.apiMessage}</Text>
              </View>
            </Overlay>

            {/* Overlay */}
            <Overlay
              isVisible={this.state.featuredShow}
              overlayStyle={[styles.overlayFeaturedStyle]}
              containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
              width="75%"
              height="auto"
              onBackdropPress={() => console.log('backdrop')}
            >
              <View>
                <View style={[styles.overlayPopContent, {borderBottomWidth: 1, borderBottomColor: 'rgba(0, 0, 0, 0.2)'}]}>
                  <Text allowFontScaling={false} style={styles.overlayPopText}>Add to Featured</Text>
                </View>
                <View style={[styles.overlayPopContent, {paddingHorizontal: 10}]}>
                  <View style={[styles.fieldWrap, styles.fieldWrapMulti]}>
                    <View style={styles.fieldWrapThreeWay}>
                      <CustomSelect
                        value={this.state.feature_day}
                        label={'Day'}
                        placeholder='Please Select Day'
                        data={aucDay}
                        style={styles.viewAllWrap}
                        baseStyle={styles.viewAllBase}
                        fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                        onChangeText={(day) => this.setState({ feature_day :  day }, () => this._checkForm())}
                      />
                    </View>
                    <View style={styles.fieldWrapThreeWay}>
                      <CustomSelect
                        value={this.state.feature_month}
                        label={'Month'}
                        placeholder='Please Select Month'
                        data={aucMonth}
                        style={styles.viewAllWrap}
                        baseStyle={styles.viewAllBase}
                        fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                        onChangeText={(month) => this.setState({ feature_month :  month }, () => this._checkForm())}
                      />
                    </View>
                    <View style={styles.fieldWrapThreeWay}>
                      <CustomSelect
                        value={this.state.feature_year}
                        label={'Year'}
                        placeholder='Please Select Year'
                        data={aucYear}
                        style={styles.viewAllWrap}
                        baseStyle={styles.viewAllBase}
                        fieldWrapStyle={{ width: 'auto', flex: 1, maxHeight: 53 }}
                        onChangeText={(year) => this.setState({ feature_year :  year }, () => this._checkForm())}
                      />
                    </View>
                  </View>
                  {this.state.featured_error &&<View style={styles.errorContainer}>
                    <Text allowFontScaling={false} style={styles.errorMsg}>Past date is not allowed</Text>
                  </View>}
                </View>
                <View style={styles.overlayPopButtons} >
                  <TouchableOpacity style={[styles.OverlayPopBtn, {borderRightColor: 'rgba(0,0,0,0.2)', borderRightWidth: 1}]} onPress={()=> this.setState({ featuredShow : false })} >
                    <Text allowFontScaling={false} style={[styles.OverlayPopBtnText, {color: '#ff3c3c'}]}>Cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.OverlayPopBtn} onPress={()=> this._makeFeatured()}>
                    <Text allowFontScaling={false} style={styles.OverlayPopBtnText}>OK</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Overlay>

            {/* Overlay */}
          <Overlay
            isVisible={this.state.isPrompt}
            overlayStyle={[styles.overlayFeaturedStyle]}
            containerStyle={[styles.overlayContainer, {backgroundColor : 'rgba(0,0,0,0.4)'}]}
            width="75%"
            height="auto"
            onBackdropPress={() => console.log('backdrop')}
          >
            <View>
              <View style={styles.overlayPopContent}>
                <Text allowFontScaling={false} style={styles.overlayPopText}>Do you wanted to delete the property?</Text>
              </View>
              <View style={styles.overlayPopButtons}>
                <TouchableOpacity style={[styles.OverlayPopBtn, {borderRightColor: 'rgba(0,0,0,0.2)', borderRightWidth: 1}]} onPress={() => {
                  this.setState({
                    isPrompt: false
                  })
                }}>
                  <Text allowFontScaling={false} style={[styles.OverlayPopBtnText, {color: '#ff3c3c'}]}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.OverlayPopBtn} onPress={() => { 
                    this.setState({
                      isPrompt: false,
                    })
                    this._deleteItem(this.state.listingDetails.listing_type, viewData.lid)
                  }
                }>
                  <Text allowFontScaling={false} style={styles.OverlayPopBtnText}>OK</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Overlay>
        </SafeAreaView>
         {
          this.state.isLoading && (
            <Loading />
          )
        }
      </View>
    );
  }
}
