/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import { CheckBox, Header, Input, Icon, Overlay, ListItem } from 'react-native-elements';
import styles from './SearchTagStyles';
import { GetData } from '../../services/api'
//import CustomSelect from '../../../components/CustomSelect';
let stop = false;
class CenterNavComponent extends Component{
  constructor(props) {
    super(props);
    this.state ={
      keyword: props.value? props.value : ''
    }
    this._textChange = this._textChange.bind(this);
  }

  _textChange = (text) => {
    this.setState({
      keyword:text
    })
    this.props.onTextChange(text)
  }

  render() { 
    return (
      <View style={styles.navCenter}>
          <Input
            allowFontScaling={false}
            placeholder='E.g. Epic Residence' 
            rightIconContainerStyle={{ position: 'absolute', right: 10, top: 0 }}
            rightIcon={
              <Icon
                name='ios-close-circle'
                size={22}
                type='ionicon'
                color='#9E9E9E'
                onPress={() => { 
                    this._textChange('')
                    stop = true;
                  }
                }
              />
            }
            inputContainerStyle={{borderBottomWidth: 0}} 
            containerStyle={styles.inputContainer} 
            inputStyle={styles.inputField}
            value={this.state.keyword}
            onChangeText={(search) => this._textChange(search)}
            onSubmitEditing={() => console.log('editing end')}
          />
        </View>
    )
  }  
}

class SearchTag extends Component{
  constructor(props) {
    super(props);
    this.data = props.navigation.state.params.data? props.navigation.state.params.data : {};
    this.keyword = {'33': 'r', '36': 'l','60': 'c','70': 'i'};
    this.state = {
      viewAlltext: 'View all',
      showCustody: true,
      items: [],
      isLoading: false
    };
   this._onSearchBack = this._onSearchBack.bind(this);
   this._updateSearch = this._updateSearch.bind(this);
   this.getDataSuccess = this.getDataSuccess.bind(this)
   this.getDataFail = this.getDataFail.bind(this)
   this.handleInput = this.handleInput.bind(this)
   this.handlePropertySearchResult = this.handlePropertySearchResult.bind(this)
   stop = false;
  }

  _updateSearch = search => {
    this.search = search;
    if(this.search.length == 0){
      this.setState({
        items : []
      })
      return;
    }
    this.setState({isLoading : true});
    stop = false;
    GetData('https://sonic.edgeprop.my/api/v1/global/assetsuggest/'+this.keyword[this.data.propertyType]+'?query='+search)
        .then(data => this.getDataSuccess(data, search))
        .catch(error => this.getDataFail(error));
  }

  getDataSuccess(data, search){
    let items = [];
    let property_type_group = this.keyword[this.data.propertyType];
    Object.entries(data).forEach(([key, value]) => {
      let dropState = value.state_s_lower ? ", "+value.state_s_lower : ""; 
      let dropArea = value.area_s_lower ? ", "+value.area_s_lower : "";
      let placeName = value.name_s_lower+dropArea+dropState;
      items.push({
        id: key,
        name: placeName,//value.name_s_lower,
        project_name: value.name_s_lower,
        alias: value.name_s_lower,
        asset_id: value.asset_id_i,
        state: value.state_s_lower,
        district: value.area_s_lower,
        lat: value.lat_d,
        lon: value.lon_d,
        tenure: value.tenure_s,
        state_id: value.state_id_i,
        district_id: value.district_id_i,
        postal_code: value.postal_code_i,
        property_type_group: property_type_group,
        street_name: value.street_name_s_lower
      });
    });
    this.setState({
      items: items,
      isLoading : false
    });
  }

  getDataFail(error){
    
  }

  _onSearchBack() {
    this.props.navigation.goBack();
  }

  updateSearch = search => {
    this.setState({ search });
  };

  handleInput(item) {
    

    if(item == null){
      this.handlePropertySearchResult({reset: true, pid: null});
      return;
    }

    if (!item || typeof item.asset_id == "undefined" || !item.asset_id) return;

    if (item.asset_id == 1100) {
      this.handlePropertySearchResult({others: true, pid: null});
      return;
    }

    this.handlePropertySearchResult({...item,pid: item.asset_id});
  }

  handlePropertySearchResult(data) {
    let response = {
      asset: data,
      lat: data.lat_d,
      lng: data.lon_d,
      state: data.state_id,
      area: data.district_id,
      street: data.street_name,
      postal: data.postal_code,
      tenure: data.tenure,
      buildingName: data.name,
      searchOthers: typeof data.others != "undefined" ? true : false,
      searchKey: this.search
    };
    if(data.hasOwnProperty('pid')){
      response.pid = data.pid;
    }
    if(data.reset){
      response.attrCheck = true;
    }else{
      response.attrCheck = false;
    }
    if(data.others){
      response.buildingName = 'Others';
      response.searchOthers = true;
      response.othersAsset = this.search;
    }else{
      response.searchOthers = false;
      response.othersAsset = '';
    }
    
    this.props.navigation.goBack();
    if(this.data.selectHandler){
      this.data.selectHandler(response)
    }
    
  }

  render() {
    let data = [{
      label: 'View All',
      value: true
    }, {
      label: 'Hide All',
      value: false
    }];
    const { search } = this.state;

    const LeftNavComponent = () => {
      return (
        <View style={styles.navLeft} >
          <TouchableOpacity onPress={()=> this._onSearchBack()} style={styles.navigation}>
            <Icon
              name='arrow-left'
              size={26}
              type='feather'
              color='#10182d'
            />
          </TouchableOpacity>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.SafeAreaView}>
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            containerStyle={styles.navContainer}
            centerContainerStyle={{paddingRight: 0 }}
            placement="left"
            centerContainerStyle={{paddingRight: 0, paddingLeft: 15}}
            leftComponent={<LeftNavComponent />}
            centerComponent={<CenterNavComponent onTextChange={this._updateSearch} value={this.search}/>}
          />
          <View style={styles.innerContainer}>
            <View style={styles.noneStyle}>
              <ListItem
                title={'None'}
                leftIcon={<Icon
                  name='frown'
                  size={20}
                  type='feather'
                  color='#646975'
                />}
                titleStyle={styles.textSm}
                titleProps={{numberOfLines: 1, ellipsizeMode: 'tail'}}
                containerStyle={{paddingVertical: 10}}
                bottomDivider
                chevron={{color: '#646975'}}
                onPress={(item) => this.props.navigation.goBack()}
              />
            </View>
            {
              this.state.isLoading && (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator
                    animating size='large'
                    style={{
                      height: 80
                    }}
                    color={'#3670e5'}
                  />
                </View>
              )
            }
            {!this.state.isLoading && (<ScrollView keyboardShouldPersistTaps={'handled'}>
              <View style={styles.searchMainWrapper}>
                <View>
                  {
                    this.state.items.map((l, i) => (
                      <ListItem
                        key={i}
                        leftIcon={
                          <Image
                            style={{width: 24, height: 24}}
                            source={require('../../../assets/images/department.png')}/>}
                        title={l.name}
                        subtitle={l.subtitle}
                        titleStyle={styles.textMd}
                        subtitleStyle={styles.textSm}
                        titleProps={{numberOfLines: 1, ellipsizeMode: 'tail'}}
                        subtitleProps={{numberOfLines: 1, ellipsizeMode: 'tail'}}
                        containerStyle={{paddingVertical: 10}}
                        bottomDivider
                        chevron={{color: '#646975'}}
                        onPress={() => this.handleInput(this.state.items[i])}
                      />
                    ))
                  }
                </View>
              </View>
            </ScrollView>)}
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

export default SearchTag;