import {StyleSheet, Dimensions, Platform} from 'react-native';
const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
    navTitle: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-Regular',
        color: '#FFFFFF',
    },
    navContainer: {
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 15,
        paddingBottom: 15,
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5,
        backgroundColor: '#ffffff',
        position: 'relative',
        zIndex: 9
    },
    navLeft: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 35,
    },
    navRight: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    navIconRight: {
        paddingHorizontal: 12,
        backgroundColor: '#3670e5',
        borderRadius: 4,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
    navCenter: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    navCenterInner: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    inputContainer: {
        borderBottomWidth: 0,
        paddingLeft: 0,
        paddingRight: 0,
        margin: 0,
    },
    inputField: {
        flex: 1,
        height: 40,
        paddingHorizontal: 20,
        paddingVertical: 13,
        backgroundColor: '#e6ecf2',
        borderRadius: 50,
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
    },
    // Navigation style End
    container: {
        flex: 1,
        width: '100%',
    },
    innerContainer: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    SafeAreaView: {
        flex: 1,
        width: '100%'
    },  
    contentSection: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(0, 0, 0, 0.2)',
    },
    searchMainWrapper: {
        flex: 1
    },
    textMd: {
        color: '#10182d',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold'
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#646975',
        fontWeight: 'normal'
    },
    propSearchItemText: {
        color: '#3670e5',
        fontFamily: 'OpenSans-SemiBold',
        width: '100%',
        textAlign: 'center'
    },
    CheckBoxWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '50%',
    },
    checkBox: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        padding: 0,
        margin: 0,
        marginLeft: -2,
        flex: 1
    },
    noneStyle: {
        paddingTop: 5,
        borderBottomWidth: 10,
        borderBottomColor: '#f5f5f5'
    },
    commonLabel: {
        color: '#8991a4',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
        paddingBottom: 10
    },
    overlayStyle: {
        position: 'absolute',
        bottom: 15,
        left: 'auto',
        padding: 0,
        backgroundColor: 'transparent',
        shadowColor: 'rgba(0, 0, 0, 0)',
    },
    overlayContainer: {
        backgroundColor: 'rgba(255, 255, 255, .15)',
    },
    overlayInner: {
        width: '100%',
        height: 'auto',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        shadowOffset:{width: 0,  height: 5,},
        shadowColor: 'rgba(0, 0, 0, 0.15)',
        shadowOpacity: 0.6,
        elevation: 5
    },
    overlayText: {
        color: '#fff',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-Regular',
        paddingLeft: 10,
        flex: 1,
        lineHeight: width * 0.035,
    },
    bgDanger: {
        backgroundColor: '#f44336'
    },
    bgWarning: {
        backgroundColor: '#ffc107'
    },
    bgSuccess: {
        backgroundColor: '#00c853'
    },
    bgInfo: {
        backgroundColor: '#3670e5'
    }
});