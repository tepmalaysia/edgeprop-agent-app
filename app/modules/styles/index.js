import {StyleSheet, Platform, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heading: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    drawerWrapper: {
        flex: 1
    },
    drawerContainer: {
        height: '100%'
    },
    drawerMain: {
        padding: 30,
        paddingBottom: 20,
        backgroundColor: '#1f2636',
    },
    avatar: {
        borderWidth: 1,
        borderColor: '#fff'
    },
    textSm: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#10182d',
        lineHeight: width * 0.04,
        fontWeight: 'normal'
    },
    hTwo: {
        fontSize: width * 0.045,
        fontFamily: 'OpenSans-SemiBold',
        color: '#FFF',
        lineHeight: width * 0.08,
        marginVertical: 5
    },
    plan: {
        flexDirection: 'row',
        position: 'relative'
    },
    premiumImage: {
        position: 'absolute',
        top: 0,
        left: -30
    },
    premiumPlan: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        color: '#ffb400'
    },
    expirePlan: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#a1adca',
        lineHeight: width * 0.04,
    },
    stats: {
        marginTop: 20,
        flexDirection: 'row'
    },
    statItem: {
        width: '50%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    statRight: {
        paddingLeft: 10
    },
    statHead: {
        fontSize: width * 0.04,
        fontFamily: 'OpenSans-SemiBold',
        color: '#33d0b1'
    },
    statSub: {
        fontSize: width * 0.028,
        fontFamily: 'OpenSans-Regular',
        color: '#c7cee0'
    },
    drawerSub: {
        backgroundColor: '#fff',
        flex: 1,
    },
    navContainer: {
        flex: 1,
        height: '100%',
        justifyContent: 'space-between'
    },
    navWrapper: {
        paddingVertical: 10,
        borderTopColor: 'rgba(0,0,0,0.2)', 
        borderTopWidth: 0.5
    },  
    menuItem: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    menuText: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        color: '#10182d',
        flex: 1
    },
    menuLabel: {
        fontSize: width * 0.032,
        fontFamily: 'OpenSans-SemiBold',
        color: '#3670e5'
    },
    commonLabel: {
        color: '#8991a4',
        fontSize: width * 0.03,
        fontFamily: 'OpenSans-SemiBold',
        paddingBottom: 10,
        paddingHorizontal: 20
    },
});