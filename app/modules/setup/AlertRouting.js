import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
import LoginLanding from '../screens/Login_Landing/LoginLanding'
//import EnquiryScreen from '../screens/EnquiryList/EnquiryListScreen';
import EnquiryAlertScreen from '../screens/EnquiryAlert/EnquiryAlertScreen';
//import EnquiryDetailsScreen from '../screens/EnquiryDetail/EnquiryDetail';

const EnquiryNavigator = createSwitchNavigator(
  {
    EnquiryAlert : {screen: EnquiryAlertScreen},
    LoginLanding : {screen: LoginLanding},
    //Enquiry : {screen: EnquiryScreen},
    //EnquiryDetails : {screen: EnquiryDetailsScreen},
  }
);

export default EnquiryNavigator;