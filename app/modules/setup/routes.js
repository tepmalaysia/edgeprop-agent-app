import React, { Component } from 'react';
import {
    SafeAreaView,
    Dimensions
} from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon, Badge } from 'react-native-elements';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator, DrawerActions } from 'react-navigation-drawer';
import { createMaterialTopTabNavigator, MaterialTopTabBar } from 'react-navigation-tabs';
import {View,Text,StyleSheet,Platform,TouchableOpacity,Image,StatusBar} from 'react-native';
import VersionNumber from 'react-native-version-number';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
//import { RealmProvider } from 'react-native-realm';

import Home from '../screens/Home/index';
import About from '../screens/About/index';
import Contact from '../screens/Contact/index';
import DrawerScreen from '../screens/Common/DrawerScreen';
import ForgotPassword from '../screens/Forgot_Password/ForgotPassword';
import LoginLanding from '../screens/Login_Landing/LoginLanding'
import Listing from '../screens/Listing/Listing';
import ListingEdit from '../screens/ListingEdit/ListingEdit';
import ListingCreate from '../screens/ListingCreate/ListingCreate';
import SearchOption from '../screens/SearchOption/SearchOption';
import SearchTag from '../screens/SearchTag/SearchTag';
import UploadImage from '../screens/UploadImage/UploadImage';
import ImageViewer from '../screens/ImageViewer/ImageViewer';
import Loading from '../screens/Loading/Loading';
import Enquiry from '../screens/Enquiry/Enquiry'
import EnquiryDetail from '../screens/EnquiryDetail/EnquiryDetail'
import EnquiryByListingDetail from '../screens/Enquiry/EnquiryByListingDetail'
import EnquiryAlert from '../screens/EnquiryAlert/EnquiryAlertScreen'
import Info from '../screens/Info/Info'

import Realm from 'realm';

const { width, height } = Dimensions.get('window');
const SafeAreaMaterialTopTabBar = ({ ...props }) => (
  <SafeAreaView>
    <MaterialTopTabBar {...props} />
  </SafeAreaView>
);

const Tabs = createMaterialTopTabNavigator({
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.params? (navigation.state.params.showTab === false? false : true): false,
        tabBarIcon: ({tintColor}) => (
          <View style={{position: 'relative'}}>
            <Icon
              name='list'
              type='feather'
              size={24}
              color={tintColor}
            />
          </View>
        )
      })
    },
    About: {
      screen: About,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: true,
        tabBarIcon: ({tintColor}) => (
          <View style={{position: 'relative'}}>
            {/*<Badge
              value="3" 
              badgeStyle={{
                backgroundColor: '#f44336'
              }}
              containerStyle={{ position: 'absolute', top: -4, right: -4, zIndex: 999 }}
            />*/}
            <Icon
              name='bell'
              type='feather'
              size={24}
              color={tintColor}
            />
          </View>
        )
      })
    },
    /*Create: {
      screen: ListingCreate,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: false,
        tabBarIcon: ({tintColor}) => (
          <View style={{position: 'relative'}}>
            <Icon
              name='add-circle'
              type='ionicons'
              size={24}
              color={tintColor}
            />
          </View>
        )
      })
    },*/
    Enquiry: {
      screen: Enquiry,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: true,
        tabBarIcon: ({tintColor}) => (
          <View style={{position: 'relative'}}>
            <Icon
              name='message-square'
              type='feather'
              size={24}
              color={tintColor}
            />
          </View>
        )
      })
    },
    Profile: {
      screen: Contact,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: true,
        tabBarIcon: ({tintColor}) => (
          <View style={{position: 'relative'}}>
            <Icon
              name='user'
              type='feather'
              size={24}
              color={tintColor}
            />
          </View>
        )
      })
    },
},{
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      activeTintColor: '#3670e5',
      inactiveTintColor: '#10182d',
      style: {
          backgroundColor: '#fff',
          borderTopWidth: 0.5,
          borderTopColor: 'rgba(0, 0, 0, 0.2)',
          elevation: 5,
          height: 50,
      },
      indicatorStyle: {
          width: 40,
          height: 40,
          bottom: 5,
          left: width*0.078,
          borderRadius: 30,
          backgroundColor: '#fff',
      },
    },
    tabBarPosition: "bottom",
    lazy: true,
    swipeEnabled: false,
    navigationOptions: ({ navigation }) => ({
        title: 'TabNavigation',  // Title to appear in status bar
    }),
    tabBarComponent: props => (<SafeAreaMaterialTopTabBar {...props} />),
});

const DrawerNavigator = createDrawerNavigator({
    About:{
        screen: Tabs,
        navigationOptions: {
          header: null
        }
    }
},{
    initialRouteName: 'About',
    contentComponent: DrawerScreen,
    //drawerWidth: 300,
    navigationOptions: ({ navigation }) => ({
        header: null
    })
});

const StackNavigator = createStackNavigator({
    
    //important: key and screen name (i.e. DrawerNavigator) should be same while using the drawer navigator inside stack navigator.
    Loading:{
      screen: Loading,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    DrawerNavigator:{
        screen: DrawerNavigator,
        navigationOptions: {
          gesturesEnabled: false,
        }
    },
    Listing:{ 
      screen: Listing,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    SearchOption: { 
      screen: SearchOption,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    SearchTag: { 
      screen: SearchTag,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    EnquiryDetail: { 
      screen: EnquiryDetail,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    EnquiryByListingDetail: { 
      screen: EnquiryByListingDetail,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    EnquiryAlert: { 
      screen: EnquiryAlert,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    ListingEdit: { 
      screen: ListingEdit,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    UploadImage: { 
      screen: UploadImage,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    ListingCreate: { 
      screen: ListingCreate,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    Info:{ 
      screen: Info,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    ImageViewer: { 
      screen: ImageViewer,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
},{
     navigationOptions: ({ navigation }) => ({
        header: null
    })
});

const AlertStackNavigator = createStackNavigator({
    
    //important: key and screen name (i.e. DrawerNavigator) should be same while using the drawer navigator inside stack navigator.
    Loading:{
      screen: Loading,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    DrawerNavigator:{
        screen: DrawerNavigator,
        navigationOptions: {
          gesturesEnabled: false,
        }
    },
    Listing:{ 
      screen: Listing,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    SearchOption: { 
      screen: SearchOption,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    SearchTag: { 
      screen: SearchTag,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    EnquiryDetail: { 
      screen: EnquiryDetail,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    EnquiryByListingDetail: { 
      screen: EnquiryByListingDetail,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    EnquiryAlert: { 
      screen: EnquiryAlert,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    ListingEdit: { 
      screen: ListingEdit,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    UploadImage: { 
      screen: UploadImage,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    ListingCreate: { 
      screen: ListingCreate,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    Info:{ 
      screen: Info,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
    ImageViewer: { 
      screen: ImageViewer,
      navigationOptions: {
        header: null,
        headerLeft: null
      }
    },
},{
     navigationOptions: ({ navigation }) => ({
        header: null
    }),
    initialRouteName: 'EnquiryAlert'
});

const SignedOutNavigator = createStackNavigator({
  'LoginLanding': {
    screen: LoginLanding,
    navigationOptions: {
      header: null,
      headerLeft: null
    }
  },
  'ForgotPassword': {
    screen: ForgotPassword,
    navigationOptions: {
      header: null,
      headerLeft: null
    }
  }
});

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator({
      SignedIn: StackNavigator,
      SignedOut: SignedOutNavigator,
    },
    {
      initialRouteName: signedIn? 'SignedIn' : 'SignedOut',
    });
};

export const createAlertRootNavigator = (signedIn = false) => {
  return createSwitchNavigator({
      SignedIn: AlertStackNavigator,
      SignedOut: SignedOutNavigator,
    },
    {
      initialRouteName: signedIn? 'SignedIn' : 'SignedOut',
    });
};

//const StateNavigator = createRootNavigator(true);

//const Navigator = createAppContainer(StateNavigator)

//export default Navigator;