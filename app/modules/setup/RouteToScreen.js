import Home from '../screens/Home/index';
import About from '../screens/About/index';
import Contact from '../screens/Contact/index';
import ForgotPassword from '../screens/Forgot_Password/ForgotPassword';
import LoginLanding from '../screens/Login_Landing/LoginLanding';

export default {
  Home: Home,
  About: About,
  Contact: Contact,
  ForgotPassword: ForgotPassword,
  LoginLanding: LoginLanding
}