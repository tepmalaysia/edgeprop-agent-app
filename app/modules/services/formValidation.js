import StateOptions from '../../assets/json/StateOptions.json'
import moment from 'moment';

export function ListingFormValidations(data, action){
  let response = {
    listing_type : { error : false }
  };
  //console.log(data.listing_type)
  if(data.listing_type){
    let listing_type = {}
    listing_type.error = true;
    listing_type.validate = {};
    listing_type.validate.required = true;
    response.listing_type = listing_type;
  }
  //console.log('response', response);
  return response;
}

export function getDataForAdd(form){
  
  let assets = {
    asset: null,
    asset_loc_1: null,
    asset_loc_2: null,
    loc_1: null,
    loc_2: null,
  };
  let multiAgent = null;
  let cover_Image = [];
  let broucher_Pdf = null;
  let is_duplicate = false;
  
 
  if(form.multiAgent.length > 0){
    multiAgent = form.multiAgent.join()  
  }
  
  if(form.asset != null){
    assets.asset = (form.searchOthers === true)? 1100 : 
      (form.pid!= null && form.pid > 0) ? parseInt(form.pid) : null;
    assets.asset_loc_1 = form.asset.lon;
    assets.asset_loc_2 = form.asset.lat;
    assets.loc_1 = form.asset.lon;
    assets.loc_2 = form.asset.lat;
  }

  let districtDatas = form.property_state ? StateOptions.find(o => o.value == form.property_state) : [];
  let district = '';
  if(districtDatas && districtDatas.drop && districtDatas.drop.length > 0 && form.property_district){
    district = districtDatas.drop.find(o => o.value == form.property_district).label;
  }

  return {
    /*
      default
    */
    log: "",
    type: "property",
    language: "und",
    field_country: "1",
    "field_geo_lat_lng[lat_sin]": (form.lat_sin)? form.lat_sin : 0,
    "field_geo_lat_lng[lat_cos]": (form.lat_cos)? form.lat_cos : 0,
    "field_geo_lat_lng[lng_rad]": (form.lng_rad)? form.lng_rad : 0,
    field_prop_cobroke: "0",
    field_prop_priority_end_date: "",
    field_prop_priority_start_date: "",
    field_prop_quality: "83",
    
    field_exclusive: form.exclusive ? "1" : "0",
    title: form.property_name,
    field_prop_asset:assets.asset?assets.asset : null,
    field_district: form.property_district,
    district_id: form.property_district,
    district: district,
    "field_geo_lat_lng[lat]": form.lat?form.lat: null,
    "field_geo_lat_lng[lng]": form.lng?form.lng: null,
    field_vpex_developer_id: form.developer ? form.developer : null,
    floorPlan: form.floorPlan? form.floorPlan : [],
    field_youtube_url: form.youtubeUrl ? form.youtubeUrl : [],
    field_associated_agents: multiAgent ? multiAgent : null,
    field_completion_year: form.yearCompletion ? form.yearCompletion : null,
    field_class: form.classes ? form.classes : null,
    field_cover_thumbnail: form.coverThumbnail ? (form.coverThumbnail.length > 0 ? form.coverThumbnail[0]:null) : null,
    field_prop_area_unit: form.property_land_area_unit,
    field_prop_asking_price: form.property_asking_price,
    field_prop_bathrooms: form.property_bathrooms,
    field_prop_bedrooms: form.property_bedrooms,
    field_prop_features: form.features,
    field_prop_fixtures_fittings: form.fixtures,
    field_prop_floor_location: form.floor,
    field_prop_furnished: form.property_furnished,
    field_prop_info: form.property_info,
    field_prop_inout_space: form.space,
    field_prop_land_area: form.property_land_area,
    field_prop_lease_term: form.tenure,
    field_prop_listing_type: form.listing_type,
    field_prop_maintenance_fee: form.maintenanceFee,
    field_prop_additional_amenities: form.property_aminities,
    field_prop_postcode: form.property_postcode,
    field_prop_price_pu: form.property_land_area? (!isNaN(form.property_asking_price / form.property_land_area)? (form.property_asking_price / form.property_land_area).toFixed(2) : 0) : 0,
    field_prop_street: form.property_street,
    field_prop_tnc_1: "1",
    field_prop_tnc_2: "1",
    field_state: form.property_state ? parseInt(form.property_state) : null,
    state_id: form.property_state ? parseInt(form.property_state) : null,
    state: form.property_state == 0 ? '' : StateOptions.find(o => o.value == form.property_state).label,
    field_property_type: form.property_type
      ? parseInt(form.property_type)
      : null,
    property_sub_type: form.sub_property_type
      ? parseInt(form.sub_property_type)
      : null,
    field_prop_unit: form.property_unit,
    field_prop_images_watermark: form.watermark ? "1" : "0",
    field_prop_asking_price_type: form.property_price_type,
    field_prop_built_up: (form.property_built_area || form.property_built_area == 0) ? form.property_built_area : null,
    field_prop_built_up_unit: form.property_built_area_unit? form.property_built_area_unit : '',
    field_prop_built_up_price_pu:
      form.property_built_area? (!isNaN(form.property_asking_price / form.property_built_area)? (form.property_asking_price / form.property_built_area).toFixed(2) : null) : null,
    "field_prop_auction_date[day]": form.property_auction_day,
    "field_prop_auction_date[month]" : form.property_auction_month,
    "field_prop_auction_date[year]" : form.property_auction_year,
    field_prop_auction_ref: form.property_auction_ref,
    field_prop_coagency:  form.property_coAgency ? form.property_coAgency : '',
    field_prop_images: form.property_images,
    asset: form.asset,
    "asset_loc[0]": assets.asset_loc_1,
    "asset_loc[1]": assets.asset_loc_2,
    "loc[0]": assets.loc_1,
    "loc[1]": assets.loc_2,
    field_others_asset: form.othersAsset,
    old_field_prop_listing_type: form.oldListingType,
    field_gallery_latitude: form.galleryLatitude,
    field_gallery_longitude: form.galleryLongitude,
    field_gallery_address: form.galleryAddress,
    field_listing_slogan: 'test'
  };
}

export function assginData(data) {
  let response = {};
  let obj = {};
  let broucher_data = [];
  let thumbArray = []
  let thumbnail = data.field_cover_thumbnail ? ((data.field_cover_thumbnail.und)? (data.field_cover_thumbnail.und[0]? data.field_cover_thumbnail.und[0].value : null) : null) : null;
  if(thumbnail != null){
    thumbArray.push(thumbnail);
  }
  let cover_details = data.field_cover_image ? ((data.field_cover_image.und)? (data.field_cover_image.und[0]? data.field_cover_image.und[0] : {}) : {}) : {};
  let coverImg_data = (Object.keys(cover_details).length>0)?[{
    s3_image_details:cover_details
  }] : [];
  let broucher_details = data.brochure ? data.brochure : null;
  obj['cdn_uri'] = broucher_details;
  broucher_data.push(obj);
  let associalteAgentsString = data.field_associated_agents ? ((data.field_associated_agents.und)? (data.field_associated_agents.und[0]? ((data.field_associated_agents.und[0].value)? data.field_associated_agents.und[0].value :[]) : []) : []): [];

  let associalteAgents = (associalteAgentsString.length > 0) ? associalteAgentsString.split(',') : [];
  response.privacy = data.field_prop_tnc_1 ? ((data.field_prop_tnc_1.und)? (data.field_prop_tnc_1.und[0]? ((data.field_prop_tnc_1.und[0].value == '1')? true :false) : false) : false): false;

  response.agreement = data.field_prop_tnc_2 ? ((data.field_prop_tnc_2.und)? (data.field_prop_tnc_2.und[0]? ((data.field_prop_tnc_2.und[0].value == '1')? true :false) : false) : false): false;

  response.listing_type = data.field_prop_listing_type ? ((data.field_prop_listing_type.und)? (data.field_prop_listing_type.und[0]? data.field_prop_listing_type.und[0].value : '') : '') : '';

  response.oldListingType = data.field_prop_listing_type ? ((data.field_prop_listing_type.und)? (data.field_prop_listing_type.und[0]? data.field_prop_listing_type.und[0].value : '') : '') : '';

  response.property_type = data.field_property_type ? ((data.field_property_type.und)? (data.field_property_type.und[0]? data.field_property_type.und[0].target_id : '') : '') : '';

  response.sub_property_type = data.field_property_type ? ((data.field_property_type.und)? (data.field_property_type.und[1]? data.field_property_type.und[1].target_id : '') : '') : '';

  response.property_name = (data.title)? data.title : '';

  response.property_state = data.field_state ? ((data.field_state.und)? ((data.field_state.und[0]) ? data.field_state.und[0].target_id : '') : '') : '';

  response.property_district = data.field_district ? ((data.field_district.und)? ((data.field_district.und[0]) ? data.field_district.und[0].target_id: '') : '') : '';

  response.property_unit = data.field_prop_unit ? ((data.field_prop_unit.und)? (data.field_prop_unit.und[0]? data.field_prop_unit.und[0].value : '') : '') : '';

  response.property_street = data.field_prop_street ? ((data.field_prop_street.und)? (data.field_prop_street.und[0]? data.field_prop_street.und[0].value : '') : '') : '';

  response.property_postcode = data.field_prop_postcode ? ((data.field_prop_postcode.und)? (data.field_prop_postcode.und[0]? data.field_prop_postcode.und[0].value : '') : '') : '';

  response.property_price_type = data.field_prop_asking_price_type ? ((data.field_prop_asking_price_type.und)? (data.field_prop_asking_price_type.und[0]? data.field_prop_asking_price_type.und[0].value : null) : null) : null;

  response.property_asking_price = data.field_prop_asking_price ? ((data.field_prop_asking_price.und)? (data.field_prop_asking_price.und[0]? data.field_prop_asking_price.und[0].value : '') : '') : '';

  response.property_land_area = data.field_prop_land_area ? ((data.field_prop_land_area.und)? (data.field_prop_land_area.und[0]? data.field_prop_land_area.und[0].value : '') : '') : '';

  response.property_land_area_unit = data.field_prop_area_unit ? ((data.field_prop_area_unit.und)? (data.field_prop_area_unit.und[0]? data.field_prop_area_unit.und[0].value : '') : '') : '';

  response.property_built_area = data.field_prop_built_up ? ((data.field_prop_built_up.und)? (data.field_prop_built_up.und[0]? data.field_prop_built_up.und[0].value : '') : '') : '';

  response.property_built_area_unit = data.field_prop_built_up_unit ? ((data.field_prop_built_up_unit.und)? (data.field_prop_built_up_unit.und[0]? data.field_prop_built_up_unit.und[0].value : '') : '') : '';

  response.tenure = data.field_prop_lease_term ? ((data.field_prop_lease_term.und)? (data.field_prop_lease_term.und[0]? data.field_prop_lease_term.und[0].value : null) : null) : null;

  response.property_coAgency = data.field_prop_coagency ? ((data.field_prop_coagency.und)? (data.field_prop_coagency.und[0]? ((data.field_prop_coagency.und[0].value)? true : false) : false) :false) : false;

  response.exclusive = data.field_exclusive ? ((data.field_exclusive.und)? (data.field_exclusive.und[0]? ((data.field_exclusive.und[0].value == "1")? true : false) : false) :false) : false;

  response.developer = data.field_vpex_developer_id ? ((data.field_vpex_developer_id.und)? (data.field_vpex_developer_id.und[0]? data.field_vpex_developer_id.und[0].value : null) : null) : null;

  response.multiAgent = (associalteAgents.length > 0) ? associalteAgents : []

  response.listingSlogan = data.field_listing_slogan ? ((data.field_listing_slogan.und)? (data.field_listing_slogan.und[0]? data.field_listing_slogan.und[0].value : null) : null) : null;

  response.classes = data.field_class ? ((data.field_class.und)? (data.field_class.und[0]? data.field_class.und[0].value : null) : null) : null;

  response.coverImage = coverImg_data;

  response.broucher = broucher_data;

  response.broucherUrl = broucher_details;

  response.coverThumbnail = thumbArray;

  response.thumbnailUrl = thumbnail;

  response.yearCompletion = data.field_completion_year ? ((data.field_completion_year.und)? (data.field_completion_year.und[0]? data.field_completion_year.und[0].value : null) : null) : null;

  response.coverUrl = (coverImg_data.length > 0) ? coverImg_data[0].s3_image_details.cdn_uri : null;

  //response.priorityListing = data.field_prop_priority ? ((data.field_prop_priority.und)? ((data.field_prop_priority.und[0].value == '1')? true :false) : false) : false;

  response.property_auction_ref = data.field_prop_auction_ref ? ((data.field_prop_auction_ref.und)? (data.field_prop_auction_ref.und[0]? data.field_prop_auction_ref.und[0].value : '') : '') : '';

  response.isDuplicate = data.is_duplicate ? ((data.is_duplicate == "true") ? true : false) : false;

  response.galleryLatitude = data.field_gallery_latitude ? ((data.field_gallery_latitude.und)? (data.field_gallery_latitude.und[0]? data.field_gallery_latitude.und[0].value : null) : null) : null;

  response.galleryLongitude = data.field_gallery_longitude ? ((data.field_gallery_longitude.und)? (data.field_gallery_longitude.und[0]? data.field_gallery_longitude.und[0].value : null) : null) : null;

  response.galleryAddress = data.field_gallery_address ? ((data.field_gallery_address.und)? (data.field_gallery_address.und[0]? data.field_gallery_address .und[0].value : null) : null) : null;

  let date = (data.field_prop_auction_date)? ((data.field_prop_auction_date.und)? (data.field_prop_auction_date.und[0]? data.field_prop_auction_date.und[0].value : '') : '') : '';
  
  if(date){
    let dateCol = date.split(' ');
    let dateArray = dateCol[0].split('-');
    if(dateArray.length == 3){
      response.property_auction_day = ("0" + Number(dateArray[2])).slice(-2); 
      response.property_auction_month = ("0" + Number(dateArray[1])).slice(-2); 
      response.property_auction_year = dateArray[0];
    }
  }else{
    response.property_auction_day = moment().format('DD');
    response.property_auction_month = moment().format('MM');
    response.property_auction_year = parseInt(moment().format('YYYY'));
  }

  response.property_furnished = data.field_prop_furnished ? ((data.field_prop_furnished.und)? (data.field_prop_furnished.und[0]? data.field_prop_furnished.und[0].value : null) : null) : null;

  response.property_bedrooms = data.field_prop_bedrooms ? ((data.field_prop_bedrooms.und)? (data.field_prop_bedrooms.und[0]? data.field_prop_bedrooms.und[0].value : null) : null) : null;

  response.property_bathrooms = data.field_prop_bathrooms ? ((data.field_prop_bathrooms.und)? (data.field_prop_bathrooms.und[0]? data.field_prop_bathrooms.und[0].value : null) : null) : null;

  response.floor = data.field_prop_floor_location ? ((data.field_prop_floor_location.und)? (data.field_prop_floor_location.und[0]? data.field_prop_floor_location.und[0].value : null) : null) : null;

  response.features = [];
  if(data.field_prop_features && data.field_prop_features.und){
  response.features = getArray(data.field_prop_features.und);
  }

  response.fixtures = [];
  if(data.field_prop_fixtures_fittings && data.field_prop_fixtures_fittings.und){
  response.fixtures = getArray(data.field_prop_fixtures_fittings.und);
  }

  response.space = [];
  if(data.field_prop_inout_space && data.field_prop_inout_space.und){
  response.space = getArray(data.field_prop_inout_space.und);
  }

  response.maintenanceFee = (data.field_prop_maintenance_fee)? ((data.field_prop_maintenance_fee.und)? data.field_prop_maintenance_fee.und[0].value : '') : '';
  response.aminities = (data.field_prop_additional_amenities)? ((data.field_prop_additional_amenities.und)? data.field_prop_additional_amenities.und[0].value : null) : null;
  response.lat = data.field_geo_lat_lng ? ((data.field_geo_lat_lng.und)? (data.field_geo_lat_lng.und[0]? data.field_geo_lat_lng.und[0].lat : '') : '') : '';
  response.lng = data.field_geo_lat_lng ? ((data.field_geo_lat_lng.und)? (data.field_geo_lat_lng.und[0]? data.field_geo_lat_lng.und[0].lng : '') : '') : '';
  response.lat_sin = data.field_geo_lat_lng ? ((data.field_geo_lat_lng.und)? (data.field_geo_lat_lng.und[0]? data.field_geo_lat_lng.und[0].lat_sin : 0) : 0) : 0;
  response.lat_cos = data.field_geo_lat_lng ? ((data.field_geo_lat_lng.und)? (data.field_geo_lat_lng.und[0]? data.field_geo_lat_lng.und[0].lat_cos : 0) : 0) : 0;
  response.lng_rad = data.field_geo_lat_lng ? ((data.field_geo_lat_lng.und)? (data.field_geo_lat_lng.und[0]? data.field_geo_lat_lng.und[0].lng_rad : 0) : 0) : 0;

  response.property_info = data.field_prop_info ? ((data.field_prop_info.und)? (data.field_prop_info.und[0]? data.field_prop_info.und[0].value : '') : ''): '';


  let images = data.field_prop_images ? ((data.field_prop_images.und)? data.field_prop_images.und : []) : [];
  response.images = [];
  response.tempImages = [];

  let is_load = 1;

  if(images.length > 0){
    // for (let i in images) {
    for(let i=0;i<images.length;i++){
      let props = {
        s3_image_details : {}
      }
      if(images[i].uri){
        var name = images[i].filename.split('?')[0];
        var id = images[i].fid;
        images[i].is_upload = is_load;
        props.s3_image_details = images[i];
        props.name = name;
        props.index = i;
        props.thumb = images[i].cdn_uri;
        props.active = false;
        props.progress = "100.00";
        props.error = "";
        props.success = true;
        props.existing = true;
        props.list_uri =images[i].list_uri;
        props.gallery_uri = images[i].gallery_uri;
        props.uploaded = true,
        props.remove_hide = true,
        props.id = id,
        response.images.push(props);
      }
    }
    for(let i=0;i<images.length;i++){
      let propstemp = {
        s3_image_details : {}
      }
      if(images[i].uri){
        var name = images[i].filename.split('?')[0];
        var id = images[i].fid;
        images[i].is_upload = is_load;
        propstemp.s3_image_details = images[i];
        propstemp.name = name;
        propstemp.index = i;
        propstemp.thumb = images[i].cdn_uri;
        propstemp.active = false;
        propstemp.progress = "100.00";
        propstemp.error = "";
        propstemp.success = true;
        propstemp.existing = true;
        propstemp.list_uri =images[i].list_uri;
        propstemp.gallery_uri = images[i].gallery_uri;
        propstemp.uploaded = true,
        propstemp.remove_hide = true,
        propstemp.id = id,
        response.tempImages.push(propstemp);
      }
    }
  }

  response.floorPlan = data.floorPlan ? ((data.floorPlan)? data.floorPlan : []) : [];
  response.youtubeUrl = (data.field_youtube_url ? (data.field_youtube_url.und ? (data.field_youtube_url.und[0] ? data.field_youtube_url.und[0] :[]) :[]) :[]); 
  

  response.watermark = data.field_prop_images_watermark ? ((data.field_prop_images_watermark.und)? (data.field_prop_images_watermark.und[0]? ((data.field_prop_images_watermark.und[0].value == '1') ? true : false): false) : false) : false;
  response.watermark_original = data.field_prop_images_watermark ? ((data.field_prop_images_watermark.und)? (data.field_prop_images_watermark.und[0]? ((data.field_prop_images_watermark.und[0].value == '1') ? true : false): false) : false) : false;
  response.searchOthers = data.field_prop_asset ? ((data.field_prop_asset.und)? (data.field_prop_asset.und[0]? (data.field_prop_asset.und[0].target_id == '1100'? true : false) : false) : false) : false;
  response.asset = data.asset? data.asset : null;
  response.pid = data.field_prop_asset ? ((data.field_prop_asset.und)? (data.field_prop_asset.und[0]? data.field_prop_asset.und[0].target_id : '') : '') : '';
  response.othersAsset = data.field_others_asset ? ((data.field_others_asset.und)? (data.field_others_asset.und[0]? data.field_others_asset.und[0].value : '') : '') : '';
  response.uid = data.uid? data.uid : null;
  response.url = (data.url) ? data.url: getUrl(data);
  response.property_images = data.field_prop_images? (data.field_prop_images.und? data.field_prop_images.und : [])  : [];
      
  return response;
}

export function checkError(key, errorObj) {
  //console.log(key, errorObj[key]);
  let lengthLimit ={
    property_name: '50',
    property_asking_price: '11',
    property_land_area: '11',
    property_built_area: '11',
    property_postcode: '5'
  };
  let requiredError = ' is Required!';
  let decimalError  = ' should be decimal!';
  let lengthError  = ' maximum length should be ';
  let inValidError = ' is inValid!';
  let message = '';
  if(errorObj[key] && errorObj[key].error){
    if(errorObj[key].validate){
      if(errorObj[key].validate.required){
        message = errorObj[key].label+requiredError;
      }else if(errorObj[key].validate.decimal){
        message = errorObj[key].label+decimalError;
      }else if(errorObj[key].validate.maxLength){
        message = errorObj[key].label+lengthError+lengthLimit[key]+'!';
      }else if(errorObj[key].validate.inValid){
        message = errorObj[key].label;
      }
    }
  }
  return message;
}

function getArray(collection){
  let resp = [];
  Object.entries(collection).forEach(([key, value]) => {
      resp.push(value.value);
  });
  return resp;
}

function getUrl(data){
  let url ='';
  if(data.nid){
    url += data.nid;
  }else if(data.mid){
    url += data.field_prop_listing_type.und[0].value+'/'+data.mid;
  }
  return url;
}