const headers = {
                  'Accept' : 'application/json',
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          };



export const futch = (url, opts={}, onProgress) => {
    return new Promise( (res, rej)=>{
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers||{})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

export const PutData = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'PUT',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => {return response.json()})
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const PostData = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const GetData = (url) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'GET',
        headers: new Headers(headers)
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const SignOut = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const SignIn = (url, body) => {
    return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const ResetPassword = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const FetchAgent = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const FetchListing = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const FetchOne = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}


export const BulkUpdate = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'PUT',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const ListStatus = (url, body) => {
  return new Promise((resolve, reject)=>{
      fetch(url, {
        method: 'POST',
        headers: new Headers(headers),
          body: body
      })
      .then((response) => response.json())
      .then((responseText) => {
        resolve(responseText); 
      })
      .catch((error) => {
        reject(error);
      });
  });
}