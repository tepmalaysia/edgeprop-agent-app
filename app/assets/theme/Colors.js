const colors = {
  clear: 'rgba(0,0,0,0)',
  transparent: 'rgba(0,0,0,0)',
  snow: '#FFFFFF',
  silver: '#F3F3F3',
  steel: '#DEDEDE',
  gray: '#444',
  grayL: 'rgba(0, 0, 0, 0.38)',
  grayD: '#222',
  darkGray: '#4D5659',

  primary: '#ffad00',
  primaryM: '#ffd694',
  primaryL: '#fff7e9',
  primaryStatus: '#FFAA22',
  secondary: '#2dadcc',
  secondaryM: 'rgba(45,173,204,0.4)',
  secondaryL: '#ecf7fa',

  facebook: '#2e87c9',
  error: '#e63e35',
  errorD: '#ca271e',
  errorStatus: '#F06061',
  success: '#28c674',
  successM: '#9ce1b6',
  successStatus: '#70B99A',
  successL: '#ebf9f0',
};

export default colors;
