// leave off @2x/@3x
const images = {
  check: require('../images/check.png'),
  sale: require('../images/sale.png'),
  saleAlt: require('../images/sale-alt.png'),
  rent: require('../images/rent.png'),
  rentAlt: require('../images/rent-alt.png'),
  bid: require('../images/bid.png'),
  bidAlt: require('../images/bid-alt.png'),
  room: require('../images/room-alt.png'),
  roomAlt: require('../images/room.png'),
}

export default images;
