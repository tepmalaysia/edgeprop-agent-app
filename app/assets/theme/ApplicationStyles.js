import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  pageScrollContainer: {
    flex: 1,
    backgroundColor: Colors.silver,
    position: 'relative',
  },
  listScrollContainer: {
    backgroundColor: Colors.snow,
  },
  pageScrollInner: {
    paddingTop: 10,
    minHeight: Metrics.scrollInnerHeight,
  },
  
  tabsPageScrollInner: {
    minHeight: Metrics.scrollInnerHeight - Metrics.tabsHeight,
    paddingBottom: 0,
  },
  tabsPage: {
    flexGrow: 1,
    flexShrink: 1,
    alignItems: 'stretch',
  },

  controlsRow: {
    backgroundColor: Colors.snow,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    borderTopWidth: 1,
    borderColor: Colors.silver,
  },
  
  strippedTableRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 50,
    borderRadius: 4,
    paddingVertical: 10,
    backgroundColor: Colors.snow,
    paddingHorizontal: 5,
  },
  strippedTableHeadRow: {
    minHeight: 40,
    opacity: 0.7,
  },
  strippedTableUnpairRow: {
    backgroundColor: Colors.secondaryL,
  },
  strippedTableHeadCol: {
    paddingHorizontal: 10,
  },
  strippedTableCol: {
    fontSize: 16,
    paddingHorizontal: 10,
  },

  noData: {
    paddingTop: 15,
    fontSize: 12,
    color: '#9E9E9E',
    textAlign: 'center'
  },
  notesField: {
    paddingBottom: 30,
    flexShrink: 0,
  },
  defaultShadow: {
    shadowOffset:{width: 0,  height: 5,},
    shadowColor: 'rgba(0, 0, 0, 0.15)',
    shadowOpacity: 0.6,
    elevation: 5,
    shadowRadius: 10,
  },
};

export default ApplicationStyles;
