/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {View, Text, Alert} from 'react-native';
import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import AppNavigator from './app/index';
import Navigator from './app/modules/setup/routes';
//import AlertNavigator from './app/alertRouting';
import {createAppContainer} from 'react-navigation';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      notification : [],
      userInfo: {},
    }
  }

  async componentDidMount() {
    const auth = await AsyncStorage.getItem("authUser");
    if(auth && auth != '') {
      let authItems = JSON.parse(auth);
      if(authItems.status == 1) {
        this.setState({ userInfo: authItems })
      }
    }
    this.checkPermission();
    this.createNotificationListeners();
  }

   // 1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    console.log("Permission: ", enabled);
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }

    //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log("Token: ",fcmToken);
    if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
         console.log("Token firebase: ",fcmToken);
        if (fcmToken) {
            // user has a device token
            await AsyncStorage.setItem('fcmToken', fcmToken);
        } else {
          this.checkPermission();
        }
    }
  }

    //2
  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
        console.log('permission rejected');
    }
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      if(notification.data) {
        this.navigate(notification.data);
      }
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      if(notificationOpen.notification.data) {
        this.navigate(notificationOpen.notification.data);
      }
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */

    const notificationOpen = await firebase.notifications()
    .getInitialNotification()
    .then(async (notificationOpen) => {
      // App was opened by a notification
      if (notificationOpen) {
        // Get information about the notification that was opened
        const notification = notificationOpen.notification;
        const lastNotification = await AsyncStorage.getItem(
          'lastNotification'
        );
        if (lastNotification !== notification.notificationId) {
          await AsyncStorage.setItem(
            'lastNotification',
            notification.notificationId
          );
          if(notificationOpen.notification.data) {
            this.navigate(notificationOpen.notification.data);
          }
        }
      }
    })
  }

  navigate(notificationData) {
    // Alert.alert(
    //   notificationData.title, notificationData.body,
    //   [
    //       { text: 'OK', onPress: () => console.log('OK Pressed') },
    //   ],
    //   { cancelable: false },
    // );
    this.setState({
      notification : notificationData
    })
    
  }

  render() {
    /*const props = this.props;
    const notification = Object.keys(this.state.notification).length > 0 ? this.state.notification : props
    //const AppNav = createAppContainer(SwitchNavigator)
    const EnquiryAlertNav = createAppContainer(AlertNavigator)
    if(((notification.project_title &&
        notification.project_title.length &&
        notification.messageId &&
        notification.messageId.length))) {
      return (
        <EnquiryAlertNav screenProps = {notification}/>
      )
    } else {
      return (
        <View>
          <AppNavigator />
        </View>
      )
    }*/

    return(
        <View>
          <Navigator />
        </View>
      )
  }
}
