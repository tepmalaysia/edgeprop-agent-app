package com.vo;

public class NotificationVO {
    private String title;
    private String project_title;
    private String message;
    private String message_id;
    private String iconUrl;
    private String action;
    private String actionDestination;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProjectTitle() {
        return project_title;
    }

    public void setProjectTitle(String project_title) {
        this.project_title = project_title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageID() {
        return message_id;
    }

    public void setMessageID(String message_id) {
        this.message_id = message_id;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}