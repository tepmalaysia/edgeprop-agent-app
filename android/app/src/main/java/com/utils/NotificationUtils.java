package com.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;

import androidx.core.app.NotificationCompat;

import com.edgeprop.agent.MainActivity;
import com.edgeprop.agent.R;
import com.vo.NotificationVO;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class NotificationUtils {
    public static final int NOTIFICATION_ID = 200;
    private static final String PUSH_NOTIFICATION = "pushNotification";
    private static final String CHANNEL_ID = "myChannel";
    private static final String CHANNEL_NAME = "myChannelName";
    private static final String URL = "url";
    private static final String ACTIVITY = "activity";
    public static Ringtone ringtone;
    public static NotificationManager notificationManager;
    Map<String, Class> activityMap = new HashMap<>();
    private Context mContext;
    PendingIntent resultPendingIntent;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }
    /**
     * Displays notification based on parameters
     *
     * @param notificationVO
     * @param resultIntent
     */
    public void displayNotification(NotificationVO notificationVO, Intent resultIntent) {

        String message = notificationVO.getMessage();
        String message_id = notificationVO.getMessageID();
        String project_title = notificationVO.getProjectTitle();
        String iconUrl = notificationVO.getIconUrl();
        String action = notificationVO.getAction();
        Bundle bundle = new Bundle();
        bundle.putString("project_title",project_title);
        bundle.putString("message",message);
        bundle.putString("messageId",message_id);
        bundle.putString("iconUrl",iconUrl);
        Bitmap iconBitMap = null;
        if (iconUrl != null) {
            iconBitMap = getBitmapFromURL(iconUrl);
        }

        final int icon = R.drawable.ic_launcher;
        PendingIntent resultPendingIntent;

        if (ACTIVITY.equals(action)) {
            Intent resultIntent1 = new Intent(mContext, MainActivity.class);
            resultIntent1.putExtra("project_title", project_title);
            resultIntent1.putExtra("image_url", iconUrl);
            resultIntent1.putExtra("message", message);
            resultIntent1.putExtra("messageId", message_id);
            resultPendingIntent =
                    PendingIntent.getActivity(
                            mContext,
                            0,
                            resultIntent1,
                            PendingIntent.FLAG_CANCEL_CURRENT
                    );
        } else {
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            resultPendingIntent =
                    PendingIntent.getActivity(
                            mContext,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_CANCEL_CURRENT
                    );
        }

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext, CHANNEL_ID);
        Notification notification;
        if (iconBitMap == null) {
            //When Inbox Style is applied, user can expand the notification
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

            inboxStyle.addLine(message);

            notification = mBuilder.setSmallIcon(icon).setTicker(project_title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(project_title)
                    .setExtras(bundle)
                    .setContentIntent(resultPendingIntent)
                    .setStyle(inboxStyle)
                    .setColor(Color.rgb(44, 76, 145))
                    .setSmallIcon(R.drawable.ic_fav_icon)
                    .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                    .setContentText(message)
                    .build();
        } else {
            //If Bitmap is created from URL, show big icon
            NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
            bigPictureStyle.setBigContentTitle(project_title);
            bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
            bigPictureStyle.bigPicture(iconBitMap);

            notification = mBuilder.setSmallIcon(icon).setTicker(project_title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(project_title)
                    .setExtras(bundle)
                    .setContentIntent(resultPendingIntent)
                    .setStyle(bigPictureStyle)
                    .setColor(Color.rgb(44, 76, 145))
                    .setSmallIcon(R.drawable.ic_fav_icon)
                    .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                    .setContentText(message)
                    .build();
        }
        if(null != notificationManager) {
            notificationManager.cancelAll();
        }
        notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        //All notifications should go through NotificationChannel on Android 26 & above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        System.out.println("displayNotification ----------");
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    /**
     * Downloads push notification image before displaying it in
     * the notification tray
     *
     * @param strURL : URL of the notification Image
     * @return : BitMap representation of notification Image
     */
    private Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Playing notification sound
     */
    public void playNotificationSound() {
        try {
            if (null != ringtone) {
                ringtone.stop();
            }
            //if (null == ringtone) {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(mContext, notification);
            if(!ringtone.isPlaying()) {
                ringtone.play();
            }
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}