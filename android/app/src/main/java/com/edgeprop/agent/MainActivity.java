package com.edgeprop.agent;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.content.SharedPreferences;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import com.utils.NotificationUtils;

import java.util.Set;
import android.text.TextUtils;

public class MainActivity extends ReactActivity {

    private static final String TAG = "FCM";
    private boolean flag = true;
    private static final String PREFERENCE = "com.edgeprop.agent.NOTIFICATION";
    private static final String NOTIFICATON_ACCESS = "ACCESS";
    private static final String DEFAULT_ACCESS = "";
    private Intent serviceIntent;
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "EdgepropAgent";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Context context = this;
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREFERENCE, Context.MODE_PRIVATE);
        String access = sharedPref.getString(NOTIFICATON_ACCESS,DEFAULT_ACCESS);
        System.out.println("access"+ access);
        if(TextUtils.isEmpty(access)){
            Log.e("ACCESS ERROR", "Access is empty");
            startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
            //int highScore = sharedPref.getInt(getString(R.string.saved_high_score_key), defaultValue);

            Log.d("ACCESS VALUE", "ASKED");
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(NOTIFICATON_ACCESS, "ASKEDD");
            editor.commit();

        }else if(access.equals("ASKED")){
            Set<String> listnerSet = NotificationManagerCompat.getEnabledListenerPackages(this);
            String haveAccess = "NO";
            for (String sd : listnerSet) {
                if (sd.equals( getPackageName())) {
                    haveAccess = "YES";
                }
            }
            Log.d("ACCESS VALUE", haveAccess);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(NOTIFICATON_ACCESS, haveAccess);
            editor.commit();
        }

        String again_access = sharedPref.getString(NOTIFICATON_ACCESS,DEFAULT_ACCESS);
        System.out.println("again_access"+ again_access);

        if(!again_access.equals("NO")){
            Log.d("CREATE", "SERVUCE REQUEST IS NOT denied");
            final Window win= getWindow();
            win.addFlags(
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            serviceIntent = new Intent(this, NotificationCollectorMonitorService.class);
            startService(serviceIntent);
            if(flag == true) {
                LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, new IntentFilter("Msg"));
            }
        }*/

    }

    /*protected void onResume(){
        super.onResume();

        Context context = this;
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREFERENCE, Context.MODE_PRIVATE);
        String access = sharedPref.getString(NOTIFICATON_ACCESS,DEFAULT_ACCESS);
        System.out.println("onResume = "+ access);
        if(access.equals("ASKEDD")){
            System.out.println("ASKEDD IF ");
            if(isMyServiceRunning(NotificationCollectorMonitorService.class)){
                System.out.println("SERVICE IS RUNNIN ");
            }
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(NOTIFICATON_ACCESS, "ASKED");
            editor.commit();
        }else if(access.equals("ASKED")){
            System.out.println("ASKED ELSE IF ");
            if(isMyServiceRunning(NotificationCollectorMonitorService.class)){
                System.out.println("SERVICE IS RUNNIN ");
            }
            Set<String> listnerSet = NotificationManagerCompat.getEnabledListenerPackages(this);
            String haveAccess = "NO";
            for (String sd : listnerSet) {
                if (sd.equals( getPackageName())) {
                    haveAccess = "YES";
                }
            }
            Log.d("ACCESS VALUE", haveAccess);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(NOTIFICATON_ACCESS, haveAccess);
            editor.commit();
        }else if(access.equals("NO")){
            System.out.println("NO ELSE IF ");
            if(isMyServiceRunning(NotificationCollectorMonitorService.class)){
                System.out.println("NotificationCollectorMonitorService running ");
                try {
                    if (serviceIntent != null) {
                        stopService(serviceIntent);
                    }
                    if(onNotice != null){
                        unregisterReceiver(onNotice);
                    }
                }catch(Exception e){
                    System.out.println("NotificationCollectorMonitorService exception ");
                }
            }
        }else{
            System.out.println("ACCESS ELSE =");
            if(isMyServiceRunning(NotificationCollectorMonitorService.class)){
                System.out.println("SERVICE IS RUNNIN ");
            }
        }
        System.out.println("RESUME END");
    }*/

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    private BroadcastReceiver onNotice= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("BroadcastReceiver ----------");
            String project_title = intent.getStringExtra("project_title");
            String message = intent.getStringExtra("message");
            String messageId = intent.getStringExtra("messageId");
            String iconUrl = intent.getStringExtra("iconUrl");
            Intent inew = new Intent(context, MainActivity.class);
            Bundle b = new Bundle();

            b.putString("project_title", project_title);
            b.putString("iconUrl", iconUrl);
            b.putString("message", message);
            b.putString("messageId", messageId);
            if (messageId != null && project_title != null && flag == true) {
                flag = false;
                if(null != NotificationUtils.notificationManager) {
                    NotificationUtils.notificationManager.cancelAll();
                }
                inew.putExtras(b);
                System.out.println("startActivity ----------");
                startActivity(inew);
            }
        }
    };

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
               return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
            @Nullable
            @Override
            protected Bundle getLaunchOptions() {
                System.out.println("getLaunchOptions ----------");
                Bundle bundle = new Bundle();
                bundle.putString("default_key", "DEFAULT");
                if(getIntent().getExtras() != null) {
                    Bundle notificationBundle = getIntent().getExtras();
                    bundle.putString("project_title", notificationBundle.getString("project_title"));
                    bundle.putString("message", notificationBundle.getString("message"));
                    bundle.putString("messageId", notificationBundle.getString("messageId"));
                    bundle.putString("image_url", notificationBundle.getString("iconUrl"));
                }
                return bundle;
            }
        };
    }
}