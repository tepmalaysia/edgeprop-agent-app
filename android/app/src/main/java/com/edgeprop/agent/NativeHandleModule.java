package com.edgeprop.agent;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.utils.NotificationUtils;

public class NativeHandleModule extends ReactContextBaseJavaModule {

    public static Ringtone ringtone;
    private Context mContext;
    public NativeHandleModule(ReactApplicationContext reactContext) {
        super(reactContext); //required by React Native
        this.mContext = reactContext.getApplicationContext();
    }

    @Override
    //getName is required to define the name of the module represented in JavaScript
    public String getName() {
        return "NativeCall";
    }

    @ReactMethod
    public void stopAlertRing(Callback errorCallback, Callback successCallback) {
        try {
            if(null != NotificationUtils.ringtone){
                if(NotificationUtils.ringtone.isPlaying()) {
                    NotificationUtils.ringtone.stop();
                }
            }
            if(null != NotificationUtils.notificationManager) {
                NotificationUtils.notificationManager.cancelAll();
            }

        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void startAlertRing(Callback errorCallback, Callback successCallback) {
        try {
            if (null != ringtone) {
                ringtone.stop();
            }
            //if (null == ringtone) {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(mContext, notification);
            if(!ringtone.isPlaying()) {
                ringtone.play();
                NotificationUtils.ringtone = ringtone;
            }
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void closeApp(Callback errorCallback, Callback successCallback) {
        try {
           
            Activity activity = getCurrentActivity();

            if(null != activity){
                KeyguardManager myKM = (KeyguardManager) activity.getSystemService(activity.KEYGUARD_SERVICE);
                if( myKM.inKeyguardRestrictedInputMode()) {
                    activity.moveTaskToBack(true);
                }
            }
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }
}