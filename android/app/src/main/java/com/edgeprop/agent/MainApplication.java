package com.edgeprop.agent;

import android.app.Application;
import android.media.Ringtone;
import android.content.Context;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.robinpowered.react.Intercom.IntercomPackage;

import com.facebook.react.shell.MainReactPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;

import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import java.util.List;
import io.intercom.android.sdk.Intercom;

public class MainApplication extends Application implements ReactApplication {
  public Ringtone ringtone;
  private final ReactNativeHost mReactNativeHost =
      new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
          return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
          @SuppressWarnings("UnnecessaryLocalVariable")
          List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example:
          if (packages.get(0).getClass() == MainReactPackage.class) {
            // hack to get around strange error:
            // "Native module TimePickerAndroid tried to override
            // com.facebook.react.modules.timepicker.TimePickerDialogModule"
            packages.remove(0);
          }
          packages.add(new MainReactPackage());
          //packages.add(new RealmReactPackage());
          //packages.add(new ReanimatedPackage());
          //packages.add(new AsyncStoragePackage());
          //packages.add(new RNGestureHandlerPackage());
          //packages.add(new RNFirebasePackage());
          packages.add(new RNFirebaseMessagingPackage());
          packages.add(new RNFirebaseNotificationsPackage());
          packages.add(new RNFirebaseCrashlyticsPackage());
          //packages.add(new LinearGradientPackage());
          //packages.add(new BackgroundTimerPackage());
          packages.add(new NativeHandlePackage());
          return packages;
        }

        @Override
        protected String getJSMainModuleName() {
          return "index";
        }
      };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Intercom.initialize(this, "android_sdk-e4e7609c783d3ed0b67ac10988a614dd53a9e2ab", "cs57e3gj");
    SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this); // Remove this line if you don't want Flipper enabled
  }

  /**
   * Loads Flipper in React Native templates.
   *
   * @param context
   */
  private static void initializeFlipper(Context context) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.facebook.flipper.ReactNativeFlipper");
        aClass.getMethod("initializeFlipper", Context.class).invoke(null, context);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
