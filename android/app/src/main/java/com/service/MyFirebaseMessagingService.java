package com.service;
import android.content.Intent;
import android.util.Log;

import com.edgeprop.agent.MainActivity;
import com.edgeprop.agent.NotificationCollectorMonitorService;
import com.utils.NotificationUtils;
import com.vo.NotificationVO;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgingService";
    private static final String TITLE = "title";
    private static final String PROJECT_TITLE = "project_title";
    private static final String MESSAGE = "message";
    private static final String MESSAGEID = "messageId";
    private static final String IMAGE = "image_url";
    private static final String ACTION = "action";

   @Override
   public void onMessageReceived(RemoteMessage remoteMessage) {
       System.out.println("onMessageReceived ----------");
       // Check if message contains a data payload.
       if (remoteMessage.getData().size() > 0) {
           Log.d(TAG, "Message data payload BG: " + remoteMessage.getData());
           Map<String, String> data = remoteMessage.getData();
           handleData(data);

       } else if (remoteMessage.getNotification() != null) {
           Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
           handleNotification(remoteMessage.getNotification());
       }// Check if message contains a notification payload.

   }

   private void handleNotification(RemoteMessage.Notification RemoteMsgNotification) {
       String message = RemoteMsgNotification.getBody();
       String title = RemoteMsgNotification.getTitle();
       NotificationVO notificationVO = new NotificationVO();
       notificationVO.setTitle(title);
       notificationVO.setMessage(message);

       Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
       NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
       notificationUtils.displayNotification(notificationVO, resultIntent);
   }

   private void handleData(Map<String, String> data) {
       String title = data.get(TITLE);
       String message = data.get(MESSAGE);
       String project_title = data.get(PROJECT_TITLE);
       String messageid = data.get(MESSAGEID);
       String iconUrl = data.get(IMAGE);
       String action = data.get(ACTION);
       NotificationVO notificationVO = new NotificationVO();
       notificationVO.setTitle(title);
       notificationVO.setProjectTitle(project_title);
       notificationVO.setMessage(message);
       notificationVO.setMessageID(messageid);
       notificationVO.setIconUrl(iconUrl);
       notificationVO.setAction(action);

       startService(new Intent(this, NotificationCollectorMonitorService.class));

       Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
       NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
       notificationUtils.displayNotification(notificationVO, resultIntent);
       //notificationUtils.playNotificationSound();

   }
}