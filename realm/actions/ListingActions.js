var Realm = require('realm')
import { ListingSchema } from '../models/listings/ListingSchema'
import RealmHelper from '../helpers/RealmHelper'
import MigrationActions from './MigrationActions'


export default class ProfileActions {
	constructor() {
        this.RealmHelper = new RealmHelper();
        this.MigrationActions = new MigrationActions();
        this.ListingSchema = 'Listings';
    }

    OpenRealmSchema(){
      this.realm = new Realm({
        schema: [ListingSchema],
        schemaVersion: this.MigrationActions.schemaVersion
      });
    }

    CloseRealmSchema(){
      this.realm.close();
    }

    CreateListing(results){
      this.WriteToListing(this.ListingSchema, results, 'lid');
    }

    GetCount(status){
      return this.FetchCount(this.ListingSchema, "prop_status = '" + status +"'");
    }

    GetListings(status, data){
      let keyValue = [];
      if(status){
        keyValue.push("prop_status = '" + status +"'");
      }
      if(data){
        for (let key in data) {
           if (data.hasOwnProperty(key)) {
              if(key == 'search'){
                if(data[key]){
                  keyValue.push("(project_name CONTAINS[c] '"+data[key]+"' OR title CONTAINS[c] '"+data[key]+"' OR district CONTAINS[c] '"+data[key]+"' OR street CONTAINS[c] '"+data[key]+"')");
                }
              }else if(key == 'category'){
                if(data[key] == 'residential'){
                  keyValue.push("(property_type = '36' OR property_type = '33')");
                }else if(data[key] == 'non-residential'){
                  keyValue.push("(property_type = '60' OR property_type = '70')");
                }
              }else if(data[key]){
                if(typeof data[key] === 'string'){
                  keyValue.push(key+" = '"+data[key]+"'");
                }else if(typeof data[key] === 'boolean'){
                  keyValue.push(key+" == "+data[key]);
                }else{
                  keyValue.push(key+" = "+data[key]);
                }
              }
           }
        }
      }
      let filter = keyValue.length>0? keyValue.join(' AND ') : '';
      return this.FetchListings(this.ListingSchema, filter);
    }

    GetListing(lid){
      return this.FetchListing(this.ListingSchema, lid)
    }

    ClearListings(){
    	this.DeleteListings(this.ListingSchema);
    }

    ClearListing(lid){
      this.DeleteListing(this.ListingSchema, lid);
    }

    SetListing(results, filter){
    	this.UpdateListing(this.ListingSchema, results, filter)
    }

    //common Realm Helper functions
    WriteToListing(schema, results, primaryKey){
      this.OpenRealmSchema();
      this.RealmHelper.WriteAll(this.realm, schema, results, primaryKey);
      this.CloseRealmSchema();
    }

    FetchListings(schema, filter){
      this.OpenRealmSchema();
      var Properties = this.RealmHelper.Read(this.realm, schema, filter);
      //Properties = Properties.slice(0, 8);
      Properties = this.RealmHelper.RealmToJson(Properties);
      Properties = this.RealmHelper.ExcludeKey('data',Properties)
      this.CloseRealmSchema();
      return Properties;
    }

    FetchCount(schema, filter){
      this.OpenRealmSchema();
      var Properties = this.RealmHelper.Read(this.realm, schema, filter);
      this.CloseRealmSchema();
      return Array.isArray(Properties)? Properties.length : 0;
    }

    FetchListing(schema, lid) {
      this.OpenRealmSchema();
      var Listing = this.RealmHelper.Read(this.realm, schema, 'lid = "' + lid + '"');
      Listing = this.RealmHelper.RealmToJson(Listing);
      Listing = this.RealmHelper.ExcludeKey('timestamp',Listing)
      this.CloseRealmSchema();
      return Listing;
    }

    DeleteListings(schema){
      this.OpenRealmSchema();
      this.RealmHelper.DeleteAll(this.realm, schema);
      this.CloseRealmSchema();
    }

    DeleteListing(schema, lid){
      this.OpenRealmSchema();
      this.RealmHelper.Delete(this.realm, schema, 'lid = "' + lid + '"');
      this.CloseRealmSchema();
    }

    UpdateListing(schema, results, filter){
    	this.OpenRealmSchema();
    	this.RealmHelper.Update(this.realm, schema, results, filter)
    	this.CloseRealmSchema();
    }
}