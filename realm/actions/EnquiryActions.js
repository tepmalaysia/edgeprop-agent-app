var Realm = require('realm')
import { EnquirySchema } from '../models/enquiry/EnquirySchema'
import RealmHelper from '../helpers/RealmHelper'
import MigrationActions from './MigrationActions'


export default class EnquiryActions {
	constructor() {
        this.RealmHelper = new RealmHelper();
        this.MigrationActions = new MigrationActions();
        this.EnquirySchema = 'PropertyEnquiry';
    }

    OpenRealmSchema(){
      this.realm = new Realm({
        schema: [EnquirySchema],
        schemaVersion: this.MigrationActions.schemaVersion
      });
    }

    CloseRealmSchema(){
      this.realm.close();
    }

    CreateEnquiry(results){
      this.WriteToEnquiry(this.EnquirySchema, results, 'message_id');
    }

    GetEnquiry(){
    	return this.FetchEnquiry(this.EnquirySchema);
    }

    GetEnquiryDetail(message_id){
      return this.FetchEnquiryDetail(this.EnquirySchema, message_id);
    }

    GetEnquiryListing(mid){
      return this.FetchEnquiryListing(this.EnquirySchema, mid)
    }

    ClearEnquiry(){
    	this.DeleteEnquiry(this.EnquirySchema);
    }

    ClearEnquiryItem(message_id){
      this.DeleteEnquiryItem(this.EnquirySchema, message_id);
    }

    SetEnquiry(results, filter){
    	this.UpdateEnquiry(this.EnquirySchema, results, filter)
    }

    //common Realm Helper functions
    WriteToEnquiry(schema, results, primaryKey){
      this.OpenRealmSchema();
      //this.RealmHelper.DeleteAll(this.realm, schema);
      this.RealmHelper.WriteAll(this.realm, schema, results, primaryKey);
      this.CloseRealmSchema();
    }

    FetchEnquiry(schema){
      this.OpenRealmSchema();
      var Properties = this.RealmHelper.ReadAll(this.realm, schema, true);
      Properties = this.RealmHelper.RealmToJson(Properties);
      Properties = this.RealmHelper.ExcludeKey('timestamp',Properties)
      this.CloseRealmSchema();
      return Properties;
    }

    FetchEnquiryListing(schema, mid) {
      this.OpenRealmSchema();
      var Properties = this.RealmHelper.Read(this.realm, schema, 'mid = "' + mid + '"');
      Properties = this.RealmHelper.RealmToJson(Properties);
      Properties = this.RealmHelper.ExcludeKey('timestamp',Properties)
      this.CloseRealmSchema();
      return Properties;
    }

    FetchEnquiryDetail(schema, message_id) {
      this.OpenRealmSchema();
      var Property = this.RealmHelper.Read(this.realm, schema, 'message_id = "' + message_id + '"');
      Property = this.RealmHelper.RealmToJson(Property);
      Property = this.RealmHelper.ExcludeKey('timestamp',Property)
      this.CloseRealmSchema();
      return Property;
    }

    DeleteEnquiry(schema){
      this.OpenRealmSchema();
      this.RealmHelper.DeleteAll(this.realm, schema);
      this.CloseRealmSchema();
    }

    DeleteEnquiryItem(schema, message_id){
      this.OpenRealmSchema();
      this.RealmHelper.Delete(this.realm, schema, 'message_id = "' + message_id + '"');
      this.CloseRealmSchema();
    }

    UpdateEnquiry(schema, results, filter){
    	this.OpenRealmSchema();
    	this.RealmHelper.Update(this.realm, schema, results, filter)
    	this.CloseRealmSchema();
    }

    GroupByProperty(data){
      let Properties = this.RealmHelper.GroupBy('lid',data);
      return Properties;
    }
}