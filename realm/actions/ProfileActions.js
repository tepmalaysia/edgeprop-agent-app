var Realm = require('realm')
import { ProfileSchema } from '../models/profile/ProfileSchema'
import RealmHelper from '../helpers/RealmHelper'
import MigrationActions from './MigrationActions'


export default class ProfileActions {
	constructor() {
        this.RealmHelper = new RealmHelper();
        this.MigrationActions = new MigrationActions();
        this.ProfileSchema = 'ProfileSchema';
    }

    OpenRealmSchema(){
      this.realm = new Realm({
        schema: [ProfileSchema],
        schemaVersion: this.MigrationActions.schemaVersion
      });
    }

    CloseRealmSchema(){
      this.realm.close();
    }

    CreateProfile(results){
      this.WriteToProfile(this.ProfileSchema, results, 'uid');
    }

    GetProfile(){
    	return this.FetchProfile(this.ProfileSchema);
    }

    ClearProfile(){
    	this.DeleteProfile(this.ProfileSchema);
    }

    SetProfile(results, filter){
    	this.UpdateProfile(this.ProfileSchema, results, filter)
    }

    //common Realm Helper functions
    WriteToProfile(schema, results, primaryKey){
      this.OpenRealmSchema();
      this.RealmHelper.DeleteAll(this.realm, schema);
      this.RealmHelper.WriteAll(this.realm, schema, results, primaryKey);
      this.CloseRealmSchema();
    }

    FetchProfile(schema){
      this.OpenRealmSchema();
      var Properties = this.RealmHelper.ReadAll(this.realm, schema);
      Properties = this.RealmHelper.RealmToJson(Properties);
      Properties = this.RealmHelper.ExcludeKey('timestamp',Properties)
      this.CloseRealmSchema();
      return Properties;
    }

    DeleteProfile(schema){
      this.OpenRealmSchema();
      this.RealmHelper.DeleteAll(this.realm, schema);
      this.CloseRealmSchema();
    }

    UpdateProfile(schema, results, filter){
    	this.OpenRealmSchema();
    	this.RealmHelper.Update(this.realm, schema, results, filter)
    	this.CloseRealmSchema();
    }
}