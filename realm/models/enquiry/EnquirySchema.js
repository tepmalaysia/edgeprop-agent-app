// Define your models and their properties
export const EnquirySchema = {
  name: 'PropertyEnquiry',
  primaryKey: 'message_id',
  properties: {
    lid: 'int', 
    nid: 'int?',
    mid: 'int?',
    district: 'string?',
    state: 'string?',
    agent_id: 'int',
    title:  'string',
    message: 'string',
    message_id: 'int',//primary key
    name:  'string',
    phone:  'string?',
    email: 'string?',
    status: 'int',
    read:{type: 'bool', default: false},
    changed: 'int',
    timestamp: {type: 'int', default: new Date().getTime()}
  }
};