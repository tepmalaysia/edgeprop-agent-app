export class ListingSchema {
  static schema = {
      name: 'Listings',
      primaryKey: 'lid',
      properties: {
        lid: 'int', //primary key
        nid: 'int?',
        mid: 'int?',
        uid: 'int',
        title: 'string',
        status: {type: 'int', default: 0},
        created: 'int',
        changed: 'int',
        district: 'string?',
        state: 'string?',
        project_name: 'string?',
        project_id: 'string?',
        prop_status: 'string?',
        isFeatured: 'bool?',
        listing_type: 'string?',
        property_type: 'string?',
        asset_id: 'string?',
        timestamp: {type: 'int', default: new Date().getTime()},
        impression: 'int?',
        show_number: 'int?',
        contacted: 'int?',
        view_contact: 'int?',
        whatsapp_count: 'int?',
        data: 'string?',
        price: 'int?',
        cover_image: 'string?',
        images: 'string?[]',
        past_transactions: 'string?',
        url: 'string?',
        street: 'string?',
        featuredData: 'string?'
    }
  }
}