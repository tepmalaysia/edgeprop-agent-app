export class ProfileSchema {
  static schema = {
      name: 'ProfileSchema',
      primaryKey: 'uid',
      properties: {
        uid: 'int', //primary key
        token: 'string',
        profile_uri: 'string?',
        agentName: 'string?',
        agentContact: 'string?',
        agencyName: 'string?',
        agentNo: 'string?',
        name: 'string?',
        email: 'string?',
        personalAddress: 'string?',
        personalState: 'string?',
        personalPostal: 'int?',
        agencyInfo: 'string?',
        agentPlan: {type: 'int', default: 1},
        agentPlanExpiry: 'string?',
        timestamp: {type: 'int', default: new Date().getTime()}
    }
  }
}